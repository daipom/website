require "cgi/util"
require "tempfile"

require_relative "_tasks/lint"

desc "Build website"
task :build do
  sh({"JEKYLL_ENV" => "production"},
     "bundle", "exec", "jekyll", "build")
end

desc "Upload website"
task :upload => [:build] do
  sh("rsync",
     "-az",
     "_site/",
     "website@www.clear-code.com:public/")
end

namespace :drafts do
  desc "Publish drafts"
  task :publish do
    Dir.glob("_drafts/*.md") do |path|
      today = Time.now.strftime("%Y-%m-%d")
      basename = File.basename(path)
      id = basename.gsub(/\.md\z/, "")
      post_path = "_posts/#{today}-#{basename}"
      sh("git", "mv", path, post_path)
      sh("git", "commit", "-m", "blog: publish #{id}")
      sh("git", "push")
    end
  end
end

def gitlab_pages_generate(project_namespace, project_name, branch)
  mkdir_p("public/#{branch}")
  extra_conf = Tempfile.new(["jekyll", ".yml"])
  extra_conf.puts("url: https://#{project_namespace}.gitlab.io")
  extra_conf.close
  sh("bundle", "exec", "jekyll", "build",
     "--config", "_config.yml,#{extra_conf.path}",
     "--baseurl", "/#{project_name}/#{branch}",
     "--destination", "public/#{branch}",
     "--drafts")
end

namespace :gitlab do
  namespace :pages do
    desc "Generate GitLab Pages"
    task :generate do
      project_namespace = ENV["CI_PROJECT_NAMESPACE"]
      project_name = ENV["CI_PROJECT_NAME"]
      current_branch = ENV["CI_COMMIT_REF_NAME"]
      branches = []
      `git branch --remote`.each_line do |line|
        branch = line.strip.gsub(/\Aorigin\//, "")
        next if branch.include?(" -> ")
        branches << branch
      end
      if current_branch
        gitlab_pages_generate(project_namespace, project_name, current_branch)
      end
      branches.each do |branch|
        next if branch == current_branch
        sh("git", "checkout", branch)
        sh("bundle", "install")
        gitlab_pages_generate(project_namespace, project_name, branch)
      end
      File.open("public/index.html", "w") do |html|
        html.puts(<<-HTML)
<!DOCTYPE html>
<html>
  <head>
    <meta name="robots" content="noindex,nofollow">
    <title>#{CGI.escape(project_namespace)}.gitlab.io/#{CGI.escape(project_name)}/ branches</title>
  </head>
  <body>
    <ul>
        HTML
        branches.each do |branch|
        html.puts(<<-HTML)
      <li><a href="#{CGI.escape(branch)}">#{CGI.escape_html(branch)}</a></li>
        HTML
        end
        html.puts(<<-HTML)
    </ul>
  </body>
</html>
        HTML
      end
    end
  end
end
