---
tags:
- test
title: GaUnit新API
---
[Gauche](http://practical-scheme.net/gauche/index-j.html)
用の単体テストフレームワーク
[GaUnit](http://www.cozmixng.org/~rwiki/?cmd=view;name=GaUnit)
の0.1.4がリリースされました。
<!--more-->


Gaucheには標準でgauche.testという単体テスト用のモジュールが付
属しています。このモジュールはテストスクリプトをはじめから順
に実行していくという素直なテスト実行方式を採用しています。こ
の方式では一連のテストがどのように実行されていくかがわかりや
すい半面、（場合によっては）以下のような問題があります。

  * 特定のテストのみを実行することができない
  * 各テスト毎にgoshプロセスを起動する必要がある
    （各テストが他のテストの影響をうけないようにするため）
  * テストを別々に走らせるため、すべてのテスト結果を見るため
    には、一度ファイル（テストログ）に保存しておいてあとで確
    認する、としなければいけない（テストを走らせる前にテスト
    ログを削除して前のテスト結果を削除する必要がある）

また、gauche.testはテストの成功、失敗にかかわらずテスト結果が
常に冗長であるという問題があります。テストが失敗した項目（修正
の必要がある項目）についてのみ情報を詳細する方が、より合理的で
しょう。

ということで、GaUnitです。GaUnitは
[xUnit](http://ja.wikipedia.org/wiki/XUnit)系の単体テ
ストフレームワークで、上記のgauche.testの問題を解決します。
また、テスト失敗時以外はテスト結果に余計な情報を出力しないため
無駄がありません。そんなGaUnitが今回のリリース（正確には1つ前の
0.1.3）からより書きやすいAPIを提供して、以下の2点を行うだけで
すむようになりました。

  * テスト用のモジュールを作成
  * その中でtest-始まりの手続きを定義

実際のコードは以下のようになります。

{% raw %}
```scheme
(define-module test-your-library
  (extend test.unit.test-case)
  (use your-library))
(select-module test-your-module)

(define (test-your-module-procedure1)
  (assert-equal "Good!" (your-module-procedure1))
  ...
  #f)

(define (test-your-module-procedure2)
  (assert-equal 29 (your-module-procedure2))
  ...
  #f)

(provide "test-your-module")
```
{% endraw %}

最後にtest-*手続きの最後に#fを付けているのは末尾再帰の最適化
でバックトレースを落とさないようにするためです。

新しいAPIでは、普通のGaucheライブラリと同じようにテストを書
けます。テストのために覚えることと言えば、どのassert-*を使お
うかということくらいです。これは、普段の別のライブラリを用い
た開発と同じですね。

GaUnitでのテスト書き方をもっと知りたい人は[チュートリアル](http://www.cozmixng.org/~rwiki/?cmd=view;name=GaUnit%3A%3ATutorial.ja)
を読んでください。
