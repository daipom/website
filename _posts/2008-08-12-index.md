---
tags:
- ruby
title: ActiveScaffoldの地域化
---
現在、Railsに対応した国際化の仕組みがいくつかあります。しかし、それぞれが
独自の方法で実現しているため、それらを組み合わせて使うと混沌
とした状態に陥ることも少なくありません。
<!--more-->


ここでは、モデルから動的にきれいな画面とコントローラ部分を生
成する[ActiveScaffold](http://activescaffold.com/)を用
いた場合の国際化（i18n）と地域化（l10n）の実現方法のひとつを
紹介します。この方法では、
[ActiveScaffoldLocalize](http://github.com/edwinmoss/active_scaffold_localize/)
と
[Ruby-GetText-Package](http://www.yotabanana.com/hiki/ja/ruby-gettext.html)
を組み合わせます。混沌とする部分はそれなりになじませます。

### 国際化の仕組み

Railsで使用できる国際化の仕組みの比較は[Rails
Wiki](http://wiki.rubyonrails.org/rails/pages/InternationalizationComparison)
（英語）が詳しいです。

Ruby-GetText-Package には、以下のような地域化対象のメンテナン
スのことを考慮した機能があるので、地域化対象メッセージが増加
したり更新される場合には有力な候補になるでしょう。

  * 地域化対象のメッセージを抽出する機能
    * テーブルにカラムを追加した場合、画面に表示するメッセージを追加・更新した場合などに利用
  * 抽出したメッセージを既存の翻訳済みメッセージにマージする機能
    * 翻訳者が新しい地域化対象のメッセージを翻訳する場合に利用
  * [gettext](https://ja.wikipedia.org/wiki/gettext)用の翻訳支援ツールを利用可能（.po
    のフォーマットがGNU gettextと互換性があるため）
    * Emacs用のpo-modeや.po専用のエディタ

Railsやプラグインなどが提供しているメッセージだけを地域化した
いなど、地域対象メッセージが変化しない場合はその他の仕組みも
有力な候補になるでしょう。例えば、ActiveScaffold用の
ActiveScaffoldLocalizeがその場合です。

### ActiveScaffoldLocalize

ActiveScaffoldは、国際化の仕組みとしてObject#as_を提供してい
ます。その仕組みを利用して国際化・地域化を実現しているのが
ActiveScaffoldLocalizeです。

ActiveScaffoldLocalizeには日本語用のメッセージも含まれている
ので、以下のようにすればActiveScaffoldのメッセージを日本語に
することができます。

{% raw %}
```
% rails shelf
% cd shelf
% script/generate resource book title:string
% rake db:migrate
% script/plugin install git://github.com/activescaffold/active_scaffold.git
% script/plugin install git://github.com/edwinmoss/active_scaffold_localize.git
```
{% endraw %}

config/routes.rb:

{% raw %}
```diff
-  map.resources :books
+  map.resources :books, :active_scaffold => true
```
{% endraw %}

app/controllers/books_controller.rb:

{% raw %}
```ruby
class BooksController < ApplicationController
  active_scaffold :book
end
```
{% endraw %}

app/views/layouts/application.html.erb:

{% raw %}
```html
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
      "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <title>ActiveScaffold l10n</title>
    <%= javascript_include_tag(:defaults) %>
    <%= active_scaffold_includes %>
  </head>

  <body>
    <h1>ActiveScaffold l10n</h1>
    <%= yield %>
  </body>
</html>
```
{% endraw %}

app/controllers/applications.rb:

{% raw %}
```ruby
class ApplicationController < ActionController::Base
  # ...
  private
  before_filter :localize_active_scaffold
  def localize_active_scaffold
    ActiveScaffold::Localization.lang = "ja-jp"
    true
  end
end
```
{% endraw %}

サーバを起動してhttp://localhost:3000/books/にアクセスします。

{% raw %}
```
% script/server
% firefox http://localhost:3000/books/
```
{% endraw %}

[![ActiveScaffold + ActiveScaffoldLocalize]({{ "/images/blog/20080812_3.png" | relative_url }} "ActiveScaffold + ActiveScaffoldLocalize")]({{ "/images/blog/20080812_0.png" | relative_url }})

見ての通り、「検索」などのメニューは日本語になりますが、テー
ブル名からきている「Books」やカラム名の「Title」などは日本語
になりません。

ActiveScaffoldLocalizeの方針では、これらを日本語にするために以下のような内容の
config/initializers/lang/ja-jp.rb[^0]を作成します。

config/initializers/lang/ja-jp.rb:

{% raw %}
```ruby
# -*- coding: utf-8 -*-

ActiveScaffold::Localization.define('ja-jp') do |lang|
  lang["Books"] = "本一覧"
  lang["Book"] = "本"
  lang["Title"] = "タイトル"
end
```
{% endraw %}

config/initializers/以下を変更したので、サーバを再起動してか
ら再度アクセスすると、日本語で表示されます。

[![ActiveScaffold + ActiveScaffoldLocalize + モデルの地域化]({{ "/images/blog/20080812_4.png" | relative_url }} "ActiveScaffold + ActiveScaffoldLocalize + モデルの地域化")]({{ "/images/blog/20080812_1.png" | relative_url }})

（「本を作成」ではなく「本一覧を作成」になっているのは[こ
のパッチ](http://code.google.com/p/activescaffold/issues/detail?id=601)
で直ります。）

ActiveScaffoldLocalizeのこのやり方は手軽ですが、地域化対象の
メッセージが変更になった場合（例: 「Title」から「Name」に変更）
や、地域化対象のメッセージをtypoした場合（例: 「Title」ではな
く「title」としていた）に気づきにくいという問題があります。
このような問題に対してはRuby-GetText-Packageが有効です。

ということで、ActiveScaffoldのメッセージは
ActiveScaffoldLocalizeで地域化し、それ以外は
Ruby-GetText-Packageで地域化するようにします。

### Ruby-GetText-Package

ActiveScaffoldLocalizeとRuby-GetText-Packageのすみわけは上述
の通りですが、エラーメッセージの地域化はRuby-GetText-Package
ではなく、ActiveScaffldLocalizeに任せます。これは、
ActiveScaffoldがエラーメッセージ部分を上書きしているため、
Ruby-GetText-Packageが提供するエラーメッセージ国際化処理とな
じまないためです。

また、Ruby-GetText-Packageが取得したロケール情報を使って
ActiveScaffoldLocalizeのlangを設定していることもコツのひとつ
です。

config/environment.rb:

{% raw %}
```ruby
# ...
Rails::Initializer.run do |config|
  # ...
  config.gem "gettext", :lib => "gettext/rails"
  # ...
end
```
{% endraw %}

lib/active_scaffold_gettext.rb:

{% raw %}
```ruby
module ActiveScaffoldGetText
  include GetText::Rails

  bindtextdomain(GETTEXT_DOMAIN)
end

class Object
  def as__with_gettext(message, *args)
    return nil if message.nil?
    localized_message = ActiveScaffoldGetText.send(:sgettext, message)
    if localized_message == message
      as__without_gettext(message, *args)
    else
      localized_message % args
    end
  end
  alias_method_chain :as_, :gettext
end

module ActiveScaffold::DataStructures
  class Column
    def initialize_with_gettext(name, active_record_class)
      initialize_without_gettext(name, active_record_class)
      self.label = "#{active_record_class.name.demodulize}|#{@label.humanize}"
    end
    alias_method_chain :initialize, :gettext
  end
end
```
{% endraw %}

config/initializers/gettext.rb:

{% raw %}
```ruby
GETTEXT_DOMAIN = "your-rails-application"
require 'active_scaffold_gettext'

class ActiveRecord::Errors
  # restore default error messages overridden by Ruby-GetText-Package.
  @@default_error_messages = {
    :inclusion => "is not included in the list",
    :exclusion => "is reserved",
    :invalid => "is invalid",
    :confirmation => "doesn't match confirmation",
    :accepted  => "must be accepted",
    :empty => "can't be empty",
    :blank => "can't be blank",
    :too_long => "is too long (maximum is %d characters)",
    :too_short => "is too short (minimum is %d characters)",
    :wrong_length => "is the wrong length (should be %d characters)",
    :taken => "has already been taken",
    :not_a_number => "is not a number",
    :greater_than => "must be greater than %d",
    :greater_than_or_equal_to => "must be greater than or equal to %d",
    :equal_to => "must be equal to %d",
    :less_than => "must be less than %d",
    :less_than_or_equal_to => "must be less than or equal to %d",
    :odd => "must be odd",
    :even => "must be even"
  }

  alias_method :on, :on_without_gettext
  alias_method :[], :on
end
```
{% endraw %}

lib/tasks/gettext.rb:

{% raw %}
```ruby
namespace :gettext do
  namespace :po do
    desc "Update pot/po files."
    task :update => :environment do
      require 'gettext/utils'

      module GetText::ActiveRecordParser
        class << self
          alias_method :add_target_original, :add_target
          def add_target(targets, file, msgid)
            if /\|/ !~ msgid
              add_target_original(targets, file, msgid.classify)
              add_target_original(targets, file, msgid.classify.pluralize)
            end
            add_target_original(targets, file, msgid)
          end
        end
      end

      targets = Dir.glob("{app,config,components,lib}/**/*.{rb,erb,rjs}")
      GetText.update_pofiles(GETTEXT_DOMAIN, targets, "#{GETTEXT_DOMAIN} 0.0.1")
    end
  end

  namespace :mo do
    desc "Create mo-files"
    task :create do
      require 'gettext/utils'
      GetText.create_mofiles(true, "po", "locale")
    end
  end
end
```
{% endraw %}

app/controllers/application.rb:

{% raw %}
```ruby
class ApplicationController < ActionController::Base
  init_gettext GETTEXT_DOMAIN
  # ...
  private
  before_filter :localize_active_scaffold
  def localize_active_scaffold
    posix_locale = GetText.locale.to_posix
    posix_locale = "#{posix_locale}-#{posix_locale}" if /_/ !~ posix_locale
    lang = posix_locale.gsub(/_/, '-').downcase
    ActiveScaffold::Localization.lang = lang
    true
  end
end
```
{% endraw %}

翻訳メッセージのファイルpoを作って翻訳します。

{% raw %}
```
% rake gettext:po:update
% mkdir po/ja
% msginit -i po/your-rails-application.pot -o po/ja/your-rails-application.po -l ja_JP
# 途中でメールアドレスを聞かれるので入力する
```
{% endraw %}

po/ja/your-rails-application.po:

{% raw %}
```
# ...
#: app/models/book.rb:-
msgid "Book"
msgstr "本"

#: app/models/book.rb:-
msgid "Books"
msgstr "本一覧"
# ...
#: app/models/book.rb:-
msgid "Book|Title"
msgstr "タイトル"
# ...
```
{% endraw %}

翻訳メッセージをmoにコンパイルしてアクセスするとテーブル名や
カラム名などが日本語になります。

{% raw %}
```
% rake gettext:mo:create
```
{% endraw %}

config/initializers/以下を変更したので、サーバを再起動してか
ら再度アクセスすると、日本語で表示されます。

[![ActiveScaffold + Ruby-GetText-Package]({{ "/images/blog/20080812_5.png" | relative_url }} "ActiveScaffold + Ruby-GetText-Package")]({{ "/images/blog/20080812_2.png" | relative_url }})

### まとめ

ActiveScaffoldLocalizeとRuby-GetText-Packageを使って、
ActiveScaffoldを用いたアプリケーションの国際化・地域化を実現する方法
のひとつを紹介しました。

基本的に複数の国際化のしくみを同時に使うと問題が起きますが、
今回は以下のようにそれぞれの長所を活かすようにすみわけて、問
題を回避しています。

  * ActiveScaffoldが利用する固定のメッセージは
    ActiveScaffoldLocalizeで地域化
  * モデル関連や追加・更新が行われるメッセージについては
    Ruby-GetText-Packageで地域化

[^0]: config/initializers/lang/以下にファイルを作るというのはActiveScaffoldLocalizeの方針ではありません。ファイルの場所は特に方針はないようです。
