---
tags:
- test
title: GaUnit 0.1.6リリース
---
[GaUnit](http://www.cozmixng.org/~rwiki/?cmd=view;name=GaUnit) 0.1.6がリリースされました。
（[[アナウンス](http://lists.sourceforge.jp/mailman/archives/gauche-devel-jp/2008-October/001824.html)]|[[ダウンロード](http://www.cozmixng.org/~kou/download/gaunit-0.1.6.tar.gz)]）
<!--more-->


GaUnitは便利な[Scheme](https://ja.wikipedia.org/wiki/Scheme)インタプリタである[Gauche](http://practical-scheme.net/gauche/index-j.html)用の[XUnit](https://ja.wikipedia.org/wiki/XUnit)ベースの単体テストフレームワークです。0.1.6ではGauche標準のgauche.testモジュール（[リンク切れしそうなリンク](http://practical-scheme.net/gauche/man/gauche-refj_94.html)）用に書かれたテストを実行することができるようになりました。（簡単なものであれば）

### gauche.test互換レイヤーの実装

gauche.testのテストを実行するために、テストスクリプトを読み込み、GaUnitが理解できるようにS式を変形します。変形されたS式はGaUnitが提供するgauche.test互換APIが使える無名モジュール内で評価され、GaUnitのテストとして認識されるようになります。

SchemeではマクロでS式を変換するということはよく行われます。もし、それで十分でない場合は上記のように、スクリプト自体をS式として読み込んで、変換して、評価、ということも行うことができます。これは、S式がプログラムで扱いやすいためにできると言えるでしょう。他の言語、例えばJavaScriptでは、プログラムを変換するためにtoSourceしたものをreplaceしてevalするということが行われることもあるようです。

ただ、非常に多くの場合はこのような方法は必要ありません。今回は(use gauche.test)をどうしてもうまく扱えなかったのでこの方式をとっています。

### ファイルの内容をS式のリストに変換

Gaucheスクリプトを書くときのちょっとした豆知識を紹介しておきます。

Gaucheスクリプトの内容をS式のリストとして読み込む処理は以下のようになります。ファイル先頭のcoding: utf-8などのような文字エンコーディング指定を考慮するためにopen-coding-aware-portをはさむことを忘れてはいけません。

{% raw %}
```scheme
(define (file->sexp-list file)
  (call-with-input-file file
    (lambda (input)
      (port->sexp-list (open-coding-aware-port input)))))
```
{% endraw %}

### まとめ

GaUnit 0.1.6ではgauche.testのテストも実行できるようになったため、既存のgauche.testのテスト（大事な資産）を捨てることなくGaUnitに移行できるようになりました。これを機にGaUnitも試してみてはいかがでしょうか。

gauche.testであれGaUnitであれ、自動化されたテストがあるということは安心できるものです。
