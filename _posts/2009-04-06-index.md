---
tags:
- ruby
- javascript
- mozilla
title: クリアコードの公開リポジトリ
---
すでにお気づきの方もいるかもしれませんが、先日から、クリアコードで開発したプログラムが入った[Subversionリポジトリ](/repos/svn/)（[リポジトリの更新状況のRSS](/svn.rdf)）の公開を始めました。
<!--more-->


クリアコードでは既存のフリーソフトウェアの開発に参加したり、新しく[milter manager](http://milter-manager.sourceforge.net/index.html.ja)などのフリーソフトウェアを開発したりしていますが、それらの開発成果の公開場所はケースバイケースとなっています。

  * 既存のフリーソフトウェアの開発に参加する場合は、基本的に、開発成果はアップストリームに還元しています。
  * 新しくフリーソフトウェアを開発する場合は、基本的には関連コミュニティで標準的なホスティングサイトを利用しています。例えば、Ruby関連のソフトウェアであれば[RubyForge](http://rubyforge.org/)、GNOME関連のソフトウェアであれば[gnome.org](http://developer.gnome.org/)といった具合です。
  * 関連するソフトウェアが[GitHub](http://github.com/)や[Google Code](http://code.google.com/)を利用している場合は、それらのサイトを利用することもあります。
  * 特に標準的なホスティングサイトが無い場合は[SourceForge](http://sourceforge.net/)を利用しています。

このように、クリアコードの開発成果のソースコードは様々なホスティングサイトのリポジトリにて管理、および公開されています。

まもなくクリアコードは設立から3年が経とうとしていますが、その間、プロジェクトを作るまでもないような小規模なソースコードがいくつかたまってきました。この度、そのようなソースコードを[Subversionリポジトリ](/repos/svn/)で公開することにしました。

このリポジトリには現在、[ページの一部を折りたたむfolding.js]({% post_url 2009-04-03-index %})や、このククログを生成するための[tDiary](http://www.tdiary.org/)関連のスクリプト（[日記のデータをSubversionで管理するIOバックエンド]({% post_url 2008-11-13-index %})や[日記を静的なHTMLに変換するスクリプト]({% post_url 2008-12-05-index %})などの記事で述べた物）、[Thunderbird用の各種アドオン](/software/tb/)のソースコードが入っています。誰でも自由にチェックアウトできますので、注意事項をご了承の上でどうぞご利用ください。

以下、現在入っているプログラムを簡単に紹介します。

### 注意事項

  * これらのプログラムはすべて無保証です。
  * プログラムは予告なく追加・削除・変更されることがあります。

### JavaScript関連

  * [/javascript/folding.js](/repos/svn/javascript/folding.js): [ページの一部を折りたたむ]({% post_url 2009-04-03-index %})

### tDiary関連

ククログだけではなく、[milter managerのブログ](http://milter-manager.sourceforge.net/blog/ja/)でも使っています。使い方は[milter managerのtdiary.conf](http://milter-manager.svn.sourceforge.net/svnroot/milter-manager/milter-manager/trunk/html/blog/tdiary.conf)が参考になると思います。

  * [/tdiary/subversionio.rb](/repos/svn/tdiary/subversionio.rb): [tDiaryのSubversionバックエンド]({% post_url 2008-11-13-index %})
  * [/tdiary/gitio.rb](/repos/svn/tdiary/gitio.rb): ↑のSubverionバックエンドのgitバージョンです。
  * [/tdiary/html-archiver.rb](/repos/svn/tdiary/html-archiver.rb): [tDiaryのデータをHTML化する]({% post_url 2008-12-05-index %})で紹介した物ですが、その後、カテゴリに対応するなどいくつかの点で改良されています。
  * [/tdiary/patches/customizable-style-path.rb](/repos/svn/tdiary/patches/customizable-style-path.rb): スタイルファイルのパスをカスタマイズできるようにします。RDスタイルなど、標準では有効になっていないスタイルを使うときに便利です。本家に提案したのですが、rejectされたのでここに入っています。
  * [/tdiary/plugin/classed-category-list.rb](/repos/svn/tdiary/plugin/classed-category-list.rb): class付きでカテゴリリストを生成します。ククログのトップに並んでいる「タグ: ...」の部分です。
  * [/tdiary/plugin/date-to-tag.rb](/repos/svn/tdiary/plugin/date-to-tag.rb): 日付を本文の下に生成します。今思えば名前が悪いですね。後で変えるかもしれません。
  * [/tdiary/plugin/link-subtitle.rb](/repos/svn/tdiary/plugin/link-subtitle.rb): サブタイトルをその記事のリンクにします。通常は日付がリンクになるのですが、このククログでは日付は本文の下に置いてあるので、代わりにサブタイトルをリンクにしています。
  * [/tdiary/plugin/multi-icon.rb](/repos/svn/tdiary/plugin/multi-icon.rb): ページアイコンとして、favicon.ico（ICO形式の画像）とfavicon.png（PNG形式の画像）を両方指定できるようにします。
  * [/tdiary/plugin/title-navi-label.rb](/repos/svn/tdiary/plugin/title-navi-label.rb): 「前の日記」「次の日記」リンクのラベルをリンク先の日記のタイトルにします。
  * [/tdiary/plugin/zz-permalink-without-section-id.rb](/repos/svn/tdiary/plugin/zz-permalink-without-section-id.rb): section_footerプラグインが生成するpermalinkから「#pXX」を削除します。ククログでは、1記事を1セクションにして同じ日には複数の記事を書かないという方針で運営しており、セクションIDが必要ないため、このプラグインを作成しました。ファイル名の「zz-」は、このプラグインの読み込み順序を最後の方にさせるためのものです。

### Thunderbird関連

[Thunderbird Add-ons - クリアコード](/software/tb/)で公開している、Thunderbirdのバグを回避するパッチや挙動の変更を行う拡張機能です。公開ページにも書いてありますが、これらの拡張機能は無保証です。業務上の必要性からの導入をお考えの場合は、[Mozilla Firefox & Mozilla Thunderbird保守・サポートサービス](/services/mozilla/menu.html#support)のご利用もご検討ください。

  * [/thunderbird-extensions/alert_for_invalid_addresses/](/repos/svn/thunderbird-extensions/alert_for_invalid_addresses/): [不正なアドレスの警告表示パッチ](/software/tb/#alertinvalidaddresses)
  * [/thunderbird-extensions/auto_decode_uri/](/repos/svn/thunderbird-extensions/auto_decode_uri/): [URIの自動デコード](/software/tb/#autodecodeuri)
  * [/thunderbird-extensions/clear_addressbook_findterm/](/repos/svn/thunderbird-extensions/clear_addressbook_findterm/): [アドレスブックの検索語句の自動クリアパッチ](/software/tb/#clearabfindterm)
  * [/thunderbird-extensions/do_not_send_linked_files/](/repos/svn/thunderbird-extensions/do_not_send_linked_files/): [リンク先ファイルの送信禁止](/software/tb/#donotsend)
  * [/thunderbird-extensions/normalize_quotes_in_addresses/](/repos/svn/thunderbird-extensions/normalize_quotes_in_addresses/): [メールアドレス分割の問題の修正パッチ](/software/tb/#normalizequotes)
  * [/thunderbird-extensions/open_windows_shortcuts_directly/](/repos/svn/thunderbird-extensions/open_windows_shortcuts_directly/): [Windowsショートカットの直接実行](/software/tb/#openshortcuts)

### おまけ

リポジトリの更新状況を配信している[RSS](/svn.rdf)は、Subversionに標準添付の[commit-email.rb](http://svn.collab.net/repos/svn/trunk/tools/hook-scripts/commit-email.rb)で生成しています。今回、RSSのタイトルや説明を日本語にしたかったので、Subversionのtrunkに[--rss-titleと--rss-descriptionオプションを追加](http://article.gmane.org/gmane.comp.version-control.subversion.svn/34698)しました。また、--repository-uriで指定されたリポジトリのURIを[コミットメールのX-SVN-Repositoryヘッダに設定](http://article.gmane.org/gmane.comp.version-control.subversion.svn/34697)するようにもしています。

[Subversionリポジトリの整形表示](/repos/svn/)には[Repos Style](http://www.reposstyle.com/)を利用しています。mod_dav_svnには[SVNIndexXSLTというオプション](http://subversion.bluegate.org/doc/book.html#svn.serverconfig.httpd.browsing)があって、それを利用しています。
