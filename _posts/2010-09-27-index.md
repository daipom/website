---
tags:
- ruby
title: git-utilsをGitHubへ移動
---
git用のコミットメール配信システムである[git-utils]({% post_url 2010-05-18-index %})のリポジトリを[GitHubへ移動](http://github.com/clear-code/git-utils)しました。こんなこともあろうかとGitHubに[クリアコードアカウント](http://github.com/clear-code)を取得しておいたのです。
<!--more-->


0.0.1以降リリースしていませんが、少しずつ機能を追加していて、[GitHubのPost-Receive Hooks](http://help.github.com/post-receive-hooks/)を用いたGitHub上のリポジトリのコミットメール配信にも対応しています。[tDiaryのコミットメール](http://sourceforge.net/mailarchive/forum.php?forum_name=tdiary-svn)や[groongaのコミットメール](http://sourceforge.jp/projects/groonga/lists/archive/commit/)などがgit-utilsを利用しています。GitHub上のリポジトリでコミットメールを配信したい場合は[お問い合わせフォーム](/contact/)からご連絡ください。（無償・無保証で対応しています。）

[github-post-reciever/](http://github.com/clear-code/git-utils/tree/master/github-post-receiver/)以下を利用して自分でPost-Receive Hooksを受け付けるサーバを立ち上げることもできます。具体的な手順のかかれたインストールドキュメントはありませんが、Rackアプリケーションなので[Passenger](http://www.modrails.com/)などを設定したことがある方は問題なく設置できるでしょう。

GitHubに移動したことでforkしたりpull requestを出しやすくなっているので、自由に利用したり変更したりしてください。改良やドキュメント作成などフィードバックは積極的に取り込んでいく予定です。ライセンスは[GNU_General_Public_License](https://ja.wikipedia.org/wiki/GNU_General_Public_License)v3 or laterです。
