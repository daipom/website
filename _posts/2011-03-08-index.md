---
tags:
- ruby
title: るりまサーチが第3回フクオカRuby大賞のコミュニティ特別賞を受賞
---
[先日、第3回フクオカRuby大賞の本審査に行ってきました]({% post_url 2011-02-03-index %})が、その[結果が発表](http://www.pref.fukuoka.lg.jp/f17/rubyforum2011.html)されました。
<!--more-->


応募していた[るりまサーチ](http://rurema.clear-code.com/)はコミュニティ特別賞に入っていました。認めてもらえてよかったです。

るりまサーチでるりまが便利だと感じた人は[るりまプロジェクトにも参加](http://redmine.ruby-lang.org/projects/rurema/wiki/HowToJoin)してみてはいかがでしょうか？また、何か最近Rubyで作ったシステムがあれば、来年のフクオカRuby大賞に応募してみてはいかがでしょうか？
