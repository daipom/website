---
tags: []
title: logalimacsをリリースしました
---
2012/2/13にEmacsで[logaling-command](http://logaling.github.com)を利用するためのフロントエンド[logalimacs](http://logaling.github.com/logalimacs)をリリースしました。
<!--more-->


### logaling-commandとは

logaling-commandは翻訳作業に欠かせない訳語の確認や選定をサポートする CUI ツールです。
「対訳用語集」を簡単に作成、編集、検索することができます。

### logalimacsとは

logalimacsはEmacsからlogalingを利用するためのフロントエンドです。
CUIで対訳用語集を利用するよりもエディタ上でシームレスに対訳用語集を利用できるとより翻訳作業が捗るため、開発に着手しました。

### 使い方

Emacsを使っていて何らかのドキュメントの翻訳中に英単語を調べる時に、わざわざブラウザに切り替えたくないですよね？
そこでlogalimacsの出番です。`C-:`を押すと、
カーソル位置の単語で対訳用語集を検索します。もしカーソル位置が空白の場合はそれより前の単語で対訳用語集を検索します。このコマンドはpopupで検索結果を表示します
[^0]。

![Emacsでpopupしたところ]({{ "/images/blog/20120213_0.png" | relative_url }} "Emacsでpopupしたところ")

また任意の情報から調べたい場合`C-u`を押してから`C-:`を押して下さい。
minibuffer に入力ボックスが出るので、そこに入力した単語で検索できます。

もしリージョンで選択した単語も検索したい場合は、リージョンの文字列が優先して検索されます。

### インストール

簡単なインストール方法は以下の通りです。
logalimacsを使いたくてウズウズしている方は試してみてください[^1]。

詳しいインストール方法の説明は[logalimacsのチュートリアルページ](http://logaling.github.com/logalimacs/tutorial)で紹介しています。

#### logaling-commandのインストール

{% raw %}
```
% gem install logaling-command
```
{% endraw %}

#### 辞書のインポート

辞書のインポートは1分から2分かかります。

{% raw %}
```
% loga import gene95
% loga import edict
```
{% endraw %}

#### logalimacsのインストール

GitHubからlogalimacsをcloneします。

{% raw %}
```
% cd ~/.emacs.d/
% git clone git://github.com/logaling/logalimacs.git
```
{% endraw %}

~/.emacs.d/init.elに以下を追加します。

{% raw %}
```
(add-to-list 'load-path "~/.emacs.d/logalimacs")
(autoload 'loga-lookup-in-popup "logalimacs" nil t)
(global-set-key (kbd "C-:") 'loga-lookup-in-popup)
```
{% endraw %}

これでlogalimacsを利用するための設定は終わりです。
試しに、*scratch*バッファに「ruby」と入力して`C-:`を押してみてください。
上記のスクリーンショットのような訳がでるはずです[^2]。

### まとめ

logaling-commandをEmacsから簡単につかうためのlogalimacsを紹介しました。ぜひ使ってみてください！

[^0]: [popup.el](https://github.com/m2ym/popup-el.git)を利用しています。

[^1]: 
Emacs24を利用されている方は、[Marmalade](http://marmalade-repo.org/)経由でインストールできます。そのためEmacs側の設定は、`global-set-key`の部分だけになります。

[^2]: 
プログラミング言語Rubyという部分は個別に登録したものなので表示されません。
