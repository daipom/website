---
tags:
- ruby
title: 手元のgemのコードを簡単検索
---
最近のプログラミング言語はパッケージ管理システムを持っていることがほとんどです。Perlには[CPAN](http://www.cpan.org/)[^0]がありますし、Pythonには[PyPi](http://www.python.org/)[^1]がありますし、Rubyには[RubyGems](https://rubygems.org/)[^2]がありますし、Node.jsには[npm](https://npmjs.org/)[^3]があります。パッケージ管理システムがあると簡単にライブラリやツールをインストールできるので、手元にたくさんのコードが集まります。そんな手元のコードを簡単に検索できるようにする方法を紹介します。ただし、ここで紹介するのはRubyGemsでインストールしたパッケージのコードを簡単に検索できるようにする方法だけです。他のパッケージ管理システムについては触れません。
<!--more-->


ライブラリを使っていて、期待した動作をしないときはどうしますか？まず、ドキュメントを確認することでしょう。ドキュメントを読んでも解決しないときはWebで検索したり、実際にコードを読んでみることでしょう。ここで紹介する方法を使えばコードを読むまでのコストが下がるので今までより気軽にコードを確認できるようになるはずです。

### gemを簡単に検索できるようにする方法

[gem-milkode](https://rubygems.org/gems/gem-milkode)というgemをインストールするだけです。

{% raw %}
```
% gem install gem-milkode
```
{% endraw %}

後はいつも通り`gem install`でgemをインストールしてください。

{% raw %}
```
% gem install rails
```
{% endraw %}

検索したくなったら`milk web`を実行してください。Webブラウザーに検索画面が表示されるので、そこからサクサク検索できます。

{% raw %}
```
% milk web
```
{% endraw %}

### `gem install`じゃなくて`bundle install`を使っているんだけど…

最近は`gem install`で個々のgemをインストールするのではなく、[Bundler](https://rubygems.org/gems/bundler)でgemをインストールすることが多くなりましたね。そんなあなたは[bundle-milkode](https://rubygems.org/gems/bundle-milkode)をインストールしてください。

{% raw %}
```
% gem install bundle-milkode
```
{% endraw %}

後は`bundle install`、`bundle update`の代わりに`bundle-milkode`を使うだけです。

{% raw %}
```
% bundle-milkode install
% bundle-milkode update
```
{% endraw %}

これでBundlerでインストールしたgemも簡単に検索できるようになります。

検索したくなったら`milk web`を実行してください。Webブラウザーに検索画面が表示されるので、そこからサクサク検索できます。

{% raw %}
```
% milk web
```
{% endraw %}

### 仕組み

gem-milkodeもbundle-milkodeもgemをインストールする時に[Milkode](http://milkode.ongaeshi.me/)にgemのコードを登録しているだけです。Milkodeは行指向のソースコード検索システムです。コードを登録すればあとはMilkodeがいい感じにやってくれます。gem-milkodeとbundle-milkodeは「Milkodeにコードを登録する」という少し面倒な作業をこっそりやってくれるだけです。でも、それが便利なんです。

さて、それでは、gem-milkodeとbundle-milkodeはどのような仕組みで動いているのでしょうか。

#### bundle-milkode

`bundle-milkode`は`bundle`のラッパーみたいなものなので特別に何かをしているわけではありません。`bundle`の機能を実行した後に新しくインストールされたgemをMilkodeに登録しているだけです。短いコードなので、コードを読むとすぐにわかります。

コード: [bundle-milkode](https://github.com/kou/bundle-milkode/blob/master/bin/bundle-milkode)

#### gem-milkode

gem-milkodeは[RubyGemsのプラグイン機能](http://guides.rubygems.org/plugins/)を使っています[^4]。プラグイン機能の使い方は簡単です。gemに`lib/rubygems_plugin.rb`[^5]というファイルを含めるだけです。後はRubyGemsが勝手に読み込んでくれます。

注意する点は、インストールされている*すべての*gemの`lib/rubygems_plugin.rb`が読み込まれるということです。例えば、gem-milkode-1.0.1とgem-milkode-1.0.2がインストールされているときは、1.0.1の`lib/rubygems_plugin.rb`も1.0.2の`lib/rubygems_plugin.rb`も読み込まれます。そのため、プラグインが提供する同じ機能が何度も実行される可能性があります。

これを回避するために、「複数のバージョンをインストールしないように！」と呼びかける方法と、複数のバージョンがインストールされていても最新のものだけ実行する方法があります。gem-milkode 1.0.2までは前者でしたが、1.0.3からは後者になっています[^6]。gem-milkodeも短いコードなので、やり方はコードを見てください。

コード: [rubygems_plugin.rb](https://github.com/kou/gem-milkode/blob/master/lib/rubygems_plugin.rb)

### まとめ

インストールしたgemを自動でMilkodeに登録する小さなツールを紹介しました。このツールを使うことで簡単にgemのコードを検索できるようになります。gemのコードを読む敷居が下がるので、たくさんコードを読んでみてください。

おそらく、RubyGems以外のパッケージ管理システムにもプラグインのような機能があるはずなので、ここで紹介したツールと同じようなツールを作ればNode.jsやPythonでも簡単にコードを検索できるようになりますね。

[^0]: フロントエンドはいくつかあるみたい。

[^1]: パッケージ管理システムというかパッケージ配布サイト。フロントエンドはいくつかある。

[^2]: これはパッケージ配布サイトもパッケージ管理ツールも提供。

[^3]: これもサイトもツールも提供。

[^4]: リンク先を見ればわかる通り、すでにいろんなプラグインがあります。エディターでgemのファイルを開くコマンドを提供するプラグインがいくつもあることが興味深いですね。やはり、みんなコードを読みたくなるようです。gemのソースを`git clone`するプラグインもあります。

[^5]: `lib/`以下じゃなくても`$LOAD_PATH`が通っている場所であればどこでもよいです。説明が面倒になるので、ここでは`lib/`に置くということで進めます。

[^6]: つまり、gem-milkode 1.0.2以下はさっさとアンインストールしてgem-milkode 1.0.3以降を使ってください、ということです。
