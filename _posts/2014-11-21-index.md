---
tags: []
title: Ubuntuでdebパッケージのお手軽クリーンルーム（chroot）ビルド環境を構築するには
---
### はじめに

debパッケージを用意してDebianの公式リポジトリからインストールできるようにするために必要な作業については以前いくつか記事を書きました。
<!--more-->


  * [Debianでパッケージをリリースできるようにしたい - WNPPへのバグ登録]({% post_url 2014-03-07-index %})
  * [Debianでパッケージをリリースできるようにしたい - よりDebianらしく]({% post_url 2014-04-03-index %})
  * [Debianでパッケージをリリースできるようにしたい - mentors.debian.netの使いかた]({% post_url 2014-07-02-index %})
  * [Debianでパッケージをリリースできるようにしたい - そしてDebianへ]({% post_url 2014-10-31-index %})

ただし、そこではパッケージのビルドそのものについては触れていませんでした。
普段Ubuntuを使っていて、sid（unstable）でのビルド環境をどうしようかというのはいくつか選択肢があります。定番のchroot環境を用意するとか、[LXC](https://ja.wikipedia.org/wiki/LXC)のコンテナを使うとか、[Vagrant](https://www.vagrantup.com/)を使うとか、はたまた[docker](https://www.docker.com/)のイメージを利用するなどさまざまです。

今回はそのうちの1つとして、Ubuntuでdebパッケージのお手軽クリーンルーム（chroot）ビルド環境を構築するのに便利な[cowbuilder](https://wiki.debian.org/cowbuilder)を紹介します。

### Cowbuilderとは

必要なパッケージが一式揃ったクリーンな環境でdebパッケージをビルドするためのツールです。
同様の目的のものとしては[pbuilder](http://pbuilder.alioth.debian.org/)が有名ですが、後発だけあって

  * pbuilderとは違ってベースイメージの展開がないので速い

というのが特徴です。

#### 設定ファイルを書く

cowbuilderはpbuilderのラッパーなので、設定ファイルは.pbuilderrcと共通です。
パッケージに同梱されているpbuilderrc（/usr/share/doc/pbuilder/examples/pbuilderrc）がありますが、そちらは複数のベースイメージをとりあつかうのには都合がよくない[^0]ので、PbuilderTricksのドキュメントの"How to build for different distributions"のセクションから設定[^1]をコピーして$HOME/.pbuilderrcとして保存します。

  * [PbuilderTricks](https://wiki.debian.org/PbuilderTricks)

ただし次の2つについては不足しているので追加します。

  * BASEPATH
  * HOOKDIR

BASEPATHはベースイメージを作成するパスを指定するのに必要です。

{% raw %}
```
BASEPATH="/var/cache/pbuilder/$NAME-base.cow/"
```
{% endraw %}

この設定を追加すると、sid（unstable）の場合だとunstable-amd64-base.cowというようにベースイメージが作成されます。

HOOKDIRは後述するフックを置くためのパスを指定するのに必要です。

{% raw %}
```
HOOKDIR="/var/cache/pbuilder/hooks"
```
{% endraw %}

#### フックスクリプトを配置する

パッケージをビルドするだけでなく、作成したパッケージに問題がないか同時に確認しておきたいというのは当然ですね。それを実現するにはフックスクリプトを使います。

先程のexamplesディレクトリにlintianを実行するためのフックスクリプトがあるので、それをHOOKDIRにコピーしましょう。

{% raw %}
```
% sudo cp /usr/share/doc/pbuilder/examples/B90lintian /var/cache/pbuilder/hooks
```
{% endraw %}

examplesディレクトリにはほかにもいくつかフックスクリプトがあるので興味があれば覗いてみるとよいでしょう。

#### ベースイメージを作成する

さて、設定ファイルが用意できたので、実際にsid（unstable）のベースイメージを作成してみましょう。
ベースイメージを作成するのには次のコマンドを実行します。

{% raw %}
```
% sudo DIST=sid cowbuilder --create --debootstrapopts --keyring=/usr/share/keyrings/debian-archive-keyring.gpg
```
{% endraw %}

パッケージのダウンロードとインストールがはじまるので、完了するまでしばらく待ちましょう。

Ubuntu上でDebian wheezy/jessie/sidのベースイメージを作成する際にエラーがでるようならkeyringをインストールし忘れていないか確認してください。
keyringパッケージは次のコマンドを実行することでインストールできます。

{% raw %}
```
% sudo apt-get install debian-archive-keyring
```
{% endraw %}

#### ベースイメージを更新する

ベースイメージを作ったばかりのときは良いのですが、sid（unstable）は日々更新されています。
ベースイメージも最新の状態に追従するには次のコマンドを実行します。

{% raw %}
```
% sudo DIST=sid cowbuilder --update
```
{% endraw %}

#### パッケージをビルドする

ベースイメージができたので、実際にパッケージをビルドしてみましょう。

sid（unstable）向けにGroongaのパッケージをcowbuilderでビルドするには次のコマンドを実行します。

{% raw %}
```
% sudo DIST=sid cowbuilder --build groonga_4.0.7-1.dsc --basepath /var/cache/pbuilder/unstable-amd64-base.cow
```
{% endraw %}

これで、クリーンルームでのパッケージのビルドとlintianによるチェックが実行されます。
ビルドされたパッケージは/var/cache/pbuilder/unstable-amd64/result以下に保存されます。

### まとめ

今回はUbuntuでdebパッケージのお手軽クリーンルーム（chroot）ビルド環境を構築するのに便利なcowbuilderの紹介をしました。
パッケージをビルドしたあとのインストールやアップグレードについては、また次の機会に紹介します。

[^0]: 例えばsidだけじゃなくてwheezyとかtrustyとかでもパッケージをビルドしたいとかには向いていない。

[^1]: wheezy,jessie,sid以外にも、hardy,lucid,natty,oneiric,precise,quantal,raring,saucy,trusty,utopicまでサポートしているらしい。すべて試したわけではないので実際にどうかはわからない。
