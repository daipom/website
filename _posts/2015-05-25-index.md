---
tags:
- groonga
- presentation
title: PostgreSQLで日本語全文検索 - LIKEとpg_bigmとPGroonga
---
[PostgreSQLアンカンファレンス＠東京（2015/5/30）](https://atnd.org/events/64543)でPostgreSQLの日本語全文検索まわりについて紹介しようかとたくらんでいます。しかし、現時点（2015-05-25）でキャンセル待ちで、当日参加できないかもしれないので紹介しようと用意している内容をここにまとめます。
<!--more-->


<div class="rabbit-slide">
  <iframe src="//slide.rabbit-shocker.org/authors/kou/postgresql-unconference-tokyo-2015/viewer.html"
          width="640" height="524"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="//slide.rabbit-shocker.org/authors/kou/postgresql-unconference-tokyo-2015/" title="PostgreSQLで日本語全文検索 - LIKEとpg_bigmとPGroonga">PostgreSQLで日本語全文検索 - LIKEとpg_bigmとPGroonga</a>
  </div>
</div>


### 内容

この資料の目的は、PostgreSQLで使える次の3つの方法の特性を紹介し、ユーザーが適切な方法を選択するための材料を提供することです。

  * `LIKE`
  * [pg_bigm](http://pgbigm.osdn.jp/)
  * [PGroonga](https://github.com/pgroonga/pgroonga)（ぴーじーるんが）

#### `LIKE`

`LIKE`のメリット・デメリットは次の通りです。

<dl>






<dt>






メリット






</dt>






<dd>


  * 標準で使える
  * インデックス作成不要（= データ更新が遅くならない）
  * データが少なければ十分速い


</dd>








<dt>






デメリット






</dt>






<dd>


  * データ量に比例して遅くなる


</dd>


</dl>

ユーザーが`LIKE`を使うかどうかの判断基準は「十分速いかどうか」（= 「データが少ないかどうか」）です。では、どのくらいなら「十分速い」のでしょうか。

「十分速い」かどうかは「検索対象のデータでの計測結果」と「要件」で判断できます。どちらもケースバイケースですが、参考情報としての計測結果なら示すことができます。

参考情報として[pg_bigmでいろんなデータを日本語検索してみよう！](http://qiita.com/fujii_masao/items/87f1d94ff4d350a718aa)の`LIKE`の結果を使ってみましょう。この記事では次の3つのデータを検索しています。

  * 青空文庫の書籍一覧データ
  * 住所データ
  * 日本版Wikipediaのタイトル一覧データ

それぞれみていきましょう。

まずは「青空文庫の書籍一覧データ」です。

このデータの特性と検索速度は次の通りです。

<table>
  <tr>
    <th>件数</th>
    <th>1レコードあたりのバイト数</th>
    <th>検索速度</th>
  </tr>
  <tr>
    <td style="text-align: right">11,818件</td>
    <td style="text-align: right">17バイト</td>
    <td style="text-align: right">6.673ms</td>
  </tr>
</table>


6msで結果が返ってくるのであれば多くの要件で「十分速い」と言えるでしょう。つまり、このデータでは日本語全文検索の方法として`LIKE`はアリということです。

次のデータは「住所データ」です。

このデータの特性と検索速度は次の通りです。

<table>
  <tr>
    <th>件数</th>
    <th>1レコードあたりのバイト数</th>
    <th>検索速度</th>
  </tr>
  <tr>
    <td style="text-align: right">147,769件</td>
    <td style="text-align: right">14バイト</td>
    <td style="text-align: right">70.684ms</td>
  </tr>
</table>


70msで結果が返ってくるのであれば多くの要件で「十分速い」と言えるでしょう。つまり、このデータでも日本語全文検索の方法として`LIKE`はアリということです。

最後のデータは「日本版Wikipediaのタイトル一覧データ」です。

このデータの特性と検索速度は次の通りです。

<table>
  <tr>
    <th>件数</th>
    <th>1レコードあたりのバイト数</th>
    <th>検索速度</th>
  </tr>
  <tr>
    <td style="text-align: right">2,461,588件</td>
    <td style="text-align: right">20バイト</td>
    <td style="text-align: right">943.450ms</td>
  </tr>
</table>


1秒くらいで結果が返ってくるのでは、「十分速い」というのは要件次第でしょう。つまり、この規模のデータでは日本語全文検索の方法として`LIKE`は使えるかもしれないし使えないかもしれない、といったところです。

これまでの結果をまとめると次の通りです。`LIKE`で十分かどうかを判断する場合は、実際のデータの傾向と検索時間を材料として検討します。

<table>
  <tr>
    <th>件数</th>
    <th>1レコードあたりのバイト数</th>
    <th>検索速度</th>
  </tr>
  <tr>
    <td style="text-align: right">11,818件</td>
    <td style="text-align: right">17バイト</td>
    <td style="text-align: right">6.673ms</td>
  </tr>
  <tr>
    <td style="text-align: right">147,769件</td>
    <td style="text-align: right">14バイト</td>
    <td style="text-align: right">70.684ms</td>
  </tr>
  <tr>
    <td style="text-align: right">2,461,588件</td>
    <td style="text-align: right">20バイト</td>
    <td style="text-align: right">943.450ms</td>
  </tr>
</table>


では、`LIKE`では十分速くない場合はどうしたらよいでしょうか。次のどちらかの方法を選びます。

  * pg_bigm
  * PGroonga

#### pg_bigm

pg_bigmのメリット・デメリットは次の通りです。

<dl>






<dt>






メリット






</dt>






<dd>


  * データ量が多くても高速
  * ストリーミングレプリケーションを使える


</dd>








<dt>






デメリット






</dt>






<dd>


  * 別途インストールしないといけない
  * インデックス作成が遅い（= データ更新が遅くなる）
  * ヒット数に比例して遅くなる


</dd>


</dl>

pg_bigmはデータ量が多くても高速ですが、ヒット数が多いほど遅くなる特性があります。pg_bigmが十分速いかどうかを判断するためにも、`LIKE`と同様に「検索対象のデータでの計測結果」と「要件」が必要です。ただし、`LIKE`とは注目するポイントが異なります。

それでは、検索結果例を見ながら注目するポイントを確認しましょう。なお、ここで紹介する検索結果例は[PGroongaとpg_bigmのベンチマーク結果 · Issue #2 · groonga/wikipedia-search](https://github.com/groonga/wikipedia-search/issues/2)にあります。手順もまとまっているので誰でも追試することができます。
ここで使うデータは日本語版Wikipediaの本文です。このデータの特性は次の通りです。

<table>
  <tr>
    <th>件数</th>
    <th>1レコードあたりのバイト数</th>
  </tr>
  <tr>
    <td style="text-align: right">1,846,514件</td>
    <td style="text-align: right">3777バイト</td>
  </tr>
</table>


注目するポイントの1つ目はインデックス作成時間です。元データのロード時間とインデックス作成時間は次の通りです。

<table>
  <tr>
    <th>元データのロード時間</th>
    <th>インデックス作成時間</th>
  </tr>
  <tr>
    <td style="text-align: right">16分31秒</td>
    <td style="text-align: right">5時間56分15秒</td>
  </tr>
</table>


約16分でロードできるデータのインデックス作成に約6時間かかるのは十分速いでしょうか。それとも遅いでしょうか。要件と照らしあわせて検討します。

なお、この時間はインデックス全体を1から作成する時間です。常に少量ずつ更新するという要件であればこの時間はあまり関係ありません。そのような要件の場合は、少量のデータの更新が滞らない（更新処理が溜まっていかない）くらいの速度がでていれば十分速いと判断できます。

注目するポイントの2つ目はヒット数と検索時間の関係です。

<table>
  <tr>
    <th>検索語</th>
    <th>ヒット数</th>
    <th>検索時間</th>
  </tr>
  <tr>
    <td>「PostgreSQL」または「MySQL」</td>
    <td style="text-align: right">361</td>
    <td style="text-align: right">0.107s</td>
  </tr>
  <tr>
    <td>データベース</td>
    <td style="text-align: right">17168</td>
    <td style="text-align: right">1.224s</td>
  </tr>
  <tr>
    <td>テレビアニメ</td>
    <td style="text-align: right">22885</td>
    <td style="text-align: right">2.472s</td>
  </tr>
  <tr>
    <td>日本</td>
    <td style="text-align: right">625792</td>
    <td style="text-align: right">0.556s</td>
  </tr>
</table>


pg_bigmはヒット数が多くなるほど遅くなります。数百件ヒットのときは0.1秒ですが、1万7千件のときは1秒ちょい、2万2千件のときは約2.5秒となりました。なお、このデータではヒット数が増えるほど遅くなる傾向がよりわかりやすいデータになっています。というのは、このデータは1レコードあたりのデータが大きいからです。pg_bigmは1レコードあたりのデータが大きいと遅くなりやすい特性があります。

なお、検索語が2文字以下の場合はヒット数が多くても遅くなりにくいという特性があります。例では62万件ヒットしても0.5秒で結果を返しています。

このような特性があるpg_bigmなので、データによっては遅いと判断する場面も十分速いと判断する場面もあるでしょう。

#### PGroonga

`LIKE`では遅い場合、pg_bigm以外の選択肢はPGroongaです。

PGroongaのメリット・デメリットは次の通りです。

<dl>






<dt>






メリット






</dt>






<dd>


  * インデックス作成が速い
  * データ量が多くても高速
  * ヒット数が多くても高速


</dd>








<dt>






デメリット






</dt>






<dd>


  * 別途インストールしないといけない
  * ストリーミングレプリケーション非対応
  * 使用ディスクサイズが多い


</dd>


</dl>

PGroongaはデータ量が多くてもヒット数が多くても速度が落ちにくく、インデックス作成も高速ですが、PostgreSQLとの連携度合いはpg_bigmほどではありません。たとえば、pg_bigmはPostgreSQL標準のストリーミングレプリケーション機能を使えますが、PGroongaは使えません。これはPostgreSQL側が連携する機能を提供していないことが原因です。今後連携できる機能が提供される見込みですが、現時点で連携できないことは事実です。

PGroongaを選ぶかどうかの基準は他の方法では遅いケースがPGroongaだと速いかどうかです。そのためには同じデータでpg_bigmと比較する必要があります。

それでは、pg_bigmと同じデータを使った場合のPGroongaの速度を確認しましょう。

まずはインデックス作成時間です。

<table>
  <tr>
    <th>元データのロード時間</th>
    <td style="text-align: right">16分31秒</td>
  </tr>
  <tr>
    <th>PGroongaのインデックス作成時間</th>
    <td style="text-align: right">25分37秒</td>
  </tr>
  <tr>
    <th>pg_bigmのインデックス作成時間</th>
    <td style="text-align: right">5時間56分15秒</td>
  </tr>
</table>


参照しやすいようにpg_bigmのインデックス作成時間も再掲しています。

PGroongaは元データのロード時間の2倍いかないくらいの時間でインデックスを作成しています。pg_bigmと比べると1/14の時間でインデックスを構築できています。

ただし、インデックスのために使用するディスクサイズは大きいです。

<table>
  <tr>
    <th>元データロード直後のデータベースサイズ</th>
    <td style="text-align: right">4.0GiB</td>
  </tr>
  <tr>
    <th>PGroongaの使用ディスクサイズ</th>
    <td style="text-align: right">14.0GiB</td>
  </tr>
  </tr>
    <th>pg_bigmの使用ディスクサイズ</th>
    <td style="text-align: right">3.6GiB</td>
  </tr>
</table>


ディスクサイズが小さい環境で動かす場合は問題になるかもしれません。

次は検索時間を確認しましょう。

<table>
  <tr>
    <th>検索語</th>
    <th>ヒット数</th>
    <th>PGroongaの検索時間</th>
    <th>pg_bigmの検索時間</th>
  </tr>
  <tr>
    <td>「PostgreSQL」または「MySQL」</td>
    <td style="text-align: right">368</td>
    <td style="text-align: right">0.030s</td>
    <td style="text-align: right">0.107s</td>
  </tr>
  <tr>
    <td>データベース</td>
    <td style="text-align: right">17172</td>
    <td style="text-align: right">0.121s</td>
    <td style="text-align: right">1.224s</td>
  </tr>
  <tr>
    <td>テレビアニメ</td>
    <td style="text-align: right">22885</td>
    <td style="text-align: right">0.179s</td>
    <td style="text-align: right">2.472s</td>
  </tr>
  <tr>
    <td>日本</td>
    <td style="text-align: right">625792</td>
    <td style="text-align: right">0.646s</td>
    <td style="text-align: right">0.556s</td>
  </tr>
</table>


PGroongaはpg_bigmほどヒット数の多さに影響をうけていません。62万件ヒットする場合でも0.6秒ちょいで結果を返しています。PGroongaは全体的に高速ですが、検索語が2文字以下の場合はpg_bigmの方が高速です。

PostgreSQLとの連携度合いよりも性能が大事な要件な場合で、pg_bigmでは性能が落ちやすい規模のデータではPGroongaが選択肢になるでしょう。

### まとめ

PostgreSQLで日本語全文検索の方法として次の3つの方法を紹介しました。

  * `LIKE`
  * pg_bigm
  * PGroonga

データが少ないなら`LIKE`で十分です。要件次第ですが、1レコード数十バイトの小さなデータなら百万件くらいまでいけるかもしれません。

データが多いならpg_bigmかPGroongaを選びます。

次のような場合はpg_bigmがよいでしょう。

  * 2文字以下での検索がほとんど
  * 1レコードあたりのデータが小さい
  * ヒット数が多くなることがほとんどない
  * ストリーミングレプリケーションを使いたい

pg_bigmで次のような問題があるならPGroongaがよいでしょう。

  * 1レコードあたりのデータが大きい
  * ヒット数が多くなるケースを考慮しないといけない
  * 更新が多くてインデックス更新速度がネックになる

ちなみに、PGroongaでもレプリケーションをする方法はあります。興味のある方は[pg_shardとPGroongaを使ったレプリケーション対応の高速日本語全文検索可能なPostgreSQLクラスターの作り方]({% post_url 2015-05-18-index %})を参考にしてください。

最後に、PostgreSQLの日本語全文検索に興味のある方にお願いです。この説明の中のpg_bigmとPGroongaの性能確認で参照したベンチマークをあなたの環境でも動かして結果を教えてもらえないでしょうか？同じ傾向があるか確認したいのです。ベンチマークの実行方法や必要な環境などの詳細は[協力者募集：PGroongaとpg_bigmのベンチマークを実行](http://groonga.org/ja/blog/2015/05/25/help-pgroonga-vs-pg-bigm-benchmark.html)を参照してください。
