---
tags:
- fluentd
title: Fluentd Sinkを用いてApache Flume NGからFluentdへレコードを出力するには
---
### はじめに

クリアコードでは[Fluentd](http://www.fluentd.org)というソフトウェアの開発に参加しています。Fluentdはログ収集や収集したデータの分配・集約などを行うソフトウェアです。
<!--more-->


また、fluent-loggerと呼ばれるFluentdへデータを転送出来る実装が各言語で公開されています。

同じようなログ収集や収集したデータ分配・集約のソフトウェアとして、[Apache Flume NG](https://flume.apache.org)というものもあります。

Apache Flume NGにはデータの入力とデータの出力のAPIが公開されており、プラグインを実装することが可能です。
Apache Flume NGの場合はそれぞれ入力がSource、出力がSinkと呼ばれています。

### Flume NGのSinkについて

FluentdのようにApache Flume NGに対してもサードパーティがプラグインを公開しています。
ただし、サードパーティのプラグインはJVMで動き、Flume NGのjarが公開しているAPIを使用できる言語なら好きなものを使う事ができます。
中にはFlumeの実装に使われているJavaではなくScalaで実装されたFlume NGプラグインも存在します。

FlumeのSinkでは次のメソッドを実装することが必要です。

```java
public void start(); // @Override org.apache.flume.AbstractSink's start()
public void stop(); // @Override org.apache.flume.AbstractSink's stop()
public void process(); // @Override org.apache.flume.Sink's process()
public void configure(); // Implement org.apache.flume.conf.Configurable 
```


そのほか、トランザクションを使う時には`org.apache.flume.Transaction` をimportし、チャンネル毎のトランザクションを発行する必要があったり、`org.apache.flume.Sink` にある `Status` 定数を用いて、トランザクションが成功すれば `READY`、失敗であれば `BACKOFF` とするように書きます。

### Fluentd Sinkの作成

Apache Flume NGはJavaで実装されている上に、特段他の言語を採用する理由が見当たらなかったのでJavaで実装しました。また、ビルドツールには[Gradle](gradle.org)を使用しました。
リポジトリは[こちら](https://github.com/cosmo0920/flume-ng-fluentd-sink)です。

Javaのfluent-loggerには[fluent-logger-java](https://github.com/fluent/fluent-logger-java)が存在しますが、より速いと謳っている[fluency](https://github.com/komamitsu/fluency)をfluent-loggerとして用いる事にしました。

FlumeのSinkとして作成をしてみると以下の利点がある事が分かりました。

  * Sinkの中でTransactionが発行出来る

  * Transactionが発行出来るため、FlumeのconfでtransactionCapacityで指定した分だけトランザクションを実行出来る

という点です。このため、設定によっては安全に転送する事が出来ます。

### 使い方

ほとんどREADMEに書いていますが、Flume NGの${FLUME_HOME}/lib 以下に `gradle shadowJar` を実行して作成されたflume-ng-fluentd-sink-${version}-all.jarを配置し、confを書くだけです。
0.0.1のリリースタグを打ってあるので、この[v0.0.1 リリースページ](https://github.com/cosmo0920/flume-ng-fluentd-sink/releases/tag/v0.0.1)からflume-ng-fluentd-sink-0.0.1-all.jarをFlume NGの${FLUME_HOME}/lib 以下に配置することでも使う事ができます。

### おわりに

Flume NGを使っていて、Fluentdに送れる機能が欲しいなぁという方にはぜひ試して頂きたいです。そうでなくてもJavaでこうした方が良いですよ、というものでもPRして下されば取り込みますのでよろしくお願いします。
