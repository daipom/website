---
tags:
- ruby
- company
title: クリアコード：RubyKaigi 2015にスピーカー・スポンサーとして参加予定
---
12月11日から13日の3日間[RubyKaigi 2015](http://rubykaigi.org/2015/)が開催されます。なんと、もう今週です。3日目は学生は無料で入れるということなので、学生の方は[3日目の16:40から](http://rubykaigi.org/2015/schedule#dec13)の[咳さんのActor, Thread and me](http://rubykaigi.org/2015/presentations/seki)を聞きに行くといいです。最後の「and me」がかっこいいですね。
<!--more-->


クリアコードは例年RubyKaigiのスポンサーをしています。去年の[RubyKaigi 2014のスポンサー]({% post_url 2014-08-12-index %})に引き続き、[RubyKaigi 2015もスポンサー](http://rubykaigi.org/2015/sponsors#clearcode)になりました。

また、須藤がスピーカーとして[2日目の12月12日の10:45からのセッション](http://rubykaigi.org/2015/schedule#dec12)で話します。[Rubyのテスティングフレームワークの歴史]({% post_url 2014-11-06-index %})+αについて話す予定です。

例年、クリアコードにとってRubyKaigiは数少ない露出の機会となっています。RubyKaigiに参加したことで正社員への応募やインターンシップへの応募がきます。昨年はRubyKaigi関連の機会を活用し、正社員が2名増えました。今回の機会もぜひ活かしたいところなので、クリアコードを知っていて応援したいという方は、会期中、まわりの人にクリアコードを紹介してください。ご協力よろしくお願いします。

[リーダブルコードワークショップ](/services/code-reader/readable-code-workshop.html)のチラシとGroonga族のステッカーも配布する予定なので、こちらも応援よろしくお願いします。
