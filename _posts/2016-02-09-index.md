---
tags:
- groonga
- presentation
title: 'MySQLとPostgreSQLと日本語全文検索：MySQL・PostgreSQLでGroonga（Mroonga・PGroonga）を使って日本語全文検索
  #mypgft'
---
年に一度の肉の日、2月9日に「[MySQLとPostgreSQLと日本語全文検索](https://groonga.doorkeeper.jp/events/35295)」というイベントを開催しました。次の2つのことについて紹介するイベントです。
<!--more-->


  * MySQLでの日本語全文検索の歴史と実現方法

  * PostgreSQLでの日本語全文検索の歴史と実現方法

その中で、MySQLで日本語全文検索を実現する[Mroonga](http://mroonga.org/ja/)（むるんが）とPostgreSQLで日本語全文検索を実現する[PGroonga](http://pgroonga.github.io/ja/)（ぴーじーるんが）を紹介しました。

<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kou/mysql-and-postgresql-and-japanese-full-text-search/viewer.html"
          width="640" height="524"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kou/mysql-and-postgresql-and-japanese-full-text-search/" title="MroongaとPGroonga">MroongaとPGroonga</a>
  </div>
</div>


Mroonga（むるんが）もPGroonga（ぴーじーるんが）もどちらも国産の全文検索エンジン[Groonga](http://groonga.org/ja/)（ぐるんが）を利用しています。MySQLにGroongaを組み込んだのがMroongaで、PostgreSQLにGroongaを組み込んだのがPGroongaです。

Mroonga・PGroongaを使うと、Groongaの高速で高機能な全文検索機能をMySQL・PostgreSQLで使えます。資料にはベンチマーク結果もありますが、全体的に他の実現方法よりも高速であることがわかります。

MySQL・PostgreSQLで日本語全文検索を実現したいときはMroonga・PGroongaを検討してみてください。

イベントではMroonga・PGroonga以外にも、MySQL 5.7のInnoDBの日本語全文検索機能、[pg_bigm](http://pgbigm.osdn.jp/)を利用したPostgreSQLでの日本語全文検索機能の紹介がありました。MySQL・PostgreSQLでの日本語全文検索の歴史についての情報も紹介してあるので、MySQL・PostgreSQLでの日本語全文検索に興味のある方はこちらの資料もご覧ください。

  * [MySQL 5.7 InnoDB日本語全文検索](http://www.slideshare.net/yoyamasaki/20160209-inno-dbftsjp)

  * [PostgreSQLでpg_bigmを使って日本語全文検索 〜pg_bigmで全文検索するときに知っておくべき8のこと〜](http://www.slideshare.net/hadoopxnttdata/postgresqlpgbigm-mysqlpostgresql)

他にも会場を提供してくれたDMM.comラボさんでの日本語全文検索の利用事例の紹介がありました。DMM.comラボさんではMySQLとMroongaを利用して日本語全文検索を実現しているとのことでした。

  * [DMM.comラボでの日本語全文検索の利用事例紹介](http://www.slideshare.net/Shinonome/dmmcom-58050429)

多くのデータはSQLで操作できる状態になっているため、SQLで日本語全文検索も実現できることは使い勝手の面で大きなメリットになります。当日の参加者の多くは「MySQLとPostgreSQLと日本語全文検索」の第2回があったら参加したいとのことでした。そのため、数カ月後に同様のイベントの開催を検討しています。SQLで日本語全文検索機能を実現したい場合はぜひご参加ください。

次回開催にあたり、会場提供をしてくれる方、次の製品での日本語全文検索機能の利用事例を紹介してくれる方を募集しています。ご協力いただける方は[@ktou](https://twitter.com/ktou)または`kou@clear-code.com`までご連絡ください。

  * MySQL 5.7 InnoDB

  * pg_bigm

  * Mroonga

  * PGroonga
