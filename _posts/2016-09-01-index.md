---
tags:
- presentation
title: 'Speee Cafe Meetup #02 ：OSS開発者を増やしたい！ #speee_lounge'
---
2016年9月1日に「[Speee Cafe Meetup #02](http://speee.connpass.com/event/38159/)」という「OSS開発」をテーマにした勉強会が[Speee Lounge](http://newgraduate-blog.jp/?p=659)で開催されました。
<!--more-->


この勉強会で「OSS開発者を増やしたい！」という話をしてきました。

<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kou/speee-cafe-meetup-02/viewer.html"
          width="640" height="524"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kou/speee-cafe-meetup-02/" title="OSS開発者を増やしたい！">OSS開発者を増やしたい！</a>
  </div>
</div>


関連リンク：

  * [スライド（Rabbit Slide Show）](https://slide.rabbit-shocker.org/authors/kou/speee-cafe-meetup-02/)

  * [スライド（SlideShare）](http://www.slideshare.net/kou/speee-cafe-meetup-02)

  * [リポジトリー](https://github.com/kou/rabbit-slide-kou-speee-cafe-meetup-02)

この話では「独自にOSSを開発する」だけでなく「既存のOSSの開発」（たとえばバグレポートとか問題修正とか）も含んで「OSS開発」と言っています。

話の流れは次の通りです。

  * 「個人」がOSSを開発する動機は「自分が割に合うか」でよい

    * OSSを開発したことがない人は「OSSを開発する」こと自体の敷居が高いので「割に合う」と判断しにくい

    * [OSS Gate](https://oss-gate.doorkeeper.jp/)という「OSS開発をする敷居を下げる」取り組みがあるので活用するとよい

  * 「個人」ではなく「組織」で「OSSを開発する」ことに取り組むにはどうすればよいか

    * まず、（多くの）組織にとって「OSSを開発する」ことは「目的」ではなく「手段」であることを認識する

    * 「目的」は技術力アップ・開発効率アップ・知名度アップなどである

    * 「OSSを開発する」ことで↑の目的を実現できることがある

    * そのため、組織でOSSを開発することに取り組む場合は「みんなでOSSを開発しよう！」と「OSS開発すること自体を目的」にするのではなく、「（↑で挙げたような）メリットを実現するためにOSSを開発するというやり方を使おう」という取り組み方の方が現実的である

      * そうしないと、OSSを開発したけど技術力アップ・開発効率アップ・知名度アップなどにつながらなかったら「なんでOSSを開発しているんだろう…やめよう…」となってしまう

なお、クリアコードは組織がOSS開発に取り組むことを支援する[OSS開発支援サービス]({% post_url 2016-06-27-index %})を提供しています。組織としてOSS開発に取り組みたいと検討している方は[ご相談](/contact/?type=oss-development)ください。なにを期待してOSS開発に取り組みたいかを明確にするところからのサポートにも対応します。
