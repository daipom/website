---
tags:
- groonga
- redmine
- presentation
title: 'MySQLとPostgreSQLと日本語全文検索3：MroongaとPGroongaの導入方法例 #mypgft'
---
2016年9月29日（肉の日！）に「[MySQLとPostgreSQLと日本語全文検索3](https://groonga.doorkeeper.jp/events/50541)」というイベントを開催しました。その名の通りMySQLとPostgreSQLでの日本語全文検索についての話題を扱うイベントです。今回も[DMM.comラボ](http://labo.dmm.com/)さんに会場を提供してもらいました。
<!--more-->


[2月9日に開催した1回目のイベント]({% post_url 2016-02-09-index %})では[Mroonga](http://mroonga.org/ja/)・[PGroonga](https://pgroonga.github.io/ja/)については次の2つのことについて紹介しました。

  * Mroonga・PGroongaが速いということ

  * Mroonga・PGroongaの使い方

[6月9日に開催した2回目のイベント]({% post_url 2016-06-09-index %})ではMroonga・PGroongaについては次の2つのことについて紹介しました。

  * Mroonga・PGroongaのオススメの使い方

  * レプリケーションまわり

今回はMroonga・PGroongaについては次のことについて紹介しました。

  * [Redmine](http://www.redmine.org/)・[Zulip](https://zulip.org/)にMroonga・PGroongaを導入する方法

<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kou/mysql-and-postgresql-and-japanese-full-text-search-3/viewer.html"
          width="640" height="524"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kou/mysql-and-postgresql-and-japanese-full-text-search-3/" title="Mroonga・PGroonga導入方法">Mroonga・PGroonga導入方法</a>
  </div>
</div>


関連リンク：

  * [スライド（Rabbit Slide Show）](https://slide.rabbit-shocker.org/authors/kou/mysql-and-postgresql-and-japanese-full-text-search-3/)

  * [スライド（SlideShare）](http://www.slideshare.net/kou/mysql-and-postgresql-and-japanese-full-text-search-3)

  * [リポジトリー](https://github.com/kou/rabbit-slide-kou-mysql-and-postgresql-and-japanese-full-text-search-3)

### Redmineへの導入方法

[Redmine](http://www.redmine.org/)というチケット管理システムへのMroonga・PGroongaの導入方法を説明します。RedmineはRuby on Railsを利用しているのでRuby on Railsを使っているアプリケーションに導入する例ということになります。

Redmineは右上の検索ボックスから全文検索できます。ここから全文検索したときにMroonga・PGroongaを使うようにします。

![Redmineの検索ボックス]({{ "/images/blog/20160929_0.png" | relative_url }} "Redmineの検索ボックス")

[redmine_full_text_searchプラグイン]({% post_url 2016-04-11-index %})を使うとRedmineでMroongaまたはPGroongaを使って全文検索できるようになります。

このプラグインを使うとRedmineの全文検索が高速になります。たとえば、クリアコードで使っているRedmineには3000件くらいのチケットがありますが、その環境では次のように高速になりました。

<table>
  <thead>
    <tr>
      <th>プラグイン</th>
      <th>時間</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>なし</td>
      <td>467ms</td>
    </tr>
    <tr>
      <td>あり</td>
      <td>93ms</td>
    </tr>
  </tbody>
</table>


200万件のチケットがある環境でも約380msで検索できているという報告もあります。

<blockquote class="twitter-tweet" data-lang="ja"><p lang="ja" dir="ltr">200万チケット<a href="https://twitter.com/MySQL">@MySQL</a>でやってみたよ。検索時間は約380ms。 <a href="https://twitter.com/hashtag/Redmine?src=hash">#Redmine</a> の未来が広がって嬉しいな。ありがたいな。／Redmineで高速に全文検索する方法 - ククログ(2016-04-11) <a href="https://t.co/s7FA4gSThu">https://t.co/s7FA4gSThu</a> <a href="https://twitter.com/_clear_code">@_clear_code</a></p>&mdash; Kuniharu AKAHANE (@akahane92) <a href="https://twitter.com/akahane92/status/733832496945594368">2016年5月21日</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>


#### Mroongaを導入する方法

Mroongaはトランザクションに対応していないのでトランザクションが必須のRedmineに組み込む場合はひと工夫必要になります。単純に、`ALTER TABLE table ENGINE=Mroonga ADD FULLTEXT INDEX (column)`とするわけにはいきません。

ではどうするかというと別途全文検索用のテーブルを作成して元のテーブルとは`JOIN`できるようにします。（他にもレプリケーションしてレプリケーション先をMroongaにするという[2回目のイベントで紹介した方法]({% post_url 2016-06-09-index %})もありますが、プラグインでやるには大掛かりなのでこの方法を使っています。）

マイグレーションファイルでいうと次のようにします。ここでは`issues`テーブル用の全文検索用のテーブルを作成しています。

```ruby
def up
  create_table(:fts_issues, # 全文検索用テーブル作成
               id: false, # idは有効・無効どっちでも可
               options: "ENGINE=Mroonga") do |t|
    t.belongs_to :issue, index: true, null: false
    t.string :subject, default: "", null: false
    t.text :description, limit: 65535, null: false
    t.index [:subject, :description], type: "fulltext"
  end
end
```


全文検索用のテーブルには元のデータをコピーする必要があります。マイグレーション時には既存のデータを一気にコピーします。そのため、本当のマイグレーションの内容は次のようになります。データコピー後にインデックスを追加するようにしているのはそっちの方が速いからです。

```ruby
def up
  create_table(:fts_issues, # 全文検索用テーブル作成
               id: false, # idは有効・無効どっちでも可
               options: "ENGINE=Mroonga") do |t|
    t.belongs_to :issue, index: true, null: false
    t.string :subject, default: "", null: false
    t.text :description, limit: 65535, null: false
  end
  execute("INSERT INTO " + # データをコピー
            "fts_issues(issue_id, subject, description) " +
            "SELECT id, subject, description FROM issues;")
  add_index(:fts_issues, [:subject, :description],
            type: "fulltext") # 静的インデックス構築（速い）
end
```


このテーブルのモデルは次のようになります。

```ruby
class FtsIssue < ActiveRecord::Base
  # 実際はissue_idカラムは主キーではない。
  # 主キーなしのテーブルなので
  # Active Recordをごまかしているだけ。
  self.primary_key = :issue_id
  belongs_to :issue
end
```


Mroonga導入後に更新されたデータはアプリケーション（Redmine）側でデータをコピーします。Active Recordの`after_save`フックを利用します。Mroongaがトランザクションをサポートしていないため、ロールバックのタイミングによってはデータに不整合が発生することがありますが、再度保存すれば復旧できることとそれほどロールバックは発生しないため、実運用時には問題になることはないでしょう。

```ruby
class Issue
  # この後にロールバックされることがあるのでカンペキではない
  # 再度同じチケットを更新するかデータを入れ直せば直る
  after_save do |record|
    fts_record = FtsIssue.find_or_initialize_by(issue_id: record.id)
    fts_record.subject     = record.subject
    fts_record.description = record.description
    fts_record.save!
  end
end
```


全文検索時は全文検索用のテーブルを`JOIN`して`MATCH AGAINST`を使います。

```ruby
issue.
  joins(:fts_issue).
  where(["MATCH(fts_issues.subject, " +
               "fts_issues.description) " +
          "AGAINST (? IN BOOLEAN MODE)",
         # ↓デフォルトANDで全文検索
         "*D+ #{keywords.join(', ')}"])
```


この説明はわかりやすさのために[実際の実装](https://github.com/okkez/redmine_full_text_search)を単純化しています。詳細が知りたい方は実装を確認してください。

#### PGroongaを導入する方法

PGroongaはトランザクションに対応しているので別途全文検索用のテーブルを作成する必要はありません。既存のテーブルに全文検索用のインデックスを作成します。

マイグレーションファイルでいうと次のようにします。ここでは`issues`テーブルに全文検索用のインデックスを作成しています。`enable_extension("pgroonga")`はPGroongaを使えるようにするためのSQLです。

```ruby
def up
  enable_extension("pgroonga")
  add_index(:issues,
            [:id, :subject, :description],
            using: "pgroonga")
end
```


あとは検索時に全文検索条件をつけるだけです。

```ruby
issue.
  # 検索対象のカラムごとに
  # クエリーを指定
  where(["subject @@ ? OR " +
         "description @@ ?",
         keywords.join(", "),
         keywords.join(", ")])
```


この説明もわかりやすさのために[実際の実装](https://github.com/okkez/redmine_full_text_search)を単純化しています。詳細が知りたい方は実装を確認してください。

### Zulipへの導入方法

[Zulip](https://zulip.org/)というチャットツールへのPGroongaの導入方法を説明します。ZulipはPostgreSQLを使っているので、導入するのはPGroongaだけです。ZulipはDjangoを使っているのでDjangoを使っているアプリケーションに導入する例ということになります。

Zulipは上部の検索ボックスから全文検索できます。ここから全文検索したときにPGroongaを使うようにします。

![Zulipの検索ボックス]({{ "/images/blog/20160929_1.png" | relative_url }} "Zulipの検索ボックス")

Zulipはチャットツールです。チャットツールなので小さなテキストの書き込みが頻繁に発生する傾向があります。各書き込みは十分速く完了する必要があります。書き込みが遅いとユーザーの不満が溜まりやすいからです。

Zulipは書き込みをできるだけ速くするためにインデックスの更新を遅延させています。インデックスの更新はデータの追加よりも重い処理なので、その処理を後回しにしているということです。（PGroongaは検索だけでなく更新も速いので遅延させずにリアルタイムで更新しても十分速いかもしれません。アプリケーションの要件次第でどのような実装にするか検討する必要があります。）

Zulipは、インデックスの更新を遅延させるため、カラムの値を直接全文検索対象にせずに、別途全文検索用のカラム（`zulip_message.search_pgroonga`カラム）を用意しています。その全文検索用のカラムの更新を後回しにすることでインデックスの更新を遅延させています。

マイグレーションファイルでいうと次のようにします。最初の`ALTER ROLE`はPGroongaが提供する`@@`という全文検索用の演算子の優先順位を調整するためのものです。本質ではないのでここでは気にしなくて構いません。

```python
migrations.RunSQL("""
ALTER ROLE zulip SET search_path
  TO zulip,public,pgroonga,pg_catalog;
ALTER TABLE zerver_message
  ADD COLUMN search_pgroonga text;
UPDATE zerver_message SET search_pgroonga =
  subject || ' ' || rendered_content;
CREATE INDEX pgrn_index ON zerver_message
  USING pgroonga(search_pgroonga);
""", "...")
```


全文検索対象のカラム（`zerver_message.subject`カラムと`zerver_message.rendered_content`カラム）が更新されたらそのレコードのIDをログテーブル（`fts_update_log`テーブル）に追加します。Zulipは次のトリガーでこれを実現しています。

```sql
CREATE FUNCTION append_to_fts_update_log()
  RETURNS trigger
  LANGUAGE plpgsql AS $$
    BEGIN
      INSERT INTO fts_update_log (message_id) VALUES (NEW.id);
      RETURN NEW;
    END
  $$;
CREATE TRIGGER update_fts_index_async
  BEFORE INSERT OR UPDATE OF
    subject, rendered_content ON zerver_message
  FOR EACH ROW
    EXECUTE PROCEDURE append_to_fts_update_log();
```


全文検索対象のカラムのインデックスは別プロセスで更新します。別プロセスで更新するためには、全文検索対象のカラムが更新されたことをその別プロセスが知らなければいけません。これを実現するためにはポーリングする方法と更新した側から通知を受け取る方法があります。PostgreSQLには[`LISTEN`](https://www.postgresql.jp/document/current/html/sql-listen.html)/[`NOTIFY`](https://www.postgresql.jp/document/current/html/sql-notify.html)という通知の仕組みがあるので、Zulipはこれらを利用して通知を受け取る方法を実現しています。

更新した側は次のトリガーで更新したことを（`fts_update_log`チャネルに）通知します。このトリガーはログテーブル（`fts_update_log`テーブル）にレコードが追加されたら呼ばれるようになっているので、レコードが追加されるごとに通知しているということです。

```sql
CREATE FUNCTION do_notify_fts_update_log()
  RETURNS trigger
  LANGUAGE plpgsql AS $$
    BEGIN
      NOTIFY fts_update_log;
      RETURN NEW;
    END
  $$;
CREATE TRIGGER fts_update_log_notify
  AFTER INSERT ON fts_update_log
  FOR EACH STATEMENT
    EXECUTE PROCEDURE do_notify_fts_update_log();
```


通知を受け取るプロセスはPythonで実装されています。単純化すると次のようになっています。（詳細は[puppet/zulip/files/postgresql/process_fts_updates](https://github.com/zulip/zulip/blob/master/puppet/zulip/files/postgresql/process_fts_updates)を参照。）（`fts_update_log`チャネルに）通知がきたら全文検索用カラムを更新する（`update_fts_columns(cursor)`を実行する）ということを繰り返しています。

```sql
  import psycopg2
  conn = psycopg2.connect("user=zulip")
  cursor = conn.cursor
  cursor.execute("LISTEN fts_update_log;")
  while True:
      if select.select([conn], [], [], 30) != ([], [], []):
          conn.poll()
          while conn.notifies:
              conn.notifies.pop()
              update_fts_columns(cursor)
```


全文検索用カラムの更新（`update_fts_columns`の実装）は次のようになっています。ログテーブル（`fts_update_log`テーブル）から更新されたレコードのIDを取得してきて各レコードごとに全文検索用カラムを更新しています。最後に処理したレコードのIDをログテーブルから削除します。

```sql
def update_fts_columns(cursor):
    cursor.execute("SELECT id, message_id FROM fts_update_log;")
    ids = []
    for (id, message_id) in cursor.fetchall():
        cursor.execute("UPDATE zerver_message SET "
                       "search_pgroonga = "
                       "subject || ' ' || rendered_content "
                       "WHERE id = %s", (message_id,))
        ids.append(id)
    cursor.execute("DELETE FROM fts_update_log "
                   "WHERE id = ANY(%s)", (ids,))
```


このようにしてインデックスの更新を遅延し、書き込み時の処理時間を短くしています。書き込み時のレスポンスが大事なチャットツールならではの工夫です。

インデックスが更新できたらあとは全文検索するだけです。全文検索は次のように`WHERE search_pgroonga @@ 'クエリー'`を追加するだけです。

```sql
from sqlalchemy.sql import column
def _by_search_pgroonga(self, query, operand):
    # WHERE search_pgroonga @@ 'クエリー'
    target = column("search_pgroonga")
    condition = target.op("@@")(operand)
    return query.where(condition)
```


全文検索してヒットしたキーワードがどこにあるかを見つけやすくするために、Zulipはキーワードハイライト機能を実現しています。以下は「problem」というキーワードをハイライトしている様子です。

![Zulipのキーワードハイライト機能]({{ "/images/blog/20160929_2.png" | relative_url }} "Zulipのキーワードハイライト機能")

PostgreSQLには標準で[`ts_headline()`関数](https://www.postgresql.jp/document/current/html/textsearch-controls.html#textsearch-headline)というキーワードハイライト機能がありますが、ZulipのようにHTMLで結果を取得したい場合には使えません。これは`ts_headline()`関数はHTMLエスケープ機能を提供していないからです。HTMLエスケープ機能がないと次のように不正なHTMLができあがってしまいます。

```sql
SELECT  ts_headline('english',
                    'PostgreSQL <is> great!',
                    to_tsquery('PostgreSQL'),
                    'HighlightAll=TRUE');
--           ts_headline          
-- -------------------------------
--  <b>PostgreSQL</b> <is> great!
-- (1 row)  不正なHTML↑
```


そのため、ZulipではPostgreSQLにキーワード出現位置を返す関数を追加して、Zulip側でキーワードハイライト機能を実現しています。PGroongaを使っている場合は[`pgroonga.match_positions_byte()`関数](https://pgroonga.github.io/reference/functions/pgroonga-match-positions-byte.html)と[`pgroonga.query_extract_keywords()`関数](https://pgroonga.github.io/reference/functions/pgroonga-query-extract-keywords.html)を利用してこの機能を実現しています。

なお、PGroongaはHTMLエスケープ機能付きのハイライト関数[`pgroonga.highlight_html()`](https://pgroonga.github.io/reference/functions/pgroonga-highlight-html.html)を提供しているため、Zulipのようにアプリケーション側でハイライト機能の一部を実装する必要はありません。Zulipではすでに実装されていたためPGroongaを使った場合でも`pgroonga.highlight_html()`関数を使わずにハイライト機能を実現しています。

この説明はわかりやすさのために[実際の実装](https://github.com/zulip/zulip/pull/700)を単純化しています。詳細が知りたい方は実装を確認してください。

### まとめ

[MySQLとPostgreSQLと日本語全文検索3](https://groonga.doorkeeper.jp/events/50541)で、実例をもとにMroonga・PGroongaの導入方法を紹介しました。たとえMySQL・PostgreSQLレベルで日本語全文検索できても、実際にアプリケーションで使えるようにならないとユーザーに日本語全文検索を提供できません。そのため、このような導入方法の紹介にしました。

アプリケーションごとになにを大事にするかは変わるので、この事例をそのまま適用できるわけではありませんが、Mroonga・PGroongaを導入する際には参考にしてください。
