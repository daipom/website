---
tags:
- fluentd
title: fluent-plugins-nurseryのご紹介
---
クリアコードでは[Fluentd](https://github.com/fluent/fluentd)の開発に参加しています。
[以前の記事]({% post_url 2016-08-29-index %})でも紹介した通り多くのプラグインの開発にも参加しています。
<!--more-->


Fluentd v0.14系がリリースされたので、最近はプラグインをFluentdのv0.14 APIに対応させるようなプルリクエストを送っています。

移行手順については[公式のドキュメント](http://docs.fluentd.org/articles/plugin-development#fluentd-version-and-plugin-api)に説明があり、ククログにもいくつか[記事](/blog/category/fluentd.html)があります。なので、やろうと思えばすぐにFluentd v0.14 APIに移行することができます。

Fluentdには600以上のプラグインがありますが、中にはメンテナンスが活発でない[^0]プラグインもあります。
今のところ足りないドキュメントがあったり、新APIに移行した事例が少なかったりして、新APIへの移行をためらっているプラグイン作者もいると思われます。

そこでFluentdのプラグインを安定してメンテナンスできる体制を構築し、Fluentdのプラグインを新しいAPIに対応する活動を活発にするために[fluent-plugins-nursery](https://github.com/fluent-plugins-nursery)というGitHub Organizationを作りました[^1]。

### fluent-plugins-nurseryの目的

Fluentdのプラグインのために、安定したメンテナンス体制を提供することが目的です。

元のプラグイン作者が、何らかの理由でメンテナンスできない状態になったときに、メンテンスを引き取ることができる場所を提供します。

### fluent-plugins-nurseryへ参加することのメリット・デメリット

  * :+1: 元のプラグイン作者が何もしなくてもIssueやPull requestに対応する

  * :+1: 元のプラグイン作者が何もしなくてもFluentdの新機能に対応する

  * :+1: 元のプラグイン作者が何もしなくてもRubyやTravis CIの設定が最新のものに対応する

  * :+1: 差分付きコミットメールがメーリングリストに飛ぶ [^2]

  * :-1: fluent-plugins-nurseryチームが苦手な分野[^3]については、対応が遅れる可能性がある

### FAQ

  * :question: 乗っ取りなの？

  * :bulb: いいえ。元のプラグイン作者が望まない限りfluent-plugins-nurseryに権限が移行されることはありません。

  * :question: 元のプラグイン作者はメンテナンスもリリースもできなくなるの？

  * :bulb: いいえ。元のプラグイン作者は自身が望めば、権限を維持することができます。完全に引き継いで手を引くこともできます。

  * :question: 元のプラグイン作者が行方不明です！どうしましょう！？

  * :bulb: 落ち着いてください。インターネット上で見つかるはずなので探してみましょう。もし見つけられなかったら相談してください。

  * :question: 私、プラグイン作者なんですが、Fluentdの新機能に対応したいです！でも、どうしたらいいかわかりません。

  * :bulb: [Fluentdのコミュニティ](http://www.fluentd.org/community)で相談してください。

  * :question: Issueをたてる前に相談したい。

  * :bulb: [fluent-plugins-nusery/Lobby](https://gitter.im/fluent-plugins-nursery/Lobby) で細かい相談ができます。

### fluent-plugins-nursery への参加方法

プラグイン作者がやることは少ないです。以下の簡単なステップを実行するだけで、最新のFluentdに対応する作業、issueやPull requestへの対応をfluent-plugins-nurseryのメンバーがベストエフォートで行います。
詳細は[fluent-plugins-nursery/contact](https://github.com/fluent-plugins-nursery/contact)を確認してください。

  1. プラグイン作者が[fluent-plugins-nursery/contactにissueを作る](https://github.com/fluent-plugins-nursery/contact/issues/new)

  1. fluent-plugins-nurseryの管理者が `accepted` ラベルを付ける

  1. プラグイン作者がTransfer ownershipする

  1. プラグイン作者がGemのownerを追加する

  1. fluent-plugins-nurseryの管理者が他の管理者もGemのリリースができるようにする

### まとめ

Fluentdのプラグインのメンテナンスに使う時間が足りないなー、新機能に対応したいけど難しそうだなーと思っているプラグイン作者はぜひfluent-plugins-nurseryまでご連絡ください。
もし、身の回りのプラグイン作者が、メンテナンスに困っている様子だったらfluent-pluguins-nurseryを紹介してあげてください。

[^0]: 小さなプルリクエストを送っても半年以上マージしてもらえない

[^1]: <a href=https://github.com/rust-lang-nursery>rust-lang-nursery</a>を参考にしました

[^2]: See http://www.commit-email.info/

[^3]: マイナーなウェブサービスや使うのにものすごく手間がかかるもの
