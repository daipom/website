---
tags:
  - apache-arrow
  - ruby
title: '名古屋Ruby会議03：Apache ArrowのRubyバインディング（1） #nagoyark03'
---
2017年2月11日に[名古屋Ruby会議03](http://regional.rubykaigi.org/nagoya03/)が開催されます。そこで「Apache ArrowのRubyバインディングをGObject Introspectionで」という話をする予定です。
<!--more-->


名古屋Ruby会議03とApache ArrowとGObject IntrospectionとRubyバインディングの宣伝も兼ねて関連情報をまとめていきます。

まずは、[Apache Arrow](http://arrow.apache.org/)について簡単に紹介します。

### Apache Arrowとは

Apache Arrowはインメモリーで高速にデータ分析をするための[データフォーマットの仕様](https://github.com/apache/arrow/tree/master/format)（たぶん）とその実装です。

Apache Arrowが開発されている目的は高速にデータ分析をすることです。そのために以下のことを大事にしています。

  * データ分析プロダクト間でのデータ交換コストを下げる

  * 複数のCPUコアを使って高速にデータ分析処理をできる

それぞれ少し補足します。

まず、データ交換コストについてです。

現在、データ分析用のプロダクトには[Apache HBase](https://hbase.apache.org/)や[Apache Spark](http://spark.apache.org/)や[Pandas](http://pandas.pydata.org/)などがあり、それらが協調してデータ分析をしています。協調するためには、それらのプロダクト間で分析対象のデータを交換する必要があります。現在は各プロダクトでそれぞれ別のデータフォーマットを使っているので、交換するときにはフォーマットを変換する必要があります。

この状況の課題は次の通りです。

  * 変換コストがムダ（Apache Arrowのサイトには70-80%のCPUが変換のために使われていると書いている）

  * それぞれのフォーマット毎に似たような処理が実装されている

Apache Arrowのように各プロダクトで共通で使えるデータフォーマットがあると変換は必要なくなりますし、そのデータに対する処理も同じ実装を共有できます。これがApache Arrowが解決しようとしているやり方です。Apache Arrowのサイトの「Advantages of a Common Data Layer」のところにこの状況のイメージがあるので、ピンとこない人はApache Arrowのサイトも見てみてください。

次に、複数のCPUコアの話です。

現在は1つのマシンで複数のCPUコアを利用できることは当たり前です。同時に複数のCPUコアを有効活用できれば処理速度を向上させることができ、より速くより多くのデータ分析処理を実現できます。

Apache Arrowはカラムベースのデータフォーマットを活用することによりこれを実現します。Apache Arrowのデータフォーマットを各プロダクトで共通に使えれば、この高速な処理も各プロダクトで共有できます。これがApache Arrowが実現しようとしていることです。これについてもApache Arrowのサイトの「Performance Advantage of Columnar In-Memory」のところにイメージがあるので、参照してください。

なお、Apache Arrowに賛同しているデータ分析プロダクトは現時点で13個あります。各プロダクトがApache Arrowを使う未来がきそうな気がしますね。プロダクトの詳細はApache Arrowのサイトを確認してください。

### まとめ

名古屋Ruby会議03で話す内容の関連情報をまとめはじめました。

今回はApache Arrowの話だけでRubyのことは全然でてきませんでした。次はなぜApache ArrowのRubyバインディングがあるとよさそうなのかについて説明します。
