---
tags:
  - apache-arrow
title: 2017年4月12日頃のApache Arrow
---
2017年4月12日頃のApache Arrowの様子を紹介します。
<!--more-->


### `arrow::Tensor`の追加

0.2.0頃のApache Arrowは`arrow::Array`で1次元のデータ（配列）、`arrow::Table`で2次元のデータ（表）を表現していました。最近のApache Arrowはこれらに加えて`arrow::Tensor`を追加しました。これはN次元のデータを表現します。

`arrow::Tensor`は以下と同じようなデータを表現します。

  * [NumPyの`ndarray`](https://docs.scipy.org/doc/numpy/reference/generated/numpy.ndarray.html)

  * [Torch](http://torch.ch/)の[`Tensor`](https://github.com/torch/torch7/blob/master/doc/tensor.md)（TourchはFacebookやTwitterなどが利用している科学計算フレームワークで機械学習もできる。Luaを使う。）

  * [TensorFlow](https://www.tensorflow.org/versions/master/api_docs/python/tf/Tensor)の[`Tensor`](https://www.tensorflow.org/versions/master/api_docs/python/tf/Tensor)

Apache Arrowはシステム間でのデータ交換のコストを下げることを重視しています。つまり、最近のApache Arrowは`arrow::Tensor`で表現するようなデータのデータ交換コストも下げる取り組みを始めた、ということです。

現時点の`arrow::Tensor`はゼロコピーでのデシリアライズに対応しています。[Ray](http://ray.readthedocs.io/en/latest/)という分散タスク実行エンジンは[NumPyのデータをシリアライズするためにApache Arrowを使うようにしました](https://github.com/ray-project/ray/pull/436)。

今後は[`arrow::Tensor`のデータに対して数学関数を使えるようにする予定](https://www.slideshare.net/wesm/memory-interoperability-in-analytics-and-machine-learning#23)です。要素毎（element-wise）の演算だけでなく行列演算もサポートするかどうかはまだわかりません。

### サブライブラリーの統合

0.2.0までのApache Arrowはlibarrowとlibarrow_io（入出力用）とlibarrow_ipc（シリアライズ・デシリアライズ用）というライブラリーに分かれていましたが、libarrowに統合されました。Apache Arrowを使う場合は全部使うことが多いので、これでシンプルに使えるようになりました。

### まとめ

2017年4月12日頃のApache Arrowの様子を紹介しました。そろそろ0.3.0がでそうなのですが、`arrow::Tensor`は0.3.0の目玉になりそうです。
