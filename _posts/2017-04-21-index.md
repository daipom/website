---
tags: []
title: Firefoxのアドオンが認識されなくて困った時の確認方法
---
### はじめに

Firefoxのアドオンを個人でインストールする場合、Firefoxのアドオンマネージャを経由するか、[Add-ons](https://addons.mozilla.org/ja/firefox/)サイトからインストールすることでしょう。
<!--more-->


個人利用の場合にはそれでよいのですが、Firefoxを企業内に導入する場合には自由にアドオンをインストールさせないというポリシーを適用することがあります。
その場合、事前に許可したアドオンだけは使えるようにしたいという要件が必須となることがあります。

今回はFirefoxの企業内利用で事前に許可したアドオンだけは使えるようにしたいという要件を満たそうとしたものの、アドオンが認識されなくて困った場合の確認方法について紹介します。

### Firefoxがアドオンを認識する場所

そもそも、アドオンはどこに配置することになっているのでしょうか。
アドオンがインストールされた状態としてFirefoxに認識されるパターンはいくつかあります。
Windowsの場合だと例えば以下です。

  * `C:\Program Files (x86)\Mozilla Firefox\browser\extensions`

  * `C:\Program Files (x86)\Mozilla Firefox\distribution\extensions`

  * `%AppData%\Mozilla\Profiles\(プロファイル)\extensions` (個人利用の場合はたいていこのケース)

基本は上記の3箇所です。企業利用において、 `browser\extensions` に配置するか `distribution\extensions` に配置するかはポリシーによって変わります。
通常は `browser\extensions` です。Firefox起動時にユーザープロファイルへとインストールさせたい場合にのみ `distribution\extensions` に配置します。

### アドオンを配置したものの認識されない？

例えば、Add-onsのサイトからアドオンファイルのみダウンロードして、企業内利用のために配置する場合を考えてみましょう。
最新のアドオンはAdd-onsのサイトの「Firefoxへ追加」ボタンのリンクをたどることで個別にダウンロードすることが可能です。

一定期間後に履歴を削除するためのアドオン「Expire history by days」なら以下のリンクから入手可能です。

```text
https://addons.mozilla.org/firefox/downloads/latest/expire-history-by-days/addon-331631-latest.xpi
```


しかし、xpiをそのまま `browser\extensions` へと配置してもFirefoxは認識しません。

実は、アドオンがFirefoxに認識されるかはある特定のファイル名になっているかによって決まります。
この決まりとは、アドオンのファイル名は `em:id` と呼ばれる値にならって命名するというものです。

では `em:id` は何を確認すればいいかというと、アドオンに含まれる `install.rdf` の中身をみるとわかります。

「Expire history by days」の場合、`em:id` は `em:id="expire-history-by-days@bonardo.net"` となっています。
そのため、ダウンロードした `addon-331631-latest.xpi` を `expire-history-by-days@bonardo.net` にリネームして `browser\extensions` 以下に配置するとFirefoxにアドオンとして認識されるようになります。

### まとめ

今回は、企業内利用で事前に許可したアドオンだけを使えるようにしようとして、アドオンがうまく認識されずに困った場合の確認方法を紹介しました。もしアドオンを所定の場所へと配置したのに認識されない場合には、ファイル名がアドオンの `em:id` と一致しているか確認してみてください。
