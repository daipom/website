---
tags:
- groonga
- presentation
title: 'MySQL・PostgreSQL上で動かす全文検索エンジン「Groonga」セミナー - Mroonga・PGroongaを使った全文検索システムの実装方法
  #groonga #mysql #mariadb #postgresql'
---
[cairo](https://cairographics.org/)に[色付きフォントサポートが入った](https://lists.cairographics.org/archives/cairo-commit/2017-July/013034.html)ので、今回のスライドでは[Noto Color Emoji](https://www.google.com/get/noto/help/emoji/)を使ってRabbitで色付きの絵文字を表示した須藤です。
<!--more-->


2017年8月1日に[MySQL・PostgreSQL上で動かす全文検索エンジン「Groonga」セミナー](https://groonga.doorkeeper.jp/events/62741)を開催しました。今回は業務でGroonga（Mroonga・PGroonga）を使いたい人向けのイベントだったので平日の午後に開催しました。

<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kou/groonga-seminar-2017-08/viewer.html"
          width="640" height="524"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kou/groonga-seminar-2017-08/" title="MySQL・PostgreSQL上で動かす全文検索エンジン「Groonga」セミナー">MySQL・PostgreSQL上で動かす全文検索エンジン「Groonga」セミナー</a>
  </div>
</div>


関連リンク：

  * [スライド（Rabbit Slide Show）](https://slide.rabbit-shocker.org/authors/kou/groonga-seminar-2017-08/)

  * [スライド（SlideShare）](https://www.slideshare.net/kou/groongaseminar201708)

  * [リポジトリー](https://github.com/kou/rabbit-slide-kou-groonga-seminar-2017-08)

### 内容

これまでのMroonga・PGroongaの紹介資料では、速さや機能を紹介するまとめ方が多かったのですが、今回の資料は「イマドキの全文検索システムを作るにはこんなSQLを使えばよい」というまとめ方にしました。逆引きレシピのようなまとめ方です。

話の流れは次のようになっています。全文検索システムを作る機会がある人は参考になるはずです。

  * イマドキの全文検索システムとは？

  * ミドルウェアの選び方は？判断基準は？

    * 全文検索サーバーを使う？

    * RDBMSだけでがんばる？

    * RDBMSに全文検索エンジンを組み込む？

  * イマドキの全文検索システムに必要な以下の機能をMroonga・PGroongaで実装するには？

    * 全文検索機能

    * キーワードハイライト機能

    * 周辺テキスト表示機能

    * 入力補完機能（今回はPGroongaのみ）

    * 同義語展開機能（今回はPGroongaのみ）

    * 関連文書の表示機能

  * 構造化された文書を検索対象にするには？

    * 構造化された文書の例：オフィス文書（Word・Excel・PowerPoint・LibreOfficeなどで作成した文書）

    * 構造化された文書の例：HTML（ヘッダー・フッター・サイドバーのテキストを取り除くなど、単に`body.innertText`の値を取得するだけではノイズが増えるので、いろいろケアが必要）

    * 構造化された文書の例：PDF

    * HTTPでテキスト抽出・スクリーンショット作成できる[ChupaText](https://github.com/ranguba/chupa-text)の紹介

    * Dockerで使う：[chupa-text-docker](https://github.com/ranguba/chupa-text-docker)

    * Vagrantで使う：[chupa-text-vagrant](https://github.com/ranguba/chupa-text-vagrant)

### まとめ

Mroonga・PGroongaでイマドキの全文検索システムを作る方法を紹介するセミナーを開催しました。クリアコードは全文検索システムの受託開発・設計支援・開発支援・障害調査支援など全文検索関係のもろもろをサポートするサービスを提供しています。とりあえず話を聞いて欲しいというレベルからでも結構ですので困っていることがあれば[お問い合わせ](/contact/?type=groonga)ください。
