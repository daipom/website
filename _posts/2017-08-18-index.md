---
tags:
- company
title: "「クリアコードをいい感じにする人」の採用を開始"
---
代表取締役の須藤です。コードを書いたり、社外向けの活動をしたり、クリアコードをいい感じにしたりしています。このうち、「クリアコードをいい感じにする」活動を主な業務とする人の採用活動を始めました。職種は「クリアコードをいい感じにする人」です。このような活動をする人が世間でどのような職種として呼ばれているのかわかっていないのでこんな職種名になっています。
<!--more-->


クリアコードは以下の2つの両立を大事にしています。

  * お金を稼ぐこと（フリーソフトウェアを「継続的に」推進していくために必要だから）

  * フリーソフトウェアの推進

これを「継続的に」今よりももっとうまくやれる私たちになりたいと思っています。「クリアコードをいい感じにする」というのはこれを実現するための活動です。

たとえば、今は次のような活動をしています。

  * 1ヶ月に1度クリアコードメンバー全員から個別に最近の話を聞く

    * どうやったら「もっといい感じ」になりそうかの情報集め

    * 困っていることがあったら解決に協力

  * クリアコードメンバーから集めた情報を元に「もっといい感じ」になることにつながりそうな活動の考案

  * 問題解決を練習する場の用意

    * 問題を1つ設定し、決められた期間内に解決案を出し、実際にそれを実施し、問題を解決する

    * 「お金を稼ぐこと」と「フリーソフトウェアの推進」をいい感じに両立するための練習

    * お客さんはこれらの両立が大事ではないことが多いので、お客さんの要望をそのまま実現するだけでは両立が難しい。そのため、お客さんの本当の問題を理解し、クリアコードが大事なことも両立もするしお客さんもいい感じになる解決案を提案し、それを実施することで両立をがんばっている。それをよりいい感じにできるようになるための練習。

  * 他にもいろいろ…

今抱えている課題は、これらの「クリアコードをいい感じにする」活動に継続的に十分な時間を確保できていないことです。今はこのような活動をしていますが、今後も同じことをやっていればよいというものではありません。私たち自身が変わっていくからです。私たちに合わせて活動内容も変わっていくはずです。そのようなことに必要な時間を確保できていません。

そこで「クリアコードをいい感じにする」活動を主な業務とする人の採用を始めました。「クリアコードをいい感じにする」活動は継続的に取り組むべき重要な活動だと考えているからです。

採用情報は[「クリアコードをいい感じにする人」の採用情報](/recruitment/fine-tuner.html)にまとめました。私たちはどういう人が「クリアコードをいい感じにする」活動をするのに向いている人なのかまだわかっていません。採用情報にも「わかっていないことがいろいろあるので一緒に考えながらうまくできるようになりましょう」と書いてあります。

そのような職種ですが、興味のある方はぜひ以下のような内容のメールを私に送ってください。一度お話しましょう！

```text
To: kou@clear-code.com
Subject: 会社説明会参加希望

名前: ○○××
職種：「クリアコードをいい感じにする人」
備考: （希望日時などがあれば）
```
