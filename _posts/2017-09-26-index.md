---
tags:
- redmine
title: Redmineのベンチマーク環境構築を自動化する方法
---
GCP上にインスタンスを立ち上げて、簡単に[redmine_full_text_search](https://github.com/okkez/redmine_full_text_search)を試すことができる環境を構築できるようにしました。
<!--more-->


成果物は[リポジトリ](https://github.com/okkez/redmine_full_text_search-benchmark)にまとまっています[^0]。

[Redmineでもっと高速に全文検索する方法]({% post_url 2017-09-25-index %})で使用したベンチマーク環境を構築するためのスクリプトです。
今後もredmine_full_text_searchをバージョンアップしたとき、性能の変化を簡単に調べれられるように作りました。

### 使い方

[Terraform](https://www.terraform.io/)と[Ansible](https://www.ansible.com/)を入れてコマンドを叩くだけです。

コマンドを実行する前に、[APIキーを作成](https://cloud.google.com/speech/docs/common/auth?hl=ja#creating_an_api_key)しておく必要があります。

```
$ git clone https://github.com/okkez/redmine_full_text_search-benchmark.git
$ cd redmine_full_text_search-benchmark
$ terraform init Terraform/
$ terraform apply Terraform/
$ ./generate-hosts.rb
```


このまま ansible を実行すると、SSHのホスト鍵のチェックが実行されて、対話的な操作が必要になるので対話的な操作が不要になるように準備します。

```
$ for h in fluentd redmine-mariadb redmine-mroonga redmine-postgresql redmine-pgroonga; do \
    gcloud compute --project "YOUR PROJECT" ssh --zone "YOUR ZONE" "fluentd" --command "echo"; done
```


また、ansible が使うUserKnownHostsFileにgcloudコマンドが生成するファイルを指定します。
ansible/ansible.cfg に以下の内容でファイルを作成しておきます。

```
[ssh_connection]
ssh_args = -o ControlMaster=auto -o ControlPersist=60s -o -o UserKnownHostsFile=~/.ssh/google_compute_known_hosts
```


**注意** DBのダンプファイルはリポジトリに含めていないので、各自DBのダンプファイルを用意してください。

  * `ansible/roles/mariadb/files/benchmark-mariadb.dump.sql.xz`

  * `ansible/roles/postgresql/files/benchmark-postgresql.dump.sql.xz`

最後に ansible-playbook コマンドを実行します。

```
$ cd ansible
$ ansible-playbook -i hosts playbook.yml
```


全部で20分もあれば、環境構築が終わります。

### 今後やりたいこと

今はベンチマーク用なので内部ネットワークからしかアクセスできないようにしてありますが、httpとhttpsは外部からアクセスできるようにして手元のブラウザからでも簡単に動作を確認できるようにしたいです。
また、ベンチマーク用のデータもどこかにアップロードしたものを簡単に使えるようにしたいです。

測定結果をFluentd経由でPostgreSQLに溜めてあるのですが、これをいい感じに可視化したいです。

[^0]: DBのダンプファイルはxzで圧縮してもサイズが大きいのでリポジトリに含めていません
