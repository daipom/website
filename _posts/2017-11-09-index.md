---
tags:
- mozilla
- feedback
- embedded
title: Gecko Embeddedプロジェクト 11月時点のステータス
---
### はじめに

[2017年7月6日の記事]({% post_url 2017-07-06-index %})で紹介した通り、クリアコードは組み込みLinux向けにMozilla Firefoxを移植するプロジェクト[Gecko Embedded](https://github.com/webdino/gecko-embedded/wiki)を[WebDINO Japan（旧Mozilla Japan）](https://www.webdino.org/)様と共同で立ち上げ、開発を進めております。[Yocto](https://www.yoctoproject.org/)を使用してFirefoxをビルドしたりハードウェアクセラレーションを有効化する際のノウハウを蓄積して公開することで、同じ問題に悩む開発者の助けになることを目指しています。
<!--more-->


その後も継続的に改善を進めており、いくつか進展がありましたので、11月時点のステータスを紹介します。

### 現在のステータス

#### ターゲットボードの追加

7月時点では、Firefox ESR52のビルド及び動作は[Renesas RZ/G1E Starter Kit](https://elinux.org/RZ-G/Boards/SK-RZG1E)のみで確認していましたが、その後[iWave RainboW-G20D Q7](http://www.iwavejapan.co.jp/product/RENESASRZG1M_DEV.html)および
[Renesas R-Car Gen3](https://elinux.org/R-Car/Boards/Yocto-Gen3)でも同様に確認しています。ビルド方法は以下のページにまとめてあります。

  * [iWave RainboW-G20D Q7](https://github.com/webdino/meta-browser/wiki/Build-RainboW-G20D-Q7-Yocto-2.0)

  * [R-Car Gen3](https://github.com/webdino/meta-browser/wiki/Build-RCar-Gen3-Yocto2.1)

#### Wayland対応のバグ修正

Firefoxは正式にはWaylandをサポートしていませんが、Red HatのMartin Stransky氏が[Waylandへの移植作業](https://bugzilla.redhat.com/show_bug.cgi?id=1054334)を行っています。
Stransky氏のパッチはFirefoxの最新バージョンを対象としていますが、Gecko Embeddedプロジェクトではこのパッチを52ESRのツリーに移植した上で、安定化作業を進めています。
本プロジェクトで発見した問題や、作成したパッチはStransky氏に随時フィードバックしています。

最近では以下のような報告を行っています。

  * https://bugzilla.redhat.com/show_bug.cgi?id=1470031

  * https://bugzilla.redhat.com/show_bug.cgi?id=1484651

  * https://bugzilla.redhat.com/show_bug.cgi?id=1495147

  * https://bugzilla.redhat.com/show_bug.cgi?id=1505745

  * https://bugzilla.redhat.com/show_bug.cgi?id=1507369

#### WebRTC対応

Wayland版FirefoxではWebRTCでビデオチャットを行おうとするとクラッシュする問題が存在していましたが、この問題を修正し、WebRTCが利用できるようになりました。

#### WebGL対応

7月時点ではドライバに絡む問題を解決できていなかったためビルド時点でのWebGL有効化手段を提供していませんでしたが、その後問題を解決できたため、パッチを更新しYoctoレシピにビルドオプションを追加しています。

WebGLを有効化するには、ビルド時に以下の設定をYoctoのlocal.confに追加して下さい。

```
PACKAGECONFIG_append_pn-firefox = " webgl "
```


また、今回新たに対象ボードとして加えたR-Car Gen3は非常に強力なため、フルHDの解像度でもWebGLが快適に動作することを確認しています。

#### Canvas 2D Contextのアクセラレーション

WebGLと同様に、Canvas 2D Contextのアクセラレーションについてもドライバに絡む問題を解決できたため、Yoctoレシピにビルドオプションを追加しています。

アクセラレーションを有効化するには、ビルド時に以下の設定をYoctoのlocal.confに追加して下さい。

```
PACKAGECONFIG_append_pn-firefox = " canvas-gpu "
```


### まとめ

Gecko Embeddedプロジェクトの2017年11月現在のステータスについて紹介しました。
安定化や、さらなるパフォーマンス改善、他のSoCのサポートなど、やるべきことはまだまだ残されていますので、
興味がある方は協力して頂けるとありがたいです。問題を発見した場合は[Gecko EmbeddedプロジェクトのIssueページ](https://github.com/mozilla-japan/gecko-embedded/issues)に報告して下さい。
