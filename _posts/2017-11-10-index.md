---
tags: []
title: Thunderbirdのスレッドペインの既定のカラムを変更するには
---
### はじめに

Thunderbirdのスレッドペインにて、既定のカラム表示をあらかじめカスタマイズしておきたいという場合があります。
今回はそのための機能を提供するアドオンによるカスタマイズ方法を紹介します。
<!--more-->


### set-default-columnsとは

スレッドペインに表示する既定のカラムを変更する機能を提供するアドオンです。
このアドオンを使うと、受信トレイや送信トレイを開いたときの表示項目を一括で設定できます。

![Set Default Columnsアドオンページ]({{ "/images/blog/20171110_0.png" | relative_url }} "Set Default Columnsアドオンページ")

  * https://addons.mozilla.org/ja/thunderbird/addon/set-default-columns/

アドオンは上記のURLからインストール可能です。

ただし、スレッドペインの各フォルダごとに表示項目をカスタマイズすることはできません。例えば「受信トレイ」と「受信トレイ配下の特定のサブフォルダ」とで表示項目を変えるというようなカスタマイズはできないということです。

### set-default-columnsによるカスタマイズ例

Thunderbird 52.4.0をインストールした初期状態では、以下のカラムが既定で表示されます。

![既定のカラム表示]({{ "/images/blog/20171110_1.png" | relative_url }} "既定のカラム表示")

  * threadCol (スレッド)

  * flaggedCol (スター)

  * attachmentCol (添付)

  * subjectCol (件名)

  * unreadButtonColHeader (既読)

  * correspondentCol (通信相手)

  * junkStatusCol (迷惑マーク)

  * dateCol (送信日時)

これを実際にカスタマイズするには、アドオンをインストールした環境で次のような設定を設定ファイルへと反映します。
主に企業利用を想定して開発されているアドオンなので、どの設定ファイルに反映するべきかは[Firefox・Thunderbirdの組織内向けカスタマイズの方法の簡単な紹介と比較]({% post_url 2014-03-27-index %})を参考にするとよいでしょう。

```
lockPref("extensions.set-default-columns@clear-code.com.columns", [
  "attachmentCol",
  "subjectCol",
  "senderCol",
  "recipientCol",
  "dateCol",
].join(","));
```


上記は「添付」の有無、「件名」、「差出人」、「受信者」、「送信日時」の順に表示させるための設定例です。

ただし、Thunderbird 49から「差出人」や「受信者」のかわりに「通信相手」カラムを既定で表示することになったため、これだけでは、「senderCol」と「recipientCol」を既定で表示させることができません。既定のカラムの表示は以下のようになってしまいます。

![差出人が表示されない状態]({{ "/images/blog/20171110_2.png" | relative_url }} "差出人が表示されない状態")

そこで、追加で「通信相手」カラムを使用しないようにする設定を追加します。

```
lockPref("mail.threadpane.use_correspondents", false);
```


`mail.threadpane.use_correspondents` が `correspondentCol` を使うかどうかを決めるフラグです。初期値が `true` となっているのでこれを `false` に設定しなおします。

これにより「受信トレイ」に「差出人」カラムが、「送信済みトレイ」に「受信者」カラムが表示されるようになりました。

![カスタマイズ後の受信トレイ]({{ "/images/blog/20171110_3.png" | relative_url }} "カスタマイズ後の受信トレイ")
![カスタマイズ後の送信済みトレイ]({{ "/images/blog/20171110_4.png" | relative_url }} "カスタマイズ後の送信済みトレイ")

プリファレンス `mail.threadpane.use_correspondents` の値による挙動の変化をまとめると次のとおりとなります。

  * `mail.threadpane.use_correspondents=true`の場合(既定値)

    * カスタマイズで `senderCol` を指定していても、受信トレイに「差出人」カラムは既定のカラムとして表示されません

    * カスタマイズで `recipientCol` を指定していても、送信済みトレイに「受信者」カラムは既定のカラムとして表示されません

  * `mail.threadpane.use_correspondents=false`の場合

    * カスタマイズで `senderCol` を指定していれば、受信トレイでは「通信相手」カラムの代わりに「差出人」カラムを既定のカラムとして表示できます

    * カスタマイズで `recipientCol` を指定していれば、送信済みトレイでは「通信相手」カラムの代わりに「受信者」カラムを既定のカラムとして表示できます

### まとめ

今回はスレッドペインの既定のカラムを変更する機能を提供するアドオンである[set-default-columns](https://github.com/clear-code/set-default-columns)によるカスタマイズ方法を紹介しました。

FirefoxやThunderbirdの導入やカスタマイズでお困りで、自力での解決が難しいという場合には、[有償サポート窓口](/contact/)までぜひ一度ご相談下さい。
