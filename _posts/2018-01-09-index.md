---
tags: []
title: PC上で最新のYoctoを試す
---
### はじめに

現在クリアコードでは様々なSoCにGecko（Firefox）を移植するプロジェクト「[Gecko Embedded](https://github.com/webdino/gecko-embedded)」を進めています。
<!--more-->


対象としているOSは組み込み用のLinuxですが、最近のSoC向けに提供されているLinuxベースのBSP（Board Support Package）は[Yocto](https://www.yoctoproject.org/)レシピの形で提供されることが多くなりました。SoCベンダーから提供されるYoctoのバージョンは、ベンダー側でビルドや動作を保証するために、最新版ではなく、少し前のバージョンであることがほとんどです。

一方で、対象BSP上でアプリケーション開発を行っていると、最新版のYoctoを使用したくなる場面も発生します。

  * BSPに含まれるライブラリにバグや不足している機能があるため、最新版で解消されているか確認したい

  * Yoctoレシピの問題を修正して、今後同じ問題でメンテナンスコストが発生しようないようにするために、開発元の最新版にフィードバックしたい

しかし、開発対象のSoCで最新のYoctoを試すのは、移植が間に合っておらず困難なことが多いです。

対象としている問題は必ずしもSoC依存とは限らないため、この様な場面ではPC上で動作確認を行うのが手軽で便利でしょう。今回はその方法を紹介します。

### リファレンス

Yoctoの公式ドキュメントは以下にまとめられています。

  * https://www.yoctoproject.org/documentation

手っ取り早くPC上でビルドおよび動作を確認をする方法は「Yocto Project Quick Start」にまとめられています。最新のリリース版である2.4（Rocko）向けのドキュメントは以下になります。

  * http://www.yoctoproject.org/docs/2.4/yocto-project-qs/yocto-project-qs.html

本記事の内容は上記のドキュメントをベースとしていますので、正確な情報はそちらを参照して下さい。ただし、上記ドキュメントはリリース版を対象としているのに対し、本記事では開発版の最新を対象としますので、使用するブランチが異なります。

なお、Yoctoのバージョンは番号ではなくコードネームで識別されることも多いです。バージョン番号とコードネームの対応は以下のページで知ることができます。

  * https://wiki.yoctoproject.org/wiki/Releases

### ビルド手順

ここではUbuntu 16.04LTSを対象としてビルド手順を紹介します。

まず、ビルド環境に必要なツールをインストールします。

```console
$ sudo apt-get install gawk wget git-core diffstat unzip texinfo gcc-multilib \
build-essential chrpath socat cpio python python3 python3-pip python3-pexpect \
xz-utils debianutils iputils-ping libsdl1.2-dev xterm
```


次に、作業用ディレクトリを作成してYoctoレシピをダウンロードし、ビルド設定を行います。

```console
$ export BUILD_DIR=/path/to/build （パスは適切に置き換えて下さい）
$ mkdir $BUILD_DIR
$ cd $BUILD_DIR
$ git clone git://git.yoctoproject.org/poky
$ source poky/oe-init-build-env
（ここでカレントディレクトリが自動的に $BUILD_DIR/build になります）
```


必要に応じて$BUILD_DIR/build/confディレクトリ下の設定ファイルを修正しますが、デフォルトではx86用のビルドイメージが作成されるようにセットされています。今回はこのデフォルトのビルド設定を使用しますので、設定ファイルの修正は必要ありません。

ここまでのセットアップが済んだら、以下のコマンドでビルドを実行します。

```console
$ bitbake core-image-sato
```


以上でブートイメージを作成することができます。

なお、一度シェルを抜けると`source poky/oe-init-build-env`でセットされたビルド用の環境変数がリセットされてしまいますので、次にビルドする際には、事前に再度`source poky/oe-init-build-env`を実行する必要があります。

### 起動方法

以下のコマンドを実行すると、ビルドしたブートイメージをQEMUでブートすることができます。

```console
$ runqemu qemux86
```


![（runqemuスクリーンショット）]({{ "/images/blog/20180109_0.png" | relative_url }} "（runqemuスクリーンショット）")

### Firefoxのビルド

以上でOSの起動はできましたが、本記事はGecko Embeddedプロジェクトを対象としたものなので、Firefoxもビルドに加えてみましょう。Firefoxをビルドするためには、meta-openembeddedレイヤおよびmeta-browserレイヤを追加する必要があります。

```console
$ cd $BUILD_DIR
$ git clone git://git.openembedded.org/meta-openembedded
$ git clone https://github.com/OSSystems/meta-browser.git
$ cd build
```


また、`conf/bblayers.conf`および`conf/local.conf`にそれぞれ以下の行を追加します。

`$BUILD_DIR/build/conf/bblayers.conf`:

```
BBLAYERS += " \
  ${TOPDIR}/../meta-openembedded/meta-oe \
  ${TOPDIR}/../meta-browser"
```


`$BUILD_DIR/build/conf/local.conf`:

```
IMAGE_INSTALL_append = " firefox "
IMAGE_INSTALL_append = " source-han-sans-jp-fonts "
```


再度`bitbake core-image-sato`を実行することで、Firefoxをブートイメージに追加することができます。

なお、Firefoxはラウンチャーから起動することができますが、デフォルトでは`/etc/resolv.conf`が設定されていないため、名前解決を行うことができません（参考: [Bug 5322](https://bugzilla.yoctoproject.org/show_bug.cgi?id=5322)）。ホスト名でWebサイトを開きたい場合は、ブートイメージ内の`/etc/resolv.conf`に適切なDNSサーバを設定する必要があります。例えば以下のような行を追加すると良いでしょう。

```
nameserver 8.8.8.8
```


これで無事Webサイトを表示できるようになります。

![（Firefoxスクリーンショット）]({{ "/images/blog/20180109_1.png" | relative_url }} "（Firefoxスクリーンショット）")

### アップストリームへのフィードバック

上記の手順は最新の開発版を対象としているため、ときにはビルドが通らないこともありますが、フリーソフトウェアですので問題に気がついたときは開発元にフィードバックしましょう。

Yoctoへのフィードバック方法は下記のページを参照すると良いでしょう。

  * https://www.yoctoproject.org/tools-resources/community

meta-openembeddedやmeta-browserへのフィードバックは、基本的にOpenEmbeddedプロジェクトのメーリングリストに行います。

  * https://www.openembedded.org/wiki/Mailing_lists

meta-browserに関しては、GitHub上のissueやPull Requestでも受け付けてくれるようです。

  * https://github.com/OSSystems/meta-browser

### まとめ

PC上でYoctoの最新版を試す方法を紹介しました。特別な手順はほとんどありませんが、周りを見渡すと意外と活用されていないことに気がついたので、敢えて紹介してみました。

Gecko Embeddedプロジェクトではmeta-browserのFirefoxを最新のESR版に更新する作業も進めたいと考えていますが、開発リソースが足りず手が回っていないのが実情です。上記手順で開発環境を整えて、この作業を手伝って下さる方が現れることを期待しています。
