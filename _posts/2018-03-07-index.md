---
tags:
- fluentd
title: Fluent-plugin-elasticsearch v2.8.0対応でわかったElasticsearch 6.0以降の動向
---
### はじめに

[fluent-plugin-elasticsearch](https://github.com/uken/fluent-plugin-elasticsearch)はよく使われているプラグインの一つです。
このプラグインをメンテナンスするためには、Fluentdの知識だけでなく、Elasticsearchが今後どのようになっていくかも知っておく必要があります。
Elasticsearch 6.xで次のようなアナウンスがありました。
<!--more-->


  * [Removal of Mapping Types in Elasticsearch 6.0](https://www.elastic.co/blog/removal-of-mapping-types-elasticsearch)

要約すると、Elasticsearch 6.0ではインデックスに対して一つのタイプしか許可されなくなり、
Elasticsearch 7.xではインデックスに付与できるタイプは `_doc` のみになる、というものです。

### fluent-plugin-elasticsearch v2.8.xでの対策

fluent-plugin-elasticsearchのデフォルトの挙動は、 `fluentd` というインデックスに対して、 `fluentd` という `_type` を付加することになっています。
しかし、問題となるパラメーターがあります。
[`target_type_key`](https://github.com/uken/fluent-plugin-elasticsearch#target_type_key) というパラメーターを使用している際に、
レコードの中から該当するキーが存在する場合は `_type` に当てはめるという動作をしていました。
このパラメーターについては `deprecated` 警告を出すようにしました。
また、Elsaticsearch 7.xでは [Removal of Mapping Types in Elasticsearch 6.0](https://www.elastic.co/blog/removal-of-mapping-types-elasticsearch) によると、
`_type` フィールドには `_doc` というダミーしか許可されなくなるので、`type_name` パラメーターについても `_doc` 固定となる動作になるよう変更を加えました。
まだ `_doc` のような `_` 始まりのタイプを許可しない環境もあるため、v2.8.1では INFO レベルのログを出力する挙動に留めるようにしました。

### まとめ

fluent-plugin-elasticsearch v2.8.0 では、接続先のElasticsearchのバージョンによって挙動が変わるようになります。
接続先がElasticsearch 5.xでは今までの動作のまま、6.xでは [`target_type_key`](https://github.com/uken/fluent-plugin-elasticsearch#target_type_key) が動作しなくなり、
7.xでは [`target_type_key`](https://github.com/uken/fluent-plugin-elasticsearch#target_type_key) が動作しなくなるのに加えて、 `type_name` パラメーターが `_doc` 固定になります。
