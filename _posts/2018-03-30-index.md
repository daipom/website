---
tags:
- mozilla
- embedded
title: Gecko Embedded 次期ESR対応
---
### はじめに

これまでにも何度か紹介してきましたが、クリアコードではGecko（Firefox）を組み込み機器向けに移植する取り組みを行っています。
<!--more-->


  * [Gecko Embeddedプロジェクト]({% post_url 2017-07-06-index %})

  * [Gecko Embeddedプロジェクト 11月時点のステータス]({% post_url 2017-11-09-index %})

当プロジェクトでは移植コストを少しでも低減するために、Firefoxの延長サポート版（ESR）のみを対象としています。これまではESR45やESR52をベースにハードウェアアクセラレーションに対応させるための作業を行ってきましたが、現在は5月にリリース予定の次期ESR（ESR60）に対応するための作業を進めています。

今回は、この作業の現在のステータスと、そのビルド方法ついて紹介します。

### 現在のステータス

現在はFirefox 60のβ版をもとに開発を進めています。ビルドは通るようになり、ソフトウェアレンダリングでの描画もある程度安定して動作するようになっています。Firefox 60からはWayland対応のコードも本体に入っているため、ソフトウェアレンダリングであればほぼ無修正で動作させることができています。

![Gecko60 on RZ/G1M]({{ "/images/blog/20180330_0.png" | relative_url }} "Gecko60 on RZ/G1M")

ESR52で実現出来ていたハードウェア対応の移植作業については、2018年3月30日現在、以下のような状況です。

  * レイヤーアクセラレーション: 移植済み（検証中）

  * OpenMAX（H.264デコード）: 移植済み

  * WebGL: 動作せず（調査中）

  * WebRTC: レイヤーアクセラレーションとの組み合わせでは動作せず（調査中）

  * Canvas 2D Contextのアクセラレーション: e10sオフでは動作

また、ESR52ではなかった要素として、Rustで書かれたコードが追加されてきている点が挙げられます。細かく動作検証はできていませんが、少なくとも[Stylo（Quantum CSS）](https://wiki.mozilla.org/Quantum/Stylo)は実機（Renesas RZ/G1M）で動作することを確認できています。

### ビルド概要

#### 対象ハードウェア

現時点ではRenesas RZ/G1Mでのみ動作を確認しています。

#### ESR52のビルド手順からの変更点

ビルドの大まかな流れは[ESR52の場合](https://github.com/webdino/meta-browser/wiki/Build-RainboW-G20D-Q7-Yocto-2.0)とほぼ同様です。ただし、ESR52からの大きな変更点として、以下の手順が必要となります。

  * Rustの導入

    * [rustup](https://github.com/rust-lang-nursery/rustup.rs)を使用

    * clangおよびllvm 3.9以上も合わせて導入

  * GCCの差し替え

    * ホスト用GCC: 4.9以上

    * ターゲット用GCC: 4.9あるいは7以上

前者については、Yocto側でmeta-rustレイヤを追加する方法についても検証中です。
後者のターゲット用GCCについては、現時点では5.xや6.xでは動作するバイナリを作成できないことを確認していますので、4.9あるいは7以上を選択する必要があります。RZ/G1MのYoctoレシピにはGCC 4.9のレシピも含まれていますので、local.confへの設定の追加で対応することができます。

### ビルド用Vagrant Box

ビルド環境のセットアップを省力化するため、現在Vagrant Boxの準備を進めています。まだ荒削りではありますが、基本的なビルドは通るようになっているので、詳細手順を紹介する代わりにこのVagrant Boxを使った方法でのビルド手順を紹介します。

#### ホスト側実行手順:

まず、ホスト側で以下のソフトウェアの最新版をそれぞれインストールする必要があります。

  * [Vagrant](https://www.vagrantup.com/)

  * [Ansible](https://www.ansible.com/)

  * [VirtualBox](https://www.virtualbox.org/)

例えばホスト側OSがUbuntuの場合には、以下の手順でインストールすることができます。

Vagrantのインストール:

  * [Vagrantのダウンロードページ](https://www.vagrantup.com/downloads.html)から最新版のdebパッケージをダウンロード

  * dpkgコマンド等で上記debパッケージをインストール

    * 例: `sudo dpkg -i vagrant_2.0.2_x86_64.deb`

Ansibleのインストール:

```
$ sudo apt-get install software-properties-common
$ sudo apt-add-repository ppa:ansible/ansible
$ sudo apt-get update
$ sudo apt-get install ansible
```


[VirtualBoxのインストール](https://www.virtualbox.org/wiki/Linux_Downloads):

```
$ sudo apt-add-repository "deb http://download.virtualbox.org/virtualbox/debian $(lsb_release -sc) contrib"
$ wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
$ wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- | sudo apt-key add -
sudo apt-get update
sudo apt-get install virtualbox-5.2
```


ホスト側の準備が完了すれば、以下のコマンドでVagrant Boxを起動することができます。

```
$ vagrant plugin install vagrant-disksize
$ vagrant init ashie/renesas-gecko-dev
$ vagrant up
$ vagrant ssh (ゲスト環境にログイン)
```


#### ゲスト側実行手順:

Vagrant Boxを起動すると、[これまでのビルド手順](https://github.com/webdino/meta-browser/wiki/Build-RainboW-G20D-Q7-Yocto-2.0)のうち、Yoctoレシピのセットアップまでがほぼ完了している状態となっています。ただし、プロプライエタリドライバのダウンロードだけは手動で行う必要があります。ここでは、ホスト側で下記サイトからそれぞれアーカイブファイルをダウンロードし、

  * https://www.renesas.com/en-us/software/D6000548.html

  * https://www.renesas.com/en-us/software/D6000544.html

`vagrant init`を実行したディレクトリ下に置いてあるものと想定します。
この状態で、ゲスト側で以下のコマンドを実行して同アーカイブファイルを展開します。

```
$ cd ~/rzg1-bsp/
$ unzip /vagrant/RZG_Series_Evaluation_Software_Package_for_Linux-20161011.tar.gz.zip
$ unzip /vagrant/RZG_Series_Evaluation_Software_Package_of_Linux_Drivers-20161011.tar.gz.zip
$ cd ~/rzg1-bsp/meta-renesas/meta-rzg1
$ ./copy_gfx_software_rzg1m.sh ~/rzg1-bsp/
$ ./copy_mm_software_lcb.sh ~/rzg1-bsp/
```


また、ビルド中に`git`コマンドでパッチ適用を行うレシピが存在しますので、以下の設定を事前に行っておきます（値は適切に変更して下さい）。

```
$ git config --global user.email "you@example.com"
$ git config --global user.name "Your Name"
```


以上の準備が整えば、以下のコマンドでGeckoを含んだブートイメージを作成することができます。

```
$ cd ~/rzg1-bsp
$ source poky/oe-init-build-env
$ bitbake core-image-weston
```


ただし現在は鋭意開発中のステータスですので、ビルドに失敗することもあります。何か問題を発見した場合には、[Gecko EmbeddedのIssues](https://github.com/webdino/gecko-embedded/issues)に報告して下さい。

### まとめ

GeckoEmbeddedプロジェクトのESR60対応の状況とビルド方法について紹介しました。対応は少しづつ進んでいるものの、まだまだ手が足りていない状況です。興味を持たれた方は、ぜひ[当プロジェクトに参加](https://github.com/webdino/gecko-embedded/wiki#%E6%9C%80%E6%96%B0%E6%83%85%E5%A0%B1%E5%8F%82%E5%8A%A0%E3%81%99%E3%82%8B%E3%81%AB%E3%81%AF--update-info--how-to-join)して頂ければ幸いです。
