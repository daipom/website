---
tags:
- mozilla
title: WebExtensionsによるFirefox用の拡張機能でメニューやパネル風のUIを提供するライブラリ：MenuUI.js
---
（この記事は、Firefoxの従来型アドオン（XULアドオン）の開発経験がある人向けに、WebExtensionsでの拡張機能開発でのノウハウを紹介する物です。）
<!--more-->


XULでは`<menupopup>`という要素の中に`<menuitem>`や`<menu>`などの要素を置いてネイティブアプリと同じ様式の階層型メニューを実装できました。それに対し、WebExtensionsベースの拡張機能ではUIは基本的にHTMLで作成します。この時に悩み所になるのが、メニューをどうやって実装するかという点です。

HTMLにも[menu要素](https://developer.mozilla.org/ja/docs/HTML/Element/menu)という物がありますが、これはXULのメニュー関連要素の完全な代替とはなりません。ボタンに対してのポップアップメニューやツールバー上のメニューは本稿執筆時点でFirefoxでは実装されておらず、唯一使用できるコンテキストメニューでの用法についても、専用のコンテキストメニューを実現する物ではなく、あくまで「Webページ上のコンテキストメニューに追加の項目を設ける」という性質の物です。

Firefoxのコンテキストメニューとは切り離された別のメニューを実現するには、今のところHTML（＋JavaScript＋CSS）でそれらしい見た目と振る舞いのUIを独自に実装するほかありません。実装例自体は既存の物が多数あり、[React用](https://github.com/fkhadra/react-contexify)、[Vue用](https://github.com/michitaro/vue-menu)、[jQuery用](https://www.jqueryscript.net/tags.php?/context%20menu/)など、採用するフレームワークに合わせて選べます。しかし、作りたいアドオンの性質上、フレームワークの導入には気乗りしないという人もいるのではないかと思います。

そこで、このような場面で使える軽量なライブラリとして[MenuUI.js](https://github.com/piroor/webextensions-lib-menu-ui)という物を開発しました。

### 基本的な使い方

このライブラリは、HTMLファイル中に静的に記述された物か動的に生成された物かを問わず、`<ul>`と`<li>`で構成された入れ子のリストを階層型のメニューとして振る舞わせるという物です。表示例はこんな感じです。
[![（Tree Style Tabでの使用例）]({{ "/images/blog/20180511_0.png" | relative_url }} "（Tree Style Tabでの使用例）")]({{ "/images/blog/20180511_0.png" | relative_url }})

このライブラリを使ってメニュー風UIを提供するには、まず任意のHTMLファイル内で`MenuUI.js`を読み込みます。

```html
<script type="application/javascript" src="./MenuUI.js"></script>
```


また、それと併せてメニューとして表示するためのリストを以下の要領で用意します。

```html
<ul id="menu">
  <li id="menu-save">保存(&amp;S)</li>
  <li>コピー(&amp;P)
    <ul>
      <li id="menu-copy-title">タイトル(&amp;T)</li>
      <li id="menu-copy-url">&amp;URL</li>
      <li>メタデータ(&amp;M)
        <ul>
          <li id="menu-copy-author">作者名(&amp;A)</li>
          <li id="menu-copy-email">&amp;Eメール</li>
        </ul>
      </li>
    </ul>
  </li>
  <li class="separator"></li>
  <li id="menu-close">閉じる(&amp;C)</li>
</ul>
```


  * サブメニューを定義したい場合はリストを入れ子にして下さい。

  * この例では、後々どの項目が選択されたかを容易に判別できるようにするために、コマンドとして実行されうる項目にIDを割り当てています。（通例、サブメニューを持つ項目はそれ自体が選択されてもコマンドとしては実行されないため、それらにはIDを割り当てていません。）

  * リストの内容となるテキストがそのままメニュー項目のラベルになります。

    * この時、ラベルの一部に`&`（静的に記述する場合は実体参照の`&amp;`）を書いておくと、その次の文字がメニュー項目のアクセスキーになります。

次に、このリストをメニューとして初期化します。以下の要領で`MenuUI`クラスのインスタンスを作成します。

```javascript
var menuUI = new MenuUI({
  root: document.getElementById('menu'), // メニューにするリストの最上位の<ul>を指定
  onCommand: (aItem, aEvent) => {
    // 項目が選択された時に実行されるコールバック。
    // ここでは項目のidで処理を振り分けるようにしている。
    switch (aItem.id) {
      case 'menu-save':
        doSave();
        return;
      case 'menu-copy-title':
        doCopy('title');
        return;
      ...
    }
  }
});
```


作成されたインスタンスは、`open()`というメソッドを実行するとメニューとして表示されます。ページ上を右クリックした際にブラウザ本来のコンテキストメニューの代わりとして表示したい場合であれば、以下のようにします。

```javascript
window.addEventListener('contextmenu', aEvent => {
  aEvent.stopPropagation();
  aEvent.preventDefault();
  menuUI.open({
    left: aEvent.clientX,
    top:  aEvent.clientY
  });
});
```


また、何かのボタンをクリックした時にドロップダウンメニューのように表示させたい場合には、以下のようにボタンのクリック操作を監視した上で、そのボタンなどの要素を`open()`メソッドの引数に`anchor`というプロパティで指定すると、ボタンの位置に合わせてメニューが表示されます。

```javascript
const button = document.getElementById('button');
button.addEventListener('click', () => {
  menuUI.open({
    anchor: button
  });
});
```


コンストラクタのオプションや`open()`のオプションの詳細な内容については、[リポジトリ内のREADMEファイル](https://github.com/piroor/webextensions-lib-menu-ui)を参照して下さい。

### キーボードでの操作への対応

このライブラリの特長として、キーボード操作への対応に力を入れているという点が挙げられます。以下は、現時点で対応している代表的な操作です。

  * ↑↓カーソルキーで項目のフォーカスを移動

  * →カーソルキーでサブメニューを開く

  * ←カーソルキーでサブメニューを閉じる

  * Escキーでメニューを閉じる

  * Enterキーでフォーカスしている項目を選択（コマンドを実行）

  * メニューのラベルから検出されたアクセスキーで項目を選択（同じアクセスキーの項目が複数ある場合はフォーカス移動のみ）

メニューを開くまではポインティングデバイスを使うが、その後の操作はキーボードで行う、というような場面は意外とあります。既存のライブラリはキーボード操作に対応していなかったり、操作体系が一般的なGUIのメニューではなくWebページ上のリンクに準拠していたりと、「ネイティブアプリのGUIと同じ感覚で使える」事が特長だったXULからの移行においてストレスになる部分があったため、この点にはとりわけ気をつけて開発しています。

### まとめ

XULアドオンでのカスタムメニューの代替となる軽量ライブラリである[MenuUI.js](https://github.com/piroor/webextensions-lib-menu-ui)について、その使い方を解説しました。[「以後確認しない」のようなチェックボックスを伴った確認ダイアログを表示する`RichConfirm.js`]({% post_url 2018-03-26-index %})と併せて、Firefox用アドオンの開発にご活用いただければ幸いです。
