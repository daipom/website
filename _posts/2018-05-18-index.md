---
tags:
- mozilla
title: Active Directoryを使用していない環境で、Firefoxに任意のルート証明書を自動インポートする方法
---
[既報の通り]({% post_url 2017-06-01-index %})、Firefox 52以降のバージョンではActive Directoryのグループポリシー経由で配布された証明書を自動的にインポートできるようになっています。アドオンなどによる証明書の自動インポートができなくなったため、現時点ではFirefoxに任意のルート証明書を組み込んだ状態にするには、これが唯一の方法となっています。
<!--more-->


ただ、Active Directoryを運用してない小規模の組織や、検証用のスタンドアロンのPCでは、何らかの方法で「Active Directoryのグループポリシー経由で証明書を配布した状態」と同じ状態にする必要があります。以下はそのための作業手順の説明となります。

### 背景

Windowsの証明書データベースは、実際にはレジストリ上の複数の位置にある情報が集約されて認識された物です。証明書のバイナリはレジストリ上ではBlob形式のレジストリ値として保存されています。

この事は、任意の証明書を主導でインポートした後でレジストリエディタを起動し `HKEY_CURRENT_USER\Software\Microsoft\SystemCertificates` 以下の状態を見ることで分かります。

Active Directoryのグループポリシーで証明書を展開すると、証明書データベースを構成する複数のレジストリキーのうち、以下の位置のいずれかに情報が書き込まれます。

  * `HKEY_LOCAL_MACHINE\Software\Microsoft\SystemCertificates\Root\Certificates`

  * `HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\SystemCertificates\Root\Certificates`

  * `HKEY_LOCAL_MACHINE\Software\Microsoft\EnterpriseCertificates\Root\Certificates`

よって、「Active Directoryのグループポリシー経由で証明書を配布した状態」と同じ状態を作り出すためには、必要な証明書の情報を上記のキー配下に書き込めばよいと言う事になります。

### 準備

以上のような背景があるため、Firefoxにインポートさせたい証明書はまず一旦、Windowsの証明書データベースにインポートしておく必要があります。まだインポートされていない場合は、以下の手順で証明書をインポートしておいて下さい。

  1. 証明書ファイルをダブルクリックして、証明書のプロパティを開く。

  1. 「詳細」タブでフィールド「拇印」の値（SHA-256ではなくSHA-1）を確認し、控えておく。

以下、拇印は`49cb1e088952d5116738db438a11c5aba7a12e01`と仮定する。

  1. 「全般」タブで「証明書のインストール」ボタンをクリックする。

  1. 証明書のインポートウィザードが開かれるので、以下の設定でインポートする。

     * 現在のユーザー 用

     * 「証明書を全て次のストアに配置する」で「エンタープライズの信頼」を選択

### レジストリファイルの作成

レジストリエディタでインポートするための証明書ファイルは、以下の手順で作成します。

  1. 「ファイル名を指定して実行」で `regedit.exe` と入力し、レジストリエディタを起動する。

  1. 「編集」→「検索」で、検索する値として、当該証明書の拇印（SHA-1 Fingerprint）を入力する。

ここでは`49cb1e088952d5116738db438a11c5aba7a12e01`と仮定する。

  1. 名前が上記の拇印と一致する[^0]キーが見つかる。
（「準備」の項の手順でインストールした場合、  `HKEY_CURRENT_USER\Software\Microsoft\SystemCertificates\trust\Certificates`配下にある。）

  1. 見つかったキーを右クリックし、「エクスポート」を選択して`EnterpriseCert.reg`などの名前で保存する。

  1. 「準備」の項の手順でインストールした場合、3で見つかったキーをレジストリエディタ上で削除する。

  1. 4でエクスポートしたファイルをテキストエディタで開く。

（ファイルのエンコーディングは「UTF-16LE（BOM有り）」となっている）

  1. ファイルの中に含まれる文字列`HKEY_〜\拇印`となっている箇所（上記の例であれば`HKEY_CURRENT_USER\Software\Microsoft\SystemCertificates\trust\Certificates\49CB1E088952D5116738DB438A11C5ABA7A12E01`）をすべて以下の文字列に置換する。

`HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\SystemCertificates\Root\Certificates\拇印`

（「準備」の項の手順でインストールした場合、`HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\SystemCertificates\Root\Certificates\49CB1E088952D5116738DB438A11C5ABA7A12E01`。）

  1. 編集したファイル「EnterpriseCert.reg」を上書き保存する。

以上の手順で作成したレジストリファイル「EnterpriseCert.reg」を各クライアントPCでインポートすると、グループポリシーで証明書を配布したのと同じ状態になります。

### まとめ

Windowsのレジストリを編集して、Active Directoryによるグループポリシーの配布ができない環境で同等の結果を得て、Firefoxのルート証明書自動インポート機能の動作を確認する手順をご説明しました。

[^0]: 大文字小文字は区別されません。
