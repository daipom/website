---
title: DebianパッケージでMesonに移行するプロジェクトに対応するには
tags:
  - debian
---
### はじめに

[Meson](https://mesonbuild.com/)はソフトウェアをビルドする、ビルドシステムの1つです。
GNOME関連だと、すでに多くのモジュールが[Mesonに対応](https://wiki.gnome.org/Initiatives/GnomeGoals/MesonPorting)しています。
<!--more-->


今回は、アップストリームがビルドシステムをMesonに移行した場合にどのように対応すればいいかを紹介します。

### libhinawaの場合

[libhinawa](https://github.com/takaswie/libhinawa)の場合、これまではビルドシステムとしてAutotoolsを採用していました。
1.0のリリースにともない、[Mesonに移行する](https://github.com/takaswie/libhinawa/tree/topic/meson-build)話があったため、実際にどうやればよいかを調べてみました。
アップストリーム側の`Meson`対応自体はトピックブランチも用意されていたので、それをもとに試してみました。

```console
% git clone https://github.com/takaswie/libhinawa.git libhinawa.meson
% cd libhinawa.meson
% git checkout -b meson-build origin/topic/meson-build
```


上記トピックブランチで用意された`meson.build`や`meson_options.txt`にあわせてDebianパッケージ側で修正が必要なのは以下の2つのファイルです。

  * `debian/rules`

  * `debian/control`

### debian/rulesを修正する

変更前の`debian/rules`は以下のようになっていました。（必要な箇所のみ抜粋）

```text
%:
        dh $@ --with gir
```


これを`Meson`に対応させるためには、次のように明示的にビルドシステムの指定を追加します。

```text
%:
        dh $@ --with gir --buildsystem=meson
```


ビルドシステムはこのほかにもいくつかサポートされているものがあります。
どんなビルドシステムがサポートされているかは、次のコマンドを実行することで確認できます。

```console
% dh_auto_configure --list
autoconf             GNU Autoconf (configure)
perl_build           Perl Module::Build (Build.PL)
perl_makemaker       Perl ExtUtils::MakeMaker (Makefile.PL)
makefile             simple Makefile
python_distutils     Python Distutils (setup.py) [DEPRECATED]
cmake+makefile       CMake (CMakeLists.txt) combined with simple Makefile
cmake+ninja          CMake (CMakeLists.txt) combined with Ninja (build.ninja)
ant                  Ant (build.xml)
qmake                qmake (*.pro)
qmake_qt4            qmake for QT 4 (*.pro)
meson+ninja          Meson (meson.build) combined with Ninja (build.ninja)
ninja                Ninja (build.ninja)
kde+makefile         CMake with KDE 4 flags combined with simple Makefile [3rd party]
kde+ninja            CMake with KDE 4 flags combined with Ninja (build.ninja) [3rd party]
kf5+makefile         CMake with KDE Frameworks 5 flags combined with simple Makefile [3rd party]
kf5+ninja            CMake with KDE Frameworks 5 flags combined with Ninja (build.ninja) [3rd party]

Auto-selected: autoconf
```


厳密には`--buildsystem=meson+ninja`が正しいのですが、歴史的経緯から`--buildsystem=meson`だけでもよいようです。

### debian/controlを修正する

ビルドシステムを`Meson`に移行するので、パッケージの依存関係も修正が必要です。

`debian/control`のAutotools依存の部分を書き換えればよいです。

```diff
diff -u libhinawa/debian/control libhinawa.meson/debian/control
--- libhinawa/debian/control    2018-04-21 17:47:20.539799697 +0900
+++ libhinawa.meson/debian/control      2018-05-25 17:07:55.979775347 +0900
@@ -4,7 +4,7 @@
 Maintainer: Takashi Sakamoto <o-takashi@sakamocchi.jp>
 Uploaders: Kentaro Hayashi <hayashi@clear-code.com>
 Build-Depends: debhelper (>= 11),
-    automake (>= 1.10), autoconf (>= 2.62), libtool (>= 2.2.6),
+    meson,
     libglib2.0-dev (>= 2.32.0),
     gtk-doc-tools (>= 1.18-2),
     gobject-introspection (>= 1.32.1),
```


### Mesonでビルドしてみる

修正ができたところで、実際に`Meson`を使ってビルドできるか試してみます。

```console
% debuild -us -uc -nc
dpkg-buildpackage -rfakeroot -us -uc -ui -nc
dpkg-buildpackage: info: source package libhinawa
dpkg-buildpackage: info: source version 1.0.0-1
dpkg-buildpackage: info: source distribution unstable
dpkg-buildpackage: info: source changed by Kentaro Hayashi <hayashi@clear-code.com>
 dpkg-source --before-build libhinawa.meson
dpkg-buildpackage: info: host architecture amd64
  debian/rules build
dh build --with gir --buildsystem=meson
   dh_update_autotools_config -O--buildsystem=meson
   dh_autoreconf -O--buildsystem=meson
dh_autoreconf: Only runs once, see dh-autoreconf(7)
   dh_auto_configure -O--buildsystem=meson
        cd obj-x86_64-linux-gnu && LC_ALL=C.UTF-8 meson .. --wrap-mode=nodownload --buildtype=plain --prefix=/usr --sysconfdir=/etc --localstatedir=/var --libdir=lib/x86_64-linux-gnu --libexecdir=lib/x86_64-linux-gnu
The Meson build system
Version: 0.46.1
（長いので省略）
```


期待通りにビルドシステムに`Meson`が使われ、パッケージがビルドできるようになりました。

### まとめ

今回は、アップストリームがビルドシステムをMesonに移行した場合にDebianパッケージ側でどのように対応すればいいかを紹介しました。
ビルドシステムが変わったときにどうすればいいかの参考にしてみてください。
