---
tags:
- mozilla
- feedback
- embedded
title: Gecko Embedded ESR60
---
### はじめに

これまでにも何度か紹介してきましたが、クリアコードではGecko（Firefox）を組み込み機器向けに移植する取り組みを行っています。
<!--more-->


  * [Gecko Embeddedプロジェクト]({% post_url 2017-07-06-index %})

  * [Gecko Embeddedプロジェクト 11月時点のステータス]({% post_url 2017-11-09-index %})

  * [Gecko Embedded 次期ESR対応]({% post_url 2018-03-30-index %})

当プロジェクトでは移植コストを少しでも低減するために、Firefoxの延長サポート版（ESR）のみを対象としています。これまではESR45やESR52をベースにハードウェアアクセラレーションに対応させるための作業を行ってきました。
現在はESR60に対応し、そのバグを修正する作業を進めています。

今回は、この作業の現在のステータスを紹介します。詳細なビルド方法については、[Gecko Embedded 次期ESR対応]({% post_url 2018-03-30-index %}) を参照してください。

### 現在のステータス

現在はFirefox ESR60の開発を進めています。ビルドは通るようになり、ソフトウェアレンダリングでの描画もある程度安定して動作するようになっています。Firefox 60からはWayland対応のコードも本体に入っているため、ソフトウェアレンダリングであればほぼ無修正で動作させることができています。
また、60ESRには入りませんでしたが、当プロジェクトが作成したEGLおよびOpenMAX対応のパッチも無事Firefox本体に取り込まれています。

  * https://bugzilla.mozilla.org/show_bug.cgi?id=1460603

  * https://bugzilla.mozilla.org/show_bug.cgi?id=1460605

  * https://bugzilla.mozilla.org/show_bug.cgi?id=1306529

ESR52で実現出来ていたハードウェア対応の移植作業については、2018年5月30日現在、以下のような状況です。

  * レイヤーアクセラレーション: 移植済み（RZ/G1M、R-Car M3、Raspberry Pi3 Model Bで検証）

  * OpenMAX（H.264デコード）: 移植済み（RZ/G1Mで検証）

  * WebGL: 移植済み（R-Car M3で動作）

  * WebRTC: レイヤーアクセラレーションとの組み合わせでは動作せず（調査中）

  * Canvas 2D Contextのアクセラレーション: e10sオフでは動作

また、ESR52ではなかった要素として、Rustで書かれたコードが追加されてきている点が挙げられます。細かく動作検証はできていませんが、少なくとも[Stylo（Quantum CSS）](https://wiki.mozilla.org/Quantum/Stylo)は実機（Renesas RZ/G1M）で動作することを確認できています。

前回からの改善点は以下の通りです。

  * EGL有効化時に表示されるUIと操作するときに反応する座標がずれる不具合が解消した

  * UIのタブのドラッグ&ドロップが動作するようになった

  * コピー＆ペーストが動作するようになった

  * WebGLがR-Car M3で動作することを確認

現状では、以下の制限があります。

  * 動画を再生＆一旦停止し、別の動画を再生した時にサウンドデバイスが解放されず、音が出力されない

  * SoCのGPUがOpenGL ES 2.0のプロファイルを持ちrobustness拡張をサポートしない場合、WebGLのコンテキストが不完全となるか、コンテキスト作成に失敗する

  * Yocto 2.4ベースのBSPではStyloのビルドが確認できていない

  * Renesas RZ/G1MのPowerVR SGX 544MPを使用する場合、コンポジターでハードウェア支援を有効にするとウィンドウリサイズ時に高確率でSEGVすることがある

  * e10s有効化時にEGLを有効化するとContentプロセスがSEGVする

### 動作確認を行ったハードウェア

現時点ではRenesas RZ/G1M、R-Car M3、また、コミュニティサポートのレベルですが、[StyloのビルドサポートなしでRaspberry Pi3 Model Bにも移植](https://github.com/cosmo0920/rpi3-yocto-conf)しました。

### まとめ

GeckoEmbeddedプロジェクトのESR60の状況とビルド方法について紹介しました。ESR60対応がひと段落し、対象ボードを拡充する対応は少しづつ進んでいるものの、まだまだ手が足りていない状況です。興味を持たれた方は、ぜひ[当プロジェクトに参加](https://github.com/webdino/gecko-embedded/wiki#%E6%9C%80%E6%96%B0%E6%83%85%E5%A0%B1%E5%8F%82%E5%8A%A0%E3%81%99%E3%82%8B%E3%81%AB%E3%81%AF--update-info--how-to-join)して頂ければ幸いです。
