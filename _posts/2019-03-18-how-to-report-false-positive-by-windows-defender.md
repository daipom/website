---
tags:
- mozilla
title: 公開中のソフトウェアがWindows Defenderでマルウェアとして判定された場合の対応
author: piro_or
---
当社では[IE View WE](https://addons.mozilla.org/firefox/addon/ie-view-we/)というアドオンを開発・公開しています。これはFirefox上で閲覧中のページやマッチングパターンに当てはまるURLのページをIEもしくは任意の外部アプリケーションで開くという物で、Firefox法人サポートでの需要を想定して、既存のアドオン「IE View」の仕様を参考にFirefox 57以降で使用できる形（WebExtensionsベース）でスクラッチで開発したという物です。
<!--more-->


WebExtensionsではアドオンが直接任意の外部アプリケーションを起動する事はできず、[Native Messaging Host](https://developer.mozilla.org/ja/docs/Mozilla/Add-ons/WebExtensions/Native_messaging)と呼ばれるヘルパープログラムを介する必要があります。Native Messaging Hostは任意の言語で実装することができますが、IE View WEのNative Messaging Hostの場合はGoで開発しており、一般のユーザのためにコンパイル済みのバイナリも公開・配布しています。

このNative Messaging Hostのバイナリについて、先日、「Windows Defenderでトロイ（マルウェア）と判定されている」というご指摘を頂きました。

結果的にはただの誤判定だったのですが、配布物がそのような警告を受ける状態となった事は当社では過去に無かったため、対応のノウハウが無く手探りでの対応とならざるを得ませんでした。本記事では、本件に際して実際に行った対応の内容をご紹介します。同様の事態に遭遇された方の参考になりましたら幸いです。

### 第一報〜状況確認〜バイナリ公開停止

最初に受け取ったのは、以下のような内容の指摘のメール（英語）でした。

> 件名: Native Messaging Host……  
> 本文: Windows 10 32bit版のWindows Defenderで、トロイと判定されます。

これを受け、まず、*本当に配布ファイルがWindows Defenderでマルウェアと判定されるのかどうか*を確認しました。

IE View WEのNative Messaging Hostのバイナリは、現在の所[GitHubのリリースページ](https://github.com/clear-code/ieview-we/releases/)でのみ配布しています。まず公開中のファイルをダウンロードし、実際にWindows Defenderに判定させてみました[^0]。その結果、確かに `Trojan:Win32/Azden.A!cl` として判定される結果となりました。

そこで、公開中のバイナリをリリースページから削除し、以下の文言を表示するようにしました。

> ieview-we-host.zip is undistributed due to security reason for now. Please wait for a while.

> ieview-we-host.zipはセキュリティ上の理由により現在公開を停止中です。しばらくお待ち下さい。

### さらなる状況確認

次に確認したのは、*ダウンロードした配布ファイルはこちらで作成してアップロードした物と同一なのかどうか*という点です。

ダウンロード用のURLはHTTPSとなっており、サーバーからのダウンロード過程でファイルが攻撃者によって置き換えられてしまう可能性はありません。しかし、バイナリ自体に電子署名は施していないため、サーバー上にあるファイル自体がこちらでアップロードした物から差し替えられてしまっている可能性は0とは言えません[^1]。そのため、次の段階としてダウンロード後のファイルとアップロード前のファイルのハッシュ値を`sha256sum`コマンドで算出し、両者が一致するかどうかを確認しました。その結果、ハッシュ値は一致し、アップロードの前後でファイルは変化していない事を確認できました。

（なお、仮にこの段階でファイルのハッシュ値が一致しなかった場合、アップロードされた後でファイルが改竄されたという事になります。それが可能となるのは、当社エンジニアが使用しているSSH秘密鍵が流出したか、GitHubのWebサービスが攻撃を受けてファイルが改竄されたというケースにあたり、また別の対応が必要になってきます。）

### アップロード前のファイルがマルウェア判定された場合の対応

アップロード前の状態と同一のファイルがマルウェアと判定されるということは、ファイルが作成された時点からそのような内容が含まれていたという事になります。そうなると、今度は以下の可能性が疑われます。

  * バイナリ作成者のPCがすでに攻撃者によって乗っ取られていた。

  * バイナリに含まれる外部ライブラリに攻撃コードが混入していた。

作業者のPCが攻撃者によって乗っ取られるというのは、現代でもあり得る脅威です。例えば、いわゆるフリーWi-Fiの使用時は、同一のLAN内に全く素性の知れない他人が接続してきている状況で、攻撃の踏み台として標的にされるという事は十分に起こり得ます。

ただ、今回の事例ではこの可能性はそう高くないと考えられました。作業者のPCはUbuntuのデスクトップ環境が動作しており、外部からの接続も公開鍵認証のSSH以外は受け付けないよう設定されていたからです。絶対数が少ない環境の上、フリーWi-Fiに接続する機会もほぼ無いため、あらかじめ攻撃を受けていて踏み台にされていたという事は考えにくいです。

また、現代のソフトウェアは多数の依存ライブラリを引用する形で成り立っている事が多く、その中にこっそり悪意あるコードが混入していたとしても容易に気付けないという状況にあります。実際、そのような攻撃が行われた事例や、攻撃が行われうる脆弱性が指摘された事例は複数あります。

  * [2018/11/27に判明したnpmパッケージ乗っ取りについて](https://qiita.com/azs/items/b15bc456bee3a7892950)

  * [2018/07/12 に発生したセキュリティ インシデント (eslint-scope@3.7.2) について](https://qiita.com/mysticatea/items/0141657e4478d9cf4614)

  * [（翻訳）RubyGems.orgでgemが置き換えられる脆弱性とその緩和策について](https://qiita.com/jnchito/items/0a5ecbbc8a13d88bfcda)

こちらの可能性を疑い始めると、調査は困難になってきます。依存ライブラリもまた別のライブラリに依存しているという事が多く、場合によっては調査範囲が際限なく広がってしまうからです。

### Microsoftへの連絡

ここで初めて、それまで意図的に敢えて除外していた、*擬陽性であった可能性*を考え始める事にしました。

現代のマルウェアは日々多種多様な亜種が発生しており、単純なパターンマッチングでは「本当はマルウェアなのにマルウェアとして判定されない」というケース（擬陰性）が発生しやすいです。そのため各セキュリティ対策ソフトのベンダーは、機械学習を用いた分析や実際のソフトウェアの動作の様子を監視するなどして、疑わしい振る舞いをするソフトウェアは積極的に脅威判定するようになっています。その結果、本来は無害なソフトウェアであってもマルウェアの疑い有りとして検出されるというケース（擬陽性）が発生してしまう事があります。

Microsoftではこのようなケースを想定して、[提出されたファイルを詳細に分析する受付窓口](https://www.microsoft.com/en-us/wdsi/filesubmission)を提供しています。Windows Defenderでマルウェアとして判定されたソフトウェアをここで提出すると、専門家が分析した上で「本当にマルウェアである」あるいは「マルウェアではない」といった判定を行った結果を連絡してくれます。

今回のように自作ソフトウェアがマルウェア判定されたケースでは、「Software Developer（ソフトウェア開発者）」を選択して報告します。実際の報告内容は以下のようにしました。

  * 会社名：ClearCode Inc.

  * 判定名：Trojan:Win32/Azden.A!cl

  * 定義ファイルバージョン：1.289.911.0

  * 追加の情報：

    > This program is a native messaging host for a Firefox addon "IE View WE", and it provides following features:  
    > 1) Read configurations from local config files placed under specific locations, distributed by the system administrator.  
    > 2) Read registry to know where the executable file of the Internet Explorer (or Edge) is.  
    > 3) Launch the Internet Explorer (or Edge) with some options.  
    > The source codes of the file are published under https://github.com/clear-code/ieview-we/tree/master/host and https://github.com/clear-code/mcd-go


  * 参考訳（※送信した報告は英語の方のみです）

    > このプログラムはFirefox用アドオン「IE View WE」用のNative Messaging Hostで、以下の機能を提供します:  
    > 1) システム管理者によって特定の位置に置かれたローカル設定ファイルから設定情報を読み取る。  
    > 2) レジストリからIE（またはEdge）の実行ファイルの位置を調べる。  
    > 3) いくつかのオプションを指定してIE（またはEdge）を起動する。  
    > ソースコードは以下のサイトで公開しています： https://github.com/clear-code/ieview-we/tree/master/host および https://github.com/clear-code/mcd-go


送信した報告内容はキューに溜められ、順次処理されていきます。有償のサポート契約や開発者ライセンスを持っている場合、高い優先度でキューに割り込む事もできるようですが、当社はそのようなライセンスを特に保持していないので、一般的なソフトウェア開発者として報告するに留まりました。

### 報告者への連絡と、追加の確認

急いで取れる対応はここまでということで、この時点で一旦報告者の方に状況を伝える事にしました。

> Hello,  
> Thank you for the information. We've removed the downloadable file for now, until those files are confirmed as completely safe. We've report those files to Microsoft security researchers, and waiting for responses.  
> regards,


> こんにちは  
> 情報をありがとうございます。安全が確認できるまで、ダウンロード可能なファイルは削除する事にしました。これらのファイルはすでにMicrosoftのセキュリティリサーチャーに報告済みで、返事を待っている状態です。  
> それでは

また、複数のセキュリティ対策ソフトでの検知結果を横断して確認できる[VirusTotal](https://www.virustotal.com/ja/)というサイトでも確認を行った所、Windows Defender以外にもいくつかのソフトで脅威判定されている様子が窺えました。具体的な結果は以下の通りです。

  * 32bit版バイナリ： 検出率: 2/70（[現時点では0/70](https://www.virustotal.com/ja/file/b72d3c9f618d57acd3e92f1f666ef2ad66bc0f64f8a81c7a3e8d02b93e2888be/analysis/)）

    * Cylance: Unsafe （20190312）

    * Microsoft: Trojan:Win32/Azden.A!cl （20190307）

  * 64bit版バイナリ： 検出率: 1/65（[現時点でも変わらず](https://www.virustotal.com/ja/file/8203604dbb8dbfb6f961b22294dcb46260e2f2840619ed04dd563e2e2653a279/analysis/)）

    * Jiangmin: Trojan.Ebowla.k （20190312）

### 安全確認〜再公開

そうこうするうちに、Microsoftから分析完了の通知メールが届きました。メールに記載されていたリンクを開くと、以下のように記載されていました。

> Submission details  
> host.exe  
> Submission ID: xxxx-xxxx-xxxx-xxxx-xxxx  
> Status: Completed  
> Submitted by: (送信者のMicrosoftアカウントのメールアドレス)  
> Submitted: Mar 12, 2019 11:19:43  
> User Opinion: Incorrect detection  
> Analyst comments:  
> We have removed the detection.  Please follow the steps below to clear cached detection and obtain the latest malware definitions.  
> [["  1. Open command prompt as administrator and change directory to c:\\Program Files\\Windows Defender"], ["  1. Run “MpCmdRun.exe -removedefinitions -dynamicsignatures”"], ["  1. Run “MpCmdRun.exe -SignatureUpdate”"]]  
> Alternatively, the latest definition is available for download here: https://www.microsoft.com/en-us/wdsi/definitions  
> Thank you for contacting Microsoft.


> 報告の詳細  
> host.exe  
> 報告ID: xxxx-xxxx-xxxx-xxxx-xxxx  
> 状態: 完了  
> 送信者: （送信者のMicrosoftアカウントのメールアドレス）  
> 送信日時: 2019年3月12日 11:19:43  
> ユーザーの意見: 誤判定  
> 分析者のコメント:  
> 私達は判定を削除しました。以下の手順で判定結果のキャッシュを消去し、最新のマルウェア定義を入手して下さい。  
> [["  1. コマンドプロンプトを管理者として開き、ディレクトリを「c:\\Program Files\\Windows Defender」に切り替える。"], ["  1. 「MpCmdRun.exe -removedefinitions -dynamicsignatures」を実行する。"], ["  1. 「MpCmdRun.exe -SignatureUpdate」を実行する。"]]  
> もしくは、最新の定義ファイルは以下からもダウンロード可能です：https://www.microsoft.com/en-us/wdsi/definitions

表示されている時刻は日本時間です。11時台に報告して、結果が返ってきたのは14時〜16時にかけてでした[^2]。数日待たされる事も覚悟していたのですが、予想よりずっと早い回答でした。

案内のあった手順の通りに操作して再確認した所、バイナリはマルウェアとは検出されないようになりました。よって、これを以て安全の確認が取れたと見なし、以下の文言を添えて、公開を取り下げていたバイナリを再公開しました

> Files named `host.exe` in `ieview-we-host.zip` may be detected as `Trojan:Win32/Azden.A!cl` by Microsoft Windows Defender, but it is false positive and actually safe. Those misdetectons have been reported to Microsoft, and go away after you update the malware definitions.

> `ieview-we-host.zip`に含まれる`host.exe`という名前のファイルがMicrosoft Windows Defenderによって`Trojan:Win32/Azden.A!cl`と判定される事がありますが、これは擬陽性で、実際には安全です。この誤判定はMicrosoftに報告済みで、マルウェア定義の更新後は警告されなくなります。

また、報告者の方にも以下の通り連絡しました。

> Hello,  
> The trojan alert was a false positive, and Microsoft researchers decided to remove them from detection. After you update malware definitions, the alert will go away. Detailed steps described by Microsft:  
> (中略)  
> So we've re-published native messaging host binaries. Thanks again!  
> regards,

> こんにちは  
> トロイ警告は擬陽性で、Microsoftのリサーチャーはこの判定結果を取り除く決定をしました。  
> マルウェア定義の更新後は、この警告は表示されなくなります。Microsoftによって案内された手順は以下の通りです:  
> (中略)  
> よって、Native Messaging Hostのバイナリを再公開しました。重ねてありがとうございます！  
> それでは

以上を以て、本件へのWindows Defenderに関する対応は完了としました。

### まとめ

以上、Windows Defenderでマルウェアとして誤判定された場合の対応フローの例をご紹介しました。

被害の拡大を防ぐ事を第一に置くと、最初に取るべき対応は「実際にマルウェア判定されるか確認する」ではなく「サンプルとしてファイルをダウンロードした上で、バイナリの公開を停止する」だったと言えます。実際にマルウェア判定されるかどうかは公開を取り下げた後で行えば良かったにも関わらず、それを後回しにして「マルウェア判定されるのは事実か？」を先に確認してしまったのは不徳の致すところです。

今回は擬陽性での誤判定という事で決着しましたが、実際にマルウェアだったという場合にはまた別の対応が必要になってきます。そのような事は起こらないに越した事はありませんが、もし万が一そのような事態が発生した際には、被害の拡大を防ぐ事を最優先として対応したいです。

なお、2019年3月18日現在、[VirusTotal](https://www.virustotal.com/ja/)で確認した限りでは、他の製品では脅威判定はなされない状態になっているようなのですが、[Jiangmin](http://www.jiangmin.com/)という製品で依然として脅威として判定されている模様です。Jiangminは中国系企業の製品で、JUSTセキュリティのバックエンドにも採用されているそうなのですが、公式サイトが中国語のみで提供されているため、報告の窓口が分からずお手上げとなっています。もし報告窓口をご存じの方がいらっしゃいましたら、情報をお寄せいただければ幸いです。

[^0]: この段階でもしマルウェアとして判定されなければ、報告者がファイルをダウンロードしたWebサイトで配布されているファイルが改竄済みの物である（改竄されたファイルが第三者によって配布されている）という事になります。

[^1]: 電子署名を施していた場合、「署名した時点のファイルから改竄されていない事」が保証されるため、この確認は不要になります。

[^2]: 32bit版と64bit版の各バイナリについて、結果が出るまでに若干タイムラグがありました。
