---
tags:
- feedback
title: MariaDBへ不具合をフィードバックするには
---
### はじめに

MySQL/MariaDB用のストレージエンジンのひとつにMroongaがあり、MySQL/MariaDBユーザーに高速な日本語全文検索機能を提供しています。
先日リリースされたMroonga 9.04では、MySQL 8.0向けのパッケージの提供（CentOS 6とCentOS 7）もはじまりました。
<!--more-->


クリアコードでは、Mroonga関連プロダクトのサポートサービスを提供しています。
今回は、サポートサービスをきっかけにMariaDBへ不具合を報告し、修正されたケースがあったのでその事例を紹介します。

### 不具合の発覚のきっかけ

Mroongaでは、ユーザーの利便性のためにMariaDBにMroongaをバンドルしたWindows版のパッケージを配布しています。
不具合発覚のきっかけは、Windowsでこのパッケージを利用中のお客様からの「レプリケーション中にスレーブがとまる」という報告でした。

実際にお客様から提供された問題発生時のログを確認すると、次のようなメッセージが記録されていました。

```
[ERROR] Semisync slave socket fd is 580. select() cannot handle if the socket fd is greater than 64 (FD_SETSIZE).
[Note] Stopping ack receiver thread
```


このエラーメッセージに対応するのはMariaDBの以下の実装箇所でした。

  * https://github.com/MariaDB/server/blob/dde2ca4aa108b611b5fdfc970146b28461ef08bf/sql/semisync_master_ack_receiver.h#L214-L222

該当するソースコードを抜粋すると次のようになっていました。

```
#ifndef WINDOWS
      if (socket_id > FD_SETSIZE)
      {
        sql_print_error("Semisync slave socket fd is %u. "
                        "select() cannot handle if the socket fd is "
                        "greater than %u (FD_SETSIZE).", socket_id, FD_SETSIZE);
        return 0;
      }
#endif //WINDOWS
```


ソケットのファイルディスクリプターの値が大きいせいで、スレーブからのackを受け取るスレッドが止まっていました。
「Windows以外で」上記のチェックが有効なはずなのに、実際には「Windowsで」エラーが発生していました。
これは、`#ifndef WINDOWS` の `WINDOWS` が定義されていないことによるものでした。
通常こういう場合は `#ifndef _WIN32` が使われるので、正しくありません。

実際、他の実装としてMySQLを確認したところ、Bug#23581389 として一年半以上前にまったく同じ問題の修正がなされていました。

  * https://github.com/mysql/mysql-server/commit/ec76a52483c3eec49a878e751341246be66a6802

### 不具合を報告するには

MariaDBの場合、[MariaDB bugs database](https://jira.mariadb.org/browse/MDEV)に不具合を報告します。

遭遇した問題は既知の問題として報告されていなかったので、新規課題の「作成」をクリックして問題を報告しました。
（課題の報告にはあらかじめアカウントを作成してログインが必要です。）

対象コンポーネントは「Replication」を選択し、環境には「Windows」であることを明記しました。
問題の説明では、エラー発生時のログや、実装のどこが問題であるかに言及しました。

実際に報告した内容は次のとおりです。

  * [Invalid #ifndef WINDOWS causes error because of needless "Semisync slave socket fd" check](https://jira.mariadb.org/browse/MDEV-19643)

![MDEV-19643の不具合報告]({{ "/images/blog/20190701_0.png" | relative_url }} "MDEV-19643の不具合報告")

この問題については、報告から1ヶ月しないうちに修正されました。
修正版はMariaDB 10.3.17や10.4.7としてリリースされる見込みです。

### まとめ

今回、サポートサービスをきっかけにMariaDBへ不具合を報告し、問題が修正されたケースがあったのでその事例を紹介しました。
Mroongaに限らず関連プロダクトのサポートが必要な方は[問い合わせフォーム](/contact/?type=groonga)からご相談ください。
