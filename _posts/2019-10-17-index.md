---
tags: []
title: SentencePiece・fastTextをEPELのパッケージを使って簡単にインストールできるようになります
---
### はじめに

SentencePieceは、言語に依存することなく学習データから文章をトークナイズすることができるものです。
MeCabでは言語に依存した辞書が必要でしたが、SentencePieceは言語非依存なので、辞書が不要になります。
SentencePieceについて詳しくは開発者による記事があるのでそちらを参照されると理解がより深まるでしょう。
<!--more-->


  * [Sentencepiece : ニューラル言語処理向けトークナイザ](https://qiita.com/taku910/items/7e52f1e58d0ea6e7859c)

一方のfastTextは文書から学習してテキストを分類し、類似する単語を単語の類似度を計算することができます。
類似度をうまくつかえば、似たような文章をサジェストすることができそうです。

どちらもデータを学習させて活用するためのものですが、SentencePieceやfastTextのことを知って、ちょっと試そうとするにもやや手間が必要でした。これまではソースコードを取得してコンパイルしたりする手順が必要だったからです。
今後はSentencePieceやfastTextをEPELのパッケージとして簡単にインストールできるようになります。ただし、まだepel-testingリポジトリからで、epelリポジトリからインストールできるようになるのはしばらく（2週間ほど）先になる見込みです。

epelリポジトリにパッケージが入ると以下のように簡単にSentencePieceやfastTextをパッケージでインストールできるようになります。

```
$ sudo yum install epel-release
$ sudo yum update
$ sudo yum install fasttext-tools
$ sudo yum install sentencepiece-tools
```


### EPELでパッケージがインストールできるとどう嬉しいか

冒頭で述べたように、SentencePieceやfastTextをこれから触ってみようという人は、簡単にインストールして試せるようになるので嬉しいです。
他にもGroongaという、クリアコードが開発に関わっている全文検索エンジン[Groonga](http://groonga.org/ja/)のユーザーにもメリットが（今後）あります。

というのも、これらをうまく使えばGroongaでいい感じの類似文書検索（例えばRedmineで似たようなチケットをサジェストするようなこと）ができるようになるかもしれないからです。

Groongaにはトークナイザーをプラグインとして組み込む仕組みがあって、使用するトークナイザーを使い分けることで、検索時の振る舞いを調整する（いい感じに検索する）ことができます。
利用可能なトークナイザーには`TokenBigram`や`TokenMecab`などがあります。`TokenBigram`だと漏れなく検索できますが、`TokenMecab`のように意味のある単位での検索は不得手といったような特色があります。逆に`TokenMecab`は意味のある単位でトークナイズできるのでよいかというと、辞書に依存する部分が大きく、未知の語句の検索に弱いため、必ずしも適切とは限りません。
また、`TokenMecab`は既存の辞書を利用するので、言語に依存した辞書が必要です。

より精度の高い検索を実現するには、SentencePieceやfastTextなどを使って、学習結果をもとにトークナイズできるトークナイザーを使えるようにするのがよさそうです。
学習結果をもとにトークナイズするので、MeCabほどそれらしい単語にトークナイズできるわけではなさそうです。ただし、類似文書検索の場合には、特徴的な言い回しがあるかどうかで判断することになるので、そのようなトークナイザーがあると有用です。

（まだそのようなトークナイザーはできていませんが）仮に新しいトークナイザーが使えるようになったとして、インストールに手間がかかるのでは、Groongaのユーザーにとってあまり嬉しくありません。
`TokenMecab`のようにパッケージでインストールできるようになっていると、インストールが容易で更新の手間も少なくできます。
パッケージ化する場合でも、Groongaプロジェクトで提供している独自リポジトリからインストールできるというのでは、Groongaをまだ使っていない人にはあまり嬉しくありません。（リポジトリを追加で登録しないといけないからです。）
また、独自のリポジトリでメンテナンスし続けないといけないというのも手間がかかります。それよりかはアップストリームでメンテンナンスするほうがより誰でも簡単にインストールできるようになって嬉しいので、EPELでパッケージを公開することにしました。[^0]

### まとめ

今回は、SentencePiece・fastTextをEPELのパッケージを使って簡単にインストールできるようになることを紹介しました。
SentencePieceやfastTextを使ってみたい人だけでなく、（今後）Groongaでいい感じの類似文書検索をできるようにするのにも必要になってくるのでEPELのパッケージとしてインストールできるようにしました。独自リポジトリにパッケージを用意して終わりにするのではなく、クリアコードではアップストリーム(Fedoraプロジェクト)へフィードバックしていくことでフリーソフトウェアの推進を実施しています。

[^0]: EPELでパッケージを公開するためにはいろんな手順が必要でした。
