---
tags: []
title: EPELでパッケージをリリースできるようにするには
---
### はじめに

[SentencePiece・fastTextをEPELのパッケージを使って簡単にインストールできるようになります]({% post_url 2019-10-17-index %})でEPELでパッケージをインストールできるようにしたことを紹介しました。
SentencePiece・fastTextを簡単に使えるようにすると嬉しい人向けの記事だったのですが、それを実現するまでにどんなことをやったのかは詳しく説明しませんでした。
今回はEPELでパッケージをリリースできるようにしたいという人向けの記事です。
<!--more-->


### 実際にEPELでパッケージをインストールできるようにしたこと

以前[Fedoraプロジェクトで新規パッケージをリリースする方法]({% post_url 2013-04-10-index %})や[Fedoraプロジェクトでパッケージを更新するには]({% post_url 2013-07-17-index %})としてFedoraプロジェクトでパッケージをリリースしたり、パッケージを更新したりするやりかたについて説明しました。

Fedoraだけでなく、EPELでもパッケージをリリースできるようにするには概ね次のような手順を踏みます。（SentencePieceもfastTextもFedoraにパッケージがないので新規パッケージをリリースするところからになります。）
[Fedoraプロジェクトで新規パッケージをリリースする方法]({% post_url 2013-04-10-index %})はやや古くなっていますが、基本的な流れはそれほど変わっていません。
そのため、一度Fedoraでパッケージをリリースしたことがあり、`packager`グループの権限をもっているという前提で変わったところを中心に説明します。

  * パッケージを準備する

  * パッケージをレビュー依頼する

  * レビューが承認されたらSCMリクエストを送る

  * 必要なブランチをリクエストする

  * 各ブランチでリリースする

#### パッケージを準備する

レビュー依頼をするときには、specファイルとSRPMを提出する必要があります。
fedora-reviewを使うとある程度事前にパッケージの体裁をチェックすることも可能です。

```
$ fedora-review --rpm-spec --name (SRPMのファイル)
```


あらかじめチェックをしておくとよいでしょう。

#### パッケージをレビュー依頼する

手元でパッケージの用意ができたら、レビュワーが参照できるようにspecやSRPMを公開の場所にアップロードします。
アップロードができたら、パッケージのレビューを依頼します。

レビュー依頼には、[request for review in bugzilla](https://bugzilla.redhat.com/bugzilla/enter_bug.cgi?product=Fedora&format=fedora-review)を使って、雛形から作成するとよいでしょう。

すでに`packager`グループに入っている場合、`FE-NEEDSPONSOR`フラグの設定は不要です。
SentencePieceの場合以下のようなレビューリクエストを作成しました。

  * [Bug 1758036 - Review Request: sentencepiece - unsupervised text tokenizer for Neural Network-based text generation](https://bugzilla.redhat.com/show_bug.cgi?id=1758036)

#### レビューが承認されたらSCMリクエストを送る

SCMリクエストはリポジトリを作成してもらうために行います。fedpkgコマンドを使ってパッケージ名と、承認されたBugzillaの番号を指定します。
SentencePieceの場合、次のようにコマンドを実行しました。

```
$ fedpkg request-repo sentencepiece 1758036
```


上記コマンドを実行すると、以下のようなissueが自動的に作成されます。

  * [#18136 New Repo for "rpms/sentencepiece"](https://pagure.io/releng/fedora-scm-requests/issue/18136)

このissueはリポジトリが作成されると閉じられます。

#### 必要なブランチをリクエストする

SCMリクエストが承認されたら、次は作業に必要なブランチをリクエストします。fedpkgコマンドのrequest-branchに必要なブランチを指定します。
SentencePieceの場合、f31とepel8、epel7のリクエストを行いました。

```
$ fedpkg clone sentencepieces
$ cd sentencepiece
$ fedpkg request-branch f31
https://pagure.io/releng/fedora-scm-requests/issue/18148
$ fedpkg request-branch epel8
https://pagure.io/releng/fedora-scm-requests/issue/18149
https://pagure.io/releng/fedora-scm-requests/issue/18150
$ fedpkg request-branch epel7
https://pagure.io/releng/fedora-scm-requests/issue/18151
```


リクエストを行うと、上記のようにブランチごとにissueが作られます。ブランチが作成されるとissueが閉じられる運用になっています。

#### 各ブランチでリリースする

ブランチが作成されたら、次は各ブランチでリリースを行います。
specファイルやソースアーカイブをインポートしたら、fedpkg buildでパッケージをビルドします。その後、fedpkg updateを行うことで対象ブランチでリリースを行えます。
SentencePieceの場合、epel7ブランチのリリースは次のようになりました。

  * [bugfix update in Fedora EPEL 7 for sentencepiece ](https://bodhi.fedoraproject.org/updates/FEDORA-EPEL-2019-e874c45c17)

既定では2週間ほどでリポジトリに反映されるようです。

### まとめ

今回はEPELでパッケージをリリースできるようにしたいという人向けに手順を説明しました。
手元で必要に迫られてパッケージ化したものがあれば、ぜひアップストリーム(Fedoraプロジェクト)へフィードバックしていきませんか？
