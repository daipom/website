---
tags:
- groonga
- redmine
- presentation
- use-case
title: 'redmine.tokyo第17回勉強会：Redmine検索の未来像 #redmineT'
---
[Redmineの全文検索プラグイン](https://github.com/clear-code/redmine_full_text_search/)を開発している須藤です。
<!--more-->


2019年11月2日に[redmine.tokyo第17回勉強会](https://redmine.tokyo/projects/shinared/wiki/%E7%AC%AC17%E5%9B%9E%E5%8B%89%E5%BC%B7%E4%BC%9A)が開催されました。私は参加していないのですが、この半年一緒に全文検索プラグインを開発してきた島津製作所の赤羽根さんが[全文検索プラグインの導入結果を報告](https://akahane92.page.link/fts20191102)しました。私はこれに合わせて技術面からの補足として次の資料を作成しました。

<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kou/redmine-tokyo-17/viewer.html"
          width="640" height="404"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kou/redmine-tokyo-17/" title="Redmine検索の未来像">Redmine検索の未来像</a>
  </div>
</div>


<div class="youtube">
  <iframe width="560" height="315" src="//www.youtube.com/embed/556_kQtzA-A?cc_load_policy=1" frameborder="0" allowfullscreen></iframe>
</div>


関連リンク：

  * [スライド（Rabbit Slide Show）](https://slide.rabbit-shocker.org/authors/kou/redmine-tokyo-17/)

  * [スライド（SlideShare）](https://www.slideshare.net/kou/redminetokyo17)

  * [動画](https://www.youtube.com/watch?v=556_kQtzA-A)

  * [リポジトリー](https://gitlab.com/ktou/rabbit-slide-kou-redmine-tokyo-17)

### 内容

従来の検索システムのなにが課題でそれをどう解決したかについてまとめました。しかし、従来の課題を解決すればすごく明るい未来があるわけではありません。少し明るい未来があるだけです。すごく明るい未来にするためにはさらになにに取り組めばよいのかについてもまとめています。そして、その取り組みはすでに始まっています。

今はクリアコードと島津製作所さんで一緒に取り組んでいますが、できれば他の会社も巻き込んですごく明るい未来を目指したいと思っています。私たちの成果はRedmine本体同様ユーザーが自由に使えるソフトウェアとして公開しています。もし、他の会社も一緒に取り組んだ場合もその成果はユーザーが自由に使えるソフトウェアとして公開します。一緒に取り組まなくても使えるように公開しますが、一緒に取り組むことですごく明るい未来がより早く実現できます。

一緒にすごく明るい未来を目指したい方は[お問い合わせ](/contact/)ください。

### まとめ

クリアコードと島津製作所さんは一緒にRedmineの全文検索プラグインを開発していました。その成果を島津製作所の赤羽根さんがredmine.tokyo第17回勉強会で紹介しました。私も紹介する資料を用意しました。

Redmineの検索機能をよりよくして、Redmineに蓄積した知識をより活用するために一緒に取り組みたい企業を募集しています。興味が湧いてきた方は[お問い合わせ](/contact/)ください。
