---
tags:
- feedback
title: ノータブルフィードバック4 - 開発者が知らない言語圏に固有の問題の報告
author: piro_or
---
結城です。
<!--more-->


[ノータブルコード](/blog/category/notable-code.html)に便乗して、実際のOSSのフィードバックを見て「ノータブルフィードバック」と題してお届けする記事の4つ目は、筆者が[チャットツールのZulipに対して行ったフィードバック](https://github.com/zulip/zulip/issues/9396)です。

### 実際の報告

> ■タイトル
> typeahead: "keydown" events for Enter and Arrow keys should be ignored while "composition"
≪インクリメンタル検索: Enterキーと矢印キーのkeydownイベントは「コンポジション」の最中は無視されるべき≫
> ■説明
> First, I describe what is the "composition".
≪最初に、「コンポジション」とは何かを説明します。≫
> In CJK language regions, people use some software named "IM" (input method) to input heir local language text. For example, when I search a Japanese term "日本語" (means "Japanese language") in a Zulip instance with Firefox, I need to do:
≪CJK（中国語・日本語・韓国語）の言語の地域では、人々は彼らの地域言語のテキストを入力するために「IM（インプットメソッド）」と呼ばれるソフトウェアを使っています。たとえば、私が日本語の単語「日本語」をFirefoxで表示したZulipで検索するとき、私は以下のような操作をする必要があります：≫
> [["  1. Click the search field.\n≪検索欄をクリック。≫"], ["  1. Activate the IM. The \"composition\" session starts.\n≪IMを有効化する。「コンポジション」のセッションが始まる。≫"], ["  1. Type keys: `n`, `i`, `h`, `o`, and `n`. (in a composition session)\n≪（コンポジションのセッションの中で）n, i, h, o, nとキーを入力する。≫"], ["  1. Hit the Space key to convert the text to Japanese term. \"日本\" is suggested. (in a composition session)\n≪（コンポジションのセッションの中で）テキストを日本語の単語に変換するために、スペースキーを押す。「日本」が提案される。≫"], ["  1. Hit the Enter key to determine the text \"日本\". (in a composition session)\n≪（コンポジションのセッションの中で）「日本語」というテキストを確定するために、Enterキーを押す。≫"], ["  1. Type keys: `g`, and `o`. (in a composition session)\n≪（コンポジションのセッションの中で）g, oとキーを入力する。≫"], ["  1. Hit the Space key to convert the text to Japanese term. \"語\" is suggested. (in a composition session)\n≪（コンポジションのセッションの中で）テキストを日本語の単語に変換するために、スペースキーを押す。「語」が提案される。≫"], ["  1. Hit the Enter key to determine the text \"語\". (in a composition session)\n≪（コンポジションのセッションの中で）「語」というテキストを確定するために、Enterキーを押す。≫"], ["  1. Deactivate the IM. The \"composition\" session ends.\n≪IMを無効化する。「コンポジション」のセッションが終了する。≫"], ["  1. Hit the Enter key again to search \"日本語\" on Zulip.\n≪「日本語」をZulipで検索するために、Enterキーをもう1度押す。≫"]]
> While the composition session, "keydown" events for special keys (Enter and Arrow) are handled by the IM to choose a term from variations >or determine the choice. Thus I hit the Enter key three times in this case. The first time and the second are notified only to IM, so Zulip receives only the third time.
≪コンポジションのセッション中は、（Enterや矢印などの）特別なキーに対するkeydownイベントは、IMによって、複数の候補の中から単語を選択したり選択を確定したりするために使われます。そのため、私はEnterキーをこの例では3回押しています。1回目と2回目はIMに対してのみ通知されるため、Zulipは3回目のみを受け取ります。≫
> And, there is one problem on lately development build of Firefox.
≪そして、最近のFirefoxの開発者向けビルドでは一つ問題があります。≫
> [["  * 1446401 - Start to dispatch keydown/keyup events even during composition in Nightly and early Beta\n\nhttps://bugzilla.mozilla.org/show_bug.cgi?id=1446401\n≪1446401 - Nightlyと初期ベータ版で、コンポジション中のkeydownとkeyupイベントを通知するようにする≫"], ["  * Intent to ship: Start to dispatch \"keydown\" and \"keyup\" events even if composing (only in Nightly and early Beta) - Google Group\n\nhttps://groups.google.com/forum/#!topic/mozilla.dev.platform/oZEz5JH9ZK8/discussion\n≪リリースしようとしているもの: Nightlyと初期ベータ版のみにおいて、コンポジション中にkeydownとkeyupイベントが通知されるようになります≫"]]
> Due to the change, now development build of Firefox (aka Nightly) notifies "keydown" events to the webpage, for all keyboard operations while "composition" sessions. As the result, the search field shows suggested results while I'm typing alphabet keys. This is good improvement, but there is one new problem: when I hit the Enter key to determine a chosen term, it is also notified to Zulip. Thus, when I just determine the first part term "日本" of the joined term "日本語", Zulip unexpectedly handles the Enter key to search the part "日本" and I cannot input following part "語" anymroe.
≪この変更のため、Firefoxの開発版ビルド（別名Nightly）は「コンポジション」セッション中の物も含めすべてのキーボード操作に対し、keydownイベントをWebページに通知します。その結果、検索欄は私がアルファベットのキーを入力している最中に候補を表示します。これは良い改善ですが、しかし新たに1つの問題が発生しています:選択を確定するために私がEnterキーを押したとき、それがZulipにも通知されます。そのため、私が「日本語」という複合語の一部として「日本」を確定しようとしたときにまで、Zulipは意図せずそのEnterキーの操作を「日本」という単語を検索するための物として取り扱い、続く「語」という単語を私は入力することができません。≫
> To fix this problem, Zulip need to ignore keydown events for Enter and Arrow keys while the composition session. While a composition session, all keydown events have the `isComposing` (https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/isComposing) property with `true` value, so you just need to return when the property is `true`. Could you apply this change to Zulip?
≪この問題を直すためには、ZulipはEnterと矢印キーのkeyダウンイベントをコンポジションのセッション中は無視する必要があります。コンポジションのセッション中は、すべてのkeydownイベントがisComposingというプロパティをtrueという値を伴って持っています。そのため、そのプロパティの値がtrueであるときはすぐに処理をリターンする必要のみあります。この変更をZulipに反映してもらえませんか？
> Environment:
≪環境≫
> [["  * Zulip 1.8.0"], ["  * Mozilla Firefox Nightly 62.0a1"], ["  * Ubuntu 16.04LTS + IIIMF ATOK X3"]]


### フィードバックの経緯

筆者の所属会社では、社内のチャットとしてSlackではなく独自にサーバーを立てたZulipを使っています。その運用中に、Firefoxの開発版で問題になる箇所があることに気付いたため、開発元にフィードバックしたという事例です。

日本語のようにキーボードのキーの数よりも入力したい文字の種類が圧倒的に多い言語では、文字入力専用のソフトウェアを介して文字を入力するのが一般的です。また、日本語の文章では単語間にスペースが入らないのが一般的です。このような言語には他に中国語と韓国語もあり、テキストデータやその入力、文字コードの取り扱いの文脈ではこれらの言語がよく話題に挙がるため、3つをひっくるめて *「CJK」* [^0]と呼ぶことがあります。

CJKを想定していない機能とCJKの言語はあまり相性が良くない傾向にあり、「インクリメンタル検索」もその一例です。英語のようにキーと入力したい文字とがほぼ1対1で対応している言語では、文字を入力したそばから検索が進行するインクリメンタル検索が好まれます。しかし、CJKの言語では「入力中の文字が最終的に入力したい文字とは異なっている（未確定である）」という状態があります。この状態を考慮していない実装でインクリメンタル検索が発動すると、日本人にとっては*文字入力がそもそもできない*という困った事態になります。

この報告を行った前後の時期には、Firefoxの仕様変更でこの種の問題が起こりやすくなっていたため、Mozillaからもソフトウェア開発者やWeb制作関係者向けに公に注意が呼びかけられていました。報告の中で紹介している記事が、まさにそれです。

### 注目したい点

CJKを母語としない開発者にはこういった事情がなかなか分からないらしく、*「そもそもどういう前提があるのか」ということから詳しく説明しないと、問題に対処してもらえない*ということも珍しくありません。そのためこの報告では、CJKの言語ではどうやって文字を入力するのか、その中でこの問題がどういう影響を及ぼすのか、ということを詳しく述べました。その後のコメントで実装の改修案を併せて示したこともあってか、開発者の方には問題をスムーズに認識してもらうことができ、迅速に解決してもらえました。

言語のように自分の生活と密接に結び付いた領域の話は、報告者にとってもあまりに当たり前のことすぎて、明確に言葉で説明するのが逆に難しいものです。報告者が「なんでこれが問題だと分かってくれないんだ！？」とフラストレーションを感じる一方で、その報告内容は、*報告を受けた側から見ると「要領を得ない言葉足らずのもの」となっている*ことも多く、何が問題なのかを理解できないために、うっかり適切でない判断をしてしまうことがあります。そのような悲しいすれ違いを避けるためにも、報告者は*自分の状態を「自明のもの」と考えず客観視して、どこが相手からは見えていない部分なのかを探り、自分から情報を積極的に開示する*姿勢を保つことが望ましいです。

なお、同様のことがアラビア語などの書字方向がRTL（右から左に文字が流れる）の言語にも言えるようです。LTR（左から右）の言語だけを想定したソフトウェアは（特にGUIが）、RTLの言語で使うと悲惨なことになりがちなようです。皆さんがOSSを公開したら、もしかするとそういった言語圏の方から逆にフィードバックを受けることになるかもしれませんので、そのときはぜひ耳を傾けてください。

ところで、以下の文のおかしいところに皆さんは気が付かれましたか？

> In CJK language regions, people use some software named "IM" (input method) to input heir local language text.


実はこの文の「heir」は誤記で、「their」が正しいです。ベテランでもこのようなミスタイプが残ったまま報告してしまうことがある[^1]と思うと、皆さんも、英語の間違いを過度に恐れる必要はないのだなと勇気づけられるのではないでしょうか。

### まとめ

ノータブルフィードバックの4回目として、開発者が日本語に詳しくないときに日本語入力の場面に固有の不具合を報告するフィードバック例をご紹介しました。

このような「身近なところで遭遇したつまずきをOSS開発プロジェクトにフィードバックする」ということをテーマに、*まだOSSにフィードバックをしたことがない人の背中を押す解説書 「これでできる！ はじめてのOSSフィードバックガイド ～ #駆け出しエンジニアと繋がりたい と言ってた私が野生のつよいエンジニアとつながるのに必要だったこと～」を、本日付けでリリースしました*。

本記事の内容はこの本からの抜粋となっています。全文を読む方法には以下の選択肢があります。

  * *EPUB/PDF形式の電子書籍データ*は、現時点では[結城が個人的に運営しているBOOTH](https://sysadgirl.booth.pm/)および[Amazon Kindleダイレクト・パブリッシング](https://www.amazon.co.jp/%E3%81%93%E3%82%8C%E3%81%A7%E3%81%A7%E3%81%8D%E3%82%8B%EF%BC%81-%E3%81%AF%E3%81%98%E3%82%81%E3%81%A6%E3%81%AEOSS%E3%83%95%E3%82%A3%E3%83%BC%E3%83%89%E3%83%90%E3%83%83%E3%82%AF%E3%82%AC%E3%82%A4%E3%83%89-%EF%BD%9E-%E9%A7%86%E3%81%91%E5%87%BA%E3%81%97%E3%82%A8%E3%83%B3%E3%82%B8%E3%83%8B%E3%82%A2%E3%81%A8%E7%B9%8B%E3%81%8C%E3%82%8A%E3%81%9F%E3%81%84-%E3%81%A8%E8%A8%80%E3%81%A3%E3%81%A6%E3%81%9F%E7%A7%81%E3%81%8C%E9%87%8E%E7%94%9F%E3%81%AE%E3%81%A4%E3%82%88%E3%81%84%E3%82%A8%E3%83%B3%E3%82%B8%E3%83%8B%E3%82%A2%E3%81%A8%E3%81%A4%E3%81%AA%E3%81%8C%E3%82%8B%E3%81%AE%E3%81%AB%E5%BF%85%E8%A6%81%E3%81%A0%E3%81%A3%E3%81%9F%E3%81%93%E3%81%A8%EF%BD%9E-ebook/dp/B0859MFRJ9/ref=sr_1_1?keywords=%E3%81%93%E3%82%8C%E3%81%A7%E3%81%A7%E3%81%8D%E3%82%8B%EF%BC%81+%E3%81%AF%E3%81%98%E3%82%81%E3%81%A6%E3%81%AEOSS%E3%83%95%E3%82%A3%E3%83%BC%E3%83%89%E3%83%90%E3%83%83%E3%82%AF%E3%82%AC%E3%82%A4%E3%83%89&qid=1582966772&sr=8-1)でご購入いただけます。

  * *紙媒体版*は、「技術書同人誌博覧会」や今後の「技術書典」に持ち込ませて頂く予定です。他には、同人誌書店での委託販売・通販なども検討中ですが、具体的な予定はまだありません。

  * [本の原稿のリポジトリ](https://github.com/oss-gate/first-feedback-guidebook)では全文を読めるほか、リポジトリをcloneしてビルドすると、これらのチャンネルで頒布している物と同等のデータをお手元で作成できます。腕に自信のある方はチャレンジしてみてもいいかもしれません。（そういうわけなので、有料の販売については投げ銭もしくはビルド作業の手間賃と考えて頂ければ幸いです）

[^0]: そのまま「Chinese, Japanese and Korean」の略です。

[^1]: ちなみに筆者は、コミットログのメッセージでもよくミスタイプをしていますが、そのままpushしてしまっています（pushした後で気が付くことが多い）。
