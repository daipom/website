---
tags: []
title: Terraformの導入 - 検証環境をコマンドで立ち上げられるようにする その２
---
### はじめに

クラウドサービスを扱うのにTerraformが便利ということに気づいたけれど、解説を書くとなると長くなってしまった…と気落ちしている畑ケです。
<!--more-->


[前回の記事]({% post_url 2020-05-25-index %})では、TerraformでWindows系のAzureのインスタンスを立ち上げるのに最低限必要であった

  * リソースグループ

  * バーチャルネットワーク

  * サブネット

  * ネットワークインターフェース

  * 仮想マシンの設定

について解説しました。今回の記事では、

  * セキュリティグループ

  * ネットワークインターフェースとセキュリティグループのアソシエーション（関連づけ）

  * パブリックIPアドレス

の項目を解説します。ここまでの設定が流し込まれていると、RDPで接続が可能なWindows Server 2019 Datacenterのインスタンスが作成できます。

Azure上のWindows 10のインスタンスについてはボリュームライセンスを用意する必要があります。ライセンス認証については https://docs.microsoft.com/ja-jp/windows/deployment/windows-10-subscription-activation の記事をご覧ください。
この記事ではWindows Server 2019 Datacenterを用いて解説します。

### 変数の切り出し

Terraformには変数があり、これによりインターネットに公開できない情報を別ファイルに切り出し、Gitのリポジトリに入れないようにできます。

例えば、variable.tfに以下の変数の定義を書くことで、tfファイルで使用する変数を定義することができます。

```terraform
variable "windows-username" {}
variable "windows-password" {}
```


これに対応するユーザー名とパスワードをterraform.tfvarsファイルで定義します。

```tfvars
windows-username = "clearcode"
windows-password = "CC/changeme1"
```


この変数を使うには、`var.windows-username`, `var.windows-password`としてtfファイル中から参照できます。

main.tfファイルを以下のように書き換えると、terraform.tfvarsに定義した変数の値を参照します。

```diff
diff --git a/main.tf b/main.tf
index 0148da0..c0e23a4 100644
--- a/main.tf
+++ b/main.tf
@@ -63,8 +63,8 @@ resource "azurerm_virtual_machine" "winservtesting" {
 
   os_profile {
     computer_name  = "cc-winserv"
-    admin_username = "clearcode"
-    admin_password = "CC/changeme1"
+    admin_username = var.windows-username
+    admin_password = var.windows-password
   }
 
   os_profile_windows_config {
```


#### ネットワークセキュリティグループ

Azureにはネットワークに対して、セキュリティのルールを決める仕組みがあります。これがセキュリティグループです。

RDPの通信と、WinRMの双方向通信を許可するネットワークセキュリティグループを作成してみます。

```terraform
resource "azurerm_network_security_group" "testing" {
  name                = "ClearCodeSecurityGroup"
  location            = "Japan East"
  resource_group_name = azurerm_resource_group.clearcode.name

  security_rule {
    name                       = "RDP"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "3389"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "WinRM"
    priority                   = 998
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "5986"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "WinRM-out"
    priority                   = 100
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "5985"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  tags = {
    environment = "Creating with Terraform"
  }
}
```


ネットワークセキュリティグループに関して詳しくは https://www.terraform.io/docs/providers/azurerm/r/network_security_group.html を参照してください。

#### ネットワークインターフェースとネットワークセキュリティグループのアソシエーション（関連づけ）

上節で作成したセキュリティグループはネットワークインターフェースに関連づけをしないといけません。そこで登場するのが、ネットワークインターフェースセキュリティーグループアソシエーションのリソースです。

```terraform
# Connect the security group to the network interface
resource "azurerm_network_interface_security_group_association" "testing" {
  network_interface_id      = azurerm_network_interface.testing.id
  network_security_group_id = azurerm_network_security_group.testing.id
}
```


AzureRMのネットワークインターフェースとネットワークセキュリティグループの関連づけに関して詳しくは https://www.terraform.io/docs/providers/azurerm/r/network_interface_security_group_association.html を参照してください。

この設定では、先に定義したtestingセキュリティーグループをtestingネットワークインターフェースへ関連づけるルールを記述しています。

#### パブリックIPアドレス

ようやくパブリックIPアドレスを定義するリソースの解説まで到達しました。
clearcodeリソースグループへ動的なパブリックIPアドレスを定義するには以下のようにします。

```terrraform
resource "azurerm_public_ip" "testing" {
  name                    = "clearcode-collector-pip"
  location                = azurerm_resource_group.clearcode.location
  resource_group_name     = azurerm_resource_group.clearcode.name
  allocation_method       = "Dynamic"
  idle_timeout_in_minutes = 30

  tags = {
    environment = "clearcode-testing-pip"
  }
}
```


AzureRMのパプリックアドレスに関して詳しくは https://www.terraform.io/docs/providers/azurerm/r/public_ip.html を参照してください。

ここまでで、AzureのWindowsインスタンスへパブリックIPアドレスを付与するための準備が整いました。

以下のようにインスタンスのネットワークインターフェースリソースに対して、パブリックIPアドレスのリソースを紐づけることで、WindowsのインスタンスがパブリックIPアドレスを持つようにできます。

```diff
diff --git a/main.tf b/main.tf
index f8d326c..0d428b1 100644
--- a/main.tf
+++ b/main.tf
@@ -98,6 +98,7 @@ resource "azurerm_network_interface" "testing" {
     subnet_id                     = azurerm_subnet.internal.id
     private_ip_address_allocation = "Static"
     private_ip_address            = "10.5.2.4"
+    public_ip_address_id          = azurerm_public_ip.testing.id
   }
 }
 
```


#### ここまでの設定の適用

ここまでの設定を適用するには前回までのリソースを消しておかないといけません。

```console
$ terraform destroy -auto-approve
```


を実行して前回までのリソースを削除しておきます。

再度、

```console
$ terraform apply -auto-approve
```


を実行することで、ここまでに書き下した設定がAzureへ適用され、Terraformで設定した設定が流し込まれたインスタンスが立ち上がります。
ここまでの設定で立ち上げたWindows Server 2019 DatacenterインスタンスはRDPプロトコルで接続ができます。

ここまでに作成したTerraformスクリプトは https://gitlab.com/clear-code/terraform-example/-/tree/ab9fe8c5c4ae4420b475eb37dc2d51ec8c3af252 に置いてあります。

### まとめ

TerraformでWindows Server 2019 DatacenterのインスタンスへRDPで接続できるインスタンスを作成するための設定を駆け足で解説してきました。
WinRMによるAnsibleでのプロビジョニングに関しては記事の長さの都合上さらに[追加の記事]({% post_url 2020-05-27-index %})で解説します。
