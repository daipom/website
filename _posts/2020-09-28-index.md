---
title: Debian Developerになるには
tags:
  - debian
---
### はじめに

最近Debian Developerになった林です。
今回は、どうするとDebian Developerになれるのかについて紹介します。
<!--more-->


### Debian Maintainerになろう

まずはDebian Maintainerである必要があります。いきなりDebian Developerになることはできません。[^0]
Debian Developerには任意のパッケージをアップロードできる権限が与えられているからです。

Debian Maintainerとは、特定のパッケージについて、アップロードすることを許可されている人のことです。
もしまだDebian Maintainerでないなら段階を踏んでDebian Maintainerになりましょう。

以前[Debian Maintainerになるには]({% post_url 2018-08-30-index %})という記事を書いたのでそれが参考になるかもしれません。

### Debian Developerになるための申請をする

Debian Maintainerになったら次はDebian Developerになるための申請をしましょう。
申請は https://nm.debian.org/ から行ないます。

その際には、申請フォームから（gpgでクリアテキスト署名されている）次のような文面を送ります。

```
For nm.debian.org, at 2020-05-17:
I agree to uphold the Social Contract, the Debian Free Software Guidelines,
and the Debian Code of Conduct, in my Debian work.
I have read the Debian Machine Usage Policy and I accept them.
```


これ以降は、次のステップを順に踏んでいく必要があります。

  * Declaration of Intent

  * SC/DFSG/DMUP agreement

  * Advocate

  * Key consistency checks

  * Application Manager report

  * Front Desk or DAM approval

### Declaration of Intent

なぜDebian Developerになりたいのかを表明するのがDeclaration of Intentというプロセスです。
こんなことをやりたいんだという熱い思いを記述します。

投稿した内容は、[debian-newmaint@lists.debian.org](https://lists.debian.org/debian-newmaint/)というメーリングリストへ送られます。

### SC/DFSG/DMUP agreement

各種規定に同意することを表明するプロセスです。各種規定とは以下を指します。

  * Debian Social Contract (SC)

  * Debian Free Software Guidelines (DFSG)

  * Debian Code of Conduct (CoC)

  * Debian Machine Usage Policies (DMUP)

これも署名して送ります。

### Advocate

既存のDebian Developerに推薦してもらうプロセスです。
あらかじめ推薦してもらえそうなあてがないとこのプロセスで詰んでしまうので、事前に根回ししておきましょう。

最近は[東京Debian勉強会](https://tokyodebian-team.pages.debian.net/)と[関西Debian勉強会](https://wiki.debian.org/KansaiDebianMeeting)が合同でオンラインの勉強会を開催しているので、ここに参加してみるとよいかもしれません。
次回はおそらく10月の中旬開催で、そのうち[connpass](https://connpass.com/)の[Debian JP](https://debianjp.connpass.com/)にてイベントの案内がでるはずです。

### Key consistency checks

申請者のGPG鍵に問題ないかチェックするプロセスです。
従来は既存のDebian Developer二人から署名（キーサイン）してもらっていないといけませんでしたが、昨今COVID-19の影響によりこの条件は緩和されました。

コミュニティでの活動で充分な成果をあげているとDebian Developerに認められ、推薦してもらえた場合もOKという条件になるようです。
もちろん従来のようにキーサインすることを推奨していますが、必須ではありません。

このあたりの詳細については、[DAM Key and identity requirements](https://lists.debian.org/debian-devel-announce/2020/09/msg00000.html)という投稿が参考になるでしょう。

### Application Manager report

申請者がDebian Developerとしてふさわしいかをチェックするプロセスです。

試験官が割り当てられ、出題された問題に答えます。内容としては以下の2つの試験を受けます。

  * Debian Developer Philosophy and Procedures Test

  * Debian Developer Task and Skills Test

Debian Developer Philosophy and Procedures Testは申請者がDebianについて正しく理解しているかを確認する試験です。
こんなときはDebian Developerとしてどう振る舞うか、などが問われます。
試験官とはメールでやりとりを行い、充分な理解があると判断されたら次のステップに進みます。

Debian Developer Task and Skills Testは申請者のパッケージングに関する知識やツールを使いこなすスキルがあるかを問う試験です。
実際にバグを修正したりすることでその能力を示す必要があります。

上記の試験を終えると、試験官による合否のレポートが提出されます。

### Front Desk or DAM approval

Application Manager reportの結果をもとに、Debianのアカウントを管理している人によるチェックを受けるプロセスです。
問題がなければDebian Developerとして認められ、アカウントの発行手続きが取られます。

こうしてDebian Developerになることができます。[^1]

### まとめ

今回は、どうするとDebian Developerになれるのかについて紹介しました。
Debian Developerであるやまねさんによる[なれる！Debian開発者](https://www.slideshare.net/henrich_d/debian-45)という資料も参考になるでしょう。

あまり知られていないかもしれませんが、Debian Developerになると[MemberBenefits](https://wiki.debian.org/MemberBenefits)にあるように各種優遇も受けられます。
[LWN](http://www.lwn.net/)を購読できたり、[Gandi](http://www.gandi.net/)でドメインをちょっとお安く維持（Level "E"扱いになる）できたりします。

Debianを好きな人は、ぜひDebian Developerになって活動してみませんか？

[^0]: よっぽど経験豊富でなんでこの人いまだに申請していないんだというケースだったら別かもしれません。

[^1]: Debian Developerになるのにどれくらいの期間かかるかは、人によって様々です。試験官や申請者が忙しかったりなどの要因があるからです。私の場合はちまちま進めていたので4ヶ月くらいでした。
