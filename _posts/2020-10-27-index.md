---
tags: []
title: BrowserSelectorのすすめ - フリーソフトウェアでマルチブラウザをもっと便利にする
---
企業の運用では複数のブラウザを使い分けることがままあります。例えばこんな具合です。
<!--more-->


![マルチブラウザ環境の例]({{ "/images/blog/20201027_0.png" | relative_url }} "マルチブラウザ環境の例")

「どうして一つのブラウザで両方見ないのか？」という疑問を覚えるかもしれませんが、このような構成を採用する理由には、大きく分けて以下の二種類があります。

  * *レガシーブラウザを使う必要がある場合。* 典型的には、社内サイトが古いIEにしか対応していない場合がこれに該当します。インターネットでは、既にIEのサポートを終了していて、IEでは正常に表示できなくなっているアプリやサイトが少なくありません。このため、社内サイトの閲覧用に限って、IEを温存させる必要があるというのは、現実の運用では非常によくあるパターンです。

  * *セキュリティ的に分けたい場合。* これは用途に応じてブラウザを分けてしまおうというアイデアです。従来から「Web分離」という文脈で論じられてきた対策の一つのバリエーションで、業務用とインターネット閲覧でブラウザを分けることで、堅牢性を高めようというセキュリティ戦略です。

クリアコードでは、こういったユースケースをサポートするために[BrowserSelector](https://gitlab.com/clear-code/BrowserSelector)というツールを開発しています。

### BrowserSelectorとは何か？

簡単に言えば *「URLごとにブラウザを自動的に切り替えるツール」* です。

冒頭に掲げた図のケースであれば、次のように設定ファイルを置くだけで、自動的にブラウザを切り替えられるようになります。

```ini
[Common]
DefaultBrowser=firefox
CloseEmptyTab=1

[URLPatterns]
0001=http*://intra.example.com/*|ie
```


この基本機能に加えて、次のような機能も備えています。

*（機能1）設定の集中管理に対応*

  * 共有フォルダに設定ファイルをおくことで、一つの設定ファイルですべての端末の動作を制御できます。

  * 同じように、Windowsのグループポリシーを利用して、レジストリを通じて設定を集中管理することができます。

*（機能2）モダンブラウザサポート*

  * Google ChromeやMozilla Firefoxなどのブラウザに対応しています。

  * FirefoxやGoogle Chromeでリンクをクリックした際に、他のブラウザをスムーズに起動できます。

*（機能3）外部アプリとの連携機能*

  * BrowserSelectorをWindowsの「標準のブラウザ」に設定できます。

  * OutlookやWordなどの外部アプリケーションからURLを開いた際に、自動的にブラウザが切り替えられます。

昨年、最初のバージョンを公開してから、導入企業様のフィードバックを集めつつ、着実に機能が増えていっています。

### BrowserSelectorの動作イメージ

動作イメージを掴んでもらうために動画を用意しました。

<div class="youtube">
  <iframe width="700" height="400" src="https://www.youtube.com/embed/F_-O1xptCFU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>


### BrowserSelectorを使い始めるには

(1) [BrowserSelectorのリリースページ](https://gitlab.com/clear-code/browserselector/-/releases)にアクセスしてください。

![利用方法1]({{ "/images/blog/20201027_4.png" | relative_url }} "利用方法1")

(2) ページのリンクからMSIインストーラをダウンロードして実行します。

![利用方法2]({{ "/images/blog/20201027_2.png" | relative_url }} "利用方法2")

(3) 設定ファイルを「C:\Program Files(x86)\ClearCode\BrowserSelector」に配置します。

![利用方法3]({{ "/images/blog/20201027_3.png" | relative_url }} "利用方法3")

基本的にこれで利用できるようになります。設定可能な項目の一覧などの詳しい使い方は [BrowserSelector利用ガイド](/downloads/browserselector-guide.html) (docx/72kb)を配布しているので、そちらを参照してください。

### ライセンスについて

BrowserSelectorはMITライセンスで配布されている自由なソフトウェアです。

サポートが必要な場合は、[お問い合わせフォーム](/contact)からご連絡ください。

### 関連リンク

  * [BrowserSelectorプロジェクト公式ページ](https://gitlab.com/clear-code/BrowserSelector)

  * [BrowserSelectorサービスメニュー](/services/browserselector.html)

  * [BrowserSelector利用ガイド](/downloads/browserselector-guide.html)

  * [2019年11月28日 ブラウザー切替ツールBrowser Selectorをリリース](/press-releases/20191128-browser-selector.html)
