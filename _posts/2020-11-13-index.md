---
tags:
- groonga
- presentation
title: 'PostgreSQL Conference Japan 2020：PGroonga 運用技法 ～PGroonga の WAL を放置していませんか？～
  #pgcon20j'
---
2020年11月13日(金)に[PostgreSQL Conference Japan 2020](https://www.postgresql.jp/jpug-pgcon2020)が開催されます。
私は、「PGroonga 運用技法 ～PGroonga の WAL を放置していませんか？～」という題名で、PGroongaのWALを使う上での注意点を紹介します。
<!--more-->


当日使用する資料は、以下に公開しています。

<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/komainu8/postgresql-conference-japan-2020/viewer.html"
          width="640" height="524"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/komainu8/postgresql-conference-japan-2020/" title="PGroonga 運用技法 ～PGroonga の WAL を放置していませんか？～">PGroonga 運用技法 ～PGroonga の WAL を放置していませんか？～</a>
  </div>
</div>


関連リンク：

  * [スライド(Rabbit Slide Show)](https://slide.rabbit-shocker.org/authors/komainu8/postgresql-conference-japan-2020/)

  * [リポジトリー](https://github.com/komainu8/rabbit-slide-komainu8-postgresql-conference-japan-2020)

### 内容

PostgreSQL で使用できる全文検索の拡張に [PGroonga(ぴーじーるんが)](https://pgroonga.github.io/ja/) という高速で高性能な拡張があります。
PGroongaは、全言語対応の超高速全文検索機能をPostgreSQLで使えるようにする拡張で、安定して高速で、かつ高機能（同義語、表記ゆれや異体字への対応、類似文書検索など）です。

PGroonga はインストールも難しくなく、インデックスの設定もそれほど複雑ではないので、全言語対応の超高速全文検索機能を容易に利用できますが、ストリーミングレプリケーションなどのWALを使用した運用においては注意が必要な点があります。 この講演では、後々トラブルが起きないようにPGroongaのWALをうまく使う方法について紹介しています。

### まとめ

この講演は、PGroongaのWALを既に使っている人、または、これから使おうと考えている人向けの内容になっています。
PGroongaのWALを使うことに興味がある方は、是非、発表資料を確認してみてください。
