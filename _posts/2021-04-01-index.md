---
title: 障害復旧機能を提供するcollectdプラグインの紹介(リモート関連機能)
author: kenhys
---

システムやアプリケーションのメトリクス情報をあつめるソフトウェアの1つに[collectd](https://collectd.org/)があります。

以前、障害復旧機能を提供するcollectdプラグイン(lua-collectd-monitor)について「[障害復旧機能を提供するcollectdプラグインの紹介(ローカル監視機能編)]({% post_url 2021-01-25-index %})」という記事で紹介しました。
[株式会社セナネットワークス様](https://www.sena-networks.co.jp/)からの発注を受けて開発したものです。

ただし紹介したのはそのうちの一部であるローカル監視機能についてでした。
今回は、リモート関連機能について紹介します。

<!--more-->

[lua-collectd-monitor](https://github.com/clear-code/lua-collectd-monitor)は障害復旧機能を提供するcollectdプラグインです。
リモートからリカバリコマンドを受け取って実行したり、ローカルのメトリクスデータをもとにリカバリコマンドを実行できます。

障害復旧機能を提供するcollectdプラグインにはローカル監視の他にもリモート関連の次の2つの機能があります。

* リモートコマンド機能
* リモート更新機能

### リモートコマンド機能とは

あらかじめ定義されたサービスと対応するコマンドをMQTT経由で外部から呼び出し実行させる機能です。
サーバーのサービスを外部から任意のタイミングで再起動させるなどの用途で使います。

### リモート更新機能とは

対象となるサーバーのcollectd.confをMQTT経由で外部から更新し、collectdを再起動させる機能です。
collectdを導入しているサーバーの設定を動的に更新する用途で使います。


では実際に使うにはどうするとよいかを、リモートコマンド機能およびリモート更新機能それぞれについて説明します。

### 動作環境について

lua-collectd-monitorはcollectdのLuaプラグインに機能追加してあることを前提としています。
また、標準では`LuaJIT`での動作を想定しています。

この記事を書いている2021年4月の時点では、upstreamにまだフィードバックした内容が取り込まれていないため、パッチをあてたcollectdが必要です。
CentOS 8であれば、パッチを適用済みのパッケージがあるのでそちらを使うのがおすすめです。

* [CentOS 8向けパッケージ](https://github.com/clear-code/collectd/releases/tag/5.12.0.16.g6e9604f)

Ubuntuを使っている人のためにUbuntu 20.04の導入例を説明します。

* collectd関連のパッケージをインストールする
* lua-collectd-monitorをインストールする
* VerneMQをインストールする
* lua-collectd-monitorを設定する

標準でサポートしている`LuaJIT`ではなく、今回はLua 5.3で動作させてみます。

#### 依存するパッケージをインストールする

```sh
$ sudo add-apt-repository ppa:kenhys/nightly
$ sudo apt-get update
$ sudo apt install lua5.3 liblua5.3-dev luarocks collectd-core libssl-dev make
```

これで collectd-core 5.9.2.g-1ubuntu5.1がインストールされます。

```sh
$ dpkg -l | grep collectd
ii  collectd-core                        5.9.2.g-1ubuntu5.1                amd64        statistics collection and monitoring daemon (core system)
```

#### lua-collectd-monitorをインストールする

次のコマンドを実行し、lua-collectd-monitorをインストールします。

```sh
$ git clone https://github.com/clear-code/lua-collectd-monitor
$ cd lua-collectd-monitor
$ sudo luarocks make
$ sudo luarocks install --server=http://luarocks.org/manifests/ntd luasyslog
$ sudo chmod 755 /usr/local/share/lua/5.3/collectd/monitor/replace-config.lua
```

lua-collectd-monitorは標準では`LuaJIT`での動作を想定しているため、`luasyslog`を使っています。
しかし、`luasyslog`は5.3には対応していないので、fork先の`luasyslog`を使います。

また、`/usr/local/share/lua/5.3/collectd/monitor/replace-config.lua`は`LuaJIT`での実行を前提としているため
`#!/usr/bin/env lua`にインタプリタの指定を書き換えます。


インストールできたら サンプルの`collectd.conf`をコピーします。

```sh
$ sudo cp /usr/share/doc/collectd-core/examples/collectd.conf /etc/collectd/
```

最後に設定ファイルを `/etc/collectd/monitor/config.json` に作成します。

```json
{
  "Host": "localhost:1883",
  "User": "test-user",
  "Password": "test-user",
  "Secure": false,
  "CleanSession": false,
  "ReconnectInterval": 5,
  "QoS": 2,
  "CommandTopic": "test-topic",
  "CommandResultTopic": "test-result-topic",
  "Services": {
    "nginx": {
      "commands" : {
        "restart": "/bin/systemctl restart nginx 2>&1"
      }
    },
    "hello": {
      "commands": {
        "exec": "/bin/echo \"Hello World!\""
      }
    },
    "collectd": {
      "ConfigPath": "/etc/collectd/collectd.conf"
    }
  }
}
```

あとはcollectdを再起動すればインストール完了です。

#### VerneMQをインストールする

リモート監視機能を使う場合には、メッセージのやりとりをMQTTのブローカーを介してやりとりします。
そのため、ブローカーとしてVerneMQをインストールします。

```sh
$ wget https://github.com/vernemq/vernemq/releases/download/1.11.0/vernemq-1.11.0.focal.x86_64.deb
$ sudo apt install ./vernemq-1.11.0.focal.x86_64.deb
$ sudo vi /etc/vernemq/vernemq.conf
```

利用条件に同意するなら`eula=yes`に変更します。


```
## To use this pre-packaged version of VerneMQ you must agree
## to our end user license agreement (EULA).
## The EULA can be found on https://vernemq.com/end-user-license-agreement.
## 
## Default: no
## 
## Acceptable values:
##   - one of: yes, no
accept_eula = yes
```

リモート監視機能を使う際のアカウントとパスワードを設定します。

`/etc/collectd/monitor/config.json` では既定値が以下のようになっています。

* アカウント名: test-user
* パスワード: test-user
* コマンド送受信用のMQTTトピック名: test-topic
* コマンド結果送受信用のMQTTトピック名: test-result-topic

パスワードは`vmq-passwd`コマンドで設定します。

```sh
$ sudo vmq-passwd -c /etc/vernemq/vmq.passwd test-user
```

`vmq.acl`を編集して`test-user`がアクセスできるトピックを設定します。

```sh
$ cat /etc/vernemq/vmq.acl
user test-user
topic test-topic
topic test-result-topic
```


VerneMQをインストールしたのとは違う端末からアクセスできるようにする場合には、
バインドするアドレスを`127.0.0.1`から`0.0.0.0`へ変更しておきます。

```sh
$ sudo grep listener /etc/vernemq/vernemq.conf | \grep -v '^#'
listener.max_connections = 10000
listener.nr_of_acceptors = 10
listener.tcp.default = 0.0.0.0:1883
listener.vmq.clustering = 0.0.0.0:44053
listener.http.default = 0.0.0.0:8888
```

vmq-adminコマンドを実行すると現在の設定値を確認できます。

```sh
$ sudo vmq-admin listener show
+------+---------+---------+-------+------------+-----------+
| type | status  | ip      | port  | mountpoint | max_conns |
+------+---------+---------+-------+------------+-----------+
| http | running | 0.0.0.0 | 8888  |            | 10000     |
+------+---------+---------+-------+------------+-----------+
| vmq  | running | 0.0.0.0 | 44053 |            | 10000     |
+------+---------+---------+-------+------------+-----------+
| mqtt | running | 0.0.0.0 | 1883  |            | 10000     |
+------+---------+---------+-------+------------+-----------+
```

#### lua-collectd-monitorを設定する

lua-collectd-monitorのカスタマイズは次の2つを修正することで行います。

* リモートコマンド機能を利用する場合の設定を追加

`/etc/collectd/collectd.conf.d/lua-monitor-remote.conf` などとしてリモートコマンド機能のための設定ファイルを作成します。

```aconf
LoadPlugin syslog
LoadPlugin memory

<LoadPlugin lua>
    Globals true
</LoadPlugin>

<Plugin syslog>
    LogLevel info
    NotifyLevel OKAY
</Plugin>

<Plugin lua>
    BasePath "/usr/local/share/lua/5.3"
    Script "collectd/monitor/remote.lua"
    <Module "collectd/monitor/remote.lua">
        MonitorConfigPath "/etc/collectd/monitor/config.json"
    </Module>
</Plugin>
```

### リモートコマンド機能を試す

準備ができたので、別のPCからコマンドを実行してみましょう。
`lua-collectd-monitor`にはあらかじめコマンドを送付するためのスクリプトが同梱されているのでこれを使います。

```sh
$ lua lua-collectd-monitor/collectd/monitor/send-command.lua \
  nginx \
  restart \
  --host $HOST \
  --user test-user \
  --password test-user \
  --topic test-topic \
  --result-topic test-result-topic
```

すると次のような結果が返ってくるはずです。

```sh
Send command: {"timestamp":"2021-02-18T09:14:16Z","service":"nginx","task_id":2281613975,"command":"restart"}
{ -- PUBREC{type=5, packet_id=2}
  packet_id = 2,
  type = 5,
  <metatable> = {
    __tostring = <function 1>
  }
}
Received a result: { -- PUBLISH{qos=2, packet_id=1, dup=false, type=3, payload="{\\"task_id\\":2281613975,\\"message\\":\\"\\",\\"timestamp\\":\\"2021-02-18T09:14:16Z\\",\\"code\\":0}", topic="test-result-topic", retain=false}
  dup = false,
  packet_id = 1,
  payload = '{"task_id":2281613975,"message":"","timestamp":"2021-02-18T09:14:16Z","code":0}',
  qos = 2,
  retain = false,
  topic = "test-result-topic",
  type = 3,
  <metatable> = {
    __tostring = <function 1>
  }
}
```

これで対象となるサーバーではnginxのサービスが再起動されているはずです。

### リモート更新機能を試す

`lua-collectd-monitor`にはあらかじめ設定ファイルを更新するためのスクリプトが同梱されているのでこれを使います。
置き換えたいcollectdの設定ファイルsample.confを用意して次のように指定します。

```sh
$ lua lua-collectd-monitor/collectd/monitor/send-config.lua \
  sample.conf \
  --host $HOST \
  --user test-user \
  --password test-user \
  --topic test-topic \
  --result-topic test-result-topic
```

すると次のような結果が返ってくるはずです。

```
Send config: {"timestamp":"2021-04-03T11:48:39Z","task_id":3836689177,"config":"# Updated! r2\nFQDNLookup true\n\nLoadPlugin syslog\n<Plugin syslog>\n  LogLevel info\n</Plugin>\n\nLoadPlugin battery\nLoadPlugin cpu\nLoadPlugin df\nLoadPlugin disk\nLoadPlugin entropy\nLoadPlugin interface\nLoadPlugin irq\nLoadPlugin load\nLoadPlugin memory\nLoadPlugin processes\nLoadPlugin rrdtool\nLoadPlugin swap\nLoadPlugin users\n\n<Plugin rrdtool>\n  DataDir \"/var/lib/collectd/rrd\"\n</Plugin>\n\n<Include \"/etc/collectd/collectd.conf.d\">\n   Filter \"*.conf\"\n</Include>\n"}
{ -- PUBREC{type=5, packet_id=2}
  packet_id = 2,
  type = 5,
  <metatable> = {
    __tostring = <function 1>
  }
}
Received a result: { -- PUBLISH{qos=2, packet_id=1, dup=false, type=3, payload="{\\"message\\":\\"Succeeded to replace the config.\\",\\"task_id\\":\\"3836689177\\",\\"timestamp\\":\\"2021-04-03T11:48:39Z\\",\\"code\\":0}", topic="test-result-topic", retain=false}
  dup = false,
  packet_id = 1,
  payload = '{"message":"Succeeded to replace the config.","task_id":"3836689177","timestamp":"2021-04-03T11:48:39Z","code":0}',
  qos = 2,
  retain = false,
  topic = "test-result-topic",
  type = 3,
  <metatable> = {
    __tostring = <function 1>
  }
}
```

これで、`/etc/collectd/collectd.conf`が置き換わった状態でcollectdが再起動されます。

### まとめ

今回は障害復旧機能を提供するcollectdプラグインにて、リモート関連機能を使う方法について説明しました。

クリアコードではフリーソフトウェアに基づいた開発、お客様のご要望に応じた既存ソフトウェアのカスタマイズのサービスを提供しています。
collectdに限らず、もしそのようなご要望があれば[お問い合わせフォーム](/contact/)からご連絡ください。
