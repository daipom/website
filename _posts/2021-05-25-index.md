---
tags:
- notable-code
title: ノータブルコード15 - MySQLの全文検索でplease, could_you
author: kou
---
最近、[Mroonga](https://mroonga.org/ja/)の[MySQL](https://www.mysql.com/jp/) 8.0対応を進めている須藤です。あとはコンディションプッシュダウンまわりを実装すればMySQL 8.0対応は完了しそうです。MySQL 8.0対応をしていて「お！」という思うコードがあったことを思い出したので15回目のノータブルコードとして紹介します。

<!--more-->

MySQLの全文検索機能は[`MATCH (...) AGAINST (...)`](https://dev.mysql.com/doc/refman/8.0/ja/fulltext-search.html)という構文を使います。この構文を`WHERE`で書けば絞り込み条件になり、`SELECT`直後の出力リストのところや`ORDER BY`のところに書けばその全文検索でのスコアーになります。

たとえば、次のSQLでは3つ同じ`MATCH (...) AGAINST (...)`がありますが、2番目の`MATCH (...) AGAINST (...)`だけが条件で残りの`MATCH (...) AGAINST (...)`は全文検索でのスコアーになります。

```sql
SELECT MATCH (content) AGAINST ('+Hello' IN BOOLEAN MODE)
  FROM memos
 WHERE MATCH (content) AGAINST ('+Hello' IN BOOLEAN MODE)
 ORDER BY MATCH (content) AGAINST ('+Hello' IN BOOLEAN MODE) DESC;
```

1つの`SELECT`中に違う種類の`MATCH (...) AGAINST (...)`を複数書くこともできます。たとえば、次のように書くこともできます。

```sql
SELECT MATCH (content) AGAINST ('+Hello' IN BOOLEAN MODE),
       MATCH (content) AGAINST ('+World' IN BOOLEAN MODE)
  FROM memos
 WHERE MATCH (content) AGAINST ('+Hello' IN BOOLEAN MODE) AND
       MATCH (content) AGAINST ('+World' IN BOOLEAN MODE)
 ORDER BY MATCH (content) AGAINST ('+Hello' IN BOOLEAN MODE) DESC;
```

このため、内部的には全文検索の種類（↑の例だと`Hello`と`World`での全文検索2種類）ごとにそれぞれリソースを確保します。このリソースには検索結果やどのレコードがどのくらいのスコアーかといった情報を格納することになります。

このリソースは各ストレージエンジンが`FT_INFO`構造体を親にした構造体の領域を確保してMySQLに返します。`FT_INFO`構造体は次のように定義されています。`_ft_vft`構造体の中は「次のレコードを取得」・「対象レコードのスコアーを取得」といった機能を実装する関数ポインターの集まりになっていて、各ストレージエンジンの実装をMySQLがコールバックできるようになっています。

```c
struct FT_INFO {
  struct _ft_vft *please; /* INTERCAL style :-) */
};
```

MySQLはストレージエンジンから`FT_INFO`構造体を受け取り、必要に応じてストレージエンジンにコールバックして必要な情報を得ます。たとえば、スコアーを取得する場合は次のようになっています。`ft_handler`が`FT_INFO`構造体を参照している変数です。

```c++
ft_handler->please->get_relevance(ft_handler);
```

なんか`please`付きでスコアー（`relevance`）を取得していて丁寧な感じがしますね！

ところで、`FT_INFO`構造体の定義のコメントの「INTERCAL style :-)」には気づいていましたか？実は、これは[INTERCAL](https://ja.wikipedia.org/wiki/INTERCAL)というプログラミング言語のマネをしています。INTERCALでは命令を実行する前に「DO」か「PLEASE」か「PLEASE DO」を指定しないといけません[^intercal-identifiers]。`FT_INFO`構造体のメンバー名を`please`にすると`please->XXX()`と呼び出さないといけなくなるのでINTERCALのような書き方になるというわけです。なお、WikipediaによるとINTERCALは1972年に開発されたプログラミング言語だそうなのでその頃からPLEASEと言っていたことになります。一方、`FT_INFO`構造体は2001年に導入されている[^ft-info]のでINTERCALから30年ほど遅れてスタイルを踏襲したことになります。

[^intercal-identifiers]: http://www.muppetlabs.com/~breadbox/intercal/intercal.txt の「4.3 IDENTIFIERS AND QUALIFIERS」を参照。

[^ft-info]: https://github.com/mysql/mysql-server/commit/3d3ef6528a48f5fba7bfcef10970c89bfb88e420

MySQLはINTERCALと違って今でも活発に開発が続いています。全文検索まわりも改良されています。2012年には次のような`FT_INFO_EXT`構造体が追加されました。InnoDBの全文検索機能のためです。

```c
struct FT_INFO_EXT {
  struct _ft_vft *please; /* INTERCAL style :-) */
  struct _ft_vft_ext *could_you;
};
```

`FT_INFO`構造体との違いは`_ft_vft_ext`構造体のメンバーが増えていることだけです。`ft_vft_ext`構造体も`_ft_vft`構造体と同じようにコールバック用の関数ポインターが集まっています。追加のコールバックを互換性を壊さずに追加するために`FT_INFO_EXT`構造体を新設したというわけです。

MySQLが`_ft_vft_ext`構造体のコールバックを呼ぶときは次のようになります。（キャストは省略しています。）

```c++
ft_handler->could_you->count_matches(ft_handler);
```

`please->XXX()`じゃなくて`could_you->XXX()`になっているので`FT_INFO`構造体のときよりさらに丁寧になっている気がしますね！10年ちょいの時を経ている[^ft-info-ext]のに、INTERCALのスタイルをマネするという既存のコードのスタイルを壊さずに拡張しています。[OSSプロジェクトにコントリビュートするときは一貫性を壊さない]({% post_url 2020-10-28-index %})という話はよくありますが、こういうことなんですね！みなさんも、既存のコードを変更するときは既存のコードの意図をしっかり読み取ってそれに合わせて拡張していってください。

[^ft-info-ext]: https://github.com/mysql/mysql-server/commit/444034b40cc6d3e4c1cb198b8b4a156d1cf75121

さらに10年経って[`PLEASE SELECT`が書けるようになっている](https://blog.gmo.media/please-clause-for-mysql-april-fool/)ので、MySQLはまだまだINTERCALの影響を受け続けていそうですね。

MySQL 8.0対応をしていたらMySQLのおもしろコードを思い出したので[ノータブルコード]({% post_url 2020-01-14-index %})として紹介しました。みなさんも「お！」と思うコードがあったらノータブルコードとして紹介してみてください。

クリアコードはユーザーがソースコードから学習する自由がある[自由なソフトウェア](https://www.gnu.org/philosophy/free-sw.html)を推進しています。今回のMroongaのMySQL 8.0対応のように業務の一環で自由なソフトウェアのソースコードを読むことも多いです。そんな仕事がいいなと思う人は[採用情報]({{ "/recruitment/" | relative_url }})を確認してみてください。最近、採用情報ページを刷新したんです！
