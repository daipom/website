---
title: Thunderbird 78から91の間の変更点の法人向けまとめ
author: piro_or
tags:
- mozilla
---

結城です。

Thunderbird 91の正式版が太平洋標準時の2021年8月11日（日本時間では8月12日）にリリースされました（[「窓の杜」のニュース記事](https://forest.watch.impress.co.jp/docs/news/1343883.html)）。過去のバージョンでは、FirefoxのESR版となるリリースが行われてから数週間ほどの間を置いてThunderbirdのメジャーリリースが行われる事が多かったのですが、今回はFirefox 91のリリースから間を置かず（翌日）のリリースとなっています。

現在Thunderbird 78を運用中の場合、今すぐThunderbird 91に更新されるということはありません。Thunderbird 78のセキュリティアップデートは、Firefox ESR78と同様に78.15.0まで提供され、Thunderbird 78からThunderbird 91への自動更新は、Thunderbird 78の最終バージョンのサポート終了（2021年11月2日以降）と同時に始まると見込まれます。言い換えますと、安全にThunderbirdを運用し続けるためには、あと2ヵ月少々でThunderbird 91への移行を完了することが推奨されます。

2022年5月18日追記：[Thundebrird 78.15.0は、ビルド時の問題のためにリリースが中止されました。Thundebrird 78の最終バージョンは78.14.0となっています。](https://bugzilla.mozilla.org/show_bug.cgi?id=1735081)

[Thunderbird 91のリリースノート](https://www.thunderbird.net/en-US/thunderbird/91.0/releasenotes/)（[有志による日本語訳](https://mozillazine.jp/?p=7015)）にはThunderbid 78からの変更点が多数記載されていますが、技術的な詳細に踏み込んだ項目が多く、エンドユーザー観点では影響度合いを読み取りづらい部分があります。そこでこの記事では、「法人運用で具体的にどのような影響があるか」という切り口で、代表的な変更点を紹介することにします。



<!--more-->

Firefox 91では「Proton」テーマが導入され、外観が大きく変わりました。それと比べると、Thunderbird 91は外観上の変化はほとんどなく、アドオンの互換性もThunderbird 78から維持されています。変更内容の多くは、細かい動作の部分となっています。


#### マルチプロセス動作が有効化されました

動作の安定性や処理速度の向上のために、マルチプロセス動作が有効化されました。搭載メモリ量が少ないPCや、多数のユーザーを1つのサーバーに収容しての運用など、空きメモリが逼迫している状況では動作パフォーマンスの低下が予想されますので、ご注意下さい。

当社で確認した限りでは、Thunderbird 91の設定を切り替えて完全なシングルプロセス動作とすることはできません[^content-multiple-processes]。

[^content-multiple-processes]: Webサイトをタブで開いた場合のサブプロセスの増加については、Firefoxと同様に`dom.ipc.processCount`を`1`に設定する事で抑止できますが、その場合でも、ファイルの処理や拡張機能の動作などのために専用のサブプロセスが生成され続けます。また、Firefoxでは、環境変数`MOZ_FORCE_DISABLE_E10S`に何らかの値を設定することで、様々な不具合のリスクと引き換えにシングルプロセス動作を強制できますが、当社での検証結果では、Thunderbird 91ではこの方法は有効ではありませんでした。


#### メール編集時にファイルを添付する際の動作が変化しました

従来版では、送信するメールにファイルを添付する際は、メニューやツールバーからの操作以外では、メール編集ウィンドウの宛先入力欄付近にファイルをドロップする方法がありました。Thunderbird 91では、ファイルのドラッグ＆ドロップでの添付操作の振る舞いが変わり、メール作成ウィンドウ全体がファイルのドロップを受け付けるようになりました。

[![（スクリーンショット：メールの編集画面へのファイルのドロップ時の様子）]({% link /images/blog/tb91-changes-for-enterprise-users/attachments-drag1.png %} "スクリーンショット：メールの編集画面へのファイルのドロップ時の様子")]({% link /images/blog/tb91-changes-for-enterprise-users/attachments-drag1.png %})

なお、HTMLメールの作成ウィンドウの場合、メール作成ウィンドウの左半分にドロップすると「本文への埋め込み」、右半分へのドロップで「添付」の動作となります。

[![（スクリーンショット：HTMLメールの編集画面へのファイルのドロップ時の様子）]({% link /images/blog/tb91-changes-for-enterprise-users/attachments-drag2.png %} "スクリーンショット：HTMLメールの編集画面へのファイルのドロップ時の様子")]({% link /images/blog/tb91-changes-for-enterprise-users/attachments-drag2.png %})

この動作は設定で無効化できません。


#### 一般に相手に読まれることが期待できないメールアドレス宛の返信の抑止

通知のメールなど、[返信先が`noreply@～`や`do-not-reply@～`のようなアドレス](https://searchfox.org/comm-central/rev/93487d295e3d3365c3082104ea980c7a0dfceeef/mail/base/content/mailCommands.js#253)になっているメールについて、返信しようとしたときに警告が行われるようになりました。

[![（スクリーンショット：警告のメッセージ）]({% link /images/blog/tb91-changes-for-enterprise-users/noreply-alert.png %} "スクリーンショット：警告のメッセージ")]({% link /images/blog/tb91-changes-for-enterprise-users/noreply-alert.png %})

この警告はあくまで「返信」操作の時のみ行われ、新規にメールを作成する場面や転送する場面では表示されません。また、この動作は設定では無効化できません。

#### Windows Liveメールと同様のメールの再編集・再送に対応

Thunderbirdの従来バージョンでは、送信済みのメールは常にビューワーで表示される動作です。これに対してWindows Liveメールでは、`X-Unsent: 1`というヘッダが付与されたメールについて、送信済みのメールであっても即座に再編集・再送が可能となっています。Thunderbird 91では、このWindows Liveメールと同様の動作になるよう実装が変更され、`X-Unsent: 1`ヘッダがあるメールを開こうとした場合には、メール編集ウィンドウが開かれるようになりました。

この動作は設定で無効化できません。

#### 非ASCII文字を含む宛先アドレスに対応

メールの宛先は`コメント部分 <メールアドレス部分>`のような形式で記入され、Thunderbirdにおいては、コメント部分の非ASCII文字は従来からも許容されていました。この度のThunderbird 91からは、「SMTPUTF8」と呼ばれる仕様に対応し、通信先のSMTPサーバーがThunderbirdに対して「SMTPUTF8対応である」旨の情報を返却した場合、Thunderbirdが送信するメールの宛先のメールアドレス部分についても非ASCII文字が許容されるようになりました。

この動作は設定で無効化できません。機能が有効となるのはSMTPサーバーが明示的に対応情報を返した場合のみのため、基本的には問題は起こらないことが期待されますが、もしメールの送受信で障害が発生した場合、この点に注意して調査を進める必要があるかもしれません。

#### メール作成ウィンドウでのリストの手動展開への対応

Thunderbirdのアドレス帳で「リスト」機能を使用している場合に、メール作成ウィンドウの宛先欄に入力されたリスト名に対し、右クリックメニューから「リストを展開」を選択することで、実際にメールが送信されることになるメールアドレスへと、即時展開できるようになりました。

[![（スクリーンショット：リストの展開操作のメニュー項目）]({% link /images/blog/tb91-changes-for-enterprise-users/expand-list.png %} "スクリーンショット：リストの展開操作のメニュー項目")]({% link /images/blog/tb91-changes-for-enterprise-users/expand-list.png %})

#### PDFビューワーの内蔵

Firefoxでは以前から利用されていた内蔵のPDFビューワーが、Thunderbirdでも利用可能になりました。添付ファイル形式ごとの取り扱いで、PDFについて「Thunderbirdでプレビュー」を選択すると、Thunderbirdの新しいタブでPDFの内容が表示されるようになります。ただし、ローカルにインストールされたPDFビューワーがある場合、初期状態ではそちらが引き続き使われます。

管理者側で内蔵PDFビューワーを強制的に無効化するためには、[MCD](https://www.mozilla.jp/business/faq/tech/setting-management/)で`pdfjs.disabled`を`true`に固定するか、後述のポリシー設定で`DisableBuiltinPDFViewer`を`true`に設定する必要があります。

#### CardDAVによるリモートアドレス帳

Thunderbirdは従来、リモートアドレス帳はLDAPによる参照専用のアドレス帳にのみ対応しており、CardDAVによる読み書き可能なリモートアドレス帳は使えませんでした（使いたい場合は、[CardBook](https://addons.thunderbird.net/thunderbird/addon/cardbook/)などのアドオンを使う必要がありました）。Thunderbird 91からは、アドオンを使わずとも、Thunderbird本体だけで読み書き可能なCardDAVアドレス帳を扱えるようになっています。具体的には、アドレス帳管理画面の「ファイル」メニューで「新規作成」→「CardDAVアドレス帳」を選択すると、CardDAVアドレス帳を作成できます。

ただ、[拡張機能向けのアドレス帳操作用API](https://webextension-api.thunderbird.net/en/latest/addressBooks.html)は、Thunderbird 91.0時点ではCardDAVアドレス帳の作成に対応していません。また、組織情報に基づく階層表示は行えず、通常のアドレス帳と同様のフラットな管理と、リストによる管理のみ行う事ができます。組織構造に基づいた階層表示などを行うためには、別途そのように設計されたアドオンを使う必要があります。

当社では現在、お客さまからのご依頼を契機としてThunderbrid 78以降のバージョン向けに新規に設計した、CardBookクローンのアドオン[Corporative Contacts](https://gitlab.com/clear-code/tb-corporative-contacts/)の開発を進めております。法人での共有アドレス帳などの利用にご興味をお持ちの情報システム管理ご担当の方は、[問い合わせフォーム](https://www.clear-code.com/contact/)よりお問い合わせを頂けましたら幸いです。


#### カレンダーアプリとしてのシステムへの登録

iCal形式のファイル（拡張子が「.ics」であるファイル）、および「webcal:」プロトコルを処理できるアプリケーションとして、Thunderbirdがシステムに登録されるようになりました。

Windowsの場合、これらのファイルやプロトコルを開くアプリケーションの選択画面にThunderbirdが表示されないようにするためには、レジストリから以下の値を削除する必要があります。

* キー `HKEY_LOCAL_MACHINE\SOFTWARE\Clients\Calendar\Mozilla Thunderbird\Capabilities\FileAssociations`、値 `.ics`
* キー `HKEY_LOCAL_MACHINE\SOFTWARE\Clients\Calendar\Mozilla Thunderbird\Capabilities\URLAssociations`、値 `webcal`
* キー `HKEY_LOCAL_MACHINE\SOFTWARE\Clients\Calendar\Mozilla Thunderbird\Capabilities\URLAssociations`、値 `webcals`


#### 機能名、メニュー項目のラベルの変更

以下の項目について、メニュー上の表示名が変更されました。

* 「アドオン」 → 「アドオンとテーマ」
* 「オプション」 → 「設定」

機能を探す際に混乱する可能性があるため、注意が必要です。


#### エンタープライズ向けのポリシー設定の強化

GPOで制御可能な設定項目が拡充されました。ポリシー項目の追加はThunderbird 78にも適宜反映されていますが、ここではThunderbird 91以降でのみ使用可能な項目を列挙します。なお、各項目は、対応関係にあるFirefoxのポリシー設定の説明へのリンクとしております。

* [`DisableBuiltinPDFViewer`](https://github.com/mozilla/policy-templates/blob/master/docs/index.md#disablebuiltinpdfviewer)
* [`DNSOverHTTPS`](https://github.com/mozilla/policy-templates/blob/master/docs/index.md#dnsoverhttps)
* [`Handlers`](https://github.com/mozilla/policy-templates/blob/master/docs/index.md#handlers)
* [`ManualAppUpdateOnly`](https://github.com/mozilla/policy-templates/blob/master/docs/index.md#manualappupdateonly)
* [`NetworkPrediction`](https://github.com/mozilla/policy-templates/blob/master/docs/index.md#networkprediction)
* [`OfferToSaveLogins`](https://github.com/mozilla/policy-templates/blob/master/docs/index.md#offertosavelogins)
* [`OfferToSaveLoginsDefault`](https://github.com/mozilla/policy-templates/blob/master/docs/index.md#offertosaveloginsdefault)
* [`PDFjs`](https://github.com/mozilla/policy-templates/blob/master/docs/index.md#pdfjs)
* [`Preferences`](https://github.com/mozilla/policy-templates/blob/master/docs/index.md#preferences)

Thunderbird用のActive Directory用ポリシーテンプレートは[Mozilla外のリポジトリで公開されています](https://github.com/drlellinger/thunderbird-policies)が、2021年8月17日現在では、まだこれらのThunderbird 91向けの項目の情報は含まれていない模様です。よって、GPOでThunderbirdのこれらのポリシーを制御するためには、ポリシーテンプレートを自作する必要があります。



### まとめ

以上、Thunderbird 91での変更点のうち法人での運用に影響があると考えられる点についての情報をご紹介しました。

なお、Firefox ESR78からESR91の間の変更点を法人運用観点のまとめについては、[ダウンロードページより資料をダウンロードして頂けます](https://www.clear-code.com/downloads/firefox-esr78-esr91-migration-report.html)。

当社のThunderbird法人向けサポート事業では、このような互換性情報の調査や、トラブル発生時の原因・解決（回避）方法の調査、アドオンの開発などを有償にて承っております。Thunderbirdno法人運用でお悩みの情報システム管理ご担当の方は、[問い合わせフォーム](https://www.clear-code.com/contact/)よりお問い合わせを頂けましたら幸いです。

