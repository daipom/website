---
title: Fluentd v1.14.5リリース -- ndjson形式対応と細かな動作改善
author: daipom
tags:
  - fluentd
---

2022年2月9日にFluentdの最新版となるv1.14.5をリリースしました。

クリアコードは[Fluentd](https://www.fluentd.org)の開発に参加し、リリースを含めたメンテナンス作業を行っています。

今回はv1.14.5のリリースについて、主なポイントを紹介します。

* [CHANGELOG](https://github.com/fluent/fluentd/blob/master/CHANGELOG.md#v1145)
* [リリースアナウンス](https://www.fluentd.org/blog/fluentd-v1.14.5-has-been-released)

<!--more-->

### Fluentd v1.14.5の最新動向

#### `in_http` "application/x-ndjson"のContent-Typeに対応しました

Content-Typeが`application/x-ndjson`のデータは、[ndjson](http://ndjson.org/)[^ndjson]と呼ばれる形式で構成されます。
まだ統一された仕様ではないようですが、基本的には改行区切り(`\n`区切り)でJSONオブジェクトが並ぶデータと考えて問題ありません[^ndmark]。

例えば以下のようなデータです。

```json
{"foo": "bar"}
{"buz": "hoge"}
```

JSONを配列で送る場合と比べ、配列の終了を待たずに1オブジェクトずつ受信できるため、ストリーミングによく用いられる形式です。

以前から`out_http`プラグインでは、JSONはデフォルトで[^jsonarray]この`ndjson`として処理されていました。

今回`in_http`プラグインにおいてもこの形式を受信できるようになりました。
特に設定は必要なく、Content-Typeが"application/x-ndjson"であれば自動で処理を行ってくれます。

[^ndjson]: `newline delimited JSON`の略です。
[^ndmark]: `\r\n`も、`\n`区切りと考えて問題なく扱うことができます。
[^jsonarray]: `json_array`設定を`true`に設定しなければ、`ndjson`形式になります: https://docs.fluentd.org/output/http#json_array

#### `out_forward` TLSによる転送時に、通信が正常にタイムアウトしないことのある問題を修正しました

`out_forward`プラグインにおいては、予期せぬ障害発生時に通信がハングアップするのを防ぐため、`connect_timeout`を設定することができます[^connecttimeout]。

しかし、TLSによる転送を行う[^usetls]場合に、`connect_timeout`を設定してもタイムアウトせず、通信がハングアップする現象が報告されていました。

今回のバージョンで、TLSによる転送時のタイムアウトの判定処理が見直され、`connect_timeout`が確実に反映され、ハングアップを防止できるようになりました。

[^connecttimeout]: https://docs.fluentd.org/output/forward#connect_timeout
[^usetls]: https://docs.fluentd.org/output/forward#how-to-connect-to-a-tls-ssl-enabled-server

#### RubyInstaller 3.1に対応しました

Windows向けのRubyインストーラーとして、[RubyInstaller](https://rubyinstaller.org/)があります。

2021年12月31日にRubyInstallerの新しいバージョン3.1.0-1がリリースされ[^rubyinstaller-release]、CランタイムがUCRTへ変わり、Rubyのプラットフォームもx64-mingw-ucrtへ変わりました。

Fluentdもこの環境向けのバイナリを提供する必要があり、本リリースから対応するようになりました[^support-rubyinstaller]。

[^rubyinstaller-release]: https://rubyinstaller.org/2021/12/31/rubyinstaller-3.1.0-1-released.html
[^support-rubyinstaller]: Fluentd v1.14.4も対応していますが、それより前のバージョンでは、Windowsに必要な依存gemがインストールされないという問題が発生します。

#### その他

* `retry_max_times`が`0`に設定されている場合、1度リトライが発生してしまう問題を修正しました。
* RubyInstaller 3.1に対応するため、依存gemである[ServerEngine](https://github.com/treasure-data/serverengine)の必要バージョンをv2.2.5以降に引き上げました。
* ログに不正な文字が含まれていた場合の処理が改善しました。
  詳細は[Issue#3595](https://github.com/fluent/fluentd/issues/3595)を参照ください。

### まとめ

今回の記事では、Fluentd v1.14.5について最新情報をお届けしました。

最新版を使ってみて、何か気になる点があればぜひ[GitHub](https://github.com/fluent/fluentd/issues)で開発チームまでフィードバックをお寄せください！
