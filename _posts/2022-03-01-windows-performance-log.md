---
title: WindowsでGroongaのメモリ使用量を記録する方法
author: hashida
tags:
- windows
---
[Groonga](https://groonga.org/ja/)という全文検索エンジンの開発に参加している橋田です。

Groongaではよくメモリ使用量を測定します。

筆者がWindowsで測定することが多いため、今回はWindowsでおすすめのGroongaのパフォーマンスログの記録方法と設定内容について紹介します。

パフォーマンス モニター(GUI)を使用した方法と、typeperfコマンド(CUI)を使用した方法の2つを紹介します。
<!--more-->

### メモリ使用量を測定する目的

Groongaでは、メモリを大量に使用するためメモリ枯渇が発生することがあります。

また、開発中の機能でメモリリークが発生することがあります。

このような問題が発生した際に、メモリ使用量に関するパフォーマンスログとGroongaのログを突き合わせて、原因調査をすることができます。

メモリ使用量が増えた時に実行していたクエリーは何で、どのようにメモリが増えたのかなどを調査することができます。

また、今回はCSV形式でログを保存する方法を紹介するため、表計算ソフトやライブラリを使っての加工、可視化も容易です。

### 測定対象のカウンター

#### カウンターとは

メモリ使用量や、CPU使用率といった、パフォーマンスデータの定義のことです。

カウンターに更にインスタンスを指定することで、指定したインスタンスのパフォーマンスデータを測定することができます。

詳細は [About Performance Counters](https://docs.microsoft.com/windows/win32/perfctrs/about-performance-counters) を参照してください。

#### Groongaのメモリ使用量のカウンター

Groongaのメモリ使用量について測定するには、以下のカウンターを測定します。

ここでは、インスタンスにgroongaを指定しています。

- Process(groonga)\Private Bytes
  - コミットされた仮想メモリのサイズ（他のプロセスと共有している部分を含まない）
- Process(groonga)\Virtual Bytes
  - コミットと予約の双方の含む仮想メモリのサイズ
- Process(groonga)\Working Set   
  - 使用物理メモリのサイズ（他のプロセスと共有している部分も含む）
- Process(groonga)\Working Set - Private
  - 使用物理メモリのサイズ（他のプロセスと共有している部分を含まない）

GroongaではテーブルやカラムといったDBオブジェクトを[Memory-mapped file](https://docs.microsoft.com/ja-jp/dotnet/standard/io/memory-mapped-files)として保存しています。
Memory-mapped fileがメモリ上に読み込まれたとき、その領域は上記の他のプロセスと共有している部分に含まれます。

したがって、Groongaがテーブルやカラムを開くと、主にVirtual Bytes、Working Setが増加していきます。
     
一方、Groongaが検索結果を一時的に保持しておくメモリ領域などは、他のプロセスと共有していない部分になるので、これら全てのカウンターに含まれます。

ここに記載以外のカウンターについては [メモリ パフォーマンス情報](https://docs.microsoft.com/en-us/windows/win32/memory/memory-performance-information) や [パフォーマンスカウンター](https://techinfoofmicrosofttech.osscons.jp/index.php?%E3%83%91%E3%83%95%E3%82%A9%E3%83%BC%E3%83%9E%E3%83%B3%E3%82%B9%20%E3%82%AB%E3%82%A6%E3%83%B3%E3%82%BF) などを参照してください。

### パフォーマンス モニターを使用してログを記録する

#### パフォーマンス モニターとは

パフォーマンス モニターとは、Microsoft 管理コンソールに含まれているシステムパフォーマンスの監視ツールです。

アプリケーションやハードウェアのパフォーマンスのリアルタイム監視、ログ取得、アラート設定といったことが出来ます。

一般的な パフォーマンスログの使用方法については、 [Windows Performance Monitor Overview](https://techcommunity.microsoft.com/t5/ask-the-performance-team/windows-performance-monitor-overview/ba-p/375481) や [[Windows Server]パフォーマンス情報の採取手順](https://www.ibm.com/support/pages/windows-server%E3%83%91%E3%83%95%E3%82%A9%E3%83%BC%E3%83%9E%E3%83%B3%E3%82%B9%E6%83%85%E5%A0%B1%E3%81%AE%E6%8E%A1%E5%8F%96%E6%89%8B%E9%A0%86) を参照してください。

#### パフォーマンス モニターによるログ記録方法

ここでは、パフォーマンス モニターでGroongaのメモリのログを記録する方法を説明します。

1. [コントロールパネル]の[システムとセキュリティ]->[管理ツール] よりパフォーマンス モニターを起動します
  
    ![パフォーマンス モニター起動]({% link /images/blog/windows-performance-monitor/start-performance-monitor.png %} "パフォーマンス モニター起動")

2. [データ コレクター セット] を展開し、[ユーザー定義] を右クリックして、[新規作成] - [データ コレクター セット] をクリックします
  
    ![データ コレクター セット選択]({% link /images/blog/windows-performance-monitor/select-data-collector-set.png %} "データ コレクター セット選択")

3. ログの名前を入力し "手動で構成する" を選択し、[次へ] をクリックします
  
    ![データ コレクター セット初期ページ]({% link /images/blog/windows-performance-monitor/data-collector-set-init-page.png %} "データ コレクター セット初期ページ")

4. [データ ログを作成する] にチェックが入っていることを確認し、"パフォーマンス カウンター" にチェックをいれ、[次へ]をクリックします
  
    ![パフォーマンス カウンター選択]({% link /images/blog/windows-performance-monitor/select-performance-counter.png %} "パフォーマンス カウンター選択")

5. 記録するパフォーマンス カウンターの項目で [追加] をクリックします
　
    ![記録するパフォーマンス カウンター選択]({% link /images/blog/windows-performance-monitor/select-performance-counter-to-record.png %} "記録するパフォーマンス カウンター選択")

6. 使用可能なカウンターの一覧から、[Groongaのメモリ使用量のカウンター](#groongaのメモリ使用量のカウンター)を選択し、"選択したオブジェクトのインスタンス" 内でgroongaを選択して追加をクリックします

    ![パフォーマンス カウンター選択]({% link /images/blog/windows-performance-monitor/select-counter.png %} "パフォーマンス カウンター選択")

7. 選択が終了したら [OK] をクリックします
8. データのサンプル間隔を設定して [次へ] をクリックします

    ここでは15秒を指定しています。

    ![サンプルの間隔設定]({% link /images/blog/windows-performance-monitor/set-sampling-interval.png %} "サンプルの間隔設定")

9. Log のルート ディレクトリの設定をして [次へ] をクリックします

    ![ルートディレクトリ設定]({% link /images/blog/windows-performance-monitor/set-root-directory.png %} "ルートディレクトリ設定")

10. "保存して閉じる" を選択して [完了] をクリックします

    ![データ コレクター セット作成]({% link /images/blog/windows-performance-monitor/create-data-collector-set.png %} "データ コレクター セット作成")

11. ユーザー定義配下に作成されたデータ コレクタ セットをクリックします
12. DataCollector01という名前のパフォーマンス カウンターを右クリックし、プロパティを表示します

    ![パフォーマンス  カウンター]({% link /images/blog/windows-performance-monitor/performance-counter.png %} "パフォーマンス  カウンター")

13. ログ フォーマットをカンマ区切りに変更し、[OK]をクリックします

    ![ログフォーマット変更]({% link /images/blog/windows-performance-monitor/change-log-format.png %} "ログフォーマット変更")

14. データのログ取得開始時には、データ コレクタ セットを右クリックして [開始] をクリックします
15. データのログ取得終了時には、データ コレクタ セットを右クリックして [停止] をクリックします
16. ルートディレクトリで指定したディレクトリ配下に、csvファイルが作成されているので確認します
  
    ```csv
    "(PDH-CSV 4.0) (","\\DESKTOP-55DDKBV\Process(groonga)\Private Bytes","\\DESKTOP-55DDKBV\Process(groonga)\Virtual Bytes","\\DESKTOP-55DDKBV\Process(groonga)\Working Set","\\DESKTOP-55DDKBV\Process(groonga)\Working Set - Private"
    "02/25/2022 15:49:00.336","151801856","4513656832","14217216","9342976"
    "02/25/2022 15:49:15.339","151801856","4513656832","14217216","9342976"
    "02/25/2022 15:49:30.355","151801856","4513656832","14217216","9342976"
    "02/25/2022 15:49:45.355","151801856","4513656832","14217216","9342976"
    "02/25/2022 15:50:00.345","151801856","4513656832","14217216","9342976"
    "02/25/2022 15:50:15.354","151801856","4513656832","14217216","9342976"
    "02/25/2022 15:50:30.341","151801856","4513656832","14217216","9342976"
    ```

### typeperfコマンドを使用してパフォーマンスログを記録する

#### typeperfコマンドとは

コマンドウィンドウまたはログファイルにパフォーマンスデータを書き込むためのコマンドです。

詳細は、 [Microsoftのドキュメント](https://docs.microsoft.com/ja-jp/windows-server/administration/windows-commands/typeperf) を参照してください。

#### typeperfコマンドによるログ記録方法

[Groongaのメモリ使用量のカウンター](#groongaのメモリ使用量のカウンター)のログを記録するには、以下のコマンドを実行します。

```console?lang=powershell
> typeperf  "\Process(groonga)\Private Bytes" "\Process(groonga)\Virtual Bytes" "\Process(groonga)\Working Set" "\Process(groonga)\Working Set - Private" -f CSV -si 15 -o C:\output_folder\output_file.csv
```

`-o`オプションで指定した出力ファイルを確認すると、パフォーマンス ログが記載されています。

```csv
"(PDH-CSV 4.0) (","\\DESKTOP-55DDKBV\Process(groonga)\Private Bytes","\\DESKTOP-55DDKBV\Process(groonga)\Virtual Bytes","\\DESKTOP-55DDKBV\Process(groonga)\Working Set","\\DESKTOP-55DDKBV\Process(groonga)\Working Set - Private"
"03/01/2022 10:43:11.103","151756800","4513751040","14180352","9318400"
"03/01/2022 10:43:26.123","151756800","4513751040","14180352","9318400"
"03/01/2022 10:43:41.139","151756800","4513751040","14180352","9318400"
"03/01/2022 10:43:56.170","151756800","4513751040","14180352","9318400"
"03/01/2022 10:44:11.185","151756800","4513751040","14180352","9318400"
```

### まとめ

WindowsでのGroongaのパフォーマンスログの記録方法について紹介しました。

Groongaに限らず、Windows上でパフォーマンスログを記録する際に、少しでも参考になれば幸いです。