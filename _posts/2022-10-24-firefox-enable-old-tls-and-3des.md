---
title: "Firefox ESR102以降で古いバージョンのTLSと3DESを有効にしてサイトにアクセスする方法"
author: kenhys
tags:
- mozilla
---

### はじめに

クリアコードでは、[Firefoxサポートサービス]({% link services/mozilla/menu.html %})の一環として、お客さまからの要望に基づいてカスタマイズしたFirefoxの提供もしています。
要望のなかには、[古いバージョンのTLS(1.0/1.1)での3DESの使用](https://github.com/clear-code/firefox-support-common/blob/master/esr102/Network#L592-L613)というものがあります。
古いバージョンのTLSの使用は情報セキュリティの観点からは非推奨なのは言うまでもありませんが、この要望で想定されているのは、システムの移行が済んでおらず塩漬けとなっている業務システムにアクセスする必要があるような場合です。

今回は、そのような事例に対応するために、Firefox ESR102ではどのようなカスタマイズが必要なのかについて説明します。

<!--more-->

### ESR91以前で必要だったカスタマイズ

TLS1.0/1.1と3DESを有効にする場合の設定についてESR91の場合を説明します。

* TLS1.0/1.1を有効にするには
* TLS1.0/1.1かつ3DESを有効にするには
* TLS1.2以降で3DESを有効にするには

#### TLS1.0/1.1を有効にするには

ESR91では、脆弱なバージョンのTLSをあえて使用するための設定は、ポリシーの`SSLVersionMin`に`tls1`を指定するというものでした。
これにより、TLS 1.0/1.1の使用を許可します。

```text
"SSLVersionMin": "tls1"
```

#### TLS1.0/1.1かつ3DESを有効にするには

`SSLVersionMin`ポリシーでサポートされる古いバージョンとして`tls1`を明示的に指定すると、暗号スイートの3DESも同時に有効にできます。

```text
"SSLVersionMin": "tls1"
```

#### TLS1.2以降で3DESを有効にするには

古いバージョンのTLSで暗号スイートとして3DESを有効にするには、`SSLVersionMin`で`tls1`を指定すれば有効にできましたが、TLS1.2以降は異なります。

autoconfig.cfgで設定するならば、次のように`security.ssl3.deprecated.rsa_des_ede3_sha`に`false`を設定します。

```text
lockPref("security.ssl3.deprecated.rsa_des_ede3_sha", false);
```

ポリシーの`DisabledCiphers`で設定するなら、  `TLS_RSA_WITH_3DES_EDE_CBC_SHA`に`false`を指定します。

```text
"DisabledCiphers": {
  "TLS_RSA_WITH_3DES_EDE_CBC_SHA": false,
},
```

### ESR102以降で必要になるカスタマイズ

TLS1.0/1.1と3DESを有効にする場合の設定についてESR102の場合を説明します。

* TLS1.0/1.1を有効にするには
* TLS1.0/1.1かつ3DESを有効にするには
* TLS1.2以降で3DESを有効にするには

#### TLS1.0/1.1を有効にするには

Firefox ESR102以降でも、TLS1.0/1.1を有効にするには`SSLVersionMin`に`tls`を指定すればよいのは同様です。

#### TLS1.0/1.1かつ3DESを有効にするには

ESR91では`SSLVersionMin`ポリシーでサポートされる古いバージョンとして`tls1`を明示的に指定すると、暗号スイートの3DESも同時に有効にできました。
しかし、ESR102ではESR91とは異なり、それだけでは暗号スイートとして3DESはサポートされません。

試しに、TLS1.0/1.1かつ暗号スイートとして3DESが有効になっているサーバーにアクセスすると次のようなエラーメッセージが表示されます。


![「安全な接続ができませんでした」のタイトルと、「myserverへの接続中にエラーが発生しました。Cannot communicate security with peer: no common encryption algorithm(s). エラーコード: SSL_ERROR_NO_CYPHER_OVERLAP」のメッセージが表示されたネットワークエラーの様子のスクリーンショット]({% link images/blog/firefox-enable-old-tls-and-3des/firefox-no-cipher-overlap.png %} "NO_CIPHER_OVERLAP")

SSL_ERROR_NO_CYPHER_OVERLAPはサーバー側でサポートしている暗号スイートと、Firefoxがサポートしている暗号スイートが合致しておらず、接続できないことを意味します。

そこで、3DESを明示的に有効にするにはpolicies.jsonに次のような設定を適用する必要があります。


```text
"Preferences": {
  "security.tls.version.enable-deprecated": {
    "Value": true,
    "Status": "locked"
  },
},
```

なお、`security.tls.version.enable-deprecated`に`true`を設定した場合、ポリシーの`SSLVersionMin`に`tls1`を設定しなくても、TLS1.0/1.1が有効になります。

そのため、`security.tls.version.enable-deprecated`に`true`を設定するのが、TLS1.0/1.1かつ3DESを有効にする場合のおすすめ設定です。

#### TLS1.2以降で3DESを有効にするには

ESR91と同様に、`security.ssl3.deprecated.rsa_des_ede3_sha`に`false`を設定するか、ポリシーの`DisabledCiphers`を使って`TLS_RSA_WITH_3DES_EDE_CBC_SHA`に`false`を指定します。

### おわりに

今回は、Firefox ESR102以降で古いバージョンのTLSと3DESを有効にしてサイトにアクセスする方法を紹介しました。
あえて脆弱なものを使用するためには、手間がかかるようになっており安全側に倒した設計となっていることがわかります。
本来は、より安全なバージョンへとサーバーの移行を進めるのが望ましいのですが、移行過程で必要になったら参考にしてみてください。

クリアコードでは、お客さまからの技術的なご質問・ご依頼に有償にて対応する[Firefoxサポートサービス]({% link services/mozilla/menu.html %})を提供しています。企業内でのFirefoxの運用でお困りの情シスご担当者さまやベンダーさまは、[お問い合わせフォーム]({% link contact/index.md %})よりお問い合わせください。


