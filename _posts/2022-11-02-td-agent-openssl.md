---
title: OpenSSL脆弱性のtd-agentへの影響について（CVE-2022-3602・CVE-2022-3786）
author: fujimotos
tags:
  - fluentd
---

2022年11月1日深夜にOpenSSLの深刻な脆弱性に関するアドバイザリが公表されました。
2014年のHeartbleedに匹敵する可能性があるとして大きなニュースになっています[^1]

[^1]: 当初はCRITICALなバグとして事前のアナウンスがありましたが、その後の様々なプロジェクトの検証結果を踏まえ、
      最終的にSeverity=HIGHの脆弱性として公表されました。

* [OpenSSL Security Advisory [01 November 2022]](https://www.openssl.org/news/secadv/20221101.txt)
* [CVE-2022-3786 and CVE-2022-3602: X.509 Email Address Buffer Overflows](https://www.openssl.org/blog/blog/2022/11/01/email-address-overflows/)

Fluentdの配布パッケージであるtd-agentでは、OpenSSLライブラリを同梱して配布しています。
このため、td-agentが今回の脆弱性の影響を受けているか、アップデートが必要か、について
懸念を覚えている運用担当者の方も少なくないと思います。

世間的な関心が高く、またtd-agentには日本語圏のユーザーも非常に多いことを踏まえ、
以下、td-agentへの影響の要点について日本語のFAQ形式でお伝えいたします。

<!--more-->

### td-agentへの影響に関するFAQ

#### Q1. td-agent v4.x のパッケージにはOpenSSLが含まれるか？

配布対象となるシステムによって同梱の有無が変わっています。以下に表形式でまとめます。

| OS      | Package | OpenSSL Bundled? |
| ------- | ------- | ---------------- |
| Linux   | DEB/RPM | N/A              |
| Windows | MSI     | OpenSSL v1.1.1 が同梱されています |
| macOS   | DMG     | OpenSSL v1.1.1 が同梱されています |

要点としては、macOS・WindowsのパッケージはOpenSSL v1.1.1を同梱しており、
Linuxは同梱せずにシステムのOpenSSLを利用します。

#### Q2. 今回の脆弱性に対して、td-agentの修正リリースを公開する予定はあるか？

現時点では、どのパッケージについてもありません。

* まず、macOS/WindowsパッケージについてはOpenSSL v1.1.1を同梱しており、
  OpenSSLのアドバイザリによると本バージョンは影響を受けていません。

  > OpenSSL versions 3.0.0 to 3.0.6 are vulnerable to this issue.
  >
  > OpenSSL 3.0 users should upgrade to OpenSSL 3.0.7.
  >
  > OpenSSL 1.1.1 and 1.0.2 are not affected by this issue.
  >
  > -- https://www.openssl.org/news/secadv/20221101.txt

* また、Linux向けパッケージについてはOpenSSLを同梱していないため、
  td-agentの新しい修正バージョンをリリースする計画はありません。

* LinuxシステムのOpenSSLが影響を受けている場合、別途システム側で対応を行って、
  td-agentを再起動する必要がある点に注意ください。

#### Q3. 本件に関するFluentdの公式情報はどこを見ればよいか？

[fluent/fluentd#3940](https://github.com/fluent/fluentd/discussions/3940) を参照ください。

### おわりに

本記事はクリアコードが11月2日時点で把握している内容をまとめたものです。

本件脆弱性に関するアップデートがあり次第、随時、内容を更新いたします。
