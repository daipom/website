---
title: YoctoのWestonをターゲットとしたFcitx5ベースの仮想キーボードの開発
author: ashie
tags:
- embedded
- use-case
---

近年、組み込みLinux（[Yocto](https://www.yoctoproject.org/)）でのウインドウシステムとして[Wayland/Weston](https://www.yoctoproject.org/)が採用される事例が増えていますが、同環境上で使用できるインプットメソッドや、それと連携して動作する仮想キーボードは無いか？というお問い合わせを頂くことが増えました。

これを受けて、クリアコードでは[サイバートラスト](https://www.cybertrust.co.jp/)様と共同で[Fcitx5](https://github.com/fcitx/fcitx5)をベースとした仮想キーボードを開発しましたので、ご紹介致します。

<div class="youtube">
  <iframe width="560" height="315" src="//www.youtube.com/embed/f1QBqWy_Ps4?cc_load_policy=1" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

<!--more-->

### Wayland/Westonでの仮想キーボード事情

Wayland上で動作するネイティブ動作する仮想キーボードとしては、例えばWestonに標準で添付されているweston-keyboardがあります。
しかし、weston-keyboardはインプットメソッドと連携して動作することは出来ない他、あくまでもサンプル実装であるため、機能も限定的です。

この他にもQt Virtual Keyboardなどいくつか候補は見られますが、いずれもインプットメソッドとの連携は出来ないようです。

### Fcitx5ベースの仮想キーボードの開発

#### Fcitx5ベースで実装した理由

2021年11月の「[PC上でWeston + Wayland版Chromium + Fcitx5での日本語入力環境を構築する]({% post_url 2021-11-24-setup-env-for-embedded-jp-software-keyboard-dev %})」という記事でも紹介していますが、Wayland/Weston上でのインプットメソッドとしては、Fcitx5が比較的よく動作することが分かっています。今回紹介する仮想キーボードは、このFcitx5のWayland用UIを拡張する形で実装しました。

Waylandは[基本理念](https://wayland.freedesktop.org/docs/html/ch04.html)として、あるアプリケーションが他のアプリケーションのウインドウを操作したり、また自身のウインドウであってもデスクトップ全体での絶対座標を制御したりすることができません。このことが原因で、従来のインプットメソッドを使用しようとしても、候補ウインドウ等を正しい位置に表示することができない・そもそもどこに表示されるか分からないという制約が発生します。Waylandのinput-methodプロトコルおよびtext-inputプロトコルを使用することでこの問題をある程度解消することができますが、Fcitx5は現時点でこれらのWaylandプロトコルに最もよく対応したインプットメソッドとなっています。

weston-keyboardを拡張するという選択肢も考えられましたが、仮想キーボードはインプットメソッドとの連携を密にした方が使い勝手が向上するため、Fcitx5のUIを拡張するという手段で実装しました。

#### 対応アプリケーション

クリアコードでは組み込みLinux向けWebブラウザの案件も多数お請けしており、今回開発した仮想キーボードも、主要なWebブラウザと組み合わせて利用することを想定しています。

* GeckoベースのWebブラウザ（Mozilla Firefox）
* ChromiumベースのWebブラウザ
* WebKitGtkベースのWebブラウザ

Weston上のGeckoやWebKitGtkで使用する場合には、以下のGTK用IMモジュールを導入する必要があります。

* [gtk-wayland-textinputv1](https://gitlab.com/clear-code/gtk-wayland-textinputv1)

同モジュールにより、他のGTKアプリケーションでもFcitx5や今回開発した仮想キーボードを使用できるようになります。

この他、Qtアプリケーションでも対応は可能と考えられます。

#### 対応する言語・配列

現時点では以下の言語に対応しています。

* 英語
* 日本語
* 簡体字中国語
* 繁体字中国語
* 韓国語
* ロシア語（ЙЦУКЕН配列）

他の言語や配列にも容易に対応可能であり、お客様のご要望に応じて順次追加しています。

#### 機能

仮想キーボードとして必要な機能は一通り実装されている他、オプションで各言語での予測入力にも対応しています。
日本語レイアウトでは、ローマ字入力だけではなく、かな入力にも対応しています。
また、カタカナや半角カタカナへのモード切り替えもサポートしています。

#### コード

実装したコードはFcitx5に対するパッチとして公開しています。

* https://github.com/clear-code/fcitx5/tree/5.0.10+virtual-keyboard-squashed

UI部分を単体のモジュールとして切り出す作業も進めていますが、

* https://github.com/clear-code/fcitx5-virtualkeyboard-ui/

現時点ではFcitx5本体側にもパッチを当てる必要があります。
パッチは随時アップストリームにフィードバックしており、その多くは既に取り込まれています。

* [fcitx/fcitx5: Fix a bug that wayland module cannot input text to weston-editor](https://github.com/fcitx/fcitx5/pull/328)
* [fcitx/fcitx5: Add touch event to WaylandWindow](https://github.com/fcitx/fcitx5/pull/401)
* [fcitx/fcitx5: Fix a problem where spellhint by enchant does not work](https://github.com/fcitx/fcitx5/pull/417)
* [fcitx/fcitx5: Fix comparison warning](https://github.com/fcitx/fcitx5/pull/420)
* [fcitx/fcitx5: Enable KeyboardEngine to use hint by default](https://github.com/fcitx/fcitx5/pull/436)
* [fcitx/fcitx5: Add simple group enumerating function for addons](https://github.com/fcitx/fcitx5/pull/452)
* [fcitx/fcitx5: Add virtual key handling to InputContext and WaylandIMInputContextV1](https://github.com/fcitx/fcitx5/pull/454)
* [fcitx/fcitx5-anthy: Add UI update when clicking candidate](https://github.com/fcitx/fcitx5-anthy/pull/2)

将来的にはパッチ不要でモジュールのビルドおよび動作が可能になるようになることを目指しています。

#### Fcitx5のYoctoレシピ

2018年の「[YoctoでFcitxをビルドする]({% post_url 2018-11-08-index %})」という記事で[meta-inuputmethod](https://gitlab.com/clear-code/meta-inputmethod/)というYoctoレイヤを開発していることを紹介しましたが、その後も開発を継続しており、現在ではFcitx5のビルドに対応しています。YoctoでのFcitx5の導入は、このレイヤを使用することで比較的楽に行うことができます。

### Westonに対する修正

Westonのインプットメソッドサポートは実際の利用例がまだ乏しいこともあり、Weston側でのバグによりFcitx5や今回開発した仮想キーボードが期待した動作をしない場面も見られました。こういった問題についても随時アップストリームにパッチを投稿しており、最新版では順次取り込まれています。

* [desktop-shell: Fix wrong initial position of input panel](https://gitlab.freedesktop.org/wayland/weston/-/merge_requests/679)
* [Fix crash on activating a text area without a real keyboard](https://gitlab.freedesktop.org/wayland/weston/-/merge_requests/703)
* [Don't send compositor's global key bindings to the input method ](https://gitlab.freedesktop.org/wayland/weston/-/merge_requests/795)

Yoctoではハードウェアベンダ等が提供するレシピを使用してWestonをビルドすることになるため、使用できるバージョンが古く、これらの修正が適用されていない可能性が高いです。

### カスタマイズのご要望をお請けしております

以上の通り、今回開発した仮想キーボードや、その動作に必要となる各種モジュール・各ソフトウェアへのパッチはほぼ全て自由なソフトウェアとして公開しています。しかし、実際にお客様の環境に導入にするにあたっては、GNU/Linux、特にWaylandにおけるインプットメソッド事情が複雑であることも相まって、一筋縄ではいかずケースバイケースで対応する必要があります。ここでは紹介していませんが、弊社お客様向けには、Chromium等のアプリケーション側で発見した不具合に対するパッチも提供しています。また、対応言語の追加やキーボード配列のカスタマイズ等が必要になってくることも多いと考えられます。

クリアコードではこういったカスタマイズやコンサルティング案件をお請けしております。クリアコードは組み込みLinux向け多言語入力や、Webブラウザを含むアプリケーションカスタマイズで豊富な経験を有しております。上記の通り、アップストリームとも協調しながら、時には必要に応じて独自プロトコルを追加する等の柔軟な対応で、お客様にとってより良いソリューションを提供できるものと考えておりますので、[お問い合わせフォーム]({% link contact/index.md %})よりお気楽にお問い合わせ下さい。
