---
title: 'YAPC::Kyoto 2023：Perlと全文検索エンジンGroongaでMySQLのデータを高速に全文検索する
  #yapcjapan'
author: komainu8
tags:
- groonga
- presentation

---
[Groonga](https://groonga.org/ja/)の[サポートサービス]({% link services/groonga.md %})を担当している堀本です。

2023年03月19日(日)に[YAPC::Kyoto 2023](https://yapcjapan.org/2023kyoto/)が開催されます。
「Perlと全文検索エンジンGroongaでMySQLのデータを高速に全文検索する」という題名で、PerlとGroongaを使ってMySQLのデータを高速に全文検索する方法を紹介します。

<!--more-->


当日使用する資料は、以下に公開しています。

<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/komainu8/yapc-kyoto-2023/viewer.html"
          width="640" height="524"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/komainu8/yapc-kyoto-2023/" title="Perlと全文検索エンジンGroongaでMySQLのデータを高速に全文検索する">Perlと全文検索エンジンGroongaでMySQLのデータを高速に全文検索する</a>
  </div>
</div>


関連リンク：

  * [スライド(Rabbit Slide Show)](https://slide.rabbit-shocker.org/authors/komainu8/yapc-kyoto-2023/)

  * [リポジトリー](https://github.com/komainu8/rabbit-slide-komainu8-yapc-kyoto-2023)

  * [講演動画（公式YouTube）](https://youtu.be/ZUnhq5EMPjc) ※2023年4月11日追記

### 内容

MySQLのデータを全文検索したい時のよくあるアプローチは以下の3つがあります。

1. MySQLのデフォルトのストレージエンジンInnoDBの全文検索機能を使う。
2. 別途Elasticsearchを用意し、アプリケーションでMySQLとElasticsearchのデータを同期し、検索はElasticsearchで行う。
3. 別途Elasticsearchを用意し、Logstashを使ってMySQLのデータをElasticsearchに同期する。

上記のアプローチにはそれぞれ課題がありますが、それらを解決する方法として、「Groonga」と「Groongaのデータ」を「MySQLに取り込むツール」、「GroongaのHTTPクライアントライブラリー」を組み合わせた構成を紹介します。

### まとめ

この講演は、MySQLのデータを全文検索することに興味のある人向けの講演です。
既にチケットは売り切れておりますが、当日の発表はYouTubeで配信されるのでそちらをご覧いただければと思います。

MySQLのデータを高速に全文検索することに興味のある方は、是非、発表を確認してみてください。
