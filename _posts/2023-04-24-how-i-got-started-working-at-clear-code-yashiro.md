---
title: 「クリアコードで働く」私のビフォーアフター
author: yashirot
tags:
- company
---
2023年1月に入社した屋代（ヤシロ）です。初の記事となります。

入社するまでと入社してからの「ビフォーアフター」を紹介します。

結果から述べますと、新しい職場を探し始めた時に希望していたことのほぼすべてが実現しました。
互いの需要と供給がマッチした幸運にも恵まれ、気持ちよく、快適に仕事をしています。

<!--more-->

### キャリアの概略

新卒で入社したSIerを退職した後、10年以上業界を離れていました。
首都圏からも離れ、兵庫県内にある実家近くに転居したのも同時期です。

その後いろいろあって、地元のハローワークで再びソフトウェア開発の求人に応募し、有期雇用契約でWebシステムのテスト、ドキュメントやリリース資材の作成と送付等の業務に就いたのが5年前のことです。

業務内容や環境面で不満はなく、四半期ごとに契約の更新も続き、小額ながら時給もたびたび上がっていったのですが、それでも地方でのレートは厳しく、生活的にはぎりぎりでした。
実際、週末に別のアルバイトをかけ持ちし、コロナ禍の時期にそちらのシフトが減ると、緊急小口資金と自立支援金の貸付制度も利用してなんとか生計を維持する状況でした。

といったところで、契約の5年目に入った2022年に、ワークライフバランスの改善を目指して新しい職場を探すことにしました。

### 転職活動

「年内に決まればいいか」の感覚でエンジニア向けの転職サイトにいくつか登録し、QAエンジニアの求人中心に応募し始めました。半年弱で延べ数十社は応募しましたでしょうか。
しかし大半から返事はなく、返事はあってもお断りされるケースばかりでした。オンライン面談のあった数社も、面談後に人事部門からいわゆる「お祈りメール」を受け取りました。
不採用の理由を具体的に教えてはもらえませんが、自身の年齢や、エンジニアのキャリアにブランクがあることがネックとなったのかなと思っています。

クリアコードの社名を知ったのは、ちょうど「転職サイトのルートは厳しそうだな」と感じ始めた頃です。TwitterでフォローしていたRuby界隈のアカウントがリツイートしていたのが最初と記憶しています。

Webサイトにひととおり目を通し、[採用情報]({% link recruitment/index.md %})から会社説明会に申し込みました。文面を通じて、価値観が一致する部分も多そうという感触もありました。たとえば[パッチ採用]({% link recruitment/patch.md %})のページにあった、“「作って終わり」の開発ではなく「作って改善していく」開発を重視している”の一文がそうです。

### 会社説明会

こうして臨んだ会社説明会で、画面越しに4名のクリアコード社員と顔を合わせました。これまでのオンライン面談の経験から、多くて2名ぐらいかなと勝手に予想していたのと、事前に[会社情報]({% link about/index.md %})から従業員数も知っておりましたので、人数と参加比率の両面で驚きました。

ちなみにこの会社説明会は、一度私の都合で延期してもらっての開催でした。当初予定していた日に、リリースに同梱したパッケージに不備があって再リリースが生じ、予定時刻に帰宅が間に合わなくなってしまったためです。当日急きょ都合が悪くなる想定もしていましたが、予定時刻の40分前に延期をお願いする連絡を入れたのは少し心苦しかったです。

参加人数の自己最多記録となったオンライン面談は、一連のやりとりも込みで興味を持ってもらえてのことかしらんと、ポジティブにとらえることにしました。

私からは説明会の申し込みに至る経緯を話しました。全体に和やか、かつ私視点では好感触で進んだように記憶しています。

それでも年齢を聞かれた時に少し身構えてしまったのは事実です。他社の面談で、同程度の感触であっても後で再三「お祈り」されていたので。一瞬だけ迷いもありましたが、そこで嘘をつくのもなと、正直に伝えました。

最近知ったのですが、「サザエさん」の磯野波平と今年タメです。  
[サザエさん キャラクター紹介](https://www.fujitv.co.jp/sazaesan/character.html)

### パッチ採用プロセス

パッチ採用プロセスに入り、「[BrowserSelector]({% link services/browserselector.html %})のバージョンアップ手順の改善」が私のテーマとなりました。

最初に、既存の手順書を修正しました。現物を使って動作を確認しながら進めることができたのはオープンソースの強みです。下は、該当するマージリクエストのリンクです。

https://gitlab.com/clear-code/browserselector/-/merge_requests/30

オープンソースなので、成果も見たければ誰でも見られるようになっています。新鮮な感覚です。

次に、バージョンアップ時に更新すべき箇所を一括で更新するPowerShellスクリプトを、「使い方」文書とともに作成しました。

https://gitlab.com/clear-code/browserselector/-/merge_requests/34

テストやドキュメンテーションに関して当時の職場で十分貢献できている自負はあったので、実際の仕事ぶりを評価してもらえて助かりました。

私の場合、平日夜と週末を使い、週あたり1営業日相当のペースで進めました。
パッチ採用のプロセスは合計10週間続きました。

期間に比べて割ける時間が限られていた状況のもとで、採用プロセスの課題そのものは、自分自身の「通常営業」で淡々と進めていった印象です。

ただひとつだけ、ドラフト段階であらかじめマージリクエスト（プルリクエスト）を作っておき、その中で改修を進めるスタイルには戸惑いがありました。担当者単位でテストまで完了させてからリクエストを発行するやり方になじみがあったためです。戸惑いも含め、途中途中で率直に意見を出し合えたのも結果的にはよかったと思います。

### 内定から入社まで

雇用のオファーをいただいた時も、半信半疑というと少し違いますが、実感としてつかみかねる面はありました。妻と義母は私以上に喜んでいましたし、採用する側としても世間一般以上の時間と手間をかけて付き合い、その上で判断されたはずと、理屈の上では理解できたのですが。

ところで近年の私の体感としては、1日あたり6時間が持続可能な労働時間のベースラインです。説明会の時点からその旨をお話しして、労働条件に盛り込んでもらえました。ありがたい話です。と同時に、世の中にそんなうまい話があるものかねとの思いがわずかにあったこともまた正直なところです。

誇張込みで表現すると、これは何か壮大なドッキリなんじゃないかという感覚がどこかにありました。

入社予定の前月になって、業務用の真新しいノートPCとディスプレイ、そして（私の希望したとおりの）ワイヤレスマウスとキーボードが自宅に届いたところで、どうやらドッキリではなさそうだ感が強くなったことを付け加えておきます。

そんな非合理な思いにとらわれたのも、ちょうどその時期、COVID-19に感染して自宅療養していたのと重なったせい、ということにしておきます。

### 入社後

現在、Mozilla製品と関連アドオンを中心に担当するチームで、サポート業務に携わっています。採用プロセス段階で話があったとおりです。

自身の「得意種目」であるテストやドキュメントについては、計画段階からほぼ一任してもらえている事案もあります。

3か月目からは、チームで毎日行っている15分弱のスタンドアップ（スタンドアップはしていない）で司会を担当しています。

チームとしていい感じに機能しています。

#### クリアコードのいいところ

ここでは1つだけ挙げます。

理念として、「フリーソフトウェアの推進」と「稼ぐ」の2軸がとてもしっかりしていることです。

2つが判断基準となっていて迷うことが少ないし、判断も早くできます。

たいていの物事が適切なタイミングで片づいてゆくのは、心地よいです。

#### 入社後に驚いたこと

ランダムに挙げると、3つあります。

1. 「オープン」の範囲がかなり広い
2. Word、Excelフリー
3. 9割のミーティングが予定どおり終わる

##### 「オープン」の範囲がかなり広い

クリアコードでの情報取り扱いのルールは「特段の事情がない限りオープン」が基本です。特徴的なのは、その「特段の事情」が、一般の相場よりもはるかに狭く、結果「オープン」となる範囲が広いことです。

たとえばこの記事自体のレビューも、[公開のマージリクエスト](https://gitlab.com/clear-code/website/-/merge_requests/253)で進めました。

中でも驚いたのは、採用過程でのディスカッションのログを、採用された側の私が入社後にそのまま閲覧できたことです。「見られてまずいことは書いてないはずなので」との話でしたが、であるにしても、かなり珍しいのではないでしょうか。

##### Word、Excelフリー

バリアフリー、ハンズフリーなどと同じ意味での「フリー」です。

Microsoft Office形式のファイルを扱う機会が格段に減りました。
使用するケースは、顧客へ提出する月次の報告資料や、社外から受領する資料などに限られています。

現在主に関わっているチームでは、案件の性質もあって強く忌避はしていない雰囲気ですが、会社としては、自由なソフトウェアではないので使わないですむなら使わずにいきましょうといった趣です。

なので、これまでの私なら間違いなくExcelのスプレッドシートで作成していただろうテスト項目表も、Markdownで作りました。また、他社からメンテナンスを引き継いでこれから改訂するドキュメントも、脱Wordとなる見込み大です。

Officeのファイルは、diffで差分表示できないところが地味にマイナスなので、その点でも好都合です。

これは余談ですが、今年確定申告をしていて、医療費集計フォームがExcelブック形式であることに初めて「エクセルて！」と軽く引いてしまったのには、われながら変わり身が早いなと思ったのでした。

##### 9割のミーティングが予定どおり終わる

以前は、とりわけ発注元との詳細検討など「ミーティングは延びるのがデフォルト」の感覚でした。

加えて前歴をさかのぼれば、開始時刻に参加者が揃わずに遅れることすらざらだったことも思い出されます。

しかしクリアコード入社後、私が参加した中で予定より延びたミーティングは、初回の1on1など、数えるほどです（延びたのも私がしゃべりすぎたせいです）。

ミーティングが予定どおりに始まり、予定どおりに終わる。このなんでもないようなことが、精神衛生上、大きくプラスに働いていることに気づきました。

付け加えると、ミーティングの数自体も、多くも少なくもなく、ちょうどいいです。

驚いたことの2点目3点目は、ソースコードを多く扱うエンジニア主体の組織にとっては「そんな驚くようなことか？」なのかもしれません。けれども私のキャリアの中ではまるで異世界です。

### まとめ

冒頭でも述べましたとおり、互いの需要と供給がマッチした幸運にも助けられ、オーダーメイドに近い形でフィットする職を得ることができました。

あえてマイナス面を探すなら、片道30分弱の通勤がなくなったことで、体重が高止まりしていることでしょうか。仕事を続けていくためにも、運動習慣をつけて減量します。
ちょうど、健保組合の企画するウォーキングラリーにエントリーしたところです。

総合すると生活の質が大きく向上しました。
おかげで支援金の返済の目処も立ち、妻の念願だった「犬を飼いたい」にも手が届きそうです。近隣のペット可物件を探す毎日です。

反面、こう快適なまま続くわけがないとの疑心暗鬼も拭えないでいます。いや、いました。

というのも、私はこれまで、職場に「殺気」は大げさにしても、殺伐とした何かは付きものだぐらいの世界観でいたからです。仕事ですから。けれどもそれはかなり間違っていた、というか歪んでいたと認識が改まりました。

自分史上異世界転生レベルで快適なクリアコードの仕事が「ゆるい」かというと決してそんなことはなく、週単位、月単位で見るとプロジェクトは進捗し、私たちのサポートによってお客様の困りごとは解決し、その対価も頂けています。

このあたりのあんばいは、文章ではなかなか説明しづらいところでもあります。
そこが異世界かどうかの感じ方に個人差はあるでしょうけれども、ともかく転職先・移籍先として興味を持たれましたら、会社説明会に申し込んでみてください。
