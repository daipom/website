---
title: Thunderbirdを既定のクライアントに設定しても他のアプリからOutlookが起動され続けてしまう問題を解消する
author: piro_or
tags:
- mozilla
---

結城です。

当社のThunderbirdサポートのお客様から、「Thunderbirdを既定のメールクライアントに設定しようとしているのに、既定のクライアントにならない」というお問い合わせを頂きました。
Thunderbirdの設定画面から既定のメールクライアントに設定する操作を行ったり、Thunderbirdを再インストールしたり、あるいはWindowsの設定で`mailto:`の関連付け先をThunderbirdに設定したりしたにもかかわらず、Office 365（デスクトップアプリ版）のExcelでドキュメントを「共有」しようとすると、ThunderbirdではなくOutlookが起動されてしまう、という状況です。

この記事では、問題の解決方法と発生原因を説明します。

<!--more-->

### Thunderbirdを改めて完全に「既定のクライアントにする」方法

最初に結論から言うと、*`-setDefaultMail` というコマンドラインオプションを指定してThunderbirdを起動する*と、この状態を解消できます。
既定のインストール先に64bit版Thunderbirdをインストールしている状況であれば、以下の要領です。

```bat
"C:\Program Files\Mozilla Thunderbird\thunderbird.exe" -setDefaultMail
```

このコマンド列を実行すると、権限昇格を求めるUACのダイアログが表示されます。
管理者権限を持つアカウントで認証して操作を実行すれば、レジストリの情報が適切に設定されます。

なお、このコマンド列は実際には、以下のコマンド列を実行するのと等価です。

```bat
"C:\Program Files\Mozilla Thunderbird\uninstall\helper.exe" /SetAsDefaultAppGlobal
```

この場合もUACの権限昇格のダイアログが表示されます。


### 問題が起こる原因

Windowsにおいて、Thunderbirdのインストール時に既定のクライアントに設定したり、Thunderbirdを起動した後で設定画面から既定のクライアントに設定したりすると、`mailto:` や `cal:` などのスキーマを伴うURLを開くアプリケーションとして、Thunderbirdが関連付けられます。

ただ、Windowsのアプリケーションがメールクライアントに任意の内容のメールの送信を任せる方法は、URLやファイルの関連付けからの起動だけではありません。
というか、「任意のファイルを添付したメールの作成・送信を、他のアプリケーションからメールクライアントに依頼する」ことは、この方法ではできません。
それをやるには、メールクライアントと他のアプリケーションの間でプログラム的に連携するための仕組みである、[MAPI](https://learn.microsoft.com/en-us/previous-versions/windows/desktop/windowsmapi/simple-mapi)[^mapi]を使う必要があります。

[^mapi]: Messaging Application Programming Interface。

このような場面では、メール作成を依頼するアプリケーション（今回の事例ではOffice 365 AppsのWordやExcel、PowerPointなど）が「MAPIクライアント」、依頼を受けてメールを作成するアプリケーション（ThunderbirdやOutlookなど）が「MAPIサービスプロバイダー」ということになります。

自分で開発したアプリケーションをMAPIサービスプロバイダーにするためには、以下のことが必要です。

1. [MAPIのサービスプロバイダーの仕様](https://learn.microsoft.com/en-us/office/client-developer/outlook/mapi/mapi-service-providers)に則ってDLLを作成する。
2. 1で作成したDLLのパスを `HKEY_LOCAL_MACHINE\SOFTWARE\Clients\Mail\(アプリケーション名)` の `DLLPath` に設定する。
   （Thunderbirdの場合は `HKEY_LOCAL_MACHINE\SOFTWARE\Clients\Mail\Mozilla Thunderbird`）
3. `HKEY_LOCAL_MACHINE\SOFTWARE\Clients\Mail` の既定の値として、2のレジストリキーの名前を設定する。

Thunderbirdには1のDLL（mozMapi32_InUse.dll）が含まれていて、インストーラは[2](https://searchfox.org/comm-esr102/rev/87b2dbcf3e8dea1bc709d322a521db7163709457/mail/installer/windows/nsis/shared.nsh#454)と[3](https://searchfox.org/comm-esr102/rev/87b2dbcf3e8dea1bc709d322a521db7163709457/mail/installer/windows/nsis/shared.nsh#202)を行うように設計されています。

しかしながら、実際には3の処理は機能しておらず、インストーラの実行後も `HKEY_CURRENT_USER\SOFTWARE\Clients\Mail` の既定の値は空のままになるようです[^cannot-set-default-app]。
そのために、Office 365 Appsで「共有」の操作からファイルをメールに添付して送ろうとしても、MAPIのサービスプロバイダーとしてThunderbirdを認識させるヒントがなく、Thunderbirdは起動されない結果となります[^mapi-default]。
これが、「Thunderbirdを既定のクライアントに設定しても、Excelからは依然としてOutlookが起動され続けてしまう」問題の原因です。

[^cannot-set-default-app]: [インストール処理の中で、`HKEY_LOCAL_MACHINE\SOFTWARE\Clients\Mail` を変更する処理が実行されるようになってはいる](https://searchfox.org/comm-esr102/rev/87b2dbcf3e8dea1bc709d322a521db7163709457/mail/installer/windows/nsis/installer.nsi#470)のですが……もしかすると、実際にはインストーラからの実行の場合にのみレジストリキーへの書き込みが遮断されるといった制約をWindows側が設けているのかもしれません。
[^mapi-default]: 筆者環境では、代わりにOutlookが起動されました。

なお、こちらで調査した限りでは、ThunderbirdのUI上で既定のクライアントに設定するための操作を行った場合、「`HKEY_LOCAL_MACHINE\Software\Clients\Mail` の既定の値」を設定する処理は実行されない様子でした。
この処理を実行するためには、Windowsの関連付けを代行するヘルパーアプリである helper.exe を、 [`/SetAsDefaultAppGlobal` オプションを指定して起動する](https://searchfox.org/comm-esr102/rev/87b2dbcf3e8dea1bc709d322a521db7163709457/mail/components/shell/nsWindowsShellService.cpp#231)必要がある模様ですが、Thunderbirdがそのような呼び出しを行う場面は、Thunderbird自体に [`-setDefaultMail` オプション](https://searchfox.org/comm-esr102/rev/87b2dbcf3e8dea1bc709d322a521db7163709457/mail/components/MessengerContentHandler.jsm#344)を指定して起動した場合に限られるようです。


### まとめ

Thunderbirdを既定のメールクライアントに設定しても他のアプリケーションでのメール送信操作でThunderbirdが起動されない問題について、発生の原因および解消方法をご紹介しました。

当社では、Thunderbirdの法人運用におけるトラブルについて、原因究明や回避方法の調査などを有償にて承っております。
Thunderbirdの運用でお悩みの企業のご担当者さまは、[お問い合わせフォームよりご相談ください]({% link contact/index.md %})。

