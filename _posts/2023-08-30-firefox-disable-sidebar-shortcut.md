---
title: Firefoxサイドバーへのショートカットキーを無効化する方法
author: kenhys
tags:
- mozilla
---

林です。

Firefoxでは、サイドバー(履歴、ブックマーク)へショートカットキーを使ってアクセスできるようになっています。
本記事では、便利そうに思えるサイドバーへのショートカットキーをあえて無効化する方法を紹介します。

<!--more-->

### Firefoxのサイドバーへアクセスしやすくするショートカットキーについて

Firefoxでは、サイドバー(履歴、ブックマーク)へ次のショートカットを用いてアクセスできるようになりました。

* 履歴 (Ctrl+H)
* ブックマーク (Ctrl+B)

しかし、Ctrl+HをBackspace相当に割り当てて使うようにしている環境のユーザーにとっては、
テキストを削除する意図で打鍵したつもりが、履歴のサイドバーがパカパカ開いたり閉じたりしてしまうことになるので、
あまり嬉しくありません。

Firefoxのショートカットキーを無効化する方法としては、[Shortkeys](https://addons.mozilla.org/ja/firefox/addon/shortkeys/)というアドオンを導入してキーバインディングを変更するというやりかたがあります。
しかし、サイトごとに事前にページを読み込んでおかないと機能しないため、リロードを忘れていたページではショートカットキーを無効にできない状態となります。
大量にタブを開いている場合には、どのサイトでショートカットキーを無効にできているかを把握しながら使うのは現実的ではありません。

<div class="callout secondary">
サイトごとに事前にページを読み込んでおかないと機能しない挙動は、Firefoxのアドオンの仕様による制約です（Firefox 117現在）。
WebExtensions APIに基づいて開発されたFirefoxアドオンは、Firefox本体が提供するキーボードショートカットを直接的には無効化できません。
そのため、閲覧中のWebページ上で任意のスクリプトを実行してCtrl+HやCtrl+Bなどのキー操作のイベントを捕捉し、イベントの伝搬をキャンセルする事によって、
間接的にFirefoxのキーボードショートカットを上書きする、という実現方法を取る必要があります。
</div>

### サイドバーへのショートカットキーを無効化するには

前提となる環境はFirefox 117です。(Firefox 115までとは一部スクリプト記述が異なります。)

<div class="callout alert">
なお、今回紹介している手法は、企業などのシステム運用管理者向けのカスタマイズ機能（AutoConfig）を用いています。
FirefoxのUI層内部で任意のスクリプトを動作させることにより、アドオンからは操作できない内部的なAPIを利用してブラウザーのキーボードショートカットを直接的に無効化する手法です。実行するスクリプトの内容は実行対象のFirefoxのバージョンに強く依存するため、Firefoxの更新によってカスタマイズが動作しなくなったり、あるいはFirefoxそのものが期待通りに動作しなくなったり、ユーザーデータが意図せず消失したり、といった予想外のトラブルが発生するリスクがあります。使用にあたってはくれぐれもご注意ください。
</div>

以下では、履歴とブックマークへのショートカットを無効化するためのサンプルを紹介します。

まず、`defaults/pref/autoconfig.js` を次のようにカスタマイズします。[^autoconfig-js]

[^autoconfig-js]: Windowsだと`c:/Program Files/Mozilla Firefox/defaults/pref/autoconfig.js`などに配置する。

```javascript
pref("general.config.obscure_value", 0);
pref("general.config.filename", "autoconfig.cfg");
pref("general.config.vendor", "autoconfig");
// サンドボックスを無効化しないとautoconfig.cfgでカスタマイズできない
pref("general.config.sandbox_enabled", false);
```

次に、`autoconfig.cfg` に次のようなコードを追加します。[^autoconfig-cfg]

[^autoconfig-cfg]: Windowsだと`c:/Program Files/Mozilla Firefox/autoconfig.cfg`などに配置する。

```javascript
//
// 以下のサイトのコードを参考に一部改変
// https://stackoverflow.com/questions/71107574/override-keyboard-shortcuts-in-firefox
//
try {
  function ConfigJS() { Services.obs.addObserver(this, 'chrome-document-global-created', false); }
  ConfigJS.prototype = {
    observe: function (aSubject) { aSubject.addEventListener('DOMContentLoaded', this, {once: true}); },
   handleEvent: function (aEvent) {
     let document = aEvent.originalTarget; let window = document.defaultView; let location = window.location;
     if (/^(chrome:(?!\/\/(global\/content\/commonDialog|browser\/content\/webext-panels)\.x?html)|about:(?!blank))/i.test(location.href)) {
       let keys = ["key_gotoHistory", "viewBookmarksSidebarKb"];
       for (var i=0; i < keys.length; i++) {
         let keyCommand = window.document.getElementById(keys[i]);
         if (keyCommand != undefined) {
           keyCommand.remove();
         }
       }
     }
    }
  };
  if (!Services.appinfo.inSafeMode) { new ConfigJS(); }
} catch(ex) {};
```

ポイントは `key_gotoHistory` や `viewBookmarksSidebarKb`といった識別子です。
これらは、それぞれサイドバーにある「履歴」や「ブックマーク」へのショートカットキー(キーID)を意味します。

無効化したいショートカットが異なる場合には、`view-source:chrome://browser/content/browser.xhtml`などを参照して該当するショートカットを探すとよいでしょう。

### まとめ

本記事では、便利そうに思えるサイドバーへのショートカットキーをあえて無効化する方法を紹介しました。
FirefoxのアドレスバーでCtrl+Hを入力してURLの編集削除を機能させることはできませんが、日本語入力中のプリエディットでは依然としてCtrl+Hでの削除が機能するため、このカスタマイズが嬉しい人もいることでしょう。

クリアコードでは、お客さまからの技術的なご質問・ご依頼に有償にて対応する[Firefoxサポートサービス]({% link services/mozilla/menu.html %})を提
供しています。企業内でのFirefoxの運用でお困りの情シスご担当者さまやベンダーさまは、[お問い合わせフォーム]({% link contact/index.md %})よりお
問い合わせください。

