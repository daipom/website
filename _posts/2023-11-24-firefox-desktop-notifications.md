---
title: Firefoxの更新後にWebサービスのデスクトップ通知が表示されなくなった場合の対処法
author: piro_or
tags:
- mozilla
---

結城です。

最近、Firefoxを更新されたお客さまから「[LINE WORKS](https://line.worksmobile.com/jp/)の通知が表示されなくなった」というお問い合わせを頂きました。
詳しく調査したところ、これはFirefox 107以降での仕様変更が影響しており、通知を表示するにはFirefoxの設定を変更する必要があると分かりました。
本記事では、調査の経緯ならびにその結果判明した設定内容について解説します。

なお、本記事で取り上げるのはLINE WORKSでの事例となりますが、他のWebサービスでも同様のトラブルが発生する可能性がありますので、参考にして頂ければ幸いです。


<!--more-->

### 基本的な通知設定の確認

Webサービスは、[Notification API](https://developer.mozilla.org/ja/docs/Web/API/Notification)を用いてデスクトップ通知を表示できます。
LINE WORKSの場合、「トーク」で新たなメッセージが届いたときに、その内容を通知として表示するようになっています。

![スクリーンショット：LINE WORKSによるデスクトップ通知の例。Windowsのデスクトップ右下に通知のポップアップが表示されている。]({% link /images/blog/firefox-web-notifications/notification.jpg %})

この通知を表示するためには、LINE WORKSに限らず必要な設定がありますので、まずはその範囲で設定を確認していきます。

Webサイトが自主的にデスクトップ通知を表示するためには、*Webサイトに対してNotification APIの使用を明示的に許可する*必要があります。
LINE WORKSの場合、ページ右上の歯車アイコンから「通知設定」を選択して、遷移先の画面で明示的にユーザーの手で許可を与えることになります。

![スクリーンショット：通知の許可の様子。LINE WORKSのWebサービス上の設定画面で「通知の設定」ボタンをクリックしたあと、Firefoxのアドレスバー付近に「notify.worksmobile.comに通知の送信を許可しますか？」というメッセージを伴うダイアログ状のポップアップが表示されている。]({% link /images/blog/firefox-web-notifications/permission-request.jpg %})

企業で全社的にLINE WORKSを導入しているような場面では、[ポリシー設定の`Permissions`](https://github.com/mozilla/policy-templates/blob/master/docs/index.md#permissions)を使ってあらかじめLINE WORKSのWebサイトにNotification APIの使用を許可しておくこともできます。
[`policies.json`](https://support.mozilla.org/ja/kb/customizing-firefox-using-policiesjson)を使うのであれば以下の要領です。

```json
{
  "policies": {
    "Permissions": {
      "Notifications": {
        "Allow": ["https://notify.worksmobile.com"]
      }
    }
  }
}
```

グループポリシー用の[ポリシーテンプレート](https://github.com/mozilla/policy-templates)を導入済みであれば、「管理用テンプレート」→「Mozilla」→「Firefox」→「Permissions」→「Allowed Sites」を「有効」にして、値に `https://notify.worksmobile.com` を登録します。

また、Windowsを使用している場合は*Windowsのデスクトップ通知設定*も有効にする必要があります。
Firefoxは従来は独自の通知ポップアップを表示する仕様でしたが、Firefox 111以降・Firefox ESR115以降のバージョンでは通知をWindowsのデスクトップ通知として表示する仕様になったため、Windows全体またはFirefoxの通知が無効化されていると、LINE WORKSの通知も表示されないことになります。

![スクリーンショット：Windowsの通知設定の様子。「システム」配下の「通知」で「通知」が「オン」の状態に、「アプリやその他の送信者からの通知」で「Mozilla Firefox」が「オン」の状態になっている。]({% link /images/blog/firefox-web-notifications/windows-desktop-notifications-options.png %})

一般的には、これらの設定が反映されていればデスクトップ通知が表示されると期待できます。
本記事で取り上げるのは、これらの設定を行ったにもかかわらず通知が表示されない場合となります。

### 原因の調査

#### Webコンソールの確認

「通知が表示されるべき場面で表示されない」状況では、何らかのエラーが発生している可能性が疑われます。
Webサイト上で発生したエラー情報はWeb開発ツールのコンソール（Webコンソール）に出力されるため、まずはそこを確認します。
Firefoxでは、キーボードショートカット「Ctrl-Shift-K」でWebコンソールを表示することができます。

この状態で、他のユーザーからメッセージを送信して通知が表示されない結果を得られた時点でWebコンソールの内容を確認したところ、特に目立ったエラーは報告されていない様子でした。
このことから、

* Notification APIがそもそも呼び出されていない。（Webサービス側の問題）
* Notification APIは呼び出されているが、その後の表示が行われていない。（Firefox側の問題）

のどちらかの状況であると考えられます。

#### Notifications APIが呼び出されているかどうかの確認

Notifications APIでは、通知は`new Notification()`でインスタンスが作成された時に表示されます。
そこで、JavaScriptデバッガーを使ってコード内からそのような記述が含まれる箇所を特定し、実際に処理が行われているかどうかを確認してみます。

JavaScriptデバッガーは、キーボードショートカット「Ctrl-Shift-Z」で開くことができます。
ここで左のペイン上部の「検索」を選択し、検索欄に`new Notification`と入力して検索を実行してみたところ、まさにその文字列を含んでいる部分が1箇所だけあると分かりました。

![スクリーンショット：デバッガーを用いた調査の様子。「デバッガー」タブを選択し、左ペインで「検索」タブを選択して、検索欄に「new Notification」と入力して検索を実行し、表示された検索結果をクリックした状態。右ペインで該当箇所がハイライト表示されている。]({% link /images/blog/firefox-web-notifications/jsdebugger-search.png %})

ただ、このファイルはいわゆるminifyがなされているために、すべての行が1行につながってしまっています。
JSデバッガーは行単位で処理を追う物なので、これでは有効な調査を行えません。

このような時は、右ペイン左下の「ソースコードを整形」ボタンをクリックして、いわゆるpretty printの状態にします。
そうするとJavaScriptの文の切れ目で改行されるようになるので、改めて、`new Notification`を含む行の行番号部分をクリックしてブレークポイントを設定します。

![スクリーンショット：デバッガーを用いた調査の様子。コードが整形表示されて、「new Notification」が含まれている箇所に正確にブレークポイントをを設定できるようになっている。]({% link /images/blog/firefox-web-notifications/jsdebugger-prettyprint.jpg %})

ブレークポイントを設定しておくと、この箇所まで処理が到達した瞬間にスクリプトの実行が一時停止されます。
デバッガー本来の使い方としては、そこからステップ実行して変数の状態などを調査することになりますが、今回はブレークポイントで処理が停止するかどうかだけを見れば充分です。
というのも、通知が表示されることが期待される場面において、

* この箇所で処理が停止されれば、Webページのスクリプトには大筋で問題がなく、問題はFirefoxの側にある。
* この箇所に到達せず処理が進行・完了すれば、問題はNotification APIの呼び出し前に起こっており、Webページのスクリプトの側にある。
  もしくは、別の箇所で別の方法でNotification APIを使用している可能性がある。

ということが言えるからです。

ブレークポイントを設定した状態で他のユーザーからのメッセージを送信してみたところ、果たしてブレークポイントで処理が一時停止する結果となりました。
よって、現象発生時の状況は、*Webサービス側はNotification APIを呼んでいるが、Firefoxがそれを無視している、もしくは通知の表示を抑制している*ということになります。

#### Notifications APIの呼び出しテスト

ここで、そもそもNotification APIが機能する状態なのかどうかを確認するために、Webコンソールで以下のコードを実行してみます。

```javascript
new Notification(
  'title',
  {
    body: 'body',
    tag: '',
  }
);
```

すると、デスクトップ通知が指定の内容で表示される結果となりました。
このことから、FirefoxのNotifications API自体が動作していないわけではないと分かりました。

また、ここでもう一つ気が付いた点があります。
先程見つけた`new Notification`の呼び出し箇所は `https://notify.worksmobile.com/bee-iframe/index-215c944c0f5f2fa7b0fd.js?779d53d248ebe3439655` というURLで参照されたファイルの中にありましたが、URLの中に`iframe`という文字列が含まれていて、トップレベルのフレームではなくインラインフレーム内に読み込まれている可能性が疑われます。
実際に、Web開発ツールの「インスペクター」で`bee-iframe/index-215c944c0f5f2fa7b0fd`を検索してみたところ、このファイルは`https://notify.worksmobile.com/bee-iframe/index-3.7.0.html`というURLのインラインフレームから読み込まれた物であることが分かりました。

![スクリーンショット：インスペクターでJavaScriptファイルが読み込まれているフレームを特定している様子。トップレベルのドキュメントではなく、その中にあるインラインフレーム内のドキュメントに読み込まれている。]({% link /images/blog/firefox-web-notifications/inspector-iframe.png %})

そこで、Web開発ツールの調査対象ドキュメントを当該インラインフレームのドキュメントに切り替えた上で、再度Webコンソールで先のコードを実行してみました。

![スクリーンショット：Web開発ツールの調査対象ドキュメントを切り替えている様子。Web開発ツール右上の調査対象ドキュメント切り替えようボタンからドロップダウンメニューを開き、その中からインラインフレームのURLを選択している。]({% link /images/blog/firefox-web-notifications/webconsole-target-frame.png %})

すると、今度はデスクトップ通知が表示されず、Webコンソール内にも特にエラー等は報告されないという結果となりました。
このことから、*インラインフレーム内でNotification APIを使用した場合にデスクトップ通知が表示されない*という状況であることが分かりました。
「ユーザーを保護するために、ユーザーを騙すような形での通知の登録や表示を抑止していく」という昨今のFirefoxの動きを鑑みると、インラインフレーム起因の呼び出しに限定して通知をブロックするということは、いかにもありそうな話です。


#### FirefoxにおけるNotifications APIの実装の確認

ここから先は、Firefoxの内部を調査していくことになります。

JavaScriptの`Notification`クラスに関わる処理は、Firefoxでは[ソースコードリポジトリ中の`dom/notification/Notification.cpp`というC++製のモジュール](https://searchfox.org/mozilla-esr115/source/dom/notification/Notification.cpp)で実装されています。
例えば、`new Notification()`が実行された時の処理は[`Notification::Constructor`](https://searchfox.org/mozilla-esr115/rev/fe48e1b635a4a84c0fb0cb4734f1bb8899782d9d/dom/notification/Notification.cpp#787)が該当します。

この周辺の処理を追っていくと、[`dom.webnotifications.allowcrossoriginiframe`という設定の値に基づいて、特定条件下で通知をキャンセルする分岐がある](https://searchfox.org/mozilla-esr115/rev/fe48e1b635a4a84c0fb0cb4734f1bb8899782d9d/dom/notification/Notification.cpp#1560)ことが分かりました。

この条件分岐は[Bug 1708354 - Notifications are allowed in a non-same-origin iframe if previously granted top-level](https://bugzilla.mozilla.org/show_bug.cgi?id=1708354)の修正の一環としてFirefox 107時点で導入されており、変更の結果、同一オリジンでないサブフレームからの通知を拒絶するように挙動が変化していたことになります。
Firefox ESR版を使用していた場合であれば、Firefox ESR102からFirefox ESR115へ更新した際に初めて影響を受けるようになります。

* お問い合わせがあったのは、Firefox ESR102からFirefox ESR115への自動更新が始まって以後であった。
* 先の通知の出所のフレームのオリジンが`https://notify.worksmobile.com`であるのに対しトップレベルのフレームであるLINE WORKSトークのWebページのオリジンは`https://talk.worksmobile.com`で、まさにクロスオリジンでの通知にあたる。

これらのことから、この度のお問い合わせの現象は「通知がクロスオリジンの通知としてブロックされていた」可能性が濃厚で、もしそうだったのであれば、`dom.webnotifications.allowcrossoriginiframe`を初期値の`false`から`true`に変更することで現象が解消される可能性があります。


#### Service Workerの状態の影響

ただ、調査時の環境では、`dom.webnotifications.allowcrossoriginiframe`の値を`true`に変更しても、依然として通知が表示されないことが度々発生していました。

Web開発ツールの「アプリケーション」でService Workerの状態を見てみたところ、この状態が「停止中」になっていると通知が表示されない様子でした。
どうやら、*極めて短い時間でService Workerが停止してしまい、その状態ではそもそも通知の受信自体ができなくなる*、ということが起こっている模様です。

こちらについても調査してみたところ、Service Workerは一定時間イベントが発生していないと停止されてしまうものの、Firefoxにおいてはそのタイムアウト時間は`dom.serviceWorkers.idle_timeout`という設定で制御できることが分かりました。

この設定の値の単位はミリ秒で、初期値は30000ミリ秒＝30秒です。
これでは、LINE WORKSのページを開いているタブをバックグラウンドにしたままで何か作業をしていると、すぐにタイムアウトしてService Workerが停止してしまいます。

イベントが発生しなくても6時間はService Workerの状態を維持するよう、値を`21600000`に変更したところ、実際に、それ以前よりは安定して通知が表示されるようになりました。


### 対策

以上の事を踏まえると、LINE WORKSの通知が安定して表示される状態にするための設定は、[`policies.json`](https://support.mozilla.org/ja/kb/customizing-firefox-using-policiesjson)を使うのであれば以下の通りに要約できます。

```json
{
  "policies": {
    "Permissions": {
      "Notifications": {
        "Allow": ["https://notify.worksmobile.com"]
      }
    },
    "Preferences": {
       "dom.webnotifications.allowcrossoriginiframe": {
         "Value": true,
         "Status": "locked"
       },
       "dom.serviceWorkers.idle_timeout": {
         "Value": 21600000,
         "Status": "locked"
       }
    }
  }
}
```

グループポリシー用の[ポリシーテンプレート](https://github.com/mozilla/policy-templates)を導入済みであれば、「管理用テンプレート」→「Mozilla」→「Firefox」で以下の通りに設定することになります。

1. 「Permissions」→「Allowed Sites」を「有効」にして、値に `https://notify.worksmobile.com` を登録する。
2. 「Preferences」を「有効」にして、値に以下のJSON文字列を登録する。
   ```json
   {
       "dom.webnotifications.allowcrossoriginiframe": {
         "Value": true,
         "Status": "locked"
       },
       "dom.serviceWorkers.idle_timeout": {
         "Value": 21600000,
         "Status": "locked"
       }
   }
   ```

ただ、この対策には以下のようなデメリットもあります。

* インラインフレームからのクロスオリジンでの通知を許容する設定は、LINE WORKSのWebサイトのみに限定して反映することができず、あらゆるWebサイトに反映される。そのため、ニュースサイトなどを閲覧した際に、そのサイトに埋め込まれたインラインフレーム型の広告やトラッキングスクリプトなどに起因するスパム的な通知が頻繁に表示されるようになる恐れがある[^notifications-permission]。
* Service Workerがタイムアウトしなくなることで、CPU資源やメモリ資源を余計に消費し続けてしまう恐れがある。

[^notifications-permission]: 通知の権限を要求された際に許可する操作を行っていなければ大丈夫なのですが、深く考えずに・よく分からないまま許可してしまっていたり、その事を忘れてしまっていたりすると、インラインフレームからのクロスオリジンの通知のブロック機能が最後の防壁となって防がれていた煩わしい通知や詐欺的な通知が、機能の無効化により一斉に表に出てくるようになる恐れがあります。

LINE WORKSのサービス側が改善されると、このような対策も必要なくなる可能性はあります。
状況に変化が見られるか、度々確認しておく必要があると言えるでしょう。


### まとめ

「Firefoxの更新後にLINE WORKSのデスクトップ通知が表示されなくなった」というお問い合わせを起点とした調査の詳しい経緯と、その結果分かった対策について紹介しました。

今回判明した状況は、LINE WORKS以外のWebサービスでも発生する可能性があります。
通知の権限の許可など、一般的な範囲で通知に関する設定を行っても通知が依然として表示されない場合には、この記事を参考にFirefoxの設定を変更してみて下さい。

当社では、Firefoxの法人運用におけるトラブルの原因究明や回避方法の調査、設定のはたらきが不明な部分の詳細な調査などを有償にて承っております。
本記事の内容も、お客さま環境でのFirefox ESR115以降に際しての検証の過程で発覚したものです。
Firefoxの運用でお悩みの企業のご担当者さまで、このようなレベルでの調査・対応を必要とされている方は、[お問い合わせフォームよりご相談ください]({% link contact/index.md %})。
