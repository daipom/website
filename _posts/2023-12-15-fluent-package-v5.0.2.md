---
title: Fluent Package v5.0.2リリース - in_tailプラグインの重大な不具合の修正
author: daipom
tags:
  - fluentd
---

こんにちは。[Fluentd](http://www.fluentd.org)のメンテナーの福田です。

2023年11月29日に Fluent Package の最新版となる v5.0.2 をリリースしました。

Fluent Package とは、コミュニティーが提供している Fluentd のパッケージです。
2023年7月28日に、それまでコミュニティーが提供していたパッケージである td-agent v4 の後継として、 Fluent Package v5.0.0 をリリースしました。
今回のリリースは、そのパッチバージョンリリースになります。

この記事では、 Fluent Package について基本的なことを紹介しつつ、 Fluentd の最新動向をメンテナーの観点から説明します。
特に、このリリースでは、in_tailプラグインがエラーなく収集を停止してしまう、という重大な不具合を修正しています。
この不具合の性質と対策について重点的に解説します。

<!--more-->

### Fluent Package とは

Fluent Packageとは、コミュニティーが提供しているFluentdのパッケージです。
このパッケージは、よく使うプラグイン、Ruby、ユニットファイル、などFluentdを便利に使うために必要なものを一通り揃えています。
このパッケージにより、Fluentdを簡単にインストールして使うことができます。

Fluent Packageはtd-agent v4の後継パッケージであり、ある程度の互換性を保っています。
td-agentとは、これまでコミュニティーが提供してきた Fluentd のパッケージです。
Fluent Packageのリリースで、次の2点が変わります。

* コミュニティーが提供するパッケージの名前が td-agent から Fluent Package に変わる
* LTS版(長期サポート版)の提供を追加する

以下でそれぞれ説明します。
また、以下の情報もぜひご覧ください。

* 弊社提供資料: [Fluent Package LTS（長期サポート版Fluentdパッケージ）ガイド ]({% link downloads/fluent-package-lts-guide.md %})
* 弊社プレスリリース: [Fluentdプロジェクトはデータ収集ツール『Fluentd』の安定運用を支援するFluent Package LTS(長期サポート版パッケージ）をリリース]({% link press-releases/20230906-fluent-lts-release.md %})
* 公式ブログ(英語): [Scheduled support lifecycle announcement about Fluent Package](https://www.fluentd.org/blog/fluent-package-scheduled-lifecycle)

#### パッケージ名の変化

パッケージ名が td-agent から Fluent Package へ変わったことにより、このパッケージがFluentdのパッケージであることが分かりやすくなりました。

以前のtd-agentは、元々の開発会社であるTreasure Data社の自社サービス用に設計されたもので、それがコミュニティー公式のパッケージとして使われるようになった、という経緯がありました。
既にtd-agentの開発はコミュニティーベースで進められるようになり、より汎用的に使われる性質のFluentdのパッケージとなっています。
そこで、名が体を表すようにすべく、コミュニティーで相談をしてパッケージ名をFluent Packageに変更しました。

#### LTS版(長期サポート版)の提供

LTS版(長期サポート版)の提供により、Fluentdの長期の安定運用がしやすくなりました。

Fluent Packageから、通常版とLTS版の2つのチャンネルからインストールをすることができるようになります。
(td-agent では通常版しかありませんでした)。
LTS版においては、あらかじめアナウンスした長期の期間(最低2年間)にわたり、バグフィックスとセキュリティフィックスのみを行います。
そのため、長期の安定運用にとって、次の2点のメリットがあります。

* 継続的なアップデートがしやすい
  * 最新のバグフィックスとセキュリティフィックスを継続して取り込める
* 次のメジャーアップデートに向けて計画的に準備ができる
  * サポート期間をあらかじめアナウンスするため、次回のメジャーアップデートの時期が事前に分かる

現在のLTS版として、Fluent Package v5.0を2025年3月いっぱいまでサポートします。

新機能をすぐに使いたい、という場合でなければ、LTS版をぜひご利用ください。

### td-agent v4 から Fluent Package へのアップデート

Fluent Packageはtd-agent v4の後継パッケージであり、ある程度の互換性を保っています。
多くのケースではtd-agent v4から簡単にアップデート可能です。
td-agent v4がインストールされている環境に、以下の通常のインストール手順でFluent Packageをインストールするだけです。

* [Installation Guide](https://docs.fluentd.org/installation)

パッケージ名が変わったことに伴い、一部コマンド名、ユーザー/グループ名、サービス名などが変化していますが、
td-agent v4からアップデートした環境では、元の名前のまま操作できるように各種エイリアスを提供しています。
そのため、ユーザーは今までどおりの方法で操作、管理を継続することができます。

また、一部のファイルパスが変化していますが、設定ファイルはアップデート時に自動でマイグレートされるため、通常は気にする必要はありません。

手動マイグレートが必要なケースとしては、カスタムユニットファイルを使用している場合や、ログローテートファイルなどをカスタマイズしている場合などが挙げられます。
この点について詳しくは、次の資料をご覧ください。

* 弊社提供資料: [Fluent Package LTS（長期サポート版Fluentdパッケージ）ガイド ]({% link downloads/fluent-package-lts-guide.md %})
* 公式ブログ(英語): [Upgrade to fluent-package v5](https://www.fluentd.org/blog/upgrade-td-agent-v4-to-v5)

### Fluentd v1.16.2 と v1.16.3 の更新内容

以下では、Fluentd v1.16.2 と v1.16.3 (Fluent Package v5.0.0 ~ Fluent Package v5.0.2) の更新内容を紹介します。
次のCHANGELOGもご覧ください。

* v1.16.3
  * [CHANGELOG](https://github.com/fluent/fluentd/blob/master/CHANGELOG.md#release-v1163---20231114)
  * [リリースアナウンス](https://www.fluentd.org/blog/fluentd-v1.16.2-v1.16.3-have-been-released)
* v1.16.2
  * [CHANGELOG](https://github.com/fluent/fluentd/blob/master/CHANGELOG.md#release-v1162---20230714)
  * [リリースアナウンス](https://www.fluentd.org/blog/fluentd-v1.16.2-v1.16.3-have-been-released)

#### `in_tail`: ログ収集を停止してしまうことがある問題を修正

`in_tail`プラグインがログ収集を停止してしまうことがある、という重大な問題をv1.16.2とv.1.16.3で修正しました。

この問題は、次の2つの厄介な特徴を持っており、非常に深刻なものでした。

* 1度発生すると、ログ収集の停止が長期的に続いてしまう
* 問題の発生に気付くのが難しい

この問題は、[follow_inodes](https://docs.fluentd.org/input/tail#follow_inodes)オプションを利用しない場合（デフォルト）と利用する場合とで、それぞれ異なった原因により発生していました。
以下、それぞれの場合について解説します。

##### `follow_inodes`オプションを利用しない場合（デフォルト）

**不具合の概要**

[follow_inodes](https://docs.fluentd.org/input/tail#follow_inodes)オプションを利用しない場合（デフォルト）の不具合は、v1.16.3で修正しました。

大量の新規ログが一気に発生するなどして処理負荷が高い状態になると、ログ収集を停止してしまうことがあります。
一度この不具合が発生すると、長期間に渡ってカレントファイルの収集を停止し続けてしまいます。
ログローテートが発生してファイルが変わっても、新規カレントファイルを全く収集しない状態が続きます。

この不具合は、 v1.12.0 ~ v1.16.2 のバージョンにおいて、デフォルト設定で発生する可能性があります。
また、v1.12.0よりも前のバージョンにおいても、`path`設定にワイルドカードを利用すると発生する可能性があります。

**復旧方法**

Fluentdをリスタートすることで、その時点のカレントファイルから収集を再開します。
また、この不具合の発生中に複数回のログローテートがあった場合、未収集ログがローテート済みファイルに残っている可能性があります。
そのような未収集ログは手動でリカバリーする必要があります。

**対策: アップデートする**

Fluentdのバージョンをv1.16.3以降にアップデートしてください。
td-agent もしくは Fluent Package v5.0.0 ~ v5.0.1 をお使いの場合は、 **Fluent Package v5.0.2** 以降へのアップデートをしてください。

また td-agent v4系としては、 td-agent v4.5.2 以降であればこの不具合は修正されています。
しかし、 td-agent は2023年12月でEOLになるので、なるべく Fluent Package へのアップデートをご検討ください。

**対策: 設定を工夫する**

アップデートをせずに対策をする方法としては、[rotate_wait](https://docs.fluentd.org/input/tail#rotate_wait)設定を`0`にする方法があります。
今回の不具合が`rotate_wait`の機能に起因するものであるため、これを`0`に設定することで回避が可能です。

`0`に設定することの影響について説明します。
この設定はデフォルトで`5`であり、ログローテート後5秒間はローテート済みファイル（旧カレントファイル）のログ収集を続ける、という動作をします。
ログを出力するアプリケーションによっては、ローテート済みファイルにしばらく出力し続けることがあります。
このデフォルトの設定値`5`は、「そのようなログを5秒間は救う」という設定になっているわけです。
これを`0`にすると、ログローテートが発生した時点でそのファイルのログ収集を停止します。
そのため、そのようなログを収集できない状態になります。
実際にどの程度のログを収集できなくなるのかどうかは環境次第です。

ご利用環境に応じてどの程度`rotate_wait`機能が必要かどうかを検討し、可能なら`0`に設定することで本不具合を回避することができます。
`0`に設定できない場合でも、なるべく小さい値にすることでこの不具合が発生する可能性を下げることができます。
また、`rotate_wait`が[refresh_interval](https://docs.fluentd.org/input/tail#refresh_interval)の値以上だと、高い確率で異常が発生することも分かっています。

**不具合の発生に気付く方法**

この不具合の深刻なポイントは、収集を停止していることに気が付きにくい点です。
目立ったエラーログを吐かずに収集が停止してしまいます。
実際に特定のログファイルの転送が止まっていないかどうかをご確認いただくのが確実です。

Fluentd v1.15.1 (td-agent v4.4.0) 以降であれば、この不具合が発生するとFluentdが次の警告ログを出力します。

```
Skip update_watcher because watcher has been already updated by other inotify event
```

この警告ログが出ている場合は、一部のファイルの収集が停止していないかどうかをご確認ください。

##### `follow_inodes`オプションを利用する場合

**不具合の概要**

[follow_inodes](https://docs.fluentd.org/input/tail#follow_inodes)オプションを利用する場合の不具合は、v1.16.2で修正しました。

この不具合を招く現実的な要因は分かっていませんが、[k8s環境での発生事例が報告されています](https://github.com/fluent/fluentd/issues/4190)。
発生すると、特定のファイル(特定のinode)の収集が途中で停止してしまいます。

**復旧方法**

Fluentdをリスタートすることで、収集を再開させることができます。

**対策: アップデートする**

Fluentdのバージョンをv1.16.2以降にアップデートしてください。
パッケージとしては **Fluent Package v5.0.1** 以降へアップデートをしてください。

また td-agent v4系としては、 td-agent v4.5.1 以降であればこの不具合は修正されています。
しかし、 td-agent は2023年12月でEOLになるので、なるべく Fluent Package へのアップデートをご検討ください。

残念ながら、アップデート以外の良い対策方法は見つかっておりません。

**不具合の発生に気付く方法**

「`follow_inodes`オプションを利用しない場合」と同様に、この不具合の発生はとても気付きにくいです。
残念ながら、実際にFluentdの出力を確認して不自然にログが取れなくなった収集対象があるかどうかを確かめる以外に、良い方法は見つかっておりません。

#### `in_tail`: `follow_inodes`オプション機能の重複収集やハンドルリークを修正

[follow_inodes](https://docs.fluentd.org/input/tail#follow_inodes)オプション機能を使用すると、ログの重複収集やハンドルリークが発生することがありました。
v1.16.2で修正しました。

ログの重複収集については、まだ発生しうるケースが残っており、重複収集を確実に回避するためには[rotate_wait](https://docs.fluentd.org/input/tail#rotate_wait)設定を`0`にする必要があります。

#### その他

* `in_forward`: 壊れたデータを受信した場合、その後に受信するデータが続いて壊れることがある問題を修正
* [ignore_same_log_interval](https://docs.fluentd.org/deployment/logging#ignore_same_log_interval)設定をしている場合、Fluentdのロガーのキャッシュが制限なく増加する問題を修正
* [rotate_age](https://docs.fluentd.org/deployment/system-config#rotate_age)に数値以外の設定をするとFluentdが起動に失敗する問題を修正
* Buffer処理が想定外エラー(`NoMethodError`)になることがある問題を修正 
* debパッケージ(Fluent Package v5.0.0, v5.0.1): Fluentd自身のログファイルのローテートに失敗する問題を修正
* Windows(msi)パッケージ: スペースや丸括弧を含むパスにインストールすると、Fluentdが起動に失敗する問題を修正 

### まとめ

今回は、Fluentdの最新パッケージ Fluent Package v5.0.2 関連の情報をお届けしました。
あわせて、Fluentd v1.16.3 と v1.16.2 の主な変更内容もご紹介しました。

Fluent Package v5.0.2 のリリースでは、昔から存在したin_tailの深刻な不具合を修正したので、最新版のFluent Packageへのアップデートを推奨します。
その際には、長期の安定運用がしやすいLTS版をぜひご活用ください。

* [Download Fluent Package](https://www.fluentd.org/download/fluent_package)
* [Installation Guide](https://docs.fluentd.org/installation)

また、クリアコードは[Fluentdのサポートサービス]({% link services/fluentd-service.md %})を行っています。
Fluent Packageへのアップデートについてもサポートいたしますので、詳しくは[Fluentdのサポートサービス]({% link services/fluentd-service.md %})をご覧いただき、[お問い合わせフォーム]({% link contact/index.md %})よりお気楽にお問い合わせください。

最新版を使ってみて何か気になる点があれば、ぜひ[GitHub](https://github.com/fluent/fluentd/issues)でコミュニティーにフィードバックをお寄せください。
