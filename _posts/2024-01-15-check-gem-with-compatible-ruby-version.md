---
title: "利用しているgemと互換性のあるRubyのバージョンを調べる方法"
author: kenhys
tags:
  - ruby
  - fluentd
---

### はじめに

パッケージにRubyおよび必要なgemがバンドルされている場合、メジャーアップグレードにともない、バンドルされているRubyの処理系のバージョンも更新されていることがあります。

パッケージにバンドルされているgemのみ使用している場合には問題になりませんが、追加で必要なgemをインストールしている場合には、採用しているgemが新しいパッケージにバンドルされている処理系で動作するのかを知りたいことがあります。

今回は利用しているgemと互換性のあるRubyのバージョンを事前に調べる方法を紹介します。

<!--more-->

### 互換性の有無の確認方法

gemではどのバージョンのRubyで動作するのか、メタ情報として指定します。

具体的には.gemspecの`Gem::Specification`における`required_ruby_version`が該当します。

このメタ情報は、rubygems.orgの各gemのページを参照すると「必要なRubyのバージョン」として参照できます。

パッケージに同梱されているRubyのバージョンが更新されている場合には、「必要なRubyのバージョン」を満たせているかを確認できればよいわけです。

自分で追加したgemの数が少ないうちは各gemのページを確認するのでもよいのですが、数が増えてくるとやや大変です。

### APIを使って必要なRubyのバージョンを確認してみる

追加したgemのリストをもとに次のようにして調べられます。

* gemの対象バージョンを調べる(たいていは最新のバージョン)
* gemの特定バージョンにおける必要なRubyのバージョンを調べる

どちらもrubygems.orgにて提供されている[GEM VERSION METHODS API](https://guides.rubygems.org/rubygems-org-api/#gem-version-methods)を利用して取得できます。


例えば、fluent-plugin-elasticsearchの最新バージョンは次のコマンドで取得できます。

```bash
$ curl --silent https://rubygems.org/api/v1/versions/fluent-plugin-elasticsearch/latest.json | jq --raw-output ".version"
5.3.0
```

これをもとに、必要なRubyのバージョンを調べてみましょう。

```bash
$ curl --silent https://rubygems.org/api/v2/rubygems/fluent-plugin-elasticsearch/versions/5.3.0 | jq --raw-output ".ruby_version" 
>= 2.3
```

このように、Ruby 2.3以降であればよいことがわかります。 [^unknovvn]

[^unknovvn]: 必要なRubyバージョンが「なし」もしくは「>= 0」となっていることがあります。その場合はリポジトリのドキュメントを参照するか、CIがRubyのバージョンごとに整備されていればそちらを確認してもよいでしょう。

### 必要なRubyバージョンの状況

ではより具体的な例で確認してみましょう。
Fluentdのディストリビューションである`td-agent`から`fluent-package`に移行する場合に問題になりそうなのは、どれくらいありそうかという題材で調べてみます。

例えば、Fluentdのpluginにおける対応状況は次のようになっています。[^target-plugin]

[^target-plugin]: `fluent-plugin-`と命名されているものが対象となっています。


| バージョンの制限の有無 | プラグイン数 |
|------------------|--------------|
| 指定あり         | 163 |
| 制限なし(>=0) | 870 |
| 不明         | 122 |

全件が1155なので、制限のないものがほとんどです。しかし、明示的に指定のあるもの、不明なものがあることがわかります。

`td-agent`から`fluent-package`に移行する場合に問題になりそうなのは、次のバージョンです。

* td-agentはRuby 2.7.8 (一部例外あり)
* fluent-packageはRuby 3.2.2

したがって、Ruby 3.2.2以降に対応しているかどうかが分岐点となります。
必要なバージョンのパターンはこれだけありました。

* `< 2.5.0, >= 2.1.0`
* `> 2.1`
* `>= 1.9.1`
* `>= 1.9.2`
* `>= 1.9.3`
* `>= 2.0`
* `>= 2.0.0`
* `>= 2.1`
* `>= 2.1, < 4`
* `>= 2.1.0`
* `>= 2.1.2`
* `>= 2.2`
* `>= 2.2.0`
* `>= 2.3`
* `>= 2.3.0`
* `>= 2.4`
* `>= 2.4.0`
* `>= 2.5`
* `>= 2.5.0`
* `>= 2.6`
* `>= 2.6.0`
* `>= 2.6.3`
* `>= 2.7`
* `>= 2.7.0`
* `>= 3.1`
* `>= 3.1.2`
* `~> 2`
* `~> 2.0`
* `~> 2.1`
* `~> 3.1`

3.2.2に対応しているかどうかという観点だと、次の制限がひっかかります。

* `< 2.5.0, >= 2.1.0`
* `~> 2`
* `~> 2.0`
* `~> 2.1`

これらは、3.2.2では動作しないということになります。

具体的には、以下のgemが制限にひっかかることがわかっています。(バージョンによる制限が不明なものを除きます。)

* fluent-plugin-postgresql_csv (`< 2.5.0, >= 2.1.0`)
  * 2018年リリース 0.0.7が最新。
  * https://github.com/masterlee998/ は404。gemのみ登録されている。
* fluent-plugin-s3in (`~>2`)
  * 2015年リリース0.1.2.2が最新。
  * https://github.com/shii-take/fluent-plugin-s3in
* fluent-plugin-keyvalue-parser (`~> 2.0`)
  * 2017年リリース 0.1.4 (pre-release)が最新。
  * https://github.com/nom3ad/fluent-plugin-keyvalue-parser
* fluent-plugin-parser_cef (`~> 2.1`)
  * https://github.com/lunardial/fluent-plugin-parser_cef
  * 2017年リリース 1.0.0が最新。
* fluent-plugin-tail-ex-rotate (`~> 2.1`)
  * 2017年リリース 0.1.1が最新。
  * https://github.com/ymizushi/fluent-plugin-tail-ex-rotate

制限が不明なgemについては、個別にみていくしかありません。

### おわりに

今回は、利用しているgemと互換性のあるRubyのバージョンを調べる方法を紹介しました。
実用上は、さらに依存先のgemとの互換性などもチェックする必要がでてきたりします。
そちらについては割愛します。
