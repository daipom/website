---
title: フィヨルドブートキャンプ様とOSS Gateワークショップをコラボ実施しました！
author: daipom
tags:
  - free-software
---

[OSS Gate](https://oss-gate.github.io/)のワークショップの企画・実施をここ1年ほど担当している福田です。

OSS Gateは、OSS（オープンソースソフトウェア）開発に参加する「入り口」を提供する取り組みです。
フリーソフトウェアを推進する活動の一貫として、クリアコードはOSS Gateの活動に参加しています。
普段から定期的にワークショップを開催して、参加する方々にOSS開発を体験してもらっています。

今回、フィヨルドブートキャンプ様から素敵なコラボ企画をいただき、[OSS Gateオンラインワークショップ - フィヨルドブートキャンプ特別版](https://oss-gate.doorkeeper.jp/events/160648)を2023年9月30日に実施しました。
この記事では、このコラボ企画について紹介します。

<!--more-->

### OSS Gateとクリアコード

[OSS Gate](https://oss-gate.github.io/)は、OSS（オープンソースソフトウェア）開発に参加する「入り口」を提供する取り組みです。
普段から定期的にワークショップを開催しています。
ワークショップでは、OSS開発を体験したい参加者（「ビギナー」）を、参加したことのある参加者（「サポーター」）がサポートしつつ、OSS開発を体験してもらいます。
例えば、GitHub上で公開されているOSSについて、issueやPull Requestを作ったりします。
実際にどのようなことをするのかは、[ワークショップレポート](https://oss-gate.github.io/workshop/report.html)にまとまっている感想やレポートをご覧ください。

フリーソフトウェア[^free]を推進する活動の一貫として、クリアコードはOSS Gateの活動に参加したり、[OSS開発支援サービス]({% link services/oss-development-support.md %})を行ったりしています。

[^free]: フリーソフトウェアとは、ユーザーが自由に使えるソフトウェアのことで、自由（な）ソフトウェア、などとも呼ばれます（[自由ソフトウェアとは?](https://www.gnu.org/philosophy/free-sw.ja.html)）。フリーソフトウェアとOSSはほぼ同じソフトウェアを指しますが、考え方が違います（[なぜ、オープンソースは自由ソフトウェアの的を外すのか](https://www.gnu.org/philosophy/open-source-misses-the-point.html)）。クリアコードはフリーソフトウェアの考え方を大事にしていますが、本記事では分かりやすさのために基本的にOSSという言葉を使います。

### フィヨルドブートキャンプ様からいただいたコラボ企画

このコラボ企画については、次のプレスリリースでも紹介しているのでそちらもご覧ください。

* [OSS Gateオンラインワークショップを1月27日に開催、特別編動画を公開]({% link press-releases/20231226-oss-gate-workshop-video.md %})

#### 受講生がOSSに関わる機会を提供する

フィヨルドブートキャンプ様は、プログラミングスクールとして受講生がOSSと関わる機会を提供したいと考えていらっしゃいました。
その目的は次の通りです。

* よりプログラミングを楽しむため
  * OSSプロジェクト、OSS開発者との関わりを持つ
  * 自身もOSSにコントリビュート[^contribute]している喜びを感じる
* レベルの高い広い世界を知るため
  * 社内など閉じた世界ではない、文字通り世界クラスのプログラマーの技術力を知る

しかし、現実的にはOSS開発に関わるような機会に遭遇することが少ない、という課題がありました。
そこで、全ての受講生がOSSやOSSへのコントリビュート方法を学び、体験できる機会として、OSS Gateワークショップを活用することを考えてくださいました。

[^contribute]: コントリビュート: 「貢献」。OSSにおいては、issueやPull Requestを作ったりしてそのOSSの開発に参加（貢献）することを指します。

#### OSS Gateを盛り上げる

フィヨルドブートキャンプ様は、OSS Gateワークショップをただ利用するのではなく、より盛り上げることも含めて考えてくださいました。

「ただ利用するだけでなく、コントリビュートをする」という姿勢は、OSSにとって本質的なものです。
何か足りないところがあったら自分で改善する、すると自分だけでなく他の人も嬉しい、それがOSSの素晴らしいところの1つです。

OSS Gateワークショップも、多くのOSSと同じように有志の方々の協力で成り立っています。
ワークショップでは、OSS開発を体験したい参加者（「ビギナー」）を、参加したことのある参加者（「サポーター」）がサポートしつつ、OSS開発を体験してもらう形になります。
つまり、ワークショップを行うには、ビギナーだけでなくサポーターとして参加される方々が必要不可欠なわけです。
実際に、参加するサポーターの人数に応じて、参加可能なビギナー数が決まるような仕組みにしています。
多くのサポーターが参加するほど、多くのビギナーが参加できるわけです。

フィヨルドブートキャンプ様は、OSS Gateワークショップを通してOSSに関わる機会として、次の2種類を考えてくださいました。

1. OSSへのコントリビュートの方法を知る
2. OSSへのコントリビュートの方法を教える

「1. OSSへのコントリビュートの方法を知る」では、受講生がワークショップにビギナーとして参加し、OSS開発を体験します。
1を終えた受講生は、次に「2. OSSへのコントリビュートの方法を教える」としてワークショップにサポーターとして参加し、ビギナーのサポートを通してOSSと関わります。

2によってサポーターが増えれば、1で参加できるビギナーの人数も増えるわけです。
フィヨルドブートキャンプ様とOSS Gateの双方にとって、とっても嬉しいアイデアです！

#### コラボ企画: フィヨルドブートキャンプ特別版ワークショップ

以上の内容をスタートするにあたり、そのスタートダッシュとして[OSS Gateオンラインワークショップ - フィヨルドブートキャンプ特別版](https://oss-gate.doorkeeper.jp/events/160648)を企画、実施しました。
この特別版ワークショップでは、多くのフィヨルドブートキャンプ様の受講生の方々にワークショップにご参加いただくことで、その後の継続的な参加の流れを作ることを狙いとしました。

受講生の方々は全国にいらっしゃるので、オンライン形式にしました。
受講生の方々の参加の敷居を下げるため、ビギナー参加者をフィヨルドブートキャンプ様の受講生限定としました。
また、Firefoxやそのプラグイン開発に関わってきたクリアコードの結城洋志さんによる講演「今日から参加できる！OSS開発」をワークショップの前に行いました。
OSSの基本的な事柄やOSS開発に参加することのメリットや楽しさを知ってもらった上で、ワークショップに参加してもらえるようにするためです。

### フィヨルドブートキャンプ特別版ワークショップの様子

以上の企画に基づき、[OSS Gateオンラインワークショップ - フィヨルドブートキャンプ特別版](https://oss-gate.doorkeeper.jp/events/160648)を2023年9月30日に実施しました。
サポーターとビギナー合わせて20名以上の方々にご参加いただいて、みんなでワイワイとOSSに触れ合うことができました！

#### 講演: 「今日から参加できる！OSS開発」

Firefoxやそのプラグイン開発に関わってきたクリアコードの結城洋志さんによる講演「今日から参加できる！OSS開発」からスタートしました！
講演の動画やスライドを公開しているのでぜひご覧ください！

動画URL https://youtu.be/F2Ey3kRZfQoq

<div class="youtube">
  <iframe width="560"
          height="315"
          src="https://www.youtube.com/embed/F2Ey3kRZfQo?si=Zkf9j131hF3DkXqU"
          title="YouTube video player"
          frameborder="0"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
          allowfullscreen></iframe>
</div>

スライド

<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/Piro/presentation-oss-gate-workshop-fjord-bootcamp-introduction/viewer.html"
          width="640" height="524"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/Piro/presentation-oss-gate-workshop-fjord-bootcamp-introduction/" title="今日から参加できる！OSS開発">今日から参加できる！OSS開発</a>
  </div>
</div>

#### ワークショップ

講演が終わったら、いよいよワークショップ本編の開始です。

最近のオンラインワークショップはOSSである[Element](https://element.io/)を使っていましたが、今回はフィヨルドブートキャンプ様が普段使われている[Remo](https://remo.co)を使わせていただきました。
バーチャルに配置されている各テーブルにサポーターとビギナーがバランスよく座ってワークショップを進めました。

ワークショップの内容は普段のワークショップと同じです。
身近にあるOSSについて、改めて公式ドキュメントを見ながら触ってみると、コントリビュートできそうなポイントが意外とすぐに見つかる、ということを体験していただけたと思います。
多くのビギナーの方がワークショップの時間内にコントリビュートすることができていました(例えばGitHub上でissueやPull Requestを作る、など)。
ワークショップ中にやりきれなかった方も、後日OSS Gateのチャットで相談したりしつつコントリビュートすることができていました。

みなさんのコントリビュート内容は、 [Workshopの成果物2023‐09‐30](https://github.com/oss-gate/workshop/wiki/Workshop%E3%81%AE%E6%88%90%E6%9E%9C%E7%89%A92023%E2%80%9009%E2%80%9030) にまとまっています。
興味のある方はぜひご覧ください。

また、フィヨルドブートキャンプ様のブログもぜひご覧ください。

* [OSS Gate と FBC のコラボ企画を開催しました](https://bootcamp.fjord.jp/articles/91)

### まとめ

その後の通常のワークショップも、多くの方に参加いただいて盛況です。

* [OSS Gateオンラインワークショップ2023-12-09](https://oss-gate.doorkeeper.jp/events/161080)
* [OSS Gateオンラインワークショップ2024-01-27](https://oss-gate.doorkeeper.jp/events/161082)

今年はオフラインのワークショップも混ぜていきたいなと思っています。
興味のある方はぜひ[今後のワークショップ](https://oss-gate.doorkeeper.jp/events/upcoming)に参加してみてください！

OSS Gateが今後もフィヨルドブートキャンプ様の受講生にとってOSSに関わる機会となれれば嬉しいです。
また、このようなOSS Gateとのコラボ企画は大歓迎ですので、興味のある方はぜひご連絡をください。
[OSS Gateのチャット](https://matrix.to/#/#oss-gate:matrix.org)にご参加いただき、そこでご連絡をいただければと思います。
詳しくは[スポンサー募集ページ](https://oss-gate.github.io/sponsors/wanted.html)もご覧ください。

また、クリアコードは[OSS開発支援サービス]({% link services/oss-development-support.md %})も行っていますので、[お問い合わせフォーム]({% link contact/index.md %})よりお気軽にお問い合わせください。
