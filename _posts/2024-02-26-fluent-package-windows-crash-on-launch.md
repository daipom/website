---
title: fluent-package不具合情報 - Windows版で環境によってはFluentdワーカープロセスの起動ができないことがある
author: ashie
tags:
  - fluentd
---

クリアコードはFluentdを利用する法人様に向けて[Fluentdのサポートサービス]({% link services/fluentd-service.md %})を提供しています。そのサービス内容の一つとして、Fluentdで何らかの不具合があってお客様に重大な影響が懸念される場合に、（契約形態にも依りますが）プッシュ型で情報をお知らせするサービスも提供しています。

今回、fluent-packageやtd-agentが環境によってはワーカープロセスを起動できない問題があることが発覚しましたので、その情報をククログでも紹介します。

<!--more-->

### 本件の概要

* fluent-packageやtd-agentのWindows版において、レジストリ`HKLM`（`HKEY_LOCAL_MACHINE`）や`HKCU`（`HKEY_CURRENT_USER`）の`SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\`以下に非ASCII文字を含むサブキーが存在すると、Fluentdのワーカープロセスが起動できないという問題があることが発覚しました。
* 上記のレジストリはWindowsの`プログラムの追加と削除`機能のために、各アプリケーションが情報を書き込む場所です。通常、各アプリケーションは上記レジストリキー直下にGUIDをサブキーとして書き込みますが、中には漢字等を含んだサブキーを作成するアプリケーションが存在します。
* 上記のようなレジストリキーが存在する場合でもFluentdの動作に影響を与えないというのが期待される動作ですが、fluent-packageやtd-agentが同梱するRubyInstallerの問題により、特定バージョンのfluent-packageやtd-agentではプロセスを起動できないという問題が発生します。

### 影響範囲

* td-agent v4.5.0 以降、fluent-package v5.0.2以前のバージョンがこの影響を受けます。
* 前述のような問題のあるアプリケーションがインストールされている場合にのみ影響を受けます。
* 以下レジストリキーのサブキーとして非ASCII文字を含むものがあるかどうかを確認することで、影響を受けるかどうかを判断できます
  * `\HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\`
  * `\HKEY_CURRENT_USER\WOW6432Node\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\`
  * `\HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\`
  * `\HKEY_LOCAL_MACHINE\WOW6432Node\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\`
* 問題なくFluentdを起動できている場合は、必ずしも対処する必要はありません。
  * ただし、Fluentd起動後に追加で上記のような問題のあるアプリケーションをインストールした場合は、次回Fluentd再起動時に影響を受けます。

### 影響

* この問題に該当する環境では、以下のようなエラーが発生してFluentdのワーカープロセスの起動に失敗し、ログ収集が行われません。

```txt
Traceback (most recent call last):
        17: from <internal:gem_prelude>:1:in `<internal:gem_prelude>'
        16: from <internal:gem_prelude>:1:in `require'
        15: from C:/opt/td-agent/lib/ruby/2.7.0/rubygems.rb:1427:in `<top (required)>'
        14: from C:/opt/td-agent/lib/ruby/2.7.0/rubygems.rb:1427:in `require'
        13: from C:/opt/td-agent/lib/ruby/2.7.0/rubygems/defaults/operating_system.rb:24:in `<top (required)>'
        12: from C:/opt/td-agent/lib/ruby/site_ruby/2.7.0/ruby_installer/runtime/singleton.rb:27:in `enable_dll_search_paths'
        11: from C:/opt/td-agent/lib/ruby/site_ruby/2.7.0/ruby_installer/runtime/msys2_installation.rb:125:in `enable_dll_search_paths'
        10: from C:/opt/td-agent/lib/ruby/site_ruby/2.7.0/ruby_installer/runtime/msys2_installation.rb:115:in `mingw_bin_path'
         9: from C:/opt/td-agent/lib/ruby/site_ruby/2.7.0/ruby_installer/runtime/msys2_installation.rb:102:in `msys_path'
         8: from C:/opt/td-agent/lib/ruby/site_ruby/2.7.0/ruby_installer/runtime/msys2_installation.rb:68:in `iterate_msys_paths'
         7: from C:/opt/td-agent/lib/ruby/site_ruby/2.7.0/ruby_installer/runtime/msys2_installation.rb:68:in `each'
         6: from C:/opt/td-agent/lib/ruby/site_ruby/2.7.0/ruby_installer/runtime/msys2_installation.rb:70:in `block in iterate_msys_paths'
         5: from C:/opt/td-agent/lib/ruby/2.7.0/win32/registry.rb:542:in `open'
         4: from C:/opt/td-agent/lib/ruby/2.7.0/win32/registry.rb:435:in `open'
         3: from C:/opt/td-agent/lib/ruby/site_ruby/2.7.0/ruby_installer/runtime/msys2_installation.rb:71:in `block (2 levels) in iterate_msys_paths'
         2: from C:/opt/td-agent/lib/ruby/2.7.0/win32/registry.rb:611:in `each_key'
         1: from C:/opt/td-agent/lib/ruby/2.7.0/win32/registry.rb:910:in `export_string'
C:/opt/td-agent/lib/ruby/2.7.0/win32/registry.rb:910:in `encode': U+767E to ASCII-8BIT in conversion from UTF-16LE to UTF-8 to ASCII-8BIT (Encoding::UndefinedConversionError)
2023-06-14 16:40:41 +0800 [error]: Worker 0 exited unexpectedly with status 1
```

### 対応方法

* 影響を受ける場合は、以下の方法でこの問題を回避することが可能です
  * td-agentの場合: `C:\opt\td-agent\msys64\usr\bin\msys-2.0.dll`というパスで空のファイルを作成する
  * fluent-packageの場合: `C:\opt\fluent\msys64\usr\bin\msys-2.0.dll`というパスで空のファイルを作成する
  * 上記ファイル作成後、自動で復旧する場合もありますが、念のためfluentdwinsvcサービスを再起動することをおすすめします。
* fluent-package v5.0.3 にてこの問題を[暫定対処](https://github.com/fluent/fluent-package-builder/pull/620)予定です。リリース時期については調整中です。
* RubyInstallerのアップストリームにも[問題を報告](https://github.com/oneclick/rubyinstaller2/issues/372)し、抜本的な対応方法を検討しています。

### 原因詳細

* Fluentdが使用するRubyは、起動時にDLLのサーチパスをリストアップします。
* この際、MSYS2がインストールされているパスをDLLサーチパスの一つとして追加しようとします。
* いくつか固定のデフォルトパスを検索してMYS2がインストールされていない場合は、上記のレジストリ値を参照してMSYS2がインストールされているかを確認します。
* Fluentdのワーカープロセスは、サービスとして起動する場合、Rubyの内部エンコーディングを`ASCII-8BIT`（コマンドラインオプション`-Eacsii-8bit:ascii-8bit`）として起動しようとするため、レジストリキーに非ASCII文字が含まれる場合、変換に失敗しクラッシュします。
  * サービスではなくコマンドラインで起動する場合、通常は`UTF-8`（コマンドラインオプション`-Eutf8`）で起動しようとするため、この問題の影響を受けません。
* `C:\opt\fluent\msys64\usr\bin\msys-2.0.dll`は上記で説明している`固定のデフォルトパス`の一つであるため、このファイルが存在する場合は、本不具合を発生させているコードの実行をスキップさせることができます。
* なお、MSYS2がインストールされていなくてもFluentdは問題なく使用することができます。`fluent-gem`コマンドで新たなプラグインをインストールする場合に一時的にMSYS2が必要となることはありますが、通常利用では必要とされません。
* 厳密には td-agent v4.4.2以前の全てのv4系バージョンにも同様の問題があります。ただし、v4.4.2以前では`HKCU`（`HKEY_CURRENT_USER`）のレジストリのみを参照し、`HKLM`（`HKEY_LOCAL_MACHINE`）のレジストリは参照しないため、サービスを起動するユーザーで影響を受けることは現実的にはほぼ無いと言えます。td-agent v4.5.0以降ではRubyInstallerの以下の変更の影響を受けて[`HKLM`のレジストリも参照するようになった](https://github.com/oneclick/rubyinstaller2/commit/4382f99bf21608a3e37b6eabe861dca5e1f015f5)ため、問題が発生するようになりました。

### さいごに

最初にも紹介した通り、この情報は当社の法人向けサポートサービスのお客様向けに提供しているものを少しアレンジして公開したものになります。

当社の法人向けサポートサービスは、お客様の問題について原因や回避方法を調査してその解決を図ると同時に、その問題がフリーソフトウェアの問題であった場合はアップストリームへのフィードバックも行う事を心がけています。本件についても、関連するフリーソフトウェアのリポジトリ上で問題を報告・管理していたものをまとめたに過ぎず、アップストリームのサイトでは既に公開されている情報となります。

ただ、各リポジトリ上ではすべて英語でやりとりしていますし、問題が複数のフリーソフトウェアの組み合わせによって起こる場合には情報が分散してしまいますので、特に日本のユーザーにとっては問題に気がつきにくかったり、全体像を捉えにくいといったことがあったかもしれません。これからもこういった情報があれば随時ククログでも公開していこうと考えていますが、もしこういった情報を迅速に受け取りたい・問題解決をサポートして欲しいといったご要望があれば、ぜひ弊社の[Fluentdサポートサービス]({% link services/fluentd-service.md %})のご利用をご検討下さい。
