---
title: UbuntuのAPTを使用して、.debパッケージでサードパーティリポジトリを登録しパッケージをインストールする仕組み
author: otegami
tags:
- groonga
- debian
---

最近、[Groonga](https://groonga.org/ja/)チームに加わると同時に普段使いのmacOSからUbuntuに乗り換え、まだ不慣れな思いをしている児玉です。

Groongaの開発環境を整えることになり、Groongaの最新版をソースコードからビルドするためにサードパーティのリポジトリからパッケージをインストールする場面に直面しました。パッケージの公式サイトを見ながら手順通りにインストールできたものの、その背後にある仕組みが理解できていない状態でした。この状態に少々モヤモヤし、気になっていた気持ちを社内でつぶやいたところ、[Debian Developerの林先輩]({% post_url 2020-09-28-index %})が.debパッケージでサードパーティリポジトリを登録しパッケージをインストールする仕組みを教えてくれました。今回は、その貴重な学びを共有します。

<!--more-->

今回は、実際にインストールする必要があったApache Arrowのパッケージである`libarrow-dev`を例にとって見ていきます。[Apache Arrow](https://arrow.apache.org/install/)公式のインストールページを見てみると下記のようなコマンドが書かれています。（今回の説明に必要な部分だけ抜粋しています。）

```console
$ wget https://apache.jfrog.io/artifactory/arrow/$(lsb_release --id --short | tr 'A-Z' 'a-z')/apache-arrow-apt-source-latest-$(lsb_release --codename --short).deb
$ sudo apt install -y -V ./apache-arrow-apt-source-latest-$(lsb_release --codename --short).deb
$ sudo apt update
$ sudo apt install -y -V libarrow-dev
```

全体的な流れとしては下記のようになっていますが、具体的に何をおこなっているのか一つ一つ見ていきたいと思います。

- サードパーティのAPTリポジトリ情報を取得する
- 取得したAPTリポジトリ情報を反映する
- APTリポジトリにアクセスしパッケージ情報を更新をする
- パッケージをインストール

### サードパーティのAPTリポジトリ情報を取得する

最初に下記のコマンドでは、LinuxのディストリビューションIDとリリースコードネームを使用して、サードパーティリポジトリであるApache ArrowのAPTリポジトリの情報を含んだファイルをダウンロードしてきています。と言われても少なくとも私は分からなかったので一つずつ見ていきます。

```console
$ wget https://apache.jfrog.io/artifactory/arrow/$(lsb_release --id --short | tr 'A-Z' 'a-z')/apache-arrow-apt-source-latest-$(lsb_release --codename --short).deb
```

まず、実際にどんなURLにリクエストを飛ばしているのかを確認します。
まずは、`$(lsb_release --id --short | tr 'A-Z' 'a-z')`の部分を確認していきましょう。
大きく`lsb_release --id --short`と`tr 'A-Z' 'a-z'`の部分に分かれていて、前者のコマンドの結果をパイプで後者のコマンドに渡しています。

`lsb_release --id --short`の結果をみてみると下記のようになります。
下記のコマンドは、`lsb_release`を使って現在利用中のLinuxディストリビューションの情報を取得しています。`--id`オプションを使ってディストリビューションのIDを取得し、合わせて`--short`オプションを指定することで、ディストリビューション名を簡潔に`Ubuntu`と取得できています。コマンドに関して詳しくは、`man lsb_release`を参照してください。

```console
$ lsb_release --id
Distributor ID: Ubuntu
$ lsb_release --id --short
Ubuntu
```

次に、`Ubuntu`と取得できた結果を`tr 'A-Z' 'a-z'`に渡すことで文字列内の大文字の部分を小文字のアルファベットに変換しています。
よって、`lsb_release --id --short | tr 'A-Z' 'a-z'`の結果は、`ubuntu`になることがわかります。コマンドに関して詳しくは`man tr`で確認してみてください。

```console
$ lsb_release --id --short | tr 'A-Z' 'a-z'
ubuntu
```

更に、`lsb_release --codename --short`の結果を見てみましょう。
こちらも先程みた`lsb_release`コマンドを利用しており、`--codename`オプションでリリースコードネームを取得し、さらに`--short`オプションを指定することで、リリースコードネーム名を簡潔に`jammy`と取得しています。

```console
$ lsb_release --codename --short
jammy
```

なので最終的に利用されるリクエスト先URLは下記のようになります。

```console
$ echo "https://apache.jfrog.io/artifactory/arrow/$(lsb_release --id --short | tr 'A-Z' 'a-z')/apache-arrow-apt-source-latest-$(lsb_release --codename --short).deb"
https://apache.jfrog.io/artifactory/arrow/ubuntu/apache-arrow-apt-source-latest-jammy.deb
```

よって、このコマンドは利用者のディストリビューションとコードネームにあった.debファイルをダウンロードしています。なお、つぎのセクションでふれますが、この.debファイルはサードパーティリポジトリであるApache ArrowのAPTリポジトリの情報を含んでいます。

```console
$ wget https://apache.jfrog.io/artifactory/arrow/ubuntu/apache-arrow-apt-source-latest-jammy.deb
```

つぎは、ダウンロードしてきたAPTリポジトリの情報を含んだ.debファイルをどのように利用していくのかをみていきます。

### 取得したAPTリポジトリ情報を反映する

先ほどダウンロードした.debファイルをインストールします。これにより、サードパーティリポジトリであるApache ArrowのAPTリポジトリの情報がシステムのリポジトリリストに反映されます。と言われてもここでも私は分からなかったので一つずつ見ていきます。

```console
$ sudo apt install -y -V ./apache-arrow-apt-source-latest-$(lsb_release --codename --short).deb
```

実際にどんな情報がインストールし反映されたのかを確認してみます。次の2点が特に重要です。1点目は、`apache-arrow.sources`というファイルに記載されているリポジトリ情報です。このファイルは`/etc/apt/sources.list.d`に置かれています。2点目は、公開鍵`apache-arrow-apt-source.gpg`で、これは`/usr/share/keyrings/`に配置されています。

```console
$ dpkg -L apache-arrow-apt-source
/.
/etc
/etc/apt
/etc/apt/sources.list.d
/etc/apt/sources.list.d/apache-arrow.sources
/usr
/usr/share
/usr/share/doc
/usr/share/doc/apache-arrow-apt-source
/usr/share/doc/apache-arrow-apt-source/changelog.Debian.gz
/usr/share/doc/apache-arrow-apt-source/copyright
/usr/share/keyrings
/usr/share/keyrings/apache-arrow-apt-source.gpg
```

実際に`apache-arrow.sources`ファイルの中身をみてみましょう。ファイルの中身を見ていくと下記のような内容になっています。
- Typesには、取得可能なパッケージのタイプが指定されています
- URIsには、サードパーティリポジトリがホストしているURLが指定されています
  - パッケージを取得する際は、`https://apache.jfrog.io/artifactory/arrow/ubuntu/`にリクエストすることを示しています
- Suitesには、リポジトリが対象とするUbuntuのリリースバージョンのコードネームが指定されています
- Componentsには、パッケージのカテゴリーが指定されています
  - `main`が指定されているので、このパッケージがフリーソフトウェアであることを示しています
  - コンポーネントに関して詳しくは、[こちら](https://help.ubuntu.com/community/Repositories/)を参照してください
- Signed-Byには、インストール時にパッケージが正当なものかを確認するのに利用される公開鍵のパスが指定されています
  - 公開鍵である`apache-arrow-apt-source.gpg`をパッケージの正当性を確認する際に利用することを示しています

```console
$ cat /etc/apt/sources.list.d/apache-arrow.sources
Types: deb deb-src
URIs: https://apache.jfrog.io/artifactory/arrow/ubuntu/
Suites: jammy
Components: main
Signed-By: /usr/share/keyrings/apache-arrow-apt-source.gpg
```

ここまでで、サードパーティリポジトリであるApache ArrowのAPTリポジトリのどんな情報が反映されたのかが分かったと同時にパッケージをインストールしたいサードパーティリポジトリ情報も分かりました。
次は、その情報を利用して実際にサードパーティリポジトリが提供しているパッケージの情報を取得するのを見ていきます。

### APTリポジトリにアクセスしパッケージ情報を更新をする

おなじみの`apt update`コマンドでパッケージ情報を更新してみると、ログから先程取得したサードパーティリポジトリであるApache ArrowのAPTリポジトリにパッケージ情報を取得して無事に情報を取得できていることがわかります。

```console
$ sudo apt update
...
Hit:8 http://jp.archive.ubuntu.com/ubuntu jammy-backports InRelease                                 
Get:9 http://jp.archive.ubuntu.com/ubuntu jammy-updates/main amd64 Packages [1,421 kB]
Get:10 http://jp.archive.ubuntu.com/ubuntu jammy-updates/main i386 Packages [579 kB]
Get:11 https://apache.jfrog.io/artifactory/arrow/ubuntu jammy InRelease [7,291 B]        
Get:12 https://apache.jfrog.io/artifactory/arrow/ubuntu jammy/main Sources [19.0 kB]                                                          
Get:13 https://apache.jfrog.io/artifactory/arrow/ubuntu jammy/main amd64 Packages [112 kB]                                                    
Fetched 2,422 kB in 7s (350 kB/s)                                                                                                             
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
2 packages can be upgraded. Run 'apt list --upgradable' to see them.
```

試しに、今回インストールしたい`libarrow-dev`の情報が更新され取得されているのかを確認してみると無事にパッケージ情報を取得できています。

```console
$ sudo apt show libarrow-dev
Package: libarrow-dev
...
```

せっかくなので、もう少しく詳しくみてみましょう。特に`APT-Sources`に注目してみると、`apache-arrow.sources`ファイル内でリクエスト先であった`https://apache.jfrog.io/artifactory/arrow/ubuntu`が指定されていることがわかります。事前に取得していたサードパーティリポジトリであるApache ArrowのAPTリポジトリのURLと合致しているのがわかります。

```console
$ sudo apt show libarrow-dev
Package: libarrow-dev
Version: 15.0.0-1
Priority: optional
Section: libdevel
Source: apache-arrow
Maintainer: Apache Arrow Developers <dev@arrow.apache.org>
Installed-Size: 116 MB
Depends: libarrow1500 (= 15.0.0-1), libbrotli-dev, libbz2-dev, libcurl4-openssl-dev, liblz4-dev, libc-ares-dev, libre2-dev, libsnappy-dev, libssl-dev, libutf8proc-dev, libzstd-dev, nlohmann-json-dev | nlohmann-json3-dev, protobuf-compiler-grpc, zlib1g-dev
Homepage: https://arrow.apache.org/
Download-Size: 14.0 MB
APT-Sources: https://apache.jfrog.io/artifactory/arrow/ubuntu jammy/main amd64 Packages
Description: Apache Arrow is a data processing library for analysis
 .
 This package provides C++ header files.
```

では、最後に目的であった`libarrow-dev`をインストールしていきます。

### パッケージをインストール

無事に`libarrow-dev`がインストールされていることが確認できました。

```console
$ sudo apt install libarrow-dev
$ sudo apt list --installed libarrow-dev
Listing... Done
libarrow-dev/jammy,now 15.0.0-1 amd64 [installed]
```

### まとめ

最後にあらためて全体の流れをおさらいすると下記のようになります。特に公式のUbuntuリポジトリからパッケージをダウンロードする時との差異は、サードバーティリポジトリの情報を取得し反映するというステップが必要になる部分です。

- (追加で必要)サードパーティのAPTリポジトリ情報を取得する
- (追加で必要)取得したAPTリポジトリ情報を反映する
- (追加で必要)APTリポジトリにアクセスしパッケージ情報を更新をする
- パッケージをインストール

### おわりに

今回は、.debパッケージを利用してUbuntuのAPTでサードパーティのリポジトリからパッケージをインストールする仕組みに関して紹介しました。
他にも、PPA(Personal Package Archives) を利用してサードパーテイリポジトリを追加してパッケージをインストールする方法などもあるようですが、今回はここまでの紹介にします。
