---
title: Fluent Package v5.0.3リリース - Windows版でサービス起動が遅い問題を修正
author: daipom
tags:
  - fluentd
---

こんにちは。[Fluentd](http://www.fluentd.org)のメンテナーの福田です。

2024年3月29日にFluent Packageの最新版となる v5.0.3 をリリースしました。

Fluent Packageとは、コミュニティーが提供しているFluentdのパッケージです。
これまで提供していたtd-agentが2023年12月31日限りでEOLとなり、
Fluent Packageはその後継パッケージにあたります。

本リリースでは、いくつかの重要なバグ修正を行いました。
特にWindows版に関しては、サービス起動が遅いため起動がタイムアウトすることがあるなど、起動に関する不具合を修正しています。
この記事では、メンテナーの観点からFluentdの最新パッケージの動向を詳しく解説します。

<!--more-->

### td-agentのEOLとFluent Package

Fluent Packageとは、コミュニティーが提供しているFluentdのパッケージです。

それまでコミュニティーが提供していたFluentdのパッケージとしてtd-agentがありましたが、
td-agentは2023年12月31日限りでサポート終了となりました。

* [Drop schedule announcement about EOL of Treasure Agent (td-agent) 4](https://www.fluentd.org/blog/schedule-for-td-agent-4-eol)

Fluent Packageはtd-agent v4の後継パッケージであり、ある程度の互換性を保っています。
多くのケースではtd-agent v4から簡単にアップデート可能です。
td-agent v4がインストールされている環境に、以下の通常のインストール手順でFluent Packageをインストールするだけです。
現在td-agentをお使いの方は、ぜひFluent Packageへのアップデートをご検討ください。

詳しくは、次の資料をご覧ください。

* 公式ブログ(英語): [Upgrade to fluent-package v5](https://www.fluentd.org/blog/upgrade-td-agent-v4-to-v5)
* 弊社提供資料: [Fluent Package LTS（長期サポート版Fluentdパッケージ）ガイド ]({% link downloads/fluent-package-lts-guide.md %})
* 弊社ブログ: [Fluent Package v5.0.2リリース - in_tailプラグインの重大な不具合の修正]({% post_url 2023-12-15-fluent-package-v5.0.2 %})
  * （前半でFluent Packageについての基本的な情報を紹介しています）

### Fluent Package v5.0.3

2024年3月29日にFluent Packageの最新版となる v5.0.3 をリリースしました。

Fluent Package v5.0.3 では、同梱するFluentdのバージョンを v1.16.3 から v1.16.5 にアップデートしています。
また、パッケージ側でも多くの修正・改善が入りました。

本記事ではそれらについて詳しく紹介します。
興味のある方は、以下の公式情報もご覧ください。

公式リリースアナウンス

* Fluentd v1.16.4: https://www.fluentd.org/blog/fluentd-v1.16.4-have-been-released
* Fluentd v1.16.5: https://www.fluentd.org/blog/fluentd-v1.16.5-have-been-released
* Fluent Package v5.0.3: https://www.fluentd.org/blog/fluent-package-v5.0.3-has-been-released

CHANGELOG

* Fluentd v1.16.4: https://github.com/fluent/fluentd/blob/v1.16.4/CHANGELOG.md#release-v1164---20240314
* Fluentd v1.16.5: https://github.com/fluent/fluentd/blob/v1.16.5/CHANGELOG.md#release-v1165---20240327
* Fluent Package v5.0.3: https://github.com/fluent/fluent-package-builder/blob/v5.0.3/CHANGELOG.md#release-v503---20240329

### 不具合修正

Fluent Package v5.0.3では、次の5点の不具合修正をしています。

* Windows: サービス起動に時間がかかり、起動がタイムアウトエラーになる問題を修正
* Windows: 環境によってはFluentdワーカープロセスの起動に失敗する問題を修正
* Windows: アップデート時に設定ファイルが古い内容で上書きされてしまうことがある問題を修正
* Buffer: チャンクに収まらない大きなデータのバッファー出力に失敗することがある問題を修正
* RHEL/Debian: 名前に空白文字を含むファイルやディレクトリーが存在すると、td-agent v4からのアップデートに失敗する問題を修正

以下でそれぞれ説明します。

#### Windows: サービス起動に時間がかかり、起動がタイムアウトエラーになる問題を修正

**概要**

Windows版において、サービス起動に時間がかかり、起動がタイムアウトエラーになることがありました。
Fluent Package v5.0.0, v5.0.1, v5.0.2がこの問題の影響を受けていました。

**影響範囲**

* Fluent Package v5.0.0, v5.0.1, v5.0.2がこの問題の影響を受けていました
* CPUコア数が2以上である環境でのみ発生する可能性が高いです

**影響**

* 起動がタイムアウトエラーになることがあります
* このタイムアウトは、[サービスの開始に30秒以上かかる場合は開始処理を中断する、というWindowsの仕様](https://learn.microsoft.com/en-us/troubleshoot/windows-server/system-management-components/service-not-start-events-7000-7011-time-out-error)によるものです

補足: タイムアウトエラーが発生した場合は、EventID 7000 および 7009 のエラーレベルのSystemイベントログが出力されます。

* EventID 7000
  * 日本語
  ```
  Fluentd Windows Service サービスを、次のエラーが原因で開始できませんでした: 
  そのサービスは指定時間内に開始要求または制御要求に応答しませんでした。
  ```
  * 英語
  ```
  The Fluentd Windows Service service failed to start due to the following error: 
  The service did not respond to the start or control request in a timely fashion.
  ```
* EventID 7009
  * 日本語
  ```
  Fluentd Windows Service サービスの接続を待機中にタイムアウト (30000 ミリ秒) になりました。
  ```
  * 英語
  ```
  A timeout was reached (30000 milliseconds) while waiting for the Fluentd Windows Service service to connect.
  ```

#### Windows: 環境によってはFluentdワーカープロセスの起動に失敗する問題を修正

td-agent v4.5.0以降、fluent-package v5.0.2以前のバージョンのWindows版において、レジストリ`HKLM`（`HKEY_LOCAL_MACHINE`）や`HKCU`（`HKEY_CURRENT_USER`）の`SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\`以下に非ASCII文字を含むサブキーが存在すると、Fluentdのワーカープロセスが起動できない、という問題がありました。

例えば、他にインストールしたアプリケーションが上記レジストリに漢字等を含んだサブキーを書き込んだ環境では、Fluentdのワーカープロセスの起動が次のように失敗します。

```
Traceback (most recent call last):
        17: from <internal:gem_prelude>:1:in `<internal:gem_prelude>'
        16: from <internal:gem_prelude>:1:in `require'
        15: from C:/opt/td-agent/lib/ruby/2.7.0/rubygems.rb:1427:in `<top (required)>'
        14: from C:/opt/td-agent/lib/ruby/2.7.0/rubygems.rb:1427:in `require'
        13: from C:/opt/td-agent/lib/ruby/2.7.0/rubygems/defaults/operating_system.rb:24:in `<top (required)>'
        12: from C:/opt/td-agent/lib/ruby/site_ruby/2.7.0/ruby_installer/runtime/singleton.rb:27:in `enable_dll_search_paths'
        11: from C:/opt/td-agent/lib/ruby/site_ruby/2.7.0/ruby_installer/runtime/msys2_installation.rb:125:in `enable_dll_search_paths'
        10: from C:/opt/td-agent/lib/ruby/site_ruby/2.7.0/ruby_installer/runtime/msys2_installation.rb:115:in `mingw_bin_path'
         9: from C:/opt/td-agent/lib/ruby/site_ruby/2.7.0/ruby_installer/runtime/msys2_installation.rb:102:in `msys_path'
         8: from C:/opt/td-agent/lib/ruby/site_ruby/2.7.0/ruby_installer/runtime/msys2_installation.rb:68:in `iterate_msys_paths'
         7: from C:/opt/td-agent/lib/ruby/site_ruby/2.7.0/ruby_installer/runtime/msys2_installation.rb:68:in `each'
         6: from C:/opt/td-agent/lib/ruby/site_ruby/2.7.0/ruby_installer/runtime/msys2_installation.rb:70:in `block in iterate_msys_paths'
         5: from C:/opt/td-agent/lib/ruby/2.7.0/win32/registry.rb:542:in `open'
         4: from C:/opt/td-agent/lib/ruby/2.7.0/win32/registry.rb:435:in `open'
         3: from C:/opt/td-agent/lib/ruby/site_ruby/2.7.0/ruby_installer/runtime/msys2_installation.rb:71:in `block (2 levels) in iterate_msys_paths'
         2: from C:/opt/td-agent/lib/ruby/2.7.0/win32/registry.rb:611:in `each_key'
         1: from C:/opt/td-agent/lib/ruby/2.7.0/win32/registry.rb:910:in `export_string'
C:/opt/td-agent/lib/ruby/2.7.0/win32/registry.rb:910:in `encode': U+767E to ASCII-8BIT in conversion from UTF-16LE to UTF-8 to ASCII-8BIT (Encoding::UndefinedConversionError)
2023-06-14 16:40:41 +0800 [error]: Worker 0 exited unexpectedly with status 1
```

詳しくは以前に次の記事で解説しておりますので、ぜひご覧ください。

* [fluent-package不具合情報 - Windows版で環境によってはFluentdワーカープロセスの起動ができないことがある]({% post_url 2024-02-26-fluent-package-windows-crash-on-launch %})

#### Windows: アップデート時に設定ファイルが古い内容で上書きされてしまうことがある問題を修正

**概要**

WindowsにおいてのFluent PackageからFluent Packageへのアップデートを行う場合に、Fluent Package用の設定ファイル`fluentd.conf`が、td-agent用の旧設定ファイル`td-agent.conf`によって上書きされてしまう場合がありました。
本バージョンでこの問題を修正したため、本バージョン以降へのアップデートであれば影響ありません。

**影響範囲**

* 過去にtd-agent v4からアップデートした環境のみ、影響を受ける可能性があります。
* Fluent Package v5.0.0, v5.0.1, v5.0.2のいずれかの間でのアップデートを行う場合に、影響を受ける可能性があります。
  * ありうるのは次のケースです。
    * Fluent Package v5.0.0 から v5.0.1 にアップデートするケース
    * Fluent Package v5.0.0 から v5.0.2 にアップデートするケース
    * Fluent Package v5.0.1 から v5.0.2 にアップデートするケース
* 上範囲間でのアップデートの際に、td-agent時代の設定ファイル`\opt\td-agent\etc\td-agent\td-agent.conf`が残っていると、その内容でFluent Package用の設定ファイル`\opt\fluent\etc\fluent\fluentd.conf`が上書きされます。

#### Buffer: チャンクに収まらない大きなデータのバッファー出力に失敗することがある問題を修正

**概要**

チャンクの最大サイズ([chunk_limit_size](https://docs.fluentd.org/configuration/buffer-section#buffering-parameters)など)を超えるような大きなデータが1度に入力された際、そのデータのバッファー出力に稀に失敗することがありました。

**影響範囲**

この問題が発生する可能性としては、以下のようなデータをInputプラグインが一度に出力する場合が考えられます。

* チャンクの最大サイズ([chunk_limit_size](https://docs.fluentd.org/configuration/buffer-section#buffering-parameters)など)を超える
  * [file buffer](https://docs.fluentd.org/buffer/file)のデフォルトの最大サイズは256MBです
* データ内に異常にサイズの大きいログが混じる
  * チャンクの最大サイズを超えるデータが一度に入力されたとしても、Bufferはそのデータを上手く分割して処理します
  * ただし、データの中に異常にサイズの大きいログが混じっている場合は、データの順番によっては本問題が発生する可能性があります

**影響**

この問題が発生した場合は、次のように`emit transaction failed: error_class=IOError`のような警告ログが発生します。
この場合、該当部分の収集データが破棄されてしまうため、ログロストを防ぐためにリカバリーが必要となります。

```
2024-03-22 14:13:35 +0900 [warn]: #0 emit transaction failed: error_class=IOError error="closed stream" location="/path/to/fluentd/lib/fluent/plugin/buffer/file_chunk.rb:82:in `pos'" tag="test"
```

また、Inputプラグインによっては動作に影響します。
`in_windows_eventlog2`と`in_forward`については、次のように影響があることが分かっています。

* `in_windows_eventlog2`
  * 本問題が発生したチャンネルの読込を停止してしまいます
* `in_forward`
  * `out_forward`側で[require_ack_response](https://docs.fluentd.org/output/forward#require_ack_response)を`true`に設定している場合、データの再送とエラーを繰り返してしまいます

#### RHEL/Debian: 名前に空白文字を含むファイルやディレクトリーが存在すると、td-agent v4からのアップデートに失敗する問題を修正

**概要**

* RHEL, Debian/Ubuntu用のFluent Package v5.0.0, v5.0.1, v5.0.2のバグです
* 特定の条件を満たす場合(後述する「影響範囲」参照)、td-agent v4からFluent Package v5.0.0 ~ v5.0.2へのアップデートが正常に行われないことがあります
* Fluent Package v5.0.3で本問題は修正されたため、v5.0.3以降へのアップデートであれば影響はありません

**影響範囲**

* RHEL, Debian/Ubuntu環境において、td-agent v4からFluent Package v5.0.0, v5.0.1, v5.0.2へアップデートする際に影響を受ける可能性があります
* 上ケースにおけるアップデート前の状態において、次の場所に、名前に空白文字を含むファイルやディレクトリーが存在すると、アップデートが正常に行われません
  * `/etc/td-agent/`直下
  * `/var/log/td-agent/`直下

**影響**

* アップデートが正常に行われず、以下の問題を引き起こす可能性があります
  * アップデート後、サービス起動に失敗する
  * アップデート後、以前のposファイルやbufferファイルを引き継がずに起動してしまう

### 機能改善

Fluent Package v5.0.3では、次の2点の機能改善をしています。

* RHEL: td-agent v4からのアップグレードの際、サービスの自動起動設定を引き継ぐように改善
* fluent-diagtool: 手動インストールプラグイン一覧確認機能をWindowsでも使えるように改善

以下でそれぞれ説明します。

#### RHEL: td-agent v4からのアップグレードの際、サービスの自動起動設定を引き継ぐように改善

RHEL版のFluent Packageの自動起動設定についての改善です。
本バージョンから、td-agent v4からのアップデート時に、アップデート前の自動起動設定が引き継がれるようになりました。

Fluent Packageの各バージョンにおける自動起動設定の取り扱いをまとめると、次のようになります。

* v5.0.0, v5.0.1, v5.0.2
  * 新規インストール時: 自動起動は無効
  * td-agent v4からのアップデート時: 自動起動は無効
  * 自動起動を有効にするには、`systemctl enable`コマンドを実行する必要があります
    * 例: `sudo systemctl enable fluentd`
* v5.0.3 以降
  * 新規インストール時: 自動起動は無効
  * td-agent v4からのアップデート時: アップデート前の自動起動設定を引き継ぐ

#### fluent-diagtool: 手動インストールプラグイン一覧確認機能をWindowsでも使えるように改善

手動インストールプラグイン一覧を確認できる`fluent-diagtool`が、Windowsでも使えるようになりました。

`fluent-diagtool`は、パッケージに同梱されている診断ツールで、Fluentd実行環境に関する様々な情報を収集することができます。
Fluent Package v5.0.2で手動インストールプラグインの一覧を確認できるようになりましたが、Windowsは非対応でした。
本バージョンから、Windowsでも手動インストールプラグインの一覧を確認できるようになりました。

以下、`fluent-diagtool`を使った手動インストールプラグイン一覧の確認方法をまとめます。

RHEL, Debian/Ubuntuでの実行例:

```bash
/opt/fluent/bin/fluent-diagtool -t fluentd -o /tmp
```

Windowsでの実行例(管理者権限の`Fluent Package Command Prompt`):

```bash
fluent-diagtool -o /opt/fluent
```

td-agent v4での実行例(RHEL, Debian/Ubuntuのみ):

```bash
sudo td-agent-gem install fluent-diagtool
/opt/td-agent/bin/fluent-diagtool -t fluentd -o /tmp
```

RHEL, Debian/Ubuntuに限り、`fluent-diagtool`の最新版をインストールすることで、td-agent v4でも確認が可能です。
td-agent v4からFluent Packageへアップデートする際などに役立ちます。
ただし、残念ながらWindowsではfluent-package v5.0.3以降でしか使えません。
元々Windowsで使えなかったのは、パッケージ本体の問題に起因していたためです。

コマンドを実行すると、次のように手動インストールプラグイン一覧を確認できます。
また、`-o`で指定したディレクトリー配下の`(実行日時)/output/gem_local_list.output`のようなパスにファイルとしても結果を出力します。

```
2023-12-07 04:26:11 +0000: [Diagtool] [INFO] Parsing command options...
(...)
2023-12-07 04:26:12 +0000: [Diagtool] [INFO] [Collect] fluent-package manually installed gem information is stored in /tmp//20231207042611/output/gem_local_list.output
2023-12-07 04:26:12 +0000: [Diagtool] [INFO] [Collect] fluent-package manually installed gems:
2023-12-07 04:26:12 +0000: [Diagtool] [INFO] [Collect]   * fluent-plugin-concat
2023-12-07 04:26:12 +0000: [Diagtool] [INFO] [Collect]   * fluent-plugin-remote_syslog
(...)
```

### まとめ

今回は、Fluentdの最新パッケージFluent Package v5.0.3の情報をお届けしました。

多くの不具合を修正しているので、最新版のFluent Packageへのアップデートを推奨します。
その際には、長期の安定運用がしやすいLTS版をぜひご活用ください。

* [Download Fluent Package](https://www.fluentd.org/download/fluent_package)
* [Installation Guide](https://docs.fluentd.org/installation)

また、クリアコードは[Fluentdのサポートサービス]({% link services/fluentd-service.md %})を行っています。
Fluent Packageへのアップデートについてもサポートいたしますので、詳しくは[Fluentdのサポートサービス]({% link services/fluentd-service.md %})をご覧いただき、[お問い合わせフォーム]({% link contact/index.md %})よりお気軽にお問い合わせください。

最新版を使ってみて何か気になる点があれば、ぜひ[GitHub](https://github.com/fluent/fluentd/issues)でコミュニティーにフィードバックをお寄せください。
また、[日本語用Q&A](https://github.com/fluent/fluentd/discussions/categories/q-a-japanese)も用意しておりますので、困ったことがあればぜひ質問をお寄せください。
