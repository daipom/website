---
title: 'PGroongaをデバッグするときの便利コマンド'
author: abetomo
tags:
- groonga
---

PGroongaをデバッグ中の阿部です。

PGroongaはPostgreSQLの拡張機能です。つまりPGroongaのデバッグをするとなったらPostgreSQLを起動する必要があります。

デバッグの際は不要なデータや不要な設定がない、起動したてのPostgreSQLで実施したくなります。
不要データや設定があるとそれによって挙動が変わってしまい、スムーズなデバッグができないことが多いためです。

ということで、今回は不要データや設定のない起動したてのPostgreSQLを手に入れる便利コマンドを紹介します。

PostgreSQLの拡張機能を開発中の方や、デバッグと同様に不要なデータがない方がよいPostgreSQLが関係するテストを
実施したい方の参考になると思います。

<!--more-->

# はじめに

ターミナルでシェルを利用している環境では、次の流れで便利になります。

1. ターミナルでいくつかのコマンドを組み合わせて1行（1回）にまとめて実行する
2. シェルの履歴に登録される
3. 履歴から同じコマンドを実行。便利！

伝わりやすさのため「便利コマンド」という表現を使いましたが、いくつかのコマンドを組み合わせているので「ワンライナー」と表現したほうが正確かもしれないです。

以降で「便利コマンド」を紹介していきますが、見やすさのために改行を入れて複数行で記載します。
適宜、1行にして実行するなど環境に応じて便利にご利用ください！

# 準備

PostgreSQLのインストールは済んでいるものとします。
どのような方法でのインストールでも構いませんが、適宜 `PATH` の設定などもお願いいたします。
紹介するコマンド例では `PATH` が通っている前提の書き方をしているので `PATH` を設定しておくか、適宜コマンドを置き換えてください。

また、以降の手順ではPostgreSQLが起動して'いない'ことを前提としています。
（すでに起動している場合は「同じPort番号でPostgreSQLがすでに起動中です」といったエラーになります。）

# 紹介するコマンド

以下の3つの便利コマンドを紹介します。

1. 不要なデータや設定がないPostgreSQLの起動
2. `psql` で接続、ついでにいろいろ初期設定
3. 不要なデータや設定がないPostgreSQLの起動（プライマリー・スタンバイ）

# 1. 不要なデータや設定がないPostgreSQLの起動

以下の5行コマンドは不要なデータや設定がないPostgreSQLを起動する例です。
実行するたびに不要なデータや設定がないPostgreSQLの起動できるので大変便利です。

```bash
ulimit -c unlimited;
base_dir=/tmp/pgroonga.debug;
rm -rf ${base_dir};
initdb --locale=C.UTF-8 --set=pgroonga.log_rotate_threshold_size=10 ${base_dir} && \
  LANG=C.UTF-8 postgres -D ${base_dir})
```

コマンドやオプションの意味を簡単に解説をします。

```bash
ulimit -c unlimited;
```

これはコアダンプの出力サイズを無制限にしています。
目的がデバッグの場合は完全なコアダンプが欲しいためです。
（コアダンプが不要であればこのコマンドは省略してください。）

```bash
base_dir=/tmp/pgroonga.debug;
rm -rf ${base_dir};
```

起動前にすべてを削除しています。

```bash
initdb --locale=C.UTF-8 --set=pgroonga.log_rotate_threshold_size=10 ${base_dir} && \
  LANG=C.UTF-8 postgres -D ${base_dir})
```

横に長いので2行にしていますが、実際は1行です。`initdb` が成功したら `postgres` を起動します。
ポイントは `--set=pgroonga.log_rotate_threshold_size=10` です。
`initdb` で適用したい設定を指定することができます。複数あれば複数 `--set=x=y` を並べればOKです。
このオプションの指定によりデバッグやテストで利用したい設定でPostgreSQLを起動することができます。

# 2. `psql` で接続、ついでにいろいろ初期化

```bash
psql -d postgres -c 'DROP DATABASE IF EXISTS pgroonga_test' -c 'CREATE DATABASE pgroonga_test' && \
  psql -d pgroonga_test -c 'CREATE EXTENSION pgroonga' && \
  psql pgroonga_test
```

まずテスト用のデータベース（`pgroonga_test`）を作ります。存在していたら一旦削除してデータベースを作っているコマンドです。
その次に拡張機能であるPGroongaを有効化しています。
最後にPGroongaが使えるようになった `pgroonga_test` に `psql` に接続しています。

# 3. 不要なデータや設定がないPostgreSQLの起動（プライマリー・スタンバイ）

### プライマリー起動

「1. 不要なデータや設定がないPostgreSQLの起動」と同様です。

PGroongaのデバッグという観点では `initdb` のときに `--set=pgroonga.enable_wal=yes` が必要です。

`psql` での接続についても「2. `psql` で接続、ついでにいろいろ初期化」と同様です。

### スタンバイ起動

プライマリーを起動したサーバ内で、スタンバイも起動するときのコマンド例です。

```bash
base_dir=/tmp/pgroonga.standby;
rm -rf ${base_dir};
pg_basebackup -D ${base_dir} --progress --verbose --write-recovery-conf && \
  (echo "port = 15432") >> ${base_dir}/postgresql.conf && \
  postgres -D ${base_dir})
```

コマンドやオプションの意味を簡単に解説をします。（最初の削除部分の説明は割愛します。）

```bash
pg_basebackup -D ${base_dir} --progress --verbose --write-recovery-conf && \
  (echo "port = 15432") >> ${base_dir}/postgresql.conf && \
  postgres -D ${base_dir})
```

`pg_basebackup` でプライマリーのバックアップを取得してレプリケーションの準備をします。
`--write-recovery-conf`オプションで最低限のスタンバイサーバの設定ファイルを生成しています。
（`--progress`は進行状況を表示、`--verbose` で出力内容が増えます。）

同一サーバ内で同じポート番号でサーバは起動できないため、次のコマンドで別のポート番号で起動するように設定ファイルに書き込んでから、PostgreSQLサーバを起動しています。

```bash
  (echo "port = 15432") >> ${base_dir}/postgresql.conf && \
  postgres -D ${base_dir})
```

スタンバイサーバの起動については以上で、最後に `psql` で接続するコマンドの説明です。

```bash
psql --port 15432 pgroonga_test
```

プライマリーのほうで初期化は済んでいるので、シンプルに接続すればよいです。
ただしポート番号を変えているので、`--port` で指定します。

# まとめ

普段利用している「PGroongaをデバッグするときの便利コマンド」を紹介しました。
シェルの履歴と組み合わせることでより効率的にデバッグやテストが行えます。

# 補足

「はじめに」にも書きましたが見やすさ重視でシェルスクリプトのような見た目で紹介しました。
参考までに1行にした例も記載いたします。

「1. 不要なデータや設定がないPostgreSQLの起動」の例:

```console
$ (ulimit -c unlimited; base_dir=/tmp/pgroonga.debug; rm -rf ${base_dir}; initdb --locale=C.UTF-8 --set=pgroonga.log_rotate_threshold_size=10 ${base_dir} && LANG=C.UTF-8 postgres -D ${base_dir})
```
