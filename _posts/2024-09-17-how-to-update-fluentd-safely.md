---
title: "Fluentdを安全にバージョンアップしよう（td-agent -> Fluent Package）"
author: daipom
tags:
  - fluentd
---

こんにちは。データ収集ツール[Fluentd](http://www.fluentd.org)のメンテナーの福田です。

Fluentdのパッケージとして有名だったtd-agent v4が2023年12月31日にEOLとなり、
その後継パッケージとしてFluent Package(fluent-package) v5をリリースしています。

Fluent Packageでは多くのバグ修正や脆弱性対応が入っているので、
ぜひ最新版にアップデートをしていただきたいですが、
互換性は大丈夫なのか、アップデート前後でデータの収集はどうなるのか、
といった不安もあるかと思います。

本記事では、Fluentdを安全にアップデートするためのポイントを紹介します。

<!--more-->

### td-agentのEOLとFluent Package(fluent-package)

Fluent Package(fluent-package)とは、コミュニティーが提供しているFluentdのパッケージです。

それまでコミュニティーが提供していたFluentdのパッケージとしてtd-agentがありましたが、
td-agent v4は2023年12月31日限りでサポート終了となりました。

* [Drop schedule announcement about EOL of Treasure Agent (td-agent) 4](https://www.fluentd.org/blog/schedule-for-td-agent-4-eol)

Fluent Package v5はtd-agent v4の後継パッケージであり、ある程度の互換性を保っています。
多くのケースではtd-agent v4から簡単にアップデート可能です。
td-agent v4がインストールされている環境に、通常のインストール手順でFluent Packageをインストールするだけです。

詳しくは、次の資料をご覧ください。

* 公式ブログ(英語): [Upgrade to fluent-package v5](https://www.fluentd.org/blog/upgrade-td-agent-v4-to-v5)
* 弊社提供資料: [Fluent Package LTS（長期サポート版Fluentdパッケージ）ガイド ]({% link downloads/fluent-package-lts-guide.md %})
* 弊社ブログ: [Treasure Agent (td-agent) v3からFluent Package v5へのアップグレード方法]({% post_url 2024-08-30-fluentd-upgrade-from-td-agent-v3 %})
  * このブログではtd-agent v3 (td-agent v4のさらに前のパッケージ) からアップデートする方法について紹介しています

また、Fluent Packageについての弊社の次の発表スライドもあります。

<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/daipom/lts-release-announce/viewer.html"
          width="640" height="404"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; box-sizing: content-box; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/daipom/lts-release-announce/" title="
Fluent Package LTS">Fluent Package LTS</a>
  </div>
</div>

### 基本的に互換性があるので安心

td-agent v4 からFluent Package v5へアップデートする際に気になる互換性ですが、
基本的には互換性を保っているので安心です。

* 以前の設定ファイルをそのまま使えます
* 以前のBufferファイルやpos/storageファイルを引き継いで動作します
* コマンドやパス構成が変わりますが、以前のコマンド名やパスも引き続き使えます

それぞれ説明します。

#### 以前の設定ファイルをそのまま使えます

設定ファイルについて互換性を失うような変更はありませんので、
以前の設定ファイルをそのまま使うことができます。

#### 以前のBufferファイルやpos/storageファイルを引き継いで動作します

Fluentdが動作に利用するBufferファイルやpos/storageファイルについても互換性がありますので、
アップデート後に以前のファイルを引き継いで動作を継続できます。

Bufferファイルの引き継ぎ例

* 一部のデータの転送が完了せずBufferファイルに残っている状態でも、アップデート後にそのデータの転送処理を継続できます

pos/storageファイルの引き継ぎ例

* `in_tail`や`in_windows_eventlog`プラグインは、アップデート後に以前の収集位置の続きから収集を再開できます
  * ただし、`in_tail`については注意点もあります。それについては後述します。

#### コマンドやパス構成が変わりますが、以前のコマンド名やパスも引き続き使えます

パッケージの操作に関するコマンド名が次のように変わりますが、
td-agent v4からアップデートした環境ではエイリアスとして以前のコマンド名も引き続き利用可能です。

* `td-agent` -> `fluentd`
* `td-agent-gem` -> `fluent-gem`

また、パスなどの一部の構成が変化しますが、td-agent v4からアップデートした環境では、
シンボリックリンクやエイリアスとして従来の形も引き続き利用可能です。
以下はLinuxを例に説明します。

* 設定ファイルディレクトリー: `/etc/td-agent/` -> `/etc/fluent/`
* 設定ファイルパス: `/etc/td-agent/td-agent.conf` -> `/etc/fluent/fluentd.conf`
* ログファイルディレクトリー: `/var/log/td-agent/` -> `/var/log/fluent/`
* サービス名: `td-agent` -> `fluentd`

`/etc/td-agent/`や`/var/log/td-agent/`配下に配置していたファイルは、
アップデート後もそのまま使えますので心配ありません。

一方で、以下については以前のパスが使えないため、注意が必要です。

* インストールディレクトリー: `/opt/td-agent/` -> `/opt/fluent/`
* ログファイル名: `td-agent.log` -> `fluentd.log`

詳しくは後述します。

### 注意が必要なポイント

以上のように基本的には互換性がありますが、一部注意が必要なポイントもあります。

以下の4点をご紹介します。

* Fluentd停止中の収集について
* Fluentd自身の動作ログファイルの変更について
* インストールディレクトリーの変更について
* Windows版の注意点について

#### Fluentd停止中の収集について

アップデートの際に一時的にFluentdを停止する必要があります。
このことが収集に与える影響について説明します。

##### `in_tail`プラグイン

`in_tail`プラグインは、posファイルを引き継いで動作するため、
アップデート後も以前の収集位置の続きから収集を再開できます。

そのため、基本的にはデータのロスや重複なくアップデート可能なのですが、
設定や対象ファイルの性質次第では一部のデータを収集できないケースがありえます。

例えば、定期的にログローテートするようなファイルを収集しているケースです。

Fluentdの停止中にログローテートが発生すると、
未収集ログがローテート済みファイルに存在する可能性があり、
動作再開後にその未収集ログを収集できません。

例えば、`/var/log/messages`を[path](https://docs.fluentd.org/input/tail#path)に設定して収集するケースを考えると、
次の場合に収集できないログが発生します。

1. アップデートのためtd-agentを停止
1. `/var/log/messages`に新規ログがいくつか発生
1. `/var/log/messages`にログローテート発生
   * `/var/log/messages-20240909`のような別ファイルにリネームされる
   * この別名ファイルに未収集ログが残っている状態になる
1. アップデートが完了し、Fluent Packageが動作開始
1. `/var/log/messages`の収集を再開するが、`/var/log/messages-20240909`の方に残った未収集ログは収集できない

こういった事態を回避するためには、ログローテートが発生する時間帯を確認しておき、その時間帯を避けてアップデートを実施するなどの対策をご検討ください。

また、[follow_inodes](https://docs.fluentd.org/input/tail#follow_inodes)設定を使って、ワイルドカードでローテート済みファイルも含めて収集対象とすることができます。
こうすることで、停止中のログローテート問題は発生しなくなります。
ただし、以前のバージョンではこの機能に深刻なバグがあり、以下のバージョンより前でこの回避策を使うことは推奨できません。

* td-agent v4.5.2
* Fluent Package v5.0.2

最新のFluent Packageにアップデートした上で、ぜひ使ってみてください。
また、このバグについて詳しくは次の記事をご覧ください。

* [Fluent Package v5.0.2リリース - in_tailプラグインの重大な不具合の修正]({% post_url 2023-12-15-fluent-package-v5.0.2 %})

##### `in_tcp`や`in_syslog`などのサーバー系のInputプラグイン

以下のようなサーバー系のInputプラグインは、停止中にデータを受け付けられません。

* `in_tcp`
* `in_udp`
* `in_http`
* `in_syslog`

そのため、送信側に再送機能がなければ、停止中に送られてきたデータをロストしてしまうことになります。

これは現状Fluentd側ではどうしようもない問題ですが、現在ダウンタイムが発生しないアップデートを可能にするための機能を開発中です。

* https://github.com/fluent/fluentd/issues/4622

今後のバージョンの話にはなってしまいますが、この開発が完了すればこのような問題は発生しなくなるでしょう。

#### Fluentd自身の動作ログファイルの変更について

Linuxを例に説明します。

Fluent Packageはデフォルトで`/var/log/fluent/fluentd.log`に自身の動作ログを出力します。

`/var/log/fluent/`というディレクトリーについては、以前のパス`/var/log/td-agent/`でもアクセスできるようにシンボリックリンクが提供されます。
しかし、新規ログは`fluentd.log`という新しいファイル名で出力される、という点には注意が必要です。

例えば、次のようにtd-agentのログファイルを`in_tail`で収集しているケースです。

```xml
<source>
 @type tail
 path /var/log/td-agent/td-agent.log
 ...
</source>
```

アップデート後も引き続きこの`td-agent.log`ファイルにアクセスできますが、
新規ログはこのファイルにもう出力されないので、設定の変更が必要です。

また、Fluentd自身の動作ログを収集したい場合は、
ログファイルを収集するのではなく、[@FLUENT_LOG](https://docs.fluentd.org/deployment/logging#capture-fluentd-logs)ラベルを使う方法を推奨します。
この方法であればログファイルパスに依らずに効率的に動作ログを扱うことが可能です。

対応するログローテート設定ファイルも、次のように変わっています。

* `/etc/logrotate.d/td-agent` -> `/etc/logrotate.d/fluentd`

もし`/etc/logrotate.d/td-agent`をカスタマイズされている場合は、
そのカスタマイズ内容を`/etc/logrotate.d/fluentd`にも適用する必要があります。

#### インストールディレクトリーの変更について

Linuxを例に説明します。

td-agent v4からFluent Package v5へアップデートするにあたり、
インストールディレクトリーが次のように変わります。

* `/opt/td-agent/` -> `/opt/fluent/`

この変更については互換性のためのシンボリックリンクが提供されないので、
実行ファイルをフルパスで指定している場合などは注意してください。

#### Windows版の注意点について

Windows版固有の注意点を説明します。

##### 自動開始設定

Fluent Packageでは、Windowsサービスの自動開始設定がデフォルトで「手動」になっており、
システムの再起動後に自動で起動しない仕様となっています。

従来どおり、再起動後に自動でサービスが開始するように設定するには、
サービスの設定で「スタートアップの種類」を「遅延開始」に変更するか、
管理者権限の`Fluent Package Command Prompt`(または管理者権限のコマンドプロンプト)で以下のコマンドを実行します。

```bash
sc config fluentdwinsvc start=delayed-auto
```

また、現在Fluent Packageをアップデートする毎にこの設定が「手動」に戻ってしまうため、
バージョンアップ後はこの自動開始設定を確認して、必要であれば再設定を行うようにしてください。

この点については現在改善を検討しています。

##### サービス起動時コマンド引数

Windowsサービスの起動時コマンド引数をカスタマイズすることが可能ですが、
これはアップデート時に初期化されるため、カスタマイズしている場合は再設定が必要です。

この点について詳しくは、

* 弊社提供資料: [Fluent Package LTS（長期サポート版Fluentdパッケージ）ガイド ]({% link downloads/fluent-package-lts-guide.md %})

の「移行に関連して注意が必要なケース」をご覧ください。

### まとめ

Fluentdを安全にアップデートするための主なポイントを紹介しました。

Fluent Packageでは多くのバグ修正や脆弱性対応が入っているので、
ぜひこの記事の内容を参考にアップデートをご検討ください！

また、クリアコードは[Fluentdのサポートサービス]({% link services/fluentd-service.md %})を行っています。
Fluent Packageへのアップデートについてもサポートいたしますので、詳しくは[Fluentdのサポートサービス]({% link services/fluentd-service.md %})をご覧いただき、[お問い合わせフォーム]({% link contact/index.md %})よりお気軽にお問い合わせください。
