---
tags:
- apache-arrow
title: Apache ArrowとGObject Introspection
author: kou
---
[Apache Arrow](https://arrow.apache.org/)の開発に参加している須藤です。現時点で[apache/arrowのコミット数](https://github.com/apache/arrow/graphs/contributors)は1位です。私はRubyでデータ処理できるようになるといいなぁと思ってApache Arrowの開発に参加し始めました。同じような人が増えるといいなぁと思ってなにか試したりしましたが、あいかわらず、今でも実質1人でApache ArrowのRuby対応をしています。何度目かの「もっと仲間を増やさないと！」という気持ちになったので、最近の活動を紹介して仲間を増やそうと試みます。

今回はGObject Introspection関連のことを紹介します。最近、GObject Introspectionを採用することで狙っていたことが実現しかけているのです。

<!--more-->

### GObject Introspection

[GObject Introspection](https://gi.readthedocs.io/en/latest/)はバインディングを自動生成するための仕組みの一つです。[バインディングを作る方法は手書きから自動生成までいろいろあります]({% post_url 2016-09-14-index %})が、GObject Introspectionのアプローチが他の自動生成系のアプローチと違うのは「バインディングを作る側ががんばるのではなく、バインディング対象の側ががんばる」という点です。

他の自動生成系のアプローチではバインディング対象が普通に提供している情報（ヘッダーファイルなど）をバインディングを作る側ががんばって解釈して利用します。一方、GObject Introspectionのアプローチではバインディング生成に必要な情報をバインディング対象が提供します。バインディングを作る側は標準化された情報を活用するだけなので、バインディング対象ごとにがんばるのではなく汎用的に用意した方法を流用できます。

### Apache ArrowとGObject Introspection

Apache ArrowのRubyバインディングはGObject Introspectionベースの仕組みで実装しました。どうしてこの仕組みにしたかというと、開発者を増やすためです！どーん！ただ、現時点でそんなに開発者は増えていないんですけどね。。。

どうしてGObject Introspectionベースの仕組みにすると開発者が増えるかというと、Ruby以外の言語のコミュニティの人とも共同で開発できるからです。

GObject IntrospectionはRubyでだけ使える仕組みではなく、プログラミング言語に依存しない汎用的な仕組みなので、ある言語にGObject Introspection対応ツールがあればそのツールを使って様々なライブラリーのその言語用バインディングを作れます。GObject Introspectionは[GNOME](https://www.gnome.org/)用に作られた仕組みなので、GTK用のライブラリーがある言語ではだいたいGObject Introspection対応ツールがあります。

Apache ArrowのC++実装にはRubyバインディングだけではなく、PythonバインディングやRバインディングなどがあります。PythonとRのバインディングは自動生成ではなく手書きで実装されていて、わりとC++実装の変更にも追従しています。各バインディングにそれなりの開発者がいれば追従していけますが、開発者が少ないと追従するのは大変です。各バインディングごとに同じようなことをしないといけないからです。

* C++実装を改良
  * →Pythonバインディングを更新
  * →Rバインディングを更新

一方、GObject Introspectionに対応した「GLibバインディング」というやつを経由すると次のような仕組みにできます。

* C++実装を改良
  * →GLibバインディングを更新
    * →Rubyバインディングは自動生成
    * →XXXバインディングは自動生成
    * →YYYバインディングは自動生成
    * →ZZZバインディングは自動生成

今はRubyバインディングだけがGLibバインディングを使っているのでRubyバインディングを手書きするのとあまり変わりませんが（経由している分、むしろ面倒になっている部分はある）、他にもGLibバインディングを使うバインディングが増えると、みんなで共通のGLibバインディングを開発するだけで関連バインディング全体で追従できます。

この体制を狙っていました。

### 最近のApache ArrowとGObject Introspection

最近、[C#バインディング](https://github.com/apache/arrow/pull/41886)と[Dバインディング](https://github.com/apache/arrow/pull/41886)がGLibバインディングを使おうかなーという雰囲気を醸し出しています。

Apache ArrowのC#実装はバインディングではなく、1から実装したものなので、普通はバインディングは必要ありません。ただ、C++実装にあってC#実装にない一部の機能を使いたいので、その部分だけバインディングを作って使いたいと考えています。

Apache ArrowのD実装はまだありません。D用のApache Arrow実装を1から作るのは大変なので、D実装は最初からすべてバインディングでいこうとしています。

どちらもまだ仕上がっていませんが、ここらへんが動いてくると私が狙っていた（間接的に）Rubyバインディングの開発者が増えるということを実現できそうです。

### まとめ

GObject Introspectionを導入した狙いや最近のGObject Introspectionまわりを紹介しました。Apache ArrowのRuby対応に興味がでてきた人は[Red Data Toolsのチャット](https://app.element.io/#/room/#red-data-tools_ja:gitter.im)に来てください。その人に合わせてどのあたりからやるとよさそうかを提案します。なお、この一連の話を書き始めてから興味を持ってくれた[@hiroysato](https://x.com/hiroysato)はGObject Introspectionに興味があったことがきっかけの一つだそうです。

それはそうとして、apache/arrowコミット数1位の私にApache Arrow関連のサポートを頼みたいという場合は[クリアコードのApache Arrowサービス]({% link services/apache-arrow.md %})をどうぞ。
