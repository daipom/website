---
title: "git push origin mainの意味をずっと勘違いしていた話"
author: daipom
tags:
  - git
---

こんにちは。Gitはコマンド操作とクライアントアプリ操作を使い分けるのが好きな福田です。

突然ですが問題です。
このGitのよくあるコマンドラインを見てください。

* `git push origin main`

この`main`って、どの`main`だと思いますか？

...答えは、ローカルリポジトリーの`main`です。

え、当たり前ですかね...？
なんと私は10年以上にわたって、これが`origin`の`main`のことだと勘違いしながら使い続けてきました。

え、結果的に同じことじゃん、って？
まあそうなんですが、この勘違いが見事にpush誤爆を生みまして、真相を知った私はまあ大きな衝撃を受けたわけです。

この記事では、私の勘違いポイントと、その勘違いが招いたpush誤爆を紹介します。

<!--more-->

### 私の勘違いポイント

```bash
git push remote branch-A
```

というGitコマンドを考えてみます。
私はこのコマンドが、

* ローカルリポジトリーの今のブランチを、`remote`の`branch-A`にpushする

というものだと勘違いしたまま、10年以上使っていました。

正しくは、

* ローカルリポジトリーの`branch-A`を、`remote`の`branch-A`にpushする

です。

このコマンドで指定する`branch-A`は、push**元**のブランチ名を指します。
私はこれをpush**先**だと勘違いしていたわけです。
いつも「元」と「先」が同じブランチ名だったので、ずっと気付かなかったわけですね。。

さて、push**先**のブランチ名はどのように決まるのでしょうか？
実は冒頭のコマンドは省略形であり、自動的にpush**元**と同じになります（大抵は[^omit-dst]）。
そして、省略しない場合は次のような形になります。

```bash
git push remote branch-A:branch-B
```

これがフルの形で、

* ローカルリポジトリーの`branch-A`を、`remote`の`branch-B`にpushする

ということになります。

この最後の`:branch-B`の部分が省略可能なわけです。
冒頭のコマンドは、次の省略形だったわけです。

```bash
git push remote branch-A:branch-A
```

[^omit-dst]: push元としては`HEAD`など単なるブランチ名以外の形式も使えます。そのような場合のpush先の省略の仕組みは私には分かりません。[ドキュメント](https://git-scm.com/docs/git-push#Documentation/git-push.txt-ltrefspecgt82308203)では`such a push will update a ref that <src> normally updates without any <refspec> on the command line.`と説明されています。

### 勘違いが招いたpush誤爆

その時私は、GitHubのとあるリポジトリーに寄せられた、とあるpull requestのレビューをしていました。
最後に少し修正が必要になったので、私が代わりに修正をpushしようと思ったのが発端です。

こういう場合、いつもの私は、pull request元のブランチをローカルリポジトリーにfetchしてブランチを切替え、修正を足してpushします。

![usual action]({% link /images/blog/my-misunderstanding-bout-git-push/usual.png %})

しかし、その時のpull requestは、forkリポジトリーの`main`ブランチからリクエストされていました。
ローカルリポジトリーの既存の`main`ブランチと被ってしまうので、仕方なく私は、適当に名前を付けたブランチを作成しました。

作成したブランチ名を仮に`tmp-feature`とすると、やりたかったのは次の図のような操作です。

![expected action]({% link /images/blog/my-misunderstanding-bout-git-push/expected.png %})

さて、`tmp-feature`に修正を足し、いよいよpushしようとした時に、私の勘違いが10年越しに火を吹きます。
私はpush先のブランチ名を書くものだと勘違いしていたので、次のようにpushしたのです。

```bash
git push --force fork main
```

* `fork`はpull request元のforkリポジトリーに名付けたリモート名とします
* なぜ`--force`を付けたのか当時の記憶が定かではないです
  * `--force`を付けていなければ、コマンドが失敗して事故は防げていたでしょう
  * もしかしたら`rebase`などをしていたのかもしれませんし、一度失敗して深く考えずに付けたのかもしれません...

さて、どうなるでしょう？

そうです、ローカルリポジトリーの`main`を、forkリポジトリーの`main`へpushすることになります。
結果、pull requestは差分なしの状態となって、自動クローズされてしまいました。
なんということでしょう[^after]。。

![actual action]({% link /images/blog/my-misunderstanding-bout-git-push/actual.png %})

正しくは、次のようにpushする必要があったわけですね。

```bash
git push fork tmp-feature:main
```

[^after]: 結局、ブランチを修正してpull requestを開き直していただくことになりました。。

### おすすめのやり方

ここまで私の勘違いと実際にやってしまった誤爆について紹介しました。

ふりかえると、そもそもpushの引数でpush元やpush先を考えるのではなく、事前にリモート追跡ブランチを設定しておいて、pushの引数を省略するのが便利でおすすめです。

forkリポジトリー毎fetchして修正をpushする例:

```bash
git remote add fork {forkリポジトリーのURL}
git fetch --all --prune --tags --force -j$(nproc)
git switch -c tmp-feature fork/main
# 修正実施
git add {修正したやつ}
git commit {略}
git push
```

※ このようにforkリポジトリーを丸ごとfetchしなくても、ブランチだけfetchすることもできるようです。
詳しくは次のドキュメントをご覧ください。

* [プルリクエストをローカルでチェック アウトする](https://docs.github.com/ja/pull-requests/collaborating-with-pull-requests/reviewing-changes-in-pull-requests/checking-out-pull-requests-locally)

### 補足: GitHubのpush権限について

GitHubにおいて他の人のリポジトリーにpushするには、その権限をもらう必要があります。

しかしpull requestの場合は、pull requestの作成者が、pull request元のブランチにpushする権限を、アップストリームのリポジトリーのメンテナーに与えることができます。

このため、今回紹介したようにpull requestにメンテナーがコミットを追加する、ということができる場合があります。

詳しくは次のドキュメントをご覧ください。

* [フォークから作成されたプルリクエストのブランチへの変更の許可](https://docs.github.com/ja/pull-requests/collaborating-with-pull-requests/working-with-forks/allowing-changes-to-a-pull-request-branch-created-from-a-fork)

### まとめ

今回は、私が10年以上にわたり勘違いしていたGitコマンドについて紹介しました。
pushの引数には気をつけましょう！なるべくリモート追跡ブランチを活用しましょう！

クリアコードは、様々な自由ソフトウェア（オープンソースソフトウェア）の開発・メンテナンスを行い、日々得られた知見を公開しています。
興味があったらぜひ他の記事もご覧ください！

また、エンタープライズ向けのサービスも幅広くやっておりますので、詳しくは[サービス一覧]({% link services/index.md %})をご覧いただき、[お問い合わせフォーム]({% link contact/index.md %})よりお気軽にご相談ください。
