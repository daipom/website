---
date: 2021-01-18
title: BrowserSelector v2.1.0
layout: default
tags:
  - BrowserSelector
---

## {{page.title}}

{{page.title}}は、{{page.date | date: "%Y年%-m月%-d日" }}に公開されたバージョンです。

### FirefoxからMicrosoft Edgeへのリダイレクトに対応しました

 - 本バージョンで、Edgeから全てブラウザ（IE・Firefox・Chrome）との相互連携が可能になりました。

 - 利用方法については、BrowserSelector利用マニュアルの「2. BrowserSelectorチュートリアル」を参照ください。

### Microsoft Edge向けのアドオンを公開しました

 - [Micorosoft Edge Add-onsストア](https://microsoftedge.microsoft.com/addons/detail/ifacgepgdnnddnckleiinelkadppopgh)からインストールいただけます。

 - 組織の端末に管理者インストールする方法については、利用マニュアルを参照ください。

![IEView WE installed on Micorsoft Edge]({% link _support/20210118/edge.png %})
