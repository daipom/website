---
layout: default
main_title: 会社情報
type: about
description: クリアコードは、 2006年7月にフリーソフトウェア開発者を中心に設立したソフトウェア開発会社です。フリーソフトウェアの開発で学んだことを継続的にビジネス分野に活用していくことで会社を継続し、それと同時に、ビジネスを継続することでフリーソフトウェアへ継続的にコミットメントしていくこと、この両立の実現が当社の目的です。Fluentd/Groonga/Apache Arrowといったソフトウェアを中心にソースコードレベルの技術・サポートを提供します。
---

<h2 class='text-center'>会社概要 Company Overview</h2>

<table>
  <tbody>
    <tr>
      <th>会社名 Name of Company</th>
      <td>株式会社クリアコード</td>
      <td>ClearCode Inc.</td>
    </tr>
    <tr>
      <th>役員 Exective Members</th>
      <td>
        <dl>
          <dt>代表取締役</dt>
          <dd>須藤　功平</dd>
        </dl>
        <dl>
          <dt>取締役</dt>
          <dd>足永　拓郎</dd>
        </dl>
        <dl>
          <dt>取締役</dt>
          <dd>南　慎一郎</dd>
        </dl>
      </td>
      <td>
        <dl>
          <dt>President</dt>
          <dd>Sutou Kouhei</dd>
        </dl>
        <dl>
          <dt>Board Member</dt>
          <dd>Ashie Takuro</dd>
        </dl>
        <dl>
          <dt>Board Member</dt>
          <dd>Minami Shinichiro</dd>
        </dl>
      </td>
    </tr>
    <tr>
      <th>従業員数 Total Number of Employees</th>
      <td>13名（役員・アルバイトを含む）</td>
      <td>13 (including Executives and Part Time)</td>
    </tr>
    <tr>
      <th>会社設立 Establishment of Company </th>
      <td>2006年7月25日</td>
      <td>July-25th, 2006</td>
    </tr>
    <tr>
      <th>所在地 Location</th>
      <td>〒359-1111
        <br>埼玉県所沢市緑町二丁目6番5号 芝﨑ビル103
      </td>
      <td> 2-6-5 Midorichou Shibazaki building 103
        <br> Tokorozawa-shi, Saitama, JAPAN 359-1111
      </td>
    </tr>
    <tr>
      <th>交通アクセス Access</th>
      <td>
        西武新宿線新所沢駅 徒歩3分
      </td>
      <td>
        3min walk from Shintokorozawa Sta. of Seibu Shinjuku line
      </td>
    </tr>
    <tr>
      <th>TEL/FAX</th>
      <td>04-2907-4726</td>
      <td>+81-4-2907-4726</td>
    </tr>
    <tr>
      <th>資本金 Capital Stock</th>
      <td>3175万円</td>
      <td>31,750,000 JPY</td>
    </tr>
    <tr>
      <th>事業内容 Description of Business</th>
      <td>ソフトウェア開発</td>
      <td>Software development</td>
    </tr>
    <tr>
      <th>取引銀行 Bank Information</th>
      <td>三井住友銀行</td>
      <td>Sumitomo Mitsui Banking Corporation</td>
    </tr>
    <tr>
      <th>決算公告 Financial Statements</th>
      <td>決算公告は<a href="{{ "/koukoku" | relative_url }}">こちら</a></td>
      <td>The mandatory publication of financial statements are only available in Japanese</td>
    </tr>
  </tbody>
</table>

[会社紹介資料（PDF）をダウンロード]({% link about/company-profile.md %})

<h2 class='text-center'>理念 Philosophy</h2>

<p>
クリアコードの理念は、<b>フリーソフトウェア</b>と<b>ビジネス</b>の両立です。

<p>
当社の目的は、単に会社を継続していくことではありません。フリーソフトウェアの開発で学んだことを継続的にビジネス分野に活用していくことで会社を継続し、それと同時に、ビジネスを継続することでフリーソフトウェアへ継続的にコミットメントしていくこと、この両立の実現が当社の目的です。この理念は、我々がフリーソフトウェアの開発で学んだことがベースとなっています。

<p>
どのようにそれを実現するかの取り組み方については、<a href='https://www.clear-code.com/blog/2015/9/17.html'>クリアコードとフリーソフトウェアとビジネス</a>という記事で詳しくご紹介しています。

<p>
We, ClearCode, believe that we can balance free-software and business.

<p>
It is not our goal just to continue the company. We are willing to grow our business while we make commitment toward free-software development continuously. And we want to balance both fields. This philosophy is based on what we learn from the free-software development. Our focus are both making the profit and driving forward the free-software development. And we believe we learn from both sides and contribute back to both sides.

<p>
Developing software is core of ClearCode's business. Our engineers are involved with various free-software projects and continue to contribute and to learn from them.

<h2>開発</h2>

<p>
開発はクリアコードの核となる活動です。そのため、「どのように開発するか」がクリアコードという会社がどのような会社となるかを大きく左右します。クリアコードでは次のように開発しています。このような開発を積み重ねてクリアコードの理念を実現しています。

<ul class='link_list grid-x grid-padding-x'>
  <li class='cell medium-6 large-4'><a href='{{ "/philosophy/development/style.html" | relative_url }}'>開発スタイル</a>
</ul>

<h2>沿革</h2>
<table>
 <tbody>
 <tr>
 <th>2006年 7月</th>
 <td>株式会社グッデイに勤務していた5名が独立し、株式会社クリアコードを設立。</td>
 </tr>
 <tr>
 <th>2006年 8月</th>
 <td>IPAオープンソースソフトウェア活用基盤整備事業「Linux環境における外字管理システムの仕様開発とプロトタイプ作成」に参加。</td>
 </tr>
 <tr>
 <th>2006年 9月</th>
 <td>有限責任中間法人Mozilla Japanのサポートパートナーに登録。
 <br><a href='{% link services/mozilla/menu.html %}'>Firefox Thunderbirdのサポートサービスを開始。</a></td>
 </tr>
 <tr>
 <th>2008年 8月</th>
 <td>IPAオープンソフトウェア利用促進事業「迷惑メール対策ポリシーを柔軟に実現するためのmilterの開発」に採択され、2009年4月に<a href='{% link services/milter-manager.html %}'>milter manager 1.0.0を開発・リリース。</a></td>
 </tr>
 <tr>
 <th>2008年 8月</th>
 <td>須藤 功平が代表取締役に就任。</td>
 </tr>
 <tr>
 <th>2008年 8月</th>
 <td>全文検索エンジンSennaの開発に参加。<a href='{% link services/groonga.md %}'>Groongaの開発につながる。</a></td>
 </tr>
 <tr>
 <th>2009年 6月</th>
 <td>NICT「先進技術型研究開発助成金」に採択され、組み込み向けブラウザの研究開発を実施。組み込み開発に着手。</td>
 </tr>
 <tr>
 <th>2010年 8月</th>
 <td>取引先3社より出資を受ける。</td>
 </tr>
 <tr>
 <th>2015年 8月</th>
 <td><a href='{% link services/fluentd-service.md %}'>Fluentdの開発に参加。</a></td>
 </tr>
 <tr>
 <th>2015年12月</th>
 <td><a href='{% link services/browser-customize.md %}'>Gecko Embeddedプロジェクトに参加。</a></td>
 </tr>
 <tr>
 <th>2016年 7月</th>
 <td><a href="{% post_url 2016-07-27-index %}">設立10周年。</a></td>
 </tr>
 <tr>
 <th>2016年12月</th>
 <td><a href='{% link services/apache-arrow.md %}'>Apache Arrowの開発に参加。</a></td>
 </tr>
 <tr>
 <th>2018年11月</th>
 <td>株式会社セナネットワークスと資本提携。</td>
 </tr>
 <tr>
 <th>2019年11月</th>
 <td><a href='{% link services/browserselector.html %}'>ブラウザ切替ツールBrowserSelectorを開発・リリース。</a></td>
 </tr>
 <tr>
 <th>2020年12月</th>
 <td>埼玉県所沢市に本社移転。</td>
 </tr>
 <tr>
 <th>2021年 7月</th>
 <td>設立15周年。</td>
 </tr>
  <tr>
 <th>2021年 7月</th>
 <td>SB C&S株式会社と企業向けブラウザソリューションでの協業開始。</td>
 </tr>
 <tr>
 <th>2022年 4月</th>
 <td><a href='{% link press-releases/20220427-flexconfirmmail-outlook.md %}'>メール誤送信対策アドイン「FlexConfirmMail」のMicrosoft Outlook版をリリース。</a></td>
 </tr>

 </tbody>
</table>

