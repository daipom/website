---
layout: default
main_title: お問い合わせ
sub_title:
description: クリアコードへのお問い合わせはこちらからお願いします。
---

<script type="text/javascript"
        src="//ma.clear-code.com/form/generate.js?id=2"></script>

個人情報の取り扱いについては[プライバシーポリシー](/privacy-policy/)をご確認ください。同意の上お問い合わせください。

If you would like English response, for any inquiry, please add the objective as a subject and send the message to info@clear-code.com with reply address. Thanks!


### 会社説明会申し込みのフォーム入力例

[採用]({% link recruitment/index.md %})希望時の会社説明会の申し込み時には、「種類」項目で「会社説明会について」を選択し、次の例のように氏名・メールアドレス・希望日時の3点を記入してご連絡ください。

```
名字: oo
名前: xx
メールアドレス: my-address@example.com
種類: 会社説明会について
内容: ooと申します。次の日時で会社説明会をお願いします。

 * 3月15日 17:00〜
 * 3月16日 17:00〜
 * 3月22日 17:00〜
```

## フォームからお問い合わせできない場合
<dl>
<dt>フォームからお問合せができない場合は下記メールアドレスにお問合せください。</dt>
<dd>info@clear-code.com</dd>
</dl>
お問合せいただく概要（〇〇サポートについて、採用について、インターンシップについてなど）を件名にご入力ください。
