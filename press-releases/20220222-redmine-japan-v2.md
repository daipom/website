---
layout: default
main_title: REDMINE JAPAN VOL.2にクリアコード　結城　洋志が登壇
sub_title: 
type: press
---

<p class='press-release-subtitle-small'>
2/25（金） 13:10- Redmineを使った技術サポートサービスの運用効率改善事例</p>

<div class="press-release-signature">
  <p class="date">2022年2月22日</p>
  <p>株式会社クリアコード</p>
</div>

株式会社クリアコード（本社：埼玉県所沢市、代表取締役：須藤 功平）は、無料で利用できるオープンソースのタスク・プロジェクト管理ツール『Redmine』のオンラインカンファレンス『REDMINE JAPAN　VOL.2』にて、社員　結城　洋志が、2022年2月25日（金曜日）13時10分より『Redmineを使った技術サポートサービスの運用効率改善事例』というタイトルで登壇することをお知らせします。

今回のトークでは、2006年の創業以来クリアコードが提供している、Firefox/Thunderbirdサポートを中心とした各種の技術サポートサービスをRedmineを活用しどのように行っているか具体的な事例を紹介します。受託開発も行っているRedmineのプラグイン機能や、メールクライアントThunderbirdのアドオン機能を連携させることで、お問い合わせから、対応策の検討、メールの記録、月次レポートの提供までを一貫して、少人数での効率的な運用を実現しています。

## 講演の概要
* **日時**：2022年2月25日（金）13時10分～13時40分
* **講演タイトル**：Redmineを使った技術サポートサービスの運用効率改善事例
* **講演者プロフィール**:   結城　洋志（piro)（Firefox/Thunderbird/ブラウザー関連技術サポート）
<p class="indent">1982年生まれ、大阪府出身。業務では主にMozilla FirefoxおよびThunderbird[^1] の法人向け技術サポートに2006年のサポート開始時より従事するほか、OSS開発者を継続的に増やす取り組み「OSS Gate」運営にも関わる。プライベートでは「Tree Style Tab」ほか多数のFirefox用拡張機能を開発・公開中。また、2011年より日経Linux誌にてLinuxコマンド操作解説漫画記事「シス管系女子」を連載中。著書に「Firefox Hacks Rebooted（共著）」、「まんがでわかるLinux シス管系女子」シリーズ、「これでできる！はじめてのOSSフィードバックガイド」など。 </p>

* **講演のポイント**
  * 顧客情報を一目で確認できるプロジェクト設定機能活用
  * 問い合わせの対応漏れ防止や管理・運用を属人的にしないためのチケット・検索機能活用
  * サポート時間管理・レポートが簡単に可能なプラグイン紹介
  
講演資料は、ククログにて紹介予定です。
[https://www.clear-code.com/blog/](https://www.clear-code.com/blog/index.html)

[^1]: Thunderbirdは、Mozilla Foundationの米国およびその他の国における商標または登録商標です。


## REDMINE JAPAN VOL.2の概要
* **開催日程**：2022年2月25日（金曜日）　10時30分～17時30分
* **会場**：オンライン
* **参加費**：無料（事前登録制）
* **公式サイト**：https://redmine-japan.org/

REDMINE JAPANは、無料で利用できるオープンソースのタスク・プロジェクト管理ツール『Redmine』のカンファレンスイベントです。第二回目の今回のテーマは「明日の仕事を変えるために必要なモノ」です。（公式ページより抜粋）

## クリアコードのテクニカルサポートについて
クリアコードでは、創業以来フリーソフトウェア開発で培ってきた技術と知見で、様々なテクニカルサポートを提供しています。今回ご紹介しているFirefox/Thunderbirdのサポートのほか、全文検索エンジンGroonga、ログ収集デーモンFluentd/FluentBitなど様々なオープンソースソフトウェアに対して、ソースコードレベルでのサポートの提供が可能です。また、Redmineのカスタマイズ支援も行っており、Redmine上での高速かつ高機能な全文検索を実現する「Full text search plugin」は多くの企業で利用されています。

導入検討時のコンサルティング、設計・導入サポート、インシデントサポートなどを提供し、多くのお客様から評価いただいております。

## クリアコードについて
クリアコードは、 2006年7月にフリーソフトウェア開発者を中心に設立したソフトウェア開発会社です。クリアコードの目的は、単に会社を継続していくことではありません。フリーソフトウェアの開発で学んだことを継続的にビジネス分野に活用していくことで会社を継続し、それと同時に、ビジネスを継続することでフリーソフトウェアへ継続的にコミットメントしていくこと、この両立の実現が当社の目的です。この理念は、我々がフリーソフトウェアの開発で学んだことがベースとなっています。


### 参考URL
【コーポレートサイト】[{{ site.url }}{% link index.html %}]({% link index.html %})

【本プレスリリース】[{{ site.url }}{% link press-releases/20220222-redmine-japan-v2.md %}]({% link press-releases/20220222-redmine-japan-v2.md %})

【関連サービス】[{{ site.url }}{% link services/mozilla/menu.html %}]({% link services/mozilla/menu.html %})


### 当リリースに関するお問合せ先
株式会社クリアコード

TEL：04-2907-4726

メール：info@clear-code.com
