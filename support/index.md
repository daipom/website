---
layout: default
title: 製品サポート情報
---

<div class='news_list'>
{%- assign sorted = site.support | sort: 'date' | reverse -%}
{%- for item in sorted %}
<a class='detail_link grid-x grid-padidng-x' href='{{ item.url | relative_url }}'>
  <p class='date cell medium-2'>{{ item.date | date: "%Y年%-m月%-d日" }}</p>
  <p class='text cell medium-10'>{{ item.title }}</p>
</a>
{%- endfor %}
</div>
