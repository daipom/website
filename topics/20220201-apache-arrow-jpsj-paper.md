---
layout: default
main_title: 論文『Apache ArrowによるRubyのデータ処理対応の可能性』が掲載されました。
sub_title:
type: topic
---

一般社団法人情報処理学会が発行する会誌「情報処理」Vol.63 No.2（Feb. 2022）「デジタルプラクティスコーナー」に、『Apache ArrowによるRubyのデータ処理対応の可能性』と題し代表取締役　須藤が共著した論文が掲載されました。

## 論文の詳細
[Apache ArrowによるRubyのデータ処理対応の可能性](https://www.ipsj.or.jp/dp/contents/publication/49/S1301-S01.html)はRubyをApache Arrowに対応させデータ処理できるようにすることの必要性・重要性とその実現方法を解説した論文です。須藤はRubyのApache Arrow対応の主開発者として本論文をレビューしました。

現状でRubyはデータ処理に使うには不向きです。しかし、著者の村田さんと須藤はApache Arrow対応などでデータ処理にもRubyを使えるようにしようと活動しています。その活動の1つが[Red Data Tools](https://red-data-tools.github.io/ja/)プロジェクトです。Apache Arrow対応もRed Data Toolsプロジェクトの活動の1つです。Rubyでデータ処理できるようにしたい人はぜひRed Data Toolsで一緒に整備していきましょう！Red Data Toolsに参加したい人は[Red Data Toolsのチャット](https://gitter.im/red-data-tools/ja)に「参加したい！」と書き込んでください。なにから取り組むか相談しましょう。

